# Vancouver OpenStreetMap Analysis Data
This page lists correct and incorrect data in OpenStreetMap in the Greater Vancouver, BC area.

Hosted here as a GitLab.com Page: [Greater Vancouver OpenStreetMap Analysis](https://jaller94.gitlab.io/vancouver-osm-analysis-data/)

[Click here](https://gitlab.com/jaller94/vancouver-osm-analyzer) for the source code to generate the report’s pages.
