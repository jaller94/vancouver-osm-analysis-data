---
layout: page
title: "TransLink area: Bus stops mapping issues"
permalink: buses/incorrect.html
categories: bus_stops
---
OpenStreetMap® is open data, licensed under the Open Data Commons Open Database License (ODbL) by the OpenStreetMap Foundation (OSMF).

## Duplicate bus stops numbers
* 50156
  * [Node 4230840511](https://www.openstreetmap.org/node/4230840511) (East 41st Avenue (WB) at Main Street)
  * [Node 8355120384](https://www.openstreetmap.org/node/8355120384) (West 41st Avenue (WB) at Main Street)
* 52107
  * [Node 5141956224](https://www.openstreetmap.org/node/5141956224)
  * [Node 7080945863](https://www.openstreetmap.org/node/7080945863) (Vancouver International Airport at Level One)
* 52114
  * [Node 4230886093](https://www.openstreetmap.org/node/4230886093) (Miller Rd (EB) at Templeton St)
  * [Node 5340005234](https://www.openstreetmap.org/node/5340005234)
* 52440
  * [Node 961847053](https://www.openstreetmap.org/node/961847053) (6 St (NB) at 3 Ave)
  * [Node 1720011174](https://www.openstreetmap.org/node/1720011174) (6 St (SB) at 3 Ave)
* 52560
  * [Node 6583218775](https://www.openstreetmap.org/node/6583218775) (Phillips Ave (NB) at Winston St)
  * [Node 6583218776](https://www.openstreetmap.org/node/6583218776) (Phillips Ave (SB) at Government Rd)
* 52863
  * [Node 5158667848](https://www.openstreetmap.org/node/5158667848) (Cameron St (WB) at Morrey Court)
  * [Node 6833718801](https://www.openstreetmap.org/node/6833718801) (Eastlake Dr (SB) at Centaurus Dr)
* 54447
  * [Node 995628784](https://www.openstreetmap.org/node/995628784) (West Georgia Street (WB) at Bute Street)
  * [Node 1012595218](https://www.openstreetmap.org/node/1012595218) (West Georgia Street (WB) at Bute Street)
* 55977
  * [Node 6575502503](https://www.openstreetmap.org/node/6575502503) (60 Ave (WB) at 172 St)
  * [Node 6575502504](https://www.openstreetmap.org/node/6575502504) (60 Ave (EB) at 172 St)
* 56590
  * [Node 6567347624](https://www.openstreetmap.org/node/6567347624) (Moncton St (WB) at No 2 Rd)
  * [Node 6567347625](https://www.openstreetmap.org/node/6567347625) (Moncton St (EB) at No 2 Rd)
* 57866
  * [Node 9009088950](https://www.openstreetmap.org/node/9009088950)
  * [Node 9009088951](https://www.openstreetmap.org/node/9009088951)
* 59992
  * [Node 2915232901](https://www.openstreetmap.org/node/2915232901)
  * [Node 6797438225](https://www.openstreetmap.org/node/6797438225) (Pat Quinn Way (NB) at Expo Blvd)
* 60295
  * [Node 725635470](https://www.openstreetmap.org/node/725635470) (Arbutus St (SB) at 4200 Block)
  * [Node 8895848528](https://www.openstreetmap.org/node/8895848528) (152 St (NB) At 7900 Blocl)
* 60526
  * [Node 4796015024](https://www.openstreetmap.org/node/4796015024) (Beach Ave (WB) at Bidwell St)
  * [Node 9691242436](https://www.openstreetmap.org/node/9691242436) (Beach Ave (EB) at Thurlow St)

## Incorrectly mapped bus stop numbers (0)
These bus stops have an identifiable 5-digit bus stop number. However, the number is mapped in `name` or `name:en` but not `ref`.
