---
layout: page
title: "TransLink area: Comparison with TransLink Data"
permalink: buses/compare_with_translink.html
categories: bus_stops
---
This page got generated using data from OpenStreetMap and the [TransLink Open API](https://developer.translink.ca/ServicesGtfs/GtfsData).

OpenStreetMap® is open data, licensed under the Open Data Commons Open Database License (ODbL) by the OpenStreetMap Foundation (OSMF).

Route and arrival data used in this product or service is provided by permission of TransLink. TransLink assumes no responsibility for the accuracy or currency of the Data used in this product or service.
## Missing stops (4283)
* `50051` – Westbound Davie St @ Jervis St
* `50146` – Northbound Main St @ E 59 Ave
* `50150` – Northbound Main St @ E 51 Ave
* `50154` – Northbound Main St @ E 43 Ave
* `50162` – Northbound Main St @ E 39 Ave
* `50190` – Southbound Richards St @ Dunsmuir St
* `50213` – Northbound Granville St @ W 14 Ave
* `50242` – Southbound Main St @ E 16 Ave
* `50243` – Westbound 24 Ave @ 188 St
* `50248` – Southbound Main St @ E 30 Ave
* `50255` – Southbound Main St @ E 43 Ave
* `50256` – Southbound Main St @ E 46 Ave
* `50258` – Southbound Main St @ E 51 Ave
* `50259` – Southbound Main St @ E 54 Ave
* `50263` – Southbound Main St @ E 62 Ave
* `50264` – Southbound Main St @ E 63 Ave
* `50265` – Eastbound E 65 Ave @ Main St
* `50270` – Eastbound University Blvd @ 5300 Block
* `50274` – Eastbound W 10 Ave @ Blanca St
* `50275` – Eastbound W 10 Ave @ Tolmie St
* `50276` – Eastbound W 10 Ave @ Sasamat St
* `50277` – Eastbound W 10 Ave @ Trimble St
* `50278` – Eastbound W 10 Ave @ Discovery St
* `50279` – Eastbound W 10 Ave @ Courtenay St
* `50338` – Southbound Oak St @ Nanton Ave
* `50435` – Eastbound Powell St @ Victoria Dr
* `50436` – Eastbound Dundas St @ Lakewood Dr
* `50438` – Northbound N Nanaimo St @ Dundas St
* `50443` – Eastbound McGill St @ N Slocan St
* `50453` – Northbound Oak St @ W 33 Ave
* `50492` – Southbound N Nanaimo St @ Cambridge St
* `50496` – Westbound Powell St @ Victoria Dr
* `50543` – Southbound Granville St @ W 13 Ave
* `50552` – Southbound Granville St @ W 33 Ave
* `50556` – Southbound Granville St @ W 40 Ave
* `50602` – Westbound University Blvd @ 5100 Block
* `50632` – Southbound Denman St @ Pendrell St
* `50649` – Eastbound E 41 Ave @ Knight St
* `50657` – Northbound Victoria Dr @ E 32 Ave
* `50669` – Southbound Nanaimo St @ Dundas St
* `50673` – Southbound Nanaimo St @ Adanac St
* `50679` – Southbound Nanaimo St @ E 3rd Ave
* `50698` – Southbound Victoria Dr @ E 31 Ave
* `50714` – Westbound E 41st Ave @ Main St
* `50716` – Northbound Nanaimo St @ E 20 Ave
* `50723` – Northbound Nanaimo St @ E 4th Ave
* `50728` – Northbound Nanaimo St @ Adanac St
* `50739` – Eastbound SW Marine Dr @ Oak St
* `50740` – Northbound Oak Cloverleaf @ SW Marine Dr
* `50743` – Northbound Oak St @ W 67 Ave
* `50745` – Northbound Oak St @ Park Dr
* `50746` – Northbound Oak St @ W 59 Ave
* `50747` – Northbound Oak St @ W 57 Ave
* `50748` – Northbound Oak St @ W 54 Ave
* `50752` – Northbound Oak St @ W 46 Ave
* `50765` – Northbound Granville St @ W 53 Ave
* `50775` – Southbound Fraser St @ E Broadway
* `50776` – Southbound Fraser St @ E 11 Ave
* `50777` – Southbound Fraser St @ E 13 Ave
* `50778` – Southbound Fraser St @ E 16 Ave
* `50782` – Southbound Fraser St @ E King Edward Ave
* `50783` – Southbound Fraser St @ E 27 Ave
* `50784` – Southbound Fraser St @ E 29 Ave
* `50787` – Southbound Fraser St @ E 35 Ave
* `50788` – Southbound Fraser St @ E 37 Ave
* `50796` – Southbound Fraser St @ E 53 Ave
* `50797` – Southbound Fraser St @ E 55 Ave
* `50798` – Southbound Fraser St @ E 57 Ave
* `50799` – Southbound Fraser St @ E 59 Ave
* `50800` – Southbound Fraser St @ E 61 Ave
* `50807` – Southbound Oak St @ W 46 Ave
* `50810` – Southbound Oak St @ W 54 Ave
* `50811` – Southbound Oak St @ W 57 Ave
* `50812` – Southbound Oak St @ W 59 Ave
* `50813` – Southbound Oak St @ Park Dr
* `50815` – Southbound Oak St @ W 67 Ave
* `50819` – Westbound SW Marine Dr @ Oak St
* `50821` – Northbound Fraser St @ E 62 Ave
* `50827` – Northbound Fraser St @ E 49 Ave
* `50838` – Northbound Fraser St @ E 28 Ave
* `50843` – Northbound Fraser St @ E 17 Ave
* `50844` – Northbound Fraser St @ E 14 Ave
* `50845` – Northbound Fraser St @ E 12 Ave
* `50846` – Northbound Fraser St @ E 10 Ave
* `50871` – Eastbound W Broadway @ Fir St
* `50897` – Eastbound E Broadway @ Nootka St
* `50898` – Eastbound E Broadway @ Lillooet St
* `50903` – Westbound E Broadway @ Cassiar St
* `50905` – Westbound E Broadway @ Windermere St
* `50912` – Westbound E Broadway @ Semlin Dr
* `50918` – Westbound E Broadway @ Windsor St
* `50927` – Westbound W 10 Ave @ Fir St
* `50932` – Eastbound W Hastings St @ Abbott St
* `50960` – Westbound E Hastings St @ Kaslo St
* `50961` – Westbound E Hastings St @ Penticton St
* `50967` – Southbound Victoria Diversion @ Commercial St
* `50986` – Southbound Cambie St @ W 54 Ave
* `50987` – Southbound Cambie St @ W 57 Ave
* `50988` – Southbound Cambie St @ W 59 Ave
* `50994` – Northbound Cambie St @ W 61 Ave
* `50995` – Northbound Cambie St @ W 59 Ave
* `50996` – Northbound Cambie St @ W 57 Ave
* `50997` – Northbound Cambie St @ W 54 Ave
* `51011` – Northbound Angus Dr @ W 64 Ave
* `51026` – Northbound Arbutus St @ Quilchena Cres
* `51040` – Southbound Renfrew St @ William St
* `51041` – Southbound Renfrew St @ Kitchener St
* `51069` – Westbound E 41 Ave @ Gladstone St
* `51070` – Eastbound E 41 Ave @ Victoria Dr
* `51108` – Southbound West Blvd @ W 43 Ave
* `51113` – Southbound West Blvd @ W 53 Ave
* `51115` – Southbound West Blvd @ W 60 Ave
* `51117` – Southbound Angus Dr @ W 63 Ave
* `51119` – Northbound Adera St @ W 64 Ave
* `51138` – Eastbound Kingsway @ Fraser St
* `51139` – Eastbound Kingsway @ St. Catherines St
* `51141` – Eastbound Kingsway @ Clark Dr
* `51145` – Eastbound Kingsway @ Miller St
* `51149` – Eastbound Kingsway @ Rupert St
* `51157` – Eastbound Kingsway @ Smith Ave
* `51210` – Northbound Victoria Dr @ Waverley Ave
* `51211` – Northbound Victoria Dr @ E 45 Ave
* `51212` – Northbound Victoria Dr @ E 43 Ave
* `51217` – Northbound Commercial Dr @ Findlay St
* `51231` – Northbound Commercial Diversion @ Venables St
* `51245` – Southbound Victoria Dr @ E 43 Ave
* `51246` – Southbound Victoria Dr @ Waverley Ave
* `51273` – Southbound Gore Ave @ Keefer St
* `51274` – Southbound Gore Ave @ Union St
* `51276` – Eastbound Terminal Ave @ Thornton St
* `51277` – Eastbound Terminal Ave @ Begg St
* `51281` – Eastbound Prior St @ Campbell Ave
* `51282` – Eastbound Venables St @ Glen Dr
* `51283` – Eastbound Venables St @ Vernon Dr
* `51296` – Southbound Knight St @ E 23 Ave
* `51298` – Southbound Knight St @ E 27 Ave
* `51300` – Southbound Knight St @ E 31 Ave
* `51302` – Southbound Knight St @ E 35 Ave
* `51303` – Southbound Knight St @ E 37 Ave
* `51304` – Southbound Knight St @ E 39 Ave
* `51306` – Southbound Knight St @ E 43 Ave
* `51307` – Southbound Knight St @ E 45 Ave
* `51308` – Southbound Knight St @ E 47 Ave
* `51321` – Southbound Viking Way @ Crestwood Place
* `51323` – Northbound Jacombs Rd @ Rowan Place
* `51333` – Northbound Knight St @ E 47 Ave
* `51334` – Northbound Knight St @ E 45 Ave
* `51335` – Northbound Knight St @ E 43 Ave
* `51337` – Northbound Knight St @ E 39 Ave
* `51338` – Northbound Knight St @ E 37 Ave
* `51340` – Northbound Knight St @ E 32 Ave
* `51342` – Northbound Knight St @ E 28 Ave
* `51345` – Northbound Knight St @ E 20 Ave
* `51346` – Northbound Knight St @ E 19 Ave
* `51351` – Northbound Clark Dr @ E 6th Ave
* `51353` – Westbound Terminal Ave @ Begg St
* `51354` – Westbound Terminal Ave @ Thornton St
* `51359` – Westbound Venables St @ Clark Dr
* `51360` – Westbound Venables St @ Vernon Dr
* `51361` – Westbound Venables St @ Raymur Ave
* `51362` – Westbound Prior St @ Hawks Ave
* `51363` – Westbound Prior St @ Heatley Ave
* `51364` – Westbound Prior St @ Jackson Ave
* `51366` – Northbound Gore Ave @ E Georgia St
* `51367` – Westbound E Pender St @ Gore Ave
* `51378` – Eastbound Hastings St @ MacDonald Ave
* `51380` – Eastbound Hastings St @ Madison Ave
* `51393` – Eastbound Canada Way @ Beta Ave
* `51394` – Eastbound Canada Way @ Wayburne Dr
* `51396` – Eastbound Canada Way @ 4800 Block
* `51397` – Eastbound Canada Way @ Kincaid St
* `51400` – Eastbound Canada Way @ Century Park Way
* `51404` – Southbound Canada Way @ Burris St
* `51406` – Southbound Canada Way @ Stanley Cres
* `51407` – Southbound Canada Way @ Morley Dr
* `51408` – Southbound Canada Way @ Imperial St
* `51409` – Southbound Canada Way @ Ulster St
* `51431` – Northbound 8th St @ 7th Ave
* `51441` – Northbound Canada Way @ Goodlad St
* `51442` – Northbound Canada Way @ Imperial St
* `51443` – Westbound Canada Way @ Morley St
* `51444` – Northbound Canada Way @ Stanley St
* `51445` – Northbound Canada Way @ Burris St
* `51446` – Westbound Canada Way @ Chiselhampton St
* `51447` – Westbound Canada Way @ Sperling Ave
* `51450` – Westbound Canada Way @ Spruce St
* `51451` – Westbound Canada Way @ Douglas Rd
* `51452` – Westbound Canada Way @ 4800 Block
* `51453` – Northbound Canada Way @ Hardwick St
* `51454` – Westbound Canada Way @ Royal Oak Ave
* `51460` – Northbound Willingdon Ave @ Midlawn Dr
* `51462` – Northbound Willingdon Ave @ William St
* `51464` – Northbound Willingdon Ave @ Georgia St
* `51466` – Westbound Hastings St @ Madison Ave
* `51468` – Westbound Hastings St @ Ingleton Ave
* `51490` – Eastbound W 16 Ave @ Camosun St
* `51501` – Eastbound W King Edward Ave @ Yew St
* `51503` – Eastbound W King Edward Ave @ Maple Cres
* `51504` – Eastbound W King Edward Ave @ Cypress St
* `51505` – Eastbound W King Edward Ave @ Marguerite St
* `51508` – Eastbound W King Edward Ave @ Hudson St
* `51511` – Eastbound W King Edward Ave @ Willow St
* `51519` – Eastbound E King Edward Ave @ Prince Edward St
* `51522` – Eastbound E King Edward Ave @ St. Catherines St
* `51536` – Eastbound E 22 Ave @ Kootenay St
* `51554` – Westbound E 22 Ave @ Anzio Dr
* `51575` – Westbound W King Edward Ave @ Heather St
* `51578` – Westbound W King Edward Ave @ Selkirk St
* `51581` – Westbound W King Edward Ave @ Marguerite St
* `51616` – Eastbound Rosemont Dr @ 3200 Block
* `51622` – Northbound Matheson Cres @ Limewood Place
* `51623` – Northbound Matheson Cres @ Raintree Court
* `51624` – Northbound Matheson Cres @ Rumble Ave
* `51631` – Northbound Tyne St @ E 49 Ave
* `51632` – Northbound Tyne St @ E 47 Ave
* `51633` – Northbound Tyne St @ E 45 Ave
* `51634` – Northbound Joyce St @ E 45 Ave
* `51635` – Northbound Joyce St @ Marmion Ave
* `51644` – Eastbound E 45 Ave @ Doman St
* `51645` – Southbound Tyne St @ E 46 Ave
* `51646` – Southbound Tyne St @ E 47 Ave
* `51654` – Southbound Matheson Cres @ Chickadee Place
* `51655` – Southbound Matheson Cres @ Raintree Court
* `51661` – Westbound Rosemont Dr @ 3200 Block
* `51676` – Northbound Rupert St @ E 27 Ave
* `51677` – Northbound Rupert St @ E 25 Ave
* `51688` – Northbound Rupert St @ Kitchener St
* `51689` – Northbound Rupert St @ William St
* `51690` – Northbound Rupert St @ Parker St
* `51700` – Southbound Rupert St @ Parker St
* `51701` – Southbound Rupert St @ Charles St
* `51702` – Southbound Rupert St @ Grant St
* `51718` – Southbound Rupert St @ Price St
* `51744` – Northbound Boundary Rd @ William St
* `51746` – Northbound Cassiar Connector @ 100 Block
* `51750` – Northbound Mountain Hwy @ Hunter St
* `51753` – Eastbound Purcell Way @ Lillooet Rd
* `51754` – Westbound Purcell Way @ Skeena Rd
* `51763` – Southbound Boundary Rd @ Napier St
* `51768` – Southbound Boundary Rd @ Henning Dr
* `51778` – Southbound Ingleton Ave @ Sunset St
* `51793` – Southbound Matheson Cres @ Champlain Cres
* `51835` – Eastbound SE Marine Dr @ Elliott St
* `51839` – Northbound Matheson Cres @ SE Marine Dr
* `51847` – Eastbound Hastings St @ Springer Ave
* `51850` – Eastbound Hastings St @ Fell Ave
* `51852` – Eastbound Hastings St @ Sperling Ave
* `51854` – Northbound Inlet Dr @ Hastings St
* `51887` – Eastbound SW Marine Dr @ Kullahun Dr
* `51896` – Southbound Crown St @ SW Marine Dr
* `51898` – Westbound SW Marine Dr @ Kullahun Dr
* `51919` – Westbound NW Marine Dr @ Cecil Green Park Rd
* `51925` – Eastbound NW Marine Dr @ West Mall
* `51944` – Southbound Dunbar St @ W 43 Ave
* `51945` – Southbound Dunbar St @ SW Marine Dr
* `51946` – Eastbound SW Marine Dr @ Blenheim St
* `51947` – Eastbound SW Marine Dr @ Balaclava St
* `51948` – Westbound SW Marine Dr @ Heather St
* `51950` – Eastbound W 49 Ave @ McCleery St
* `51957` – Eastbound W 49 Ave @ Angus Dr
* `51958` – Eastbound W 49 Ave @ Marguerite St
* `51959` – Eastbound W 49 Ave @ Churchill St
* `51960` – Eastbound W 49 Ave @ Granville St
* `51961` – Eastbound W 49 Ave @ Hudson St
* `51968` – Eastbound W 49 Ave @ Alberta St
* `51971` – Eastbound E 49 Ave @ Main St
* `51972` – Eastbound E 49 Ave @ Prince Edward St
* `51973` – Eastbound E 49 Ave @ St. George St
* `51976` – Eastbound E 49 Ave @ Prince Albert St
* `51977` – Eastbound E 49 Ave @ Windsor St
* `51978` – Eastbound E 49 Ave @ Sherbrooke St
* `51990` – Northbound Tyne St @ E 53 Ave
* `51991` – Northbound Tyne St @ E 50 Ave
* `52005` – Westbound Imperial St @ Mandy Ave
* `52006` – Southbound Tyne St @ E 49 Ave
* `52007` – Southbound Tyne St @ E 52 Ave
* `52012` – Westbound E 49 Ave @ Killarney St
* `52023` – Westbound E 49 Ave @ Inverness St
* `52024` – Westbound E 49 Ave @ Elgin St
* `52025` – Westbound E 49 Ave @ St. Catherines St
* `52028` – Westbound E 49 Ave @ St. George St
* `52029` – Westbound E 49 Ave @ Prince Edward St
* `52030` – Westbound E 49 Ave @ Main St
* `52036` – Westbound W 49 Ave @ Laurel St
* `52042` – Westbound W 49 Ave @ Adera St
* `52043` – Westbound W 49 Ave @ Wiltshire St
* `52047` – Westbound W 49 Ave @ Larch St
* `52048` – Westbound W 49 Ave @ Macdonald St
* `52050` – Westbound SW Marine Dr @ Carnarvon St
* `52051` – Westbound SW Marine Dr @ Blenheim St
* `52053` – Northbound Dunbar St @ SW Marine Dr
* `52054` – Northbound Dunbar St @ W 43 Ave
* `52060` – Westbound Lameys Mill Rd @ 1300 Block
* `52081` – Northbound Garden City Rd @ Lansdowne Rd
* `52089` – Westbound Lougheed Hwy @ Austin Rd
* `52092` – Westbound Lougheed Hwy @ Boundary Loop
* `52120` – Southbound Hudson St @ W 72 Ave
* `52133` – Eastbound SE Marine Dr @ Main St
* `52134` – Eastbound SE Marine Dr @ Prince Edward St
* `52135` – Eastbound SE Marine Dr @ St. George St
* `52141` – Eastbound SE Marine Dr @ Argyle St
* `52149` – Eastbound Marine Dr @ Strathearn Ave
* `52159` – Southbound Willard St @ 14 Ave
* `52160` – Southbound Willard St @ 12 Ave
* `52161` – Southbound Willard St @ 10 Ave
* `52162` – Southbound Willard St @ 9th Ave
* `52163` – Southbound Willard St @ Thorne Ave
* `52169` – Southbound 12 St @ 6th Ave
* `52188` – Northbound Willard St @ Thorne Ave
* `52189` – Northbound Willard St @ 9th Ave
* `52190` – Northbound Willard St @ 10 Ave
* `52191` – Northbound Willard St @ 12 Ave
* `52192` – Northbound Willard St @ 14 Ave
* `52195` – Westbound Marine Dr @ Macpherson Ave
* `52197` – Westbound Marine Dr @ 4900 Block
* `52204` – Westbound SE Marine Dr @ Argyle St
* `52210` – Westbound SE Marine Dr @ Buscombe St
* `52219` – Westbound SW Marine Dr @ Laurel St
* `52231` – Eastbound 8th Ave @ 23 St
* `52233` – Eastbound 8th Ave @ 21 St
* `52252` – Northbound 1st St @ 14 Ave
* `52254` – Eastbound 16 Ave @ Newcombe St
* `52255` – Eastbound 16 Ave @ Wright St
* `52258` – Eastbound Armstrong Ave @ Cumberland St
* `52259` – Eastbound Armstrong Ave @ Coquitlam St
* `52260` – Eastbound Armstrong Ave @ Langley St
* `52262` – Northbound Cariboo Rd @ 7600 Block
* `52265` – Northbound Cariboo Rd @ 7100 Block
* `52266` – Northbound Cariboo Rd @ Stormont Ave
* `52267` – Northbound Cariboo Rd @ Avalon Ave
* `52281` – Southbound Cariboo Rd @ Stormont Ave
* `52283` – Southbound Cariboo Rd @ 7100 Block
* `52284` – Southbound Cariboo Rd @ 16 Ave
* `52285` – Southbound Cariboo Rd @ 7600 Block
* `52287` – Westbound Armstrong Ave @ Langley St
* `52288` – Westbound Armstrong Ave @ Coquitlam St
* `52289` – Northbound Cumberland St @ Armstrong Ave
* `52291` – Westbound 16 Ave @ Wright St
* `52292` – Westbound 16 Ave @ Newcombe St
* `52293` – Southbound 1st St @ 16 Ave
* `52294` – Southbound 1st St @ 14 Ave
* `52323` – Southbound Cumberland St @ E 8th Ave
* `52324` – Southbound Cumberland St @ E 7th Ave
* `52325` – Eastbound Cumberland St @ Beth St
* `52327` – Southbound Richmond St @ Seymour Court
* `52328` – Southbound Richmond St @ Miner St
* `52336` – Eastbound Quayside Dr @ Reliance Court
* `52344` – Eastbound Derwent Way @ Eaton Way
* `52345` – Eastbound Derwent Way @ Ebury Place
* `52346` – Eastbound Derwent Way @ Fraserview Place
* `52347` – Eastbound Derwent Way @ Derwent Place
* `52348` – Eastbound Derwent Way @ Chester Rd
* `52349` – Eastbound Derwent Way @ Annance Court
* `52350` – Eastbound Derwent Way @ 900 Block
* `52351` – Eastbound Derwent Way @ 800 Block
* `52352` – Eastbound Derwent Way @ Caldew St
* `52353` – Eastbound Derwent Way @ 600 Block
* `52364` – Westbound Ewen Ave @ Gifford St
* `52365` – Westbound Ewen Ave @ Hume St
* `52378` – Eastbound Ewen Ave @ Johnston St
* `52381` – Westbound Belgrave Way @ Caldew St
* `52382` – Southbound Audley Blvd @ Belgrave Way
* `52383` – Westbound Cliveden Ave @ 900 Block
* `52384` – Westbound Cliveden Ave @ Carlisle Rd
* `52385` – Westbound Cliveden Ave @ Chester Rd
* `52386` – Westbound Cliveden Ave @ Cliveden Place
* `52387` – Westbound Cliveden Ave @ Carleton Court
* `52401` – Eastbound Kingsway @ Gilley Ave
* `52403` – Eastbound Kingsway @ Sperling Ave
* `52407` – Westbound Edmonds St @ Salisbury Ave
* `52409` – Eastbound Edmonds St @ 18 St
* `52423` – Southbound 6th St @ 3rd Ave
* `52427` – Northbound 4th St @ Agnes St
* `52452` – Westbound Edmonds St @ Linden Ave
* `52454` – Westbound Kingsway @ Walker Ave
* `52456` – Westbound Kingsway @ Griffiths Ave
* `52457` – Westbound Kingsway @ Sperling Ave
* `52458` – Westbound Kingsway @ Arcola St
* `52472` – Westbound Royal Ave @ 1st St
* `52474` – Northbound 2nd St @ Clinton Place
* `52475` – Northbound 2nd St @ Queens Ave
* `52476` – Northbound 2nd St @ 3rd Ave
* `52477` – Northbound 2nd St @ 4th Ave
* `52478` – Northbound 2nd St @ 5th Ave
* `52481` – Eastbound 8th Ave @ 2nd St
* `52482` – Westbound Ovens Ave @ Colborne St
* `52489` – Southbound 2nd St @ 6th Ave
* `52490` – Southbound 2nd St @ 5th Ave
* `52506` – Northbound Nelson Ave @ Sardis Cres
* `52511` – Northbound Willingdon Ave @ Burke St
* `52512` – Northbound Willingdon Ave @ Grassmere St
* `52524` – Eastbound Deer Lake Parkway @ Wayburne Dr
* `52533` – Eastbound Broadway @ Kensington Ave
* `52535` – Eastbound Broadway @ E Ellerslie Ave
* `52537` – Southbound Bainbridge Ave @ Broadway
* `52538` – Southbound Bainbridge Ave @ Lougheed Hwy
* `52539` – Westbound Lougheed Hwy @ Bainbridge Ave
* `52546` – Eastbound Government Rd @ Lozells Ave
* `52547` – Eastbound Government Rd @ Jensen Place
* `52548` – Eastbound Government Rd @ Piper Ave
* `52549` – Eastbound Government Rd @ 8000 Block
* `52550` – Eastbound Government Rd @ Lakedale Ave
* `52551` – Eastbound Government Rd @ 8300 Block
* `52552` – Eastbound Government Rd @ Brighton Ave
* `52553` – Westbound Government Rd @ Brighton Ave
* `52554` – Westbound Government Rd @ 8300 Block
* `52555` – Westbound Government Rd @ Lakedale Ave
* `52556` – Westbound Government Rd @ 8000 Block
* `52557` – Westbound Government Rd @ Piper Ave
* `52558` – Westbound Government Rd @ Lozells Ave
* `52559` – Westbound Government Rd @ 7500 Block
* `52561` – Westbound Winston St @ 7200 Block
* `52565` – Northbound Bainbridge Ave @ Lougheed Hwy
* `52566` – Westbound Broadway @ Bainbridge Ave
* `52567` – Westbound Broadway @ Cliff Ave
* `52575` – Westbound Gilpin St @ Mahon Ave
* `52576` – Westbound Gilpin St @ Gatenby Ave
* `52578` – Westbound Deer Lake Parkway @ Wayburne Dr
* `52595` – Southbound Nelson Ave @ Buxton St
* `52607` – Eastbound 14 Ave @ Griffiths Dr
* `52610` – Eastbound 14 Ave @ Kingsway
* `52628` – Westbound 14 Ave @ Kingsway
* `52631` – Westbound 14 Ave @ Griffiths Dr
* `52651` – Southbound Royal Oak Ave @ Sidley St
* `52658` – Eastbound Clinton St @ Curragh Ave
* `52660` – Southbound Gilley Ave @ Ewart St
* `52663` – Northbound Gilley Ave @ Clinton St
* `52664` – Eastbound Rumble St @ Gilley Ave
* `52671` – Westbound Clinton St @ Gilley Ave
* `52672` – Westbound Clinton St @ Curragh Ave
* `52674` – Westbound Clinton St @ Plum Ave
* `52706` – Eastbound North Fraser Way @ Fraserwood Court
* `52707` – Eastbound North Fraser Way @ 3700 Block
* `52708` – Eastbound North Fraser Way @ Fraserton Court
* `52721` – Northbound Gilmore Way @ Canada Way
* `52722` – Northbound Gilmore Diversion @ Canada Way
* `52728` – Westbound Dawson St @ Willingdon Ave
* `52733` – Southbound Gilmore Way @ Canada Way
* `52746` – Southbound Gilmore Ave @ Kitchener St
* `52755` – Southbound Douglas Rd @ Norland Ave
* `52756` – Southbound Douglas Rd @ Dominion St
* `52757` – Southbound Douglas Rd @ Laurel St
* `52758` – Southbound Douglas Rd @ Hardwick St
* `52759` – Southbound Douglas Rd @ Sprott St
* `52761` – Westbound Burris St @ Buckingham Ave
* `52762` – Westbound Burris St @ Braemar Ave
* `52764` – Southbound Sperling Ave @ Oakland St
* `52765` – Eastbound Stanley St @ Sperling Ave
* `52766` – Southbound Walker Ave @ Stanley St
* `52767` – Southbound Walker Ave @ Morley St
* `52776` – Northbound Walker Ave @ Morley St
* `52777` – Westbound Stanley St @ Walker Ave
* `52778` – Northbound Sperling Ave @ Stanley St
* `52780` – Eastbound Burris St @ Braemar Ave
* `52781` – Eastbound Burris St @ Buckingham Ave
* `52783` – Northbound Douglas Rd @ Sprott St
* `52784` – Northbound Douglas Rd @ Hardwick St
* `52785` – Northbound Douglas Rd @ Laurel St
* `52786` – Northbound Douglas Rd @ Norfolk St
* `52787` – Northbound Douglas Rd @ Hwy One Overpass
* `52788` – Northbound Douglas Rd @ Norland Ave
* `52799` – Northbound Gilmore Ave @ Kitchener St
* `52800` – Northbound Gilmore Ave @ William St
* `52803` – Northbound Gilmore Ave @ Frances St
* `52809` – Eastbound Midlawn Dr @ Willingdon Ave
* `52810` – Southbound Fairlawn Dr @ Midlawn Dr
* `52811` – Eastbound Brentlawn Dr @ Beta Ave
* `52813` – Northbound Delta Ave @ Southlawn Dr
* `52814` – Northbound Delta Ave @ Northlawn Dr
* `52815` – Northbound Delta Ave @ Eastlawn Dr
* `52817` – Eastbound Parker St @ Springer Ave
* `52820` – Eastbound Curtis St @ Fell Ave
* `52821` – Eastbound Curtis St @ Kensington Ave
* `52825` – Southbound Duthie Ave @ Ridley Dr
* `52826` – Southbound Duthie Ave @ Kitchener St
* `52828` – Southbound Burnwood Dr @ Greystone Dr
* `52832` – Southbound Duthie Ave @ Camarillo Place
* `52833` – Southbound Duthie Ave @ Gibson St
* `52835` – Westbound Lougheed Hwy @ Sperling Ave
* `52837` – Eastbound Broadway @ Phillips Place
* `52838` – Eastbound Broadway @ Lawrence Dr
* `52839` – Eastbound Broadway @ Lake City Way
* `52842` – Northbound Underhill Ave @ Broadway
* `52843` – Eastbound Forest Grove Dr @ Underhill Ave
* `52844` – Eastbound Forest Grove Dr @ Meridian Place
* `52845` – Eastbound Forest Grove Dr @ Hillside Place
* `52846` – Eastbound Forest Grove Dr @ Maple Grove Cres West
* `52853` – Southbound Eastlake Dr @ Centaurus Dr
* `52857` – Eastbound Cameron St @ Beaverbrook Dr
* `52858` – Eastbound Cameron St @ Bartlett Court
* `52870` – Eastbound Forest Grove Dr @ Goldhurst Terrace East
* `52872` – Westbound Forest Grove Dr @ Ash Grove Cres East
* `52873` – Westbound Forest Grove Dr @ Maple Grove Cres East
* `52875` – Westbound Forest Grove Dr @ Ash Grove Cres
* `52876` – Westbound Forest Grove Dr @ Hillside Place
* `52877` – Westbound Forest Grove Dr @ Meridian Place
* `52879` – Southbound Underhill Ave @ Broadway
* `52880` – Westbound Broadway @ 7900 Block
* `52881` – Westbound Broadway @ Lawrence Dr
* `52882` – Westbound Broadway @ Phillips Ave
* `52884` – Northbound Duthie Ave @ Montecito Dr
* `52887` – Northbound Burnwood Dr @ Halifax St
* `52890` – Northbound Duthie Ave @ Greystone Dr
* `52895` – Westbound Curtis St @ Kensington Ave
* `52896` – Westbound Curtis St @ Fell Ave
* `52898` – Westbound Parker St @ Springer Ave
* `52900` – Southbound Delta Ave @ Northlawn Dr
* `52901` – Southbound Delta Ave @ Southlawn Dr
* `52903` – Westbound Brentlawn Dr @ Beta Ave
* `52904` – Westbound Brentlawn Dr @ Fairlawn Dr
* `52905` – Eastbound W Hastings St @ Carrall St
* `52909` – Southbound Holdom Ave @ Georgia St
* `52911` – Southbound Holdom Ave @ Charles St
* `52913` – Eastbound Halifax St @ Warwick Ave
* `52914` – Eastbound Halifax St @ Fell Ave
* `52915` – Eastbound Halifax St @ Woolwich Ave
* `52916` – Eastbound Halifax St @ Kensington Ave
* `52918` – Eastbound Halifax St @ Yeovil Ave
* `52919` – Eastbound Halifax St @ Cliff Ave
* `52920` – Eastbound Halifax St @ Sherlock Ave
* `52925` – Westbound Halifax St @ Sherlock Ave
* `52926` – Westbound Halifax St @ Cliff Ave
* `52927` – Westbound Halifax St @ Yeovil Ave
* `52929` – Westbound Halifax St @ Tallin Ave
* `52930` – Westbound Halifax St @ Kensington Ave
* `52931` – Westbound Halifax St @ Woolwich Ave
* `52932` – Southbound Fell Ave @ Halifax St
* `52934` – Westbound Broadway @ Fell Ave
* `52936` – Northbound Holdom Ave @ Sumas St
* `52938` – Northbound Holdom Ave @ Winch St
* `52939` – Northbound Holdom Ave @ Kitchener St
* `52941` – Northbound Holdom Ave @ Union St
* `52942` – Northbound Holdom Ave @ Frances St
* `52945` – Northbound Gamma Ave @ Pandora St
* `52946` – Northbound N Gamma Ave @ Empire Dr
* `52947` – Eastbound Empire Dr @ Delta Ave
* `52948` – Northbound Hythe Ave @ Empire Dr
* `52949` – Eastbound Dundas St @ Hythe Ave
* `52950` – Northbound N Glynde Ave @ Dundas St
* `52951` – Eastbound Cambridge St @ N Glynde Ave
* `52952` – Eastbound Cambridge St @ N Springer Ave
* `52953` – Eastbound Cambridge St @ N Ranelagh Ave
* `52954` – Eastbound Cambridge St @ N Grosvenor Ave
* `52955` – Westbound Dundas St @ N Howard Ave
* `52956` – Westbound Dundas St @ N Springer Ave
* `52957` – Westbound Dundas St @ N Hythe Ave
* `52958` – Westbound Empire Dr @ Hythe Ave
* `52959` – Westbound Empire Dr @ Delta Ave
* `52960` – Southbound N Gamma Ave @ Cambridge St
* `52961` – Southbound Gamma Ave @ Pandora St
* `52963` – Southbound Willingdon Ave @ Kitchener St
* `52964` – Northbound Boundary Rd @ Albert St
* `52972` – Eastbound Eton St @ N Madison Ave
* `52995` – Northbound Duthie Ave @ Union St
* `53002` – Eastbound Como Lake Ave @ Townley St
* `53007` – Eastbound Como Lake Ave @ Wasco St
* `53014` – Eastbound Como Lake Ave @ 2500 Block
* `53017` – Northbound Mariner Way @ Daybreak Ave
* `53025` – Southbound Shaughnessy St @ McAllister Ave
* `53026` – Eastbound Wilson Ave @ Shaughnessy St
* `53027` – Eastbound Wilson Ave @ Mary Hill Rd
* `53030` – Westbound Wilson Ave @ Shaughnessy St
* `53033` – Westbound Lougheed Hwy @ Hastings St
* `53037` – Westbound Dewdney Trunk Rd @ Norman Ave
* `53038` – Westbound Mariner Way @ Dewdney Trunk Rd
* `53039` – Westbound Mariner Way @ Dolphin St
* `53040` – Westbound Mariner Way @ Windward Dr
* `53041` – Southbound Mariner Way @ Hawser Ave
* `53042` – Southbound Mariner Way @ Daybreak Ave
* `53043` – Southbound Mariner Way @ 2900 Block
* `53044` – Southbound Mariner Way @ Lighthouse Court
* `53045` – Westbound Como Lake Ave @ 2500 Block
* `53046` – Westbound Como Lake Ave @ Seymour Dr
* `53047` – Westbound Como Lake Ave @ Thermal Dr
* `53048` – Westbound Como Lake Ave @ Custer Court
* `53049` – Westbound Como Lake Ave @ Linton St
* `53050` – Westbound Como Lake Ave @ Prospect St
* `53051` – Westbound Como Lake Ave @ Poirier St
* `53052` – Westbound Como Lake Ave @ Wasco St
* `53053` – Westbound Como Lake Ave @ Crestwood Dr
* `53055` – Westbound Como Lake Ave @ Porter St
* `53057` – Westbound Como Lake Ave @ Townley St
* `53062` – Eastbound Oakland St @ Royal Oak Ave
* `53065` – Eastbound Oakland St @ Waltham Ave
* `53066` – Eastbound Oakland St @ Gilley Ave
* `53067` – Eastbound Oakland St @ Brantford Ave
* `53068` – Southbound Sperling Ave @ Canada Way
* `53070` – Westbound Deer Lake Ave @ Rowan Ave
* `53073` – Northbound Sperling Ave @ Adair St
* `53074` – Northbound Sperling Ave @ Jordan Dr
* `53076` – Northbound Sperling Ave @ Winch St
* `53077` – Northbound Sperling Ave @ Kitchener St
* `53078` – Northbound Sperling Ave @ Aubrey St
* `53081` – Southbound Sperling Ave @ Aubrey St
* `53082` – Southbound Sperling Ave @ Kitchener St
* `53083` – Southbound Sperling Ave @ Winch St
* `53085` – Southbound Sperling Ave @ Jordan Dr
* `53086` – Southbound Sperling Ave @ Hummel St
* `53087` – Eastbound Deer Lake Ave @ Rowan Ave
* `53090` – Westbound Oakland St @ Brantford Ave
* `53091` – Westbound Oakland St @ Gilley Ave
* `53092` – Westbound Oakland St @ Waltham Ave
* `53094` – Westbound Oakland St @ Oakdale Rd
* `53102` – Westbound E Columbia St @ McBride Blvd
* `53113` – Northbound E Columbia St @ Holmes St
* `53114` – Northbound North Rd @ Delestre Ave
* `53118` – Northbound North Rd @ Cameron St
* `53119` – Northbound North Rd @ 600 Block
* `53120` – Northbound North Rd @ Foster Ave
* `53124` – Northbound Clarke Rd @ Morrison Ave
* `53125` – Northbound Clarke Rd @ Robinson St
* `53126` – Northbound Clarke Rd @ Glenayre Dr
* `53127` – Northbound Glenayre Dr @ Clarke Rd
* `53128` – Northbound Glenayre Dr @ Glenacre Court
* `53129` – Northbound Glenayre Dr @ Mount Royal Dr
* `53130` – Northbound Glenayre Dr @ Valour Dr
* `53131` – Northbound Glenayre Dr @ Angela Dr
* `53132` – Northbound Princeton Ave @ Harvard Dr
* `53133` – Northbound Princeton Ave @ McGill Dr
* `53134` – Eastbound Washington Dr @ 800 Block
* `53135` – Eastbound Washington Dr @ 800 Block
* `53136` – Southbound Washington Dr @ Yale Rd
* `53137` – Southbound College Park Way @ Oxford Dr
* `53139` – Southbound Cecile Dr @ Evergreen Dr
* `53140` – Southbound Cecile Dr @ Angela Dr
* `53141` – Eastbound Cecile Dr @ 1000 Block
* `53142` – Eastbound Cecile Dr @ Highview Place
* `53144` – Eastbound St. Johns St @ Douglas St
* `53145` – Eastbound St. Johns St @ Elgin St
* `53154` – Eastbound Barnet Hwy @ Lansdowne Dr
* `53162` – Northbound Johnson St @ Packard Ave
* `53163` – Northbound Johnson St @ Glen Dr
* `53168` – Southbound Pinetree Way @ Glen Dr
* `53172` – Westbound Barnet Hwy @ Falcon Dr
* `53182` – Westbound St. Johns St @ Buller St
* `53187` – Westbound St. Johns St @ Barnet Hwy
* `53188` – Westbound Cecile Dr @ Highview Place
* `53189` – Westbound Cecile Dr @ 1000 Block
* `53190` – Northbound Cecile Dr @ Cecile Place
* `53191` – Northbound Cecile Dr @ Evergreen Dr
* `53192` – Northbound College Park Way @ Oxford Dr
* `53193` – Northbound Washington Dr @ Yale Rd
* `53194` – Westbound Washington Dr @ 800 Block
* `53195` – Westbound Washington Dr @ 800 Block
* `53196` – Westbound Washington Dr @ Princeton Ave
* `53197` – Southbound Princeton Ave @ Yale Rd
* `53198` – Southbound Princeton Ave @ McMaster Court
* `53199` – Southbound Princeton Ave @ Harvard Dr
* `53200` – Southbound Glenayre Dr @ College Park Way
* `53201` – Southbound Glenayre Dr @ Angela Dr
* `53202` – Southbound Glenayre Dr @ Valour Dr
* `53203` – Southbound Glenayre Dr @ Glencoe Dr
* `53204` – Southbound Glenayre Dr @ Caithness Cres
* `53205` – Southbound Clarke Rd @ Glenayre Dr
* `53206` – Southbound Clarke Rd @ Thompson Ave
* `53207` – Southbound Clarke Rd @ Kemsley Ave
* `53211` – Southbound North Rd @ Foster Ave
* `53212` – Southbound North Rd @ Cameron St
* `53215` – Southbound North Rd @ Rochester St
* `53216` – Southbound North Rd @ Delestre Ave
* `53217` – Southbound E Columbia St @ Holmes St
* `53221` – Southbound E Columbia St @ Richmond St
* `53231` – Westbound Ioco Rd @ Metta St
* `53232` – Westbound Ioco Rd @ Harbour Place
* `53233` – Westbound Ioco Rd @ Mercier Rd
* `53234` – Westbound Ioco Rd @ Kicking Horse Way
* `53235` – Westbound Ioco Rd @ Barber St
* `53236` – Westbound Ioco Rd @ 1100 Block
* `53237` – Westbound Ioco Rd @ April Rd
* `53238` – Northbound 1st Ave @ Ioco Rd
* `53239` – Southbound 2nd Ave @ Ioco Rd
* `53240` – Eastbound Sunnyside Rd @ 2000 Block
* `53241` – Northbound Sunnyside Rd @ Alder Way
* `53243` – Northbound Sunnyside Rd @ Eaglecrest Dr
* `53247` – Southbound Sunnyside Rd @ Elementary Rd
* `53248` – Westbound Sunnyside Rd @ Summerwood Lane
* `53250` – Westbound Bedwell Bay Rd @ Senkler Rd
* `53254` – Eastbound Bedwell Bay Rd @ Kelly Rd
* `53255` – Eastbound Ioco Rd @ Alderside Rd
* `53256` – Eastbound Ioco Rd @ 1100 Block
* `53261` – Eastbound Ioco Rd @ Campbell Rd
* `53263` – Eastbound Ioco Rd @ Knowle St
* `53268` – Westbound St. Johns St @ Mary St
* `53276` – Eastbound Foster Ave @ Whiting Way
* `53279` – Westbound Guildford Way @ Lansdowne Dr
* `53280` – Westbound Guildford Way @ Falcon Dr
* `53284` – Northbound Heritage Mountain Blvd @ Ravine Dr
* `53285` – Northbound Heritage Mountain Blvd @ Parkglen Place
* `53286` – Northbound Heritage Mountain Blvd @ Turner Creek Dr
* `53287` – Northbound Turner Creek Dr @ Parkglen Place
* `53289` – Southbound Ravine Dr @ Buckhorn Place
* `53292` – Southbound Ravine Dr @ Boulderwood Place
* `53293` – Westbound Ravine Dr @ Heritage Mountain Blvd
* `53296` – Eastbound Guildford Way @ Falcon Dr
* `53297` – Eastbound Guildford Way @ Eagleridge Dr
* `53308` – Westbound Lougheed Hwy @ Lake City Way
* `53312` – Burrard Station @ Bay 6
* `53315` – Eastbound Lougheed Hwy @ Guilby St
* `53317` – Eastbound Lougheed Hwy @ 700 Block
* `53318` – Eastbound Brunette Ave @ Lougheed Hwy
* `53319` – Eastbound Brunette Ave @ Woolridge St
* `53321` – Eastbound Brunette Ave @ King Edward St
* `53326` – Eastbound Laurentian Cres @ Sheridan Ave
* `53327` – Eastbound Laurentian Cres @ Glendale Ave
* `53328` – Northbound Laurentian Cres @ Seaforth Cres
* `53329` – Northbound Laurentian Cres @ Hammond Ave
* `53330` – Northbound Laurentian Cres @ Madore Ave
* `53331` – Northbound Laurentian Cres @ Austin Ave
* `53335` – Westbound Foster Ave @ Linton St
* `53336` – Westbound Foster Ave @ 1700 Block
* `53338` – Northbound Schoolhouse St @ Winslow Ave
* `53339` – Northbound Schoolhouse St @ Foster Ave
* `53340` – Northbound Schoolhouse St @ Eden Ave
* `53341` – Northbound Schoolhouse St @ Smith Ave
* `53342` – Northbound Schoolhouse St @ Regan Ave
* `53344` – Eastbound Dewdney Trunk Rd @ Irvine St
* `53345` – Southbound Westwood St @ Dewdney Trunk Rd
* `53346` – Eastbound Kingsway Ave @ Burleigh Ave
* `53347` – Eastbound Kingsway Ave @ Dixon St
* `53351` – Eastbound Spuraway Ave @ Cove Place
* `53352` – Northbound Spuraway Ave @ Wagon Wheel Circle
* `53353` – Northbound Spuraway Ave @ Pasture Circle
* `53354` – Northbound Ranch Park Way @ Spuraway Ave
* `53355` – Northbound Ranch Park Way @ Norman Ave
* `53359` – Southbound Ranch Park Way @ Norman Ave
* `53360` – Southbound Spuraway Ave @ 3000 Block
* `53361` – Southbound Spuraway Ave @ Pasture Circle
* `53362` – Westbound Spuraway Ave @ Starlight Way
* `53364` – Westbound Spuraway Ave @ Pinnacle St
* `53365` – Southbound Schoolhouse St @ Como Lake Ave
* `53366` – Southbound Schoolhouse St @ Regan Ave
* `53367` – Southbound Schoolhouse St @ Smith Ave
* `53368` – Southbound Schoolhouse St @ Cambridge Dr
* `53369` – Eastbound Foster Ave @ Schoolhouse St
* `53371` – Southbound Schoolhouse St @ Winslow Ave
* `53372` – Southbound Schoolhouse St @ King Albert Ave
* `53373` – Eastbound Austin Ave @ Schoolhouse St
* `53374` – Eastbound Austin Ave @ Poirier St
* `53375` – Southbound Laurentian Cres @ Austin Ave
* `53376` – Southbound Laurentian Cres @ Dansey Ave
* `53377` – Southbound Laurentian Cres @ Rochester Ave
* `53378` – Southbound Laurentian Cres @ Thomas Ave
* `53379` – Westbound Laurentian Cres @ Glendale Ave
* `53380` – Westbound Laurentian Cres @ Sheridan Ave
* `53381` – Westbound Brunette Ave @ Laurentian Cres
* `53382` – Westbound Brunette Ave @ Schoolhouse St
* `53383` – Westbound Brunette Ave @ Laval St
* `53384` – Westbound Brunette Ave @ Marmont St
* `53385` – Westbound Brunette Ave @ Nelson St
* `53386` – Westbound Brunette Ave @ Lebleu St
* `53388` – Westbound Lougheed Hwy @ 700 Block
* `53389` – Westbound Lougheed Hwy @ 600 Block
* `53390` – Westbound Lougheed Hwy @ 500 Block
* `53396` – Southbound Guilby St @ Dansey Ave
* `53397` – Eastbound Rochester Ave @ Guilby St
* `53398` – Eastbound Rochester Ave @ Donald St
* `53399` – Eastbound Rochester Ave @ Walker St
* `53400` – Northbound Blue Mountain St @ Rochester Ave
* `53401` – Northbound Blue Mountain St @ Dansey Ave
* `53402` – Northbound Blue Mountain St @ Austin Ave
* `53403` – Eastbound Austin Ave @ Nelson St
* `53404` – Eastbound Austin Ave @ Marmont St
* `53405` – Eastbound Austin Ave @ Gatensbury St
* `53407` – Northbound Poirier St @ King Albert Ave
* `53408` – Eastbound Austin Ave @ Laurentian Cres
* `53409` – Eastbound Austin Ave @ Linton St
* `53410` – Eastbound Austin Ave @ Draycott St
* `53411` – Eastbound Austin Ave @ Mundy St
* `53412` – Eastbound Austin Ave @ 2300 Block
* `53413` – Southbound Hickey Dr @ Austin Ave
* `53414` – Southbound Hickey Dr @ Tolmie Ave
* `53415` – Eastbound Hickey Dr @ Bognor St
* `53422` – Eastbound Cape Horn Ave @ Colony Farm Rd
* `53423` – Northbound Holly Dr @ Fern Terrace
* `53425` – Northbound Holly Dr @ Lilac Place
* `53428` – Eastbound Hickey Dr @ Dartmoor Dr
* `53429` – Northbound Mariner Way @ Riverview Cres
* `53431` – Eastbound Chilko Dr @ Mariner Way
* `53432` – Eastbound Chilko Dr @ Douglas Dr
* `53433` – Eastbound Chilko Dr @ Mara Dr
* `53434` – Eastbound Chilko Dr @ Riverview Cres
* `53435` – Eastbound Chilko Dr @ Eastlake Gate
* `53436` – Eastbound Chilko Dr @ Silver Lake Place
* `53437` – Eastbound Chilko Dr @ Tatla Place
* `53438` – Northbound Lougheed Hwy @ Como Lake Ave
* `53445` – Westbound Kingsway Ave @ Jane St
* `53448` – Southbound Lougheed Hwy @ Dewdney Trunk Rd
* `53449` – Southbound Lougheed Hwy @ Como Lake Ave
* `53452` – Southbound Holly Dr @ Lilac Place
* `53454` – Southbound Holly Dr @ Fern Terrace
* `53455` – Westbound Cape Horn Ave @ Colony Farm Rd
* `53461` – Westbound Chilko Dr @ Sharpe St
* `53462` – Westbound Chilko Dr @ Tatla Place
* `53463` – Westbound Chilko Dr @ Silver Lake Place
* `53464` – Westbound Chilko Dr @ Eastlake Gate
* `53465` – Westbound Chilko Dr @ Riverview Cres
* `53466` – Westbound Chilko Dr @ Mara Dr
* `53467` – Westbound Chilko Dr @ Nadina Dr
* `53472` – Westbound Hickey Dr @ Balfour Dr
* `53473` – Northbound Hickey Dr @ Leduc Ave
* `53474` – Northbound Hickey Dr @ Latimer Ave
* `53475` – Westbound Austin Ave @ Hickey St
* `53476` – Westbound Austin Ave @ 2300 Block
* `53477` – Westbound Austin Ave @ Hillcrest St
* `53478` – Westbound Austin Ave @ Linton St
* `53479` – Westbound Austin Ave @ Laurentian Cres
* `53480` – Southbound Schoolhouse St @ Austin Ave
* `53481` – Westbound Austin Ave @ Gatensbury St
* `53482` – Westbound Austin Ave @ Marmont St
* `53483` – Westbound Austin Ave @ Nelson St
* `53484` – Southbound Blue Mountain St @ Dansey Ave
* `53485` – Westbound Rochester Ave @ Blue Mountain St
* `53486` – Westbound Rochester Ave @ Walker St
* `53487` – Westbound Rochester Ave @ Donald St
* `53505` – Eastbound E 8th Ave @ York St
* `53508` – Eastbound E 8th Ave @ Richmond St
* `53509` – Eastbound E 8th Ave @ Buchanan Ave
* `53511` – Eastbound Braid St @ Fader St
* `53514` – Eastbound Brunette Ave @ Bernatchey St
* `53515` – Eastbound Brunette Ave @ Blue Mountain St
* `53516` – Eastbound Brunette Ave @ Laurentian Cres
* `53517` – Eastbound Brunette Ave @ Cayer St
* `53519` – Eastbound Brunette Ave @ Peterson Ave
* `53521` – Northbound Mundy St @ Cape Horn Ave
* `53523` – Northbound Mundy St @ Dawes Hill Rd
* `53525` – Northbound Mundy St @ Kugler Ave
* `53526` – Northbound Mundy St @ 400 Block
* `53527` – Northbound Hillcrest St @ Austin Ave
* `53528` – Northbound Hillcrest St @ King Albert Ave
* `53529` – Northbound Hillcrest St @ Winslow Ave
* `53530` – Westbound Foster Ave @ Hillcrest St
* `53531` – Westbound Foster Ave @ Midvale St
* `53532` – Southbound Poirier St @ Winslow Ave
* `53533` – Westbound Foster Ave @ Schoolhouse St
* `53534` – Westbound Foster Ave @ Gatensbury St
* `53535` – Westbound Foster Ave @ Porter St
* `53536` – Westbound Foster Ave @ Colinet St
* `53537` – Westbound Cottonwood Ave @ Hailey St
* `53538` – Westbound Cottonwood Ave @ Easterbrook St
* `53543` – Eastbound Cottonwood Ave @ Robinson St
* `53544` – Eastbound Cottonwood Ave @ Easterbrook St
* `53545` – Eastbound Cottonwood Ave @ Hailey St
* `53546` – Eastbound Foster Ave @ Colinet St
* `53547` – Eastbound Foster Ave @ Porter St
* `53548` – Eastbound Foster Ave @ Gatensbury St
* `53549` – Eastbound Foster Ave @ 1700 Block
* `53550` – Eastbound Foster Ave @ Linton St
* `53551` – Eastbound Foster Ave @ Midvale St
* `53552` – Southbound Hillcrest St @ Foster Ave
* `53553` – Southbound Hillcrest St @ Winslow Ave
* `53554` – Southbound Hillcrest St @ King Albert Ave
* `53555` – Southbound Mundy St @ Austin Ave
* `53556` – Southbound Mundy St @ 400 Block
* `53557` – Southbound Mundy St @ Gale Ave
* `53558` – Southbound Mundy St @ Monterey Ave
* `53559` – Southbound Mundy St @ Leclair Dr
* `53560` – Southbound Mundy St @ Dawes Hill Rd
* `53561` – Southbound Mundy St @ Hillside Ave
* `53564` – Westbound Brunette Ave @ Hillside Ave
* `53565` – Westbound Brunette Ave @ Wiltshire Ave
* `53568` – Westbound Brunette Ave @ Lougheed Hwy
* `53569` – Westbound Brunette Ave @ Blue Mountain St
* `53570` – Westbound Brunette Ave @ Bernatchey St
* `53572` – Westbound Braid St @ Rousseau St
* `53573` – Westbound Braid St @ Fader St
* `53574` – Westbound Braid St @ E Columbia St
* `53575` – Westbound E 8th Ave @ Buchanan Ave
* `53576` – Westbound E 8th Ave @ Richmond St
* `53577` – Westbound E 8th Ave @ Cherry St
* `53578` – Westbound E 8th Ave @ William St
* `53579` – Westbound E 8th Ave @ York St
* `53590` – Eastbound 6th Ave @ 1st St
* `53593` – Eastbound E 6th Ave @ Glenbrook Dr
* `53594` – Northbound Richmond St @ Cumberland St
* `53595` – Northbound Richmond St @ Alberta St
* `53598` – Northbound Lebleu St @ Brunette Ave
* `53599` – Eastbound Alderson Ave @ Lebleu St
* `53600` – Eastbound Alderson Ave @ Nelson St
* `53602` – Northbound Marmont St @ Cartier Ave
* `53603` – Northbound Marmont St @ Thomas Ave
* `53604` – Eastbound Rochester Ave @ Marmont St
* `53605` – Eastbound Rochester Ave @ Laval St
* `53606` – Northbound Schoolhouse St @ Madore Ave
* `53607` – Westbound Rochester Ave @ Schoolhouse St
* `53608` – Westbound Rochester Ave @ Laval St
* `53610` – Southbound Marmont St @ Delestre Ave
* `53612` – Westbound Alderson Ave @ Nelson St
* `53615` – Southbound Richmond St @ Shiles St
* `53616` – Southbound Richmond St @ Alberta St
* `53620` – Westbound 6th Ave @ McBride Blvd
* `53626` – Southbound Linton St @ Regan Ave
* `53627` – Southbound Linton St @ Walnut Cres
* `53628` – Northbound Linton St @ Foster Ave
* `53629` – Northbound Linton St @ Rideau Ave
* `53630` – Northbound Linton St @ Grover Ave
* `53634` – Eastbound United Blvd @ 1300 Block
* `53635` – Eastbound United Blvd @ Clipper St
* `53636` – Eastbound United Blvd @ Brigantine Dr
* `53637` – Eastbound United Blvd @ Schooner St
* `53638` – Westbound United Blvd @ Fawcett Rd
* `53639` – Eastbound United Blvd @ Leeder St
* `53640` – Eastbound United Blvd @ Burbidge St
* `53644` – Eastbound Citadel Dr @ Capital Court
* `53645` – Eastbound Citadel Dr @ Palisade Cres
* `53646` – Eastbound Citadel Dr @ Confederation Dr
* `53647` – Eastbound Citadel Dr @ 900 Block
* `53648` – Eastbound Citadel Dr @ Castle Cres
* `53649` – Eastbound Citadel Dr @ Castle Cres
* `53650` – Eastbound Citadel Dr @ Pitt River Rd
* `53651` – Northbound Pitt River Rd @ Columbia Ave
* `53653` – Northbound Pitt River Rd @ Cameron Ave
* `53654` – Northbound Pitt River Rd @ Warwick Ave
* `53655` – Westbound Pitt River Rd @ McLean Ave
* `53656` – Westbound Pitt River Rd @ Mary Hill Rd
* `53657` – Northbound Mary Hill Rd @ Hawthorne Ave
* `53660` – Westbound United Blvd @ Burbidge St
* `53661` – Westbound United Blvd @ Leeder St
* `53663` – Westbound United Blvd @ Schooner St
* `53669` – Westbound Lougheed Hwy @ King Edward St
* `53671` – Eastbound Lougheed Hwy @ King Edward St
* `53672` – Eastbound Lougheed Hwy @ Schoolhouse St
* `53674` – Eastbound Citadel Dr @ Gateway Place
* `53680` – Southbound Broadway St @ Mary Hill Bypass
* `53681` – Eastbound Kebet Way @ Spitfire Place
* `53683` – Eastbound Kebet Way @ Kingsway Ave
* `53684` – Westbound Kingsway Ave @ Mary Hill Bypass
* `53685` – Westbound Kingsway Ave @ Coast Meridian Rd
* `53687` – Westbound McLean Ave @ Kingsway Ave
* `53688` – Southbound Reeve St @ Riverside Secondary
* `53689` – Southbound Mary Hill Rd @ Wilson Ave
* `53690` – Southbound Mary Hill Rd @ Welcher Ave
* `53691` – Southbound Mary Hill Rd @ Central Ave
* `53692` – Eastbound Pitt River Rd @ Mary Hill Rd
* `53693` – Eastbound Pitt River Rd @ Tyner St
* `53694` – Southbound Pitt River Rd @ Warwick Cres
* `53695` – Southbound Pitt River Rd @ Cameron Ave
* `53696` – Southbound Pitt River Rd @ Pooley Ave
* `53697` – Southbound Pitt River Rd @ Columbia Ave
* `53699` – Southbound Broadway St @ Langan Ave
* `53700` – Southbound Broadway St @ Industrial Ave
* `53711` – Westbound Citadel Dr @ Gateway Place
* `53712` – Westbound Citadel Dr @ Kensington Cres East
* `53713` – Westbound Citadel Dr @ Kensington Cres West
* `53714` – Westbound Citadel Dr @ 900 Block
* `53715` – Southbound Citadel Dr @ Fortress Court
* `53717` – Westbound Lougheed Hwy @ Schoolhouse St
* `53718` – Northbound Inlet Dr @ Ridge Dr
* `53719` – Northbound Inlet Dr @ Bayview Dr
* `53720` – Eastbound Barnet Rd @ 7600 Block
* `53722` – Eastbound Barnet Rd @ Barnet Marine Park
* `53723` – Eastbound Barnet Rd @ 9900 Block
* `53724` – Eastbound Barnet Hwy @ Reed Point Way
* `53725` – Southbound Barnet Hwy @ Gore St
* `53726` – Southbound Barnet Hwy @ Clarke St
* `53729` – Eastbound Guildford Way @ Town Centre Blvd
* `53731` – Southbound Shaughnessy St @ Atkins Ave
* `53733` – Southbound Shaughnessy St @ Kelly Ave
* `53734` – Southbound Shaughnessy St @ Hawthorne Ave
* `53735` – Southbound Shaughnessy St @ Rindall Ave
* `53736` – Westbound Pitt River Rd @ Rowland St
* `53737` – Northbound Reeve St @ Hawthorne Ave
* `53738` – Northbound Reeve St @ Atkins Ave
* `53739` – Eastbound Wilson Ave @ Bury Ave
* `53745` – Westbound Guildford Way @ Johnson St
* `53746` – Westbound Guildford Way @ 2800 Block
* `53747` – Westbound Guildford Way @ Eagleridge Dr
* `53753` – Northbound Barnet Hwy @ View St
* `53754` – Northbound Barnet Hwy @ Union St
* `53755` – Westbound Barnet Hwy @ Reed Point Way
* `53756` – Westbound Barnet Rd @ 9900 Block
* `53757` – Westbound Barnet Rd @ Barnet Marine Park
* `53758` – Westbound Barnet Rd @ Texaco Dr
* `53759` – Westbound Barnet Rd @ 7500 Block
* `53761` – Southbound Inlet Dr @ Ridge Dr
* `53765` – Southbound Westwood St @ Anson Ave
* `53768` – Southbound Eastern Dr @ Belle Place
* `53769` – Southbound Eastern Dr @ Carmen Place
* `53770` – Westbound Eastern Dr @ Claudia Place
* `53771` – Northbound Western Dr @ Eastern Dr
* `53772` – Northbound Western Dr @ Celeste Cres
* `53773` – Northbound Western Dr @ Anita Dr
* `53774` – Northbound Western Dr @ Lamprey Dr
* `53775` – Northbound Western Dr @ Aire Cres
* `53776` – Northbound Mary Hill Rd @ Lobb Ave
* `53784` – Eastbound Riverwood Gate @ Terry Fox Secondary
* `53787` – Northbound Oxford St @ Westminster Ave
* `53788` – Northbound Oxford St @ Coquitlam Ave
* `53789` – Eastbound Coquitlam Ave @ York St
* `53790` – Eastbound Coquitlam Ave @ Wellington St
* `53792` – Northbound Coast Meridian Rd @ Prairie Ave
* `53801` – Northbound Oxford St @ Mason Ave
* `53802` – Northbound Oxford St @ Forestgate Place
* `53804` – Northbound Coast Meridian Rd @ Greenmount Ave
* `53805` – Eastbound Victoria Dr @ Toronto St
* `53806` – Eastbound Victoria Dr @ Apel Dr
* `53807` – Eastbound Victoria Dr @ Burke Mountain St
* `53811` – Southbound Cedar Dr @ Lincoln Ave
* `53812` – Southbound Cedar Dr @ Pinemont Ave
* `53813` – Westbound Prairie Ave @ Cedar Dr
* `53815` – Westbound Prairie Ave @ Newberry St
* `53817` – Westbound Prairie Ave @ Sefton St
* `53818` – Westbound Prairie Ave @ Wellington St
* `53819` – Westbound Prairie Ave @ York St
* `53822` – Southbound Shaughnessy St @ David Place
* `53823` – Southbound Shaughnessy St @ Flynn Cres
* `53824` – Southbound Shaughnessy St @ Parkland Dr
* `53825` – Southbound Shaughnessy St @ Lodge Dr
* `53828` – Southbound Shaughnessy St @ Dorset Ave
* `53829` – Southbound Shaughnessy St @ Prairie Ave
* `53830` – Southbound Shaughnessy St @ Fraser Ave
* `53831` – Westbound Dominion Ave @ Archbishop Carney Secondary
* `53832` – Northbound Shaughnessy St @ Fraser Ave
* `53835` – Eastbound Prairie Ave @ Oxford St
* `53836` – Eastbound Prairie Ave @ York St
* `53837` – Eastbound Prairie Ave @ Wellington St
* `53839` – Eastbound Prairie Ave @ Kilmer St
* `53840` – Eastbound Prairie Ave @ Norfolk St
* `53841` – Eastbound Prairie Ave @ Cedar Dr
* `53842` – Northbound Cedar Dr @ Pinemont Ave
* `53843` – Northbound Cedar Dr @ Lincoln Ave
* `53844` – Northbound Cedar Dr @ Chelsea Ave
* `53846` – Westbound Victoria Dr @ 3500 Block
* `53847` – Westbound Victoria Dr @ Burke Mountain St
* `53848` – Westbound Victoria Dr @ Soball St
* `53849` – Westbound Victoria Dr @ Toronto St
* `53850` – Southbound Coast Meridian Rd @ Victoria Dr
* `53851` – Westbound Greenmount Ave @ Coast Meridian Rd
* `53858` – Southbound Coast Meridian Rd @ Salisbury Ave
* `53861` – Westbound Coquitlam Ave @ Wellington St
* `53862` – Southbound Oxford St @ Coquitlam Ave
* `53863` – Westbound Lougheed Hwy @ Oxford St
* `53865` – Northbound Wellington St @ Wellington Court
* `53871` – Northbound Pipeline Rd @ Glen Dr
* `53873` – Eastbound Dunkirk Ave @ Pipeline Rd
* `53874` – Eastbound Dunkirk Ave @ Galiano St
* `53875` – Eastbound Dunkirk Ave @ Hornby St
* `53876` – Northbound Gabriola Dr @ Georgeson Ave
* `53877` – Northbound Gabriola Dr @ Savary Ave
* `53878` – Westbound Harwood Ave @ Gabriola Dr
* `53879` – Westbound Harwood Ave @ Hornby St
* `53882` – Southbound Pipeline Rd @ Dunkirk Ave
* `53883` – Southbound Pipeline Rd @ Guildford Way
* `53888` – Southbound Pinetree Way @ Pinewood Ave
* `53892` – Northbound Falcon Dr @ Guildford Way
* `53893` – Westbound Falcon Dr @ 1200 Block
* `53894` – Westbound Noons Creek Dr @ Falcon Dr
* `53896` – Eastbound Noons Creek Dr @ Maude Court
* `53897` – Eastbound Noons Creek Dr @ Noons Creek Close
* `53898` – Northbound Noons Creek Dr @ Barberry Place
* `53899` – Northbound Noons Creek Dr @ Hemlock Place
* `53901` – Northbound Noons Creek Dr @ Shale Court
* `53904` – Southbound Lansdowne Dr @ David Ave
* `53906` – Southbound Lansdowne Dr @ Briarcliffe Dr South
* `53907` – Westbound Lansdowne Dr @ Turret Cres
* `53908` – Eastbound Lansdowne Dr @ Steeple Dr
* `53909` – Westbound Lansdowne Dr @ Honeysuckle Lane
* `53910` – Southbound Lansdowne Dr @ Steeple Dr
* `53911` – Southbound Lansdowne Dr @ Charter Hill Dr
* `53912` – Southbound Lansdowne Dr @ Guildford Way
* `53913` – Northbound Lansdowne Dr @ Guildford Way
* `53914` – Northbound Lansdowne Dr @ Charter Hill Dr
* `53915` – Northbound Lansdowne Dr @ Steeple Dr
* `53916` – Eastbound Lansdowne Dr @ Honeysuckle Lane
* `53917` – Eastbound Lansdowne Dr @ Turret Cres
* `53918` – Northbound Lansdowne Dr @ Briarcliffe Dr
* `53919` – Northbound Lansdowne Dr @ Greenhill Court
* `53920` – Northbound Lansdowne Dr @ David Ave
* `53921` – Northbound Lansdowne Dr @ Lupine Court
* `53923` – Eastbound Panorama Dr @ Lansdowne Dr
* `53924` – Eastbound Panorama Dr @ Parkway Blvd
* `53928` – Southbound Johnson St @ Delahaye Dr
* `53930` – Southbound Johnson St @ Durant Dr
* `53931` – Southbound Johnson St @ Guildford Way
* `53932` – Southbound Johnson St @ Glen Dr
* `53933` – Southbound Johnson St @ Packard Ave
* `53934` – Northbound Johnson St @ Guildford Way
* `53935` – Northbound Johnson St @ Durant Dr
* `53936` – Northbound Johnson St @ Walton Ave
* `53937` – Northbound Johnson St @ Delahaye Dr
* `53938` – Northbound Johnson St @ David Ave
* `53939` – Northbound Johnson St @ Valley Vista Dr
* `53940` – Northbound Johnson St @ Meadowvista Place
* `53941` – Eastbound Johnson St @ Robson Dr
* `53942` – Eastbound Robson Dr @ Waterford Place
* `53943` – Eastbound Robson Dr @ Pinetree Way
* `53944` – Eastbound Robson Dr @ Purcell Dr
* `53945` – Eastbound Robson Dr @ Blackwater Place
* `53946` – Southbound Pipeline Rd @ Robson Dr
* `53954` – Eastbound E 3rd St @ Moody Ave
* `53960` – Westbound E 3rd St @ Queensbury Ave
* `53961` – Westbound E 3rd St @ Moody Ave
* `53963` – Westbound E Keith Rd @ Mountain Hwy
* `53965` – Northbound Mountain Hwy @ E 15 St
* `53966` – Northbound Mountain Hwy @ E 18 St
* `53967` – Northbound Mountain Hwy @ E 20 St
* `53968` – Northbound Mountain Hwy @ E 24 St
* `53978` – Northbound Mountain Hwy @ Argyle Rd
* `53979` – Northbound Mountain Hwy @ Dovercourt Rd
* `53980` – Northbound Mountain Hwy @ Doran Rd
* `53981` – Northbound Mountain Hwy @ Kilmer Rd
* `53982` – Northbound Mountain Hwy @ Dempsey Rd
* `53983` – Northbound Mountain Hwy @ Coleman St
* `53984` – Eastbound McNair Dr @ Tourney Rd
* `53986` – Southbound Hoskins Rd @ 4600 Block
* `53987` – Southbound Hoskins Rd @ Coleman St
* `53988` – Eastbound Dempsey Rd @ Hoskins Rd
* `53991` – Westbound Coleman St @ Underwood Ave
* `53992` – Northbound Hoskins Rd @ Coleman St
* `53993` – Northbound Hoskins Rd @ 4700 Block
* `53995` – Westbound McNair Dr @ Tourney Rd
* `53996` – Southbound Mountain Hwy @ Coleman St
* `53997` – Southbound Mountain Hwy @ Dyck Rd
* `53998` – Southbound Mountain Hwy @ Kilmer Rd
* `53999` – Southbound Mountain Hwy @ Doran Rd
* `54001` – Southbound Mountain Hwy @ Frederick Rd
* `54013` – Westbound Lynn Valley Rd @ E 29 St
* `54014` – Westbound Lynn Valley Rd @ Fromme Rd
* `54015` – Westbound Lynn Valley Rd @ Mollie Nye Way
* `54016` – Westbound Lynn Valley Rd @ William Ave
* `54017` – Southbound Boulevard Cres @ Sutherland Ave
* `54018` – Southbound Grand Blvd East @ E 19 St
* `54020` – Southbound Grand Blvd East @ E 15 St
* `54021` – Southbound Grand Blvd East @ E 13 St
* `54022` – Southbound Grand Blvd East @ E 11 St
* `54023` – Southbound Grand Blvd East @ E 9th St
* `54024` – Southbound Queensbury Ave @ E Keith Rd
* `54025` – Southbound Queensbury Ave @ E 5th St
* `54029` – Eastbound Mt Seymour Parkway @ Seymour Blvd
* `54030` – Eastbound Mt Seymour Parkway @ Riverside Dr
* `54035` – Eastbound Mt Seymour Parkway @ Tollcross Rd
* `54036` – Eastbound Mt Seymour Parkway @ Strathaven Dr
* `54037` – Eastbound Mt Seymour Parkway @ Apex Ave
* `54038` – Eastbound Mt Seymour Parkway @ Parkgate Ave
* `54040` – Eastbound Mt Seymour Parkway @ 3800 Block
* `54041` – Eastbound Mt Seymour Parkway @ 4000 Block
* `54042` – Southbound Fairway Dr @ Mt Seymour Parkway
* `54043` – Southbound Fairway Dr @ Huntingdon Cres
* `54044` – Southbound Fairway Dr @ Caddy Rd
* `54045` – Eastbound Dollar Rd @ Fairway Dr
* `54047` – Northbound N Dollarton Hwy @ Caddy Rd
* `54049` – Northbound Deep Cove Rd @ Strathcona Rd
* `54050` – Northbound Deep Cove Rd @ 1600 Block
* `54051` – Northbound Deep Cove Rd @ Cliffmont Rd
* `54052` – Northbound Deep Cove Rd @ Badger Rd
* `54053` – Southbound Panorama Dr @ Naughton Ave
* `54055` – Southbound Deep Cove Rd @ Badger Rd
* `54056` – Southbound Deep Cove Rd @ Strathcona Rd
* `54058` – Southbound N Dollarton Hwy @ Caddy Rd
* `54060` – Northbound Fairway Dr @ Dollar Rd
* `54061` – Northbound Fairway Dr @ Caddy Rd
* `54062` – Northbound Fairway Dr @ Huntingdon Cres
* `54063` – Westbound Mt Seymour Parkway @ Beaufort Rd
* `54064` – Westbound Mt Seymour Parkway @ 3900 Block
* `54065` – Westbound Mt Seymour Parkway @ 3800 Block
* `54067` – Westbound Mt Seymour Parkway @ Parkgate Ave
* `54068` – Westbound Mt Seymour Parkway @ Apex Ave
* `54069` – Westbound Mt Seymour Parkway @ Strathaven Dr
* `54070` – Westbound Mt Seymour Parkway @ Northlands Dr
* `54071` – Westbound Mt Seymour Parkway @ Broadview Dr
* `54072` – Westbound Mt Seymour Parkway @ Berkley Rd
* `54073` – Westbound Mt Seymour Parkway @ Browning Place
* `54074` – Westbound Mt Seymour Parkway @ Riverside Dr
* `54076` – Westbound Mt Seymour Parkway @ Seymour Blvd
* `54080` – Eastbound Dollarton Hwy @ 2400 Block
* `54081` – Eastbound Dollarton Hwy @ Ellis St
* `54082` – Eastbound Dollarton Hwy @ 3000 Block
* `54083` – Eastbound Dollarton Hwy @ 3100 Block
* `54084` – Eastbound Dollarton Hwy @ 3300 Block
* `54085` – Eastbound Dollarton Hwy @ Raven Woods Dr
* `54086` – Eastbound Dollarton Hwy @ 3700 Block
* `54087` – Eastbound Dollarton Hwy @ 4000 Block
* `54088` – Northbound N Dollarton Hwy @ Dollar Rd
* `54090` – Westbound Dollarton Hwy @ 4000 Block
* `54091` – Westbound Dollarton Hwy @ 3800 Block
* `54092` – Westbound Dollarton Hwy @ Raven Woods Dr
* `54093` – Westbound Dollarton Hwy @ 3300 Block
* `54094` – Westbound Dollarton Hwy @ 3100 Block
* `54095` – Westbound Dollarton Hwy @ Ellis St
* `54096` – Westbound Dollarton Hwy @ 2500 Block
* `54098` – Westbound Old Dollarton Rd @ Riverside Dr
* `54099` – Westbound Old Dollarton Rd @ Front St
* `54101` – Northbound Emerson Way @ Mt Seymour Parkway
* `54102` – Westbound Carnation St @ Emerson Way
* `54103` – Northbound Berkley Rd @ Carnation St
* `54104` – Northbound Berkley Rd @ Shelley Rd
* `54105` – Northbound Berkley Rd @ Boswell Ave
* `54106` – Northbound Berkley Rd @ Layton Dr
* `54107` – Northbound Berkley Rd @ Sechelt Dr
* `54108` – Northbound Berkley Ave @ Derbyshire Place
* `54109` – Eastbound Berkley Ave @ Hyannis Dr
* `54110` – Southbound Hyannis Dr @ Berkley Ave
* `54111` – Westbound Sechelt Dr @ Hyannis Dr
* `54112` – Westbound Sechelt Dr @ Derbyshire Way
* `54113` – Westbound Sechelt Dr @ Berkley Ave
* `54114` – Southbound Berkley Rd @ Byron Rd
* `54115` – Southbound Berkley Rd @ Shelley Rd
* `54116` – Eastbound Carnation St @ Berkley Rd
* `54117` – Southbound Emerson Way @ Carnation St
* `54118` – Southbound Emerson Way @ Mt Seymour Parkway
* `54120` – Northbound Ellis St @ Dollarton Hwy
* `54121` – Eastbound Plymouth Dr @ Ellis St
* `54122` – Eastbound Plymouth Dr @ Eddystone Cres
* `54123` – Northbound Plymouth Dr @ Fairfield Rd
* `54125` – Northbound Mt Seymour Rd @ Hamber Place
* `54126` – Eastbound Indian River Dr @ Lighthall Court
* `54127` – Northbound Inlet Cres @ Indian River Dr
* `54128` – Westbound Indian River Dr @ Cascade Court
* `54129` – Southbound Mt Seymour Rd @ Hamber Place
* `54130` – Westbound Plymouth Dr @ Fairfield Rd
* `54131` – Southbound Plymouth Dr @ Brixham Rd
* `54132` – Southbound Plymouth Dr @ Tollcross Rd
* `54133` – Southbound Plymouth Dr @ McCartney Close
* `54144` – Northbound Lonsdale Ave @ E Keith Rd
* `54145` – Northbound Lonsdale Ave @ E 8th St
* `54146` – Northbound Lonsdale Ave @ E 13 St
* `54150` – Westbound E 3rd St @ St. Patricks Ave
* `54155` – Southbound Lonsdale Ave @ W 13 St
* `54170` – Eastbound E 3rd St @ St. Patricks Ave
* `54172` – Northbound Queensbury Ave @ E 4th St
* `54173` – Northbound Queensbury Ave @ E 7th St
* `54174` – Northbound Grand Blvd East @ E Keith Rd
* `54175` – Northbound Grand Blvd East @ E 9th St
* `54176` – Northbound Grand Blvd East @ E 11 St
* `54179` – Eastbound E 15 St @ Grand Blvd East
* `54183` – Eastbound Lynn Valley Rd @ Harold Rd
* `54184` – Eastbound Lynn Valley Rd @ Allan Rd
* `54185` – Eastbound Lynn Valley Rd @ Peters Rd
* `54186` – Northbound Lynn Valley Rd @ Burrill Ave
* `54187` – Northbound Lynn Valley Rd @ Langworthy St
* `54188` – Northbound Lynn Valley Rd @ Dempsey Rd
* `54190` – Southbound Hoskins Rd @ Dempsey Rd
* `54191` – Southbound Hoskins Rd @ Langworthy St
* `54192` – Southbound Hoskins Rd @ Burrill Ave
* `54193` – Westbound Lynn Valley Rd @ Peters Rd
* `54194` – Westbound Lynn Valley Rd @ Allan Rd
* `54195` – Westbound Lynn Valley Rd @ Church St
* `54197` – Northbound Lonsdale Ave @ E 15 St
* `54198` – Northbound Lonsdale Ave @ E 17 St
* `54200` – Northbound Lonsdale Ave @ E 21 St
* `54201` – Northbound Lonsdale Ave @ E 23 St
* `54202` – Northbound Lonsdale Ave @ E 27 St
* `54203` – Eastbound E 29 St @ Lonsdale Ave
* `54204` – Eastbound E 29 St @ St. Georges Ave
* `54205` – Eastbound E 29 St @ St. Andrews Ave
* `54206` – Eastbound E 29 St @ St. Kilda Ave
* `54207` – Eastbound E 29 St @ Brand St
* `54208` – Eastbound E 29 St @ Tempe Glen Dr
* `54209` – Eastbound E 29 St @ William Ave
* `54210` – Eastbound E 29 St @ St. Christophers Rd
* `54211` – Eastbound E 29 St @ Fromme Rd
* `54214` – Eastbound Peters Rd @ Henderson Ave
* `54215` – Eastbound Peters Rd @ Duval Rd
* `54216` – Southbound Duval Rd @ Westover Rd
* `54218` – Westbound Ross Rd @ 1700 Block
* `54219` – Southbound Hoskins Rd @ Ross Rd
* `54220` – Southbound Hoskins Rd @ Kilmarnock Cres North
* `54221` – Southbound Hoskins Rd @ Torquay Ave
* `54222` – Southbound Hoskins Rd @ Kilmarnock Cres South
* `54223` – Southbound Hoskins Rd @ Greenock Place
* `54224` – Southbound Arborlynn Dr @ Hoskins Rd
* `54225` – Southbound Arborlynn Dr @ Avonlynn Cres
* `54230` – Northbound Arborlynn Dr @ Hoskins Rd
* `54231` – Northbound Hoskins Rd @ Greenock Place
* `54232` – Northbound Hoskins Rd @ Kilmarnock Cres South
* `54233` – Northbound Hoskins Rd @ Torquay Ave
* `54234` – Northbound Hoskins Rd @ Wembley Dr
* `54235` – Eastbound Ross Rd @ Hoskins Rd
* `54236` – Eastbound Ross Rd @ Wembley Dr
* `54237` – Northbound Duval Rd @ Robinson Rd
* `54238` – Northbound Duval Rd @ Westover Rd
* `54239` – Westbound Peters Rd @ Duval Rd
* `54240` – Westbound Peters Rd @ Henderson Ave
* `54241` – Westbound E 29 St @ Fromme Rd
* `54242` – Westbound E 29 St @ St. Christophers Rd
* `54243` – Westbound E 29 St @ William Ave
* `54245` – Westbound E 29 St @ Somerset St
* `54246` – Westbound E 29 St @ St. Andrews Ave
* `54247` – Westbound E 29 St @ St. Georges Ave
* `54248` – Southbound Lonsdale Ave @ W 29 St
* `54249` – Southbound Lonsdale Ave @ W 27 St
* `54250` – Southbound Lonsdale Ave @ W 23 St
* `54251` – Southbound Lonsdale Ave @ W 21 St
* `54252` – Southbound Lonsdale Ave @ W 19 St
* `54253` – Southbound Lonsdale Ave @ W 17 St
* `54254` – Southbound Lonsdale Ave @ W 15 St
* `54256` – Northbound Lonsdale Ave @ E Windsor Rd
* `54257` – Eastbound E Osborne Rd @ Lonsdale Ave
* `54258` – Eastbound E Osborne Rd @ St. Georges Ave
* `54259` – Eastbound E Osborne Rd @ St. Andrews Ave
* `54260` – Eastbound E Osborne Rd @ Regent Ave
* `54261` – Northbound Princess Ave @ E Osborne Rd
* `54262` – Northbound Princess Ave @ Wellington Dr
* `54263` – Northbound Princess Ave @ 3800 Block
* `54264` – Northbound Princess Ave @ Regal Cres
* `54265` – Westbound E Braemar Rd @ 700 Block
* `54266` – Westbound E Braemar Rd @ Regent Ave
* `54267` – Westbound E Braemar Rd @ Braemar Place
* `54268` – Westbound E Braemar Rd @ 200 Block
* `54269` – Westbound E Braemar Rd @ St. Georges Ave
* `54274` – Southbound Lonsdale Ave @ Kensington Cres
* `54276` – Southbound Lonsdale Ave @ W Osborne Rd
* `54277` – Southbound Lonsdale Ave @ W Windsor Rd
* `54280` – Westbound W 3rd St @ Mahon Ave
* `54281` – Westbound W 3rd St @ Forbes Ave
* `54282` – Northbound Bewicke Ave @ W Keith Rd
* `54283` – Northbound Bewicke Ave @ W 16 St
* `54284` – Northbound Westview Dr @ Larson Rd
* `54285` – Northbound Westview Dr @ W 19 St
* `54287` – Northbound Westview Dr @ W 23 St
* `54288` – Northbound Westview Dr @ Hwy One
* `54289` – Northbound Westview Dr @ W 28 St
* `54295` – Eastbound Clements Ave @ 1000 Block
* `54296` – Eastbound Clements Ave @ Cliffridge Ave
* `54297` – Southbound Cliffridge Ave @ Montroyal Blvd
* `54298` – Westbound Montroyal Blvd @ Belvedere Dr
* `54302` – Southbound Capilano Rd @ Eldon Rd
* `54306` – Eastbound Edgemont Blvd @ Sunset Blvd
* `54307` – Eastbound Edgemont Blvd @ Ridgewood Dr
* `54308` – Eastbound Edgemont Blvd @ Highland Blvd
* `54309` – Eastbound W Queens Rd @ Edgemont Blvd
* `54312` – Eastbound W Queens Rd @ Westview Dr
* `54313` – Eastbound W Queens Rd @ Larson Rd
* `54314` – Eastbound W Queens Rd @ Jones Ave
* `54315` – Eastbound W Queens Rd @ Mahon Ave
* `54316` – Eastbound W Queens Rd @ Chesterfield Ave
* `54317` – Eastbound E Keith Rd @ Lonsdale Ave
* `54318` – Eastbound E Keith Rd @ St. Georges Ave
* `54319` – Eastbound E Keith Rd @ St. Andrews Ave
* `54320` – Eastbound E Keith Rd @ St. Davids Ave
* `54321` – Eastbound E Keith Rd @ Ridgeway Ave
* `54322` – Eastbound E Keith Rd @ Moody Ave
* `54324` – Eastbound E Keith Rd @ Sutherland Ave
* `54325` – Eastbound E Keith Rd @ Hendry Ave
* `54326` – Eastbound E Keith Rd @ Adderley St
* `54327` – Eastbound E Keith Rd @ Cloverley St
* `54329` – Southbound Brooksbank Ave @ E Keith Rd
* `54330` – Southbound Brooksbank Ave @ E 4th St
* `54331` – Southbound Brooksbank Ave @ E 3rd St
* `54332` – Eastbound Main St @ Lynn Ave
* `54334` – Northbound Brooksbank Ave @ E 3rd St
* `54335` – Northbound Brooksbank Ave @ E 5th St
* `54337` – Westbound E Keith Rd @ Shavington St
* `54338` – Westbound E Keith Rd @ Cloverley St
* `54339` – Westbound E Keith Rd @ Adderley St
* `54340` – Westbound E Keith Rd @ Hendry Ave
* `54343` – Westbound E Keith Rd @ Grand Blvd West
* `54344` – Westbound E Keith Rd @ Moody Ave
* `54345` – Westbound E Keith Rd @ Ridgeway Ave
* `54346` – Westbound E Keith Rd @ St. Andrews Ave
* `54347` – Westbound E Keith Rd @ St. Georges Ave
* `54350` – Westbound W Queens Rd @ Lonsdale Ave
* `54351` – Westbound W Queens Rd @ Chesterfield Ave
* `54352` – Westbound W Queens Rd @ Mahon Ave
* `54353` – Westbound W Queens Rd @ Jones Ave
* `54355` – Westbound W Queens Rd @ Delbrook Ave
* `54357` – Westbound W Queens Rd @ Thorncliffe Dr
* `54358` – Westbound W Queens Rd @ Colwood Dr
* `54359` – Southbound Highland Blvd @ Woodbine Dr
* `54361` – Westbound Edgemont Blvd @ Sunset Blvd
* `54364` – Westbound Ridgewood Dr @ Norcross Way
* `54366` – Northbound Capilano Rd @ Mt Crown Rd
* `54376` – Westbound W Esplanade Ave @ Semisch Ave
* `54380` – Westbound Marine Dr @ Fell Ave
* `54381` – Westbound Marine Dr @ Hanes Ave
* `54382` – Westbound Marine Dr @ Hamilton Ave
* `54383` – Northbound Mackay Ave @ Marine Dr
* `54384` – Northbound Mackay Ave @ W 19 St
* `54385` – Northbound Mackay Ave @ 2000 Block
* `54386` – Westbound W 22 St @ Cortell St
* `54387` – Westbound W 22 St @ Lloyd Ave
* `54388` – Westbound W 22 St @ Pemberton Ave
* `54389` – Westbound W 22 St @ Philip Ave
* `54390` – Eastbound W 22 St @ Philip Ave
* `54392` – Northbound Capilano Rd @ Paisley Rd
* `54398` – Southbound Capilano Rd @ Ridgewood Dr
* `54399` – Southbound Capilano Rd @ Paisley Rd
* `54400` – Eastbound W 22 St @ Bridgman Ave
* `54403` – Southbound Mackay Ave @ W 21 St
* `54404` – Southbound Mackay Ave @ W 19 St
* `54414` – Eastbound Marine Dr @ Tatlow Ave
* `54415` – Eastbound Marine Dr @ Philip Ave
* `54419` – Southbound Chesterfield Ave @ W 3rd St
* `54420` – Southbound Chesterfield Ave @ W 1st St
* `54426` – Northbound Chesterfield Ave @ W 1st St
* `54427` – Northbound Chesterfield Ave @ W 2nd St
* `54428` – Westbound Marine Dr @ Mackay Ave
* `54436` – Westbound Marine Dr @ Bridgman Ave
* `54437` – Westbound Marine Dr @ Philip Ave
* `54438` – Westbound Marine Dr @ Bowser Ave
* `54451` – Eastbound W Keith Rd @ Delbruck Ave
* `54452` – Eastbound W Keith Rd @ Forbes Ave
* `54453` – Northbound Jones Ave @ W Keith Rd
* `54454` – Eastbound W 15 St @ Jones Ave
* `54455` – Eastbound W 15 St @ Mahon Ave
* `54456` – Eastbound W 15 St @ Chesterfield Ave
* `54458` – Eastbound E 15 St @ St. Andrews Ave
* `54459` – Eastbound E 15 St @ Ridgeway Ave
* `54460` – Eastbound E 15 St @ Moody Ave
* `54461` – Westbound E 15 St @ Moody Ave
* `54462` – Westbound E 15 St @ Ridgeway Ave
* `54463` – Westbound E 15 St @ St. Andrews Ave
* `54466` – Westbound W 15 St @ Mahon Ave
* `54467` – Southbound Jones Ave @ W 15 St
* `54468` – Southbound Jones Ave @ W Keith Rd
* `54469` – Westbound W Keith Rd @ Forbes Ave
* `54470` – Westbound W Keith Rd @ Delbruck Ave
* `54471` – Southbound Lions Gate Bridge Onramp @ Marine Dr
* `54474` – Northbound Lonsdale Ave @ E Kings Rd
* `54475` – Northbound Lonsdale Ave @ E Osborne Rd
* `54480` – Northbound Capilano Rd @ W 21 St
* `54482` – Eastbound Ridgewood Dr @ Norcross Way
* `54483` – Eastbound Ridgewood Dr @ Bluebonnet Dr
* `54484` – Eastbound Ridgewood Dr @ Brookridge Dr
* `54486` – Northbound Highland Blvd @ Belmont Ave
* `54487` – Northbound Highland Blvd @ Glenora Ave
* `54488` – Northbound Highland Blvd @ Melbourne Ave
* `54489` – Northbound Highland Blvd @ Leovista Ave
* `54490` – Northbound Highland Blvd @ Wavertree Rd
* `54491` – Northbound Highland Blvd @ Tudor Ave
* `54492` – Northbound Highland Blvd @ Derby Place
* `54493` – Northbound Highland Blvd @ Handsworth Rd
* `54494` – Northbound Highland Blvd @ Belvedere Dr
* `54495` – Northbound Highland Blvd @ Montroyal Blvd
* `54496` – Eastbound Montroyal Blvd @ Marineview Cres
* `54497` – Eastbound Montroyal Blvd @ Bonita Dr
* `54498` – Eastbound Montroyal Blvd @ Skyline Dr
* `54499` – Eastbound Montroyal Blvd @ Glencanyon Dr
* `54500` – Eastbound Montroyal Blvd @ Delbrook Ave
* `54501` – Southbound Delbrook Ave @ Hermosa Ave
* `54502` – Southbound Delbrook Ave @ Granada Cres
* `54503` – Southbound Delbrook Ave @ Verona Place
* `54504` – Southbound Delbrook Ave @ Silverdale Place
* `54505` – Southbound Delbrook Ave @ Kerry Place
* `54506` – Southbound Delbrook Ave @ Donegal Place
* `54508` – Southbound Westview Dr @ W 28 St
* `54509` – Southbound Westview Dr @ Hwy One
* `54510` – Southbound Westview Dr @ W 23 St
* `54511` – Southbound Westview Dr @ W 21 St
* `54512` – Southbound Westview Dr @ W 20 St
* `54514` – Southbound Bewicke Ave @ Larson Rd
* `54515` – Southbound Bewicke Ave @ W 14 St
* `54517` – Northbound Delbrook Ave @ W Queens Rd
* `54518` – Northbound Delbrook Ave @ W Windsor Rd
* `54519` – Northbound Delbrook Ave @ St. Ives Cres
* `54520` – Northbound Delbrook Ave @ Elstree Place
* `54521` – Northbound Delbrook Ave @ Evergreen Place
* `54522` – Northbound Delbrook Ave @ Croydon Place
* `54523` – Northbound Delbrook Ave @ Lucerne Place
* `54524` – Northbound Delbrook Ave @ Silverdale Place
* `54525` – Northbound Delbrook Ave @ Hermosa Ave
* `54526` – Westbound Montroyal Blvd @ Delbrook Ave
* `54527` – Westbound Montroyal Blvd @ Glencanyon Dr
* `54528` – Westbound Montroyal Blvd @ Skyline Dr
* `54529` – Westbound Montroyal Blvd @ Bonita Dr
* `54530` – Westbound Montroyal Blvd @ Ranger Ave
* `54532` – Southbound Highland Blvd @ Montroyal Blvd
* `54533` – Southbound Highland Blvd @ Belvedere Dr
* `54534` – Southbound Highland Blvd @ Handsworth Rd
* `54535` – Southbound Highland Blvd @ Derby Place
* `54536` – Southbound Highland Blvd @ Devon Rd
* `54537` – Southbound Highland Blvd @ Beaconsfield Rd
* `54538` – Southbound Highland Blvd @ Sunnycrest Dr
* `54539` – Southbound Highland Blvd @ Beaumont Dr
* `54540` – Southbound Highland Blvd @ Forest Hills Dr
* `54541` – Southbound Highland Blvd @ Kennedy Ave
* `54542` – Southbound Highland Blvd @ Ridgewood Dr
* `54543` – Westbound Ridgewood Dr @ Brookridge Dr
* `54544` – Southbound Capilano Rd @ Hwy One Overpass
* `54546` – Southbound Garden Ave @ Hope Rd
* `54548` – Eastbound Clements Ave @ Capilano Rd
* `54549` – Northbound Cliffridge Ave @ Sonora Dr South
* `54550` – Northbound Cliffridge Ave @ Sonora Dr North
* `54551` – Eastbound Ranger Ave @ Malaspina Place
* `54552` – Eastbound Ranger Ave @ Sarita Ave
* `54553` – Westbound Prospect Ave @ Ranger Ave
* `54554` – Westbound Prospect Ave @ Sarita Ave
* `54555` – Southbound Cliffridge Ave @ Prospect Ave
* `54558` – Southbound Nelson Ave @ 6400 Block
* `54560` – Southbound Marine Dr @ 6300 Block
* `54561` – Southbound Marine Dr @ Horseshoe Bay Park & Ride
* `54563` – Southbound Marine Dr @ Eagleridge Dr
* `54564` – Southbound Marine Dr @ Gleneagles Dr
* `54565` – Eastbound Marine Dr @ 5800 Block
* `54566` – Eastbound Marine Dr @ Primrose Place
* `54567` – Eastbound Marine Dr @ Eagle Harbour Rd
* `54568` – Southbound Marine Dr @ Gallagher Place
* `54569` – Southbound Marine Dr @ Parthenon Place
* `54570` – Southbound Marine Dr @ Kew Cliff Rd
* `54571` – Southbound Marine Dr @ 5200 Block
* `54572` – Southbound Marine Dr @ Crossway
* `54574` – Eastbound Marine Dr @ The Dale
* `54576` – Eastbound Marine Dr @ N Piccadilly Rd
* `54577` – Eastbound Marine Dr @ Morgan Cres
* `54579` – Eastbound Marine Dr @ Sherman St
* `54580` – Eastbound Marine Dr @ Rose Cres
* `54581` – Eastbound Marine Dr @ Sharon Dr
* `54582` – Eastbound Marine Dr @ 3700 Block
* `54583` – Eastbound Marine Dr @ 3600 Block
* `54585` – Eastbound Marine Dr @ Gables Lane
* `54586` – Eastbound Marine Dr @ Radcliffe Ave
* `54588` – Eastbound Marine Dr @ 30 St
* `54589` – Eastbound Marine Dr @ 29 St
* `54590` – Eastbound Marine Dr @ 27 St
* `54591` – Eastbound Marine Dr @ 26 St
* `54592` – Eastbound Marine Dr @ 25 St
* `54594` – Eastbound Marine Dr @ 23 St
* `54597` – Eastbound Marine Dr @ 20 St
* `54598` – Eastbound Marine Dr @ 19 St
* `54599` – Eastbound Marine Dr @ 18 St
* `54603` – Eastbound Marine Dr @ 11 St
* `54604` – Eastbound Marine Dr @ Main St
* `54609` – Westbound Marine Dr @ Main St
* `54610` – Westbound Marine Dr @ 11 St
* `54612` – Westbound Marine Dr @ 15 St
* `54613` – Westbound Marine Dr @ 17 St
* `54614` – Westbound Marine Dr @ 18 St
* `54615` – Westbound Marine Dr @ 19 St
* `54616` – Westbound Marine Dr @ 20 St
* `54623` – Westbound Marine Dr @ 26 St
* `54625` – Westbound Marine Dr @ 29 St
* `54626` – Westbound Marine Dr @ 30 St
* `54627` – Westbound Marine Dr @ 31 St
* `54628` – Westbound Marine Dr @ Travers Ave
* `54629` – Westbound Marine Dr @ Gables Lane
* `54630` – Westbound Marine Dr @ Creery Ave
* `54631` – Westbound Marine Dr @ 3600 Block
* `54632` – Westbound Marine Dr @ 3700 Block
* `54633` – Westbound Marine Dr @ Sharon Dr
* `54640` – Westbound Marine Dr @ The Dale
* `54641` – Westbound Marine Dr @ Beacon Lane
* `54643` – Northbound Marine Dr @ 5200 Block
* `54649` – Westbound Marine Dr @ 5800 Block
* `54650` – Northbound Marine Dr @ Gleneagles Dr
* `54651` – Northbound Marine Dr @ Eagleridge Dr
* `54653` – Northbound Marine Dr @ Horseshoe Bay Park & Ride
* `54654` – Northbound Marine Dr @ 6300 Block
* `54655` – Northbound Nelson Ave @ Chatham St
* `54659` – Northbound 11 St @ Inglewood Ave
* `54660` – Northbound 11 St @ Kings Ave
* `54661` – Westbound Mathers Ave @ 11 St
* `54662` – Westbound Mathers Ave @ 13 St
* `54663` – Westbound Mathers Ave @ 14 St
* `54664` – Westbound Mathers Ave @ 15 St
* `54665` – Northbound 15 St @ Nelson Ave
* `54666` – Westbound Queens Ave @ 15 St
* `54667` – Westbound Queens Ave @ 17 St
* `54668` – Westbound Queens Ave @ 18 St
* `54669` – Westbound Queens Ave @ 19 St
* `54670` – Westbound Queens Ave @ 21 St
* `54671` – Westbound Queens Ave @ 22 St
* `54672` – Westbound Queens Ave @ Orchard Way
* `54673` – Westbound Queens Ave @ Orchard Way
* `54674` – Westbound Queens Ave @ 2400 Block
* `54675` – Westbound Queens Ave @ 2500 Block
* `54676` – Westbound Queens Ave @ 26 St
* `54677` – Westbound Queens Ave @ 27 St
* `54678` – Southbound 27 St @ 28 St
* `54679` – Southbound 27 St @ Palmerston Ave
* `54680` – Southbound 27 St @ Nelson Ave
* `54681` – Southbound 27 St @ Mathers Ave
* `54682` – Southbound 27 St @ Marine Dr
* `54684` – Northbound 17 St @ Esquimalt Ave
* `54685` – Eastbound Fulton Ave @ 15 St
* `54686` – Eastbound Fulton Ave @ 14 St
* `54687` – Eastbound Fulton Ave @ 13 St
* `54688` – Eastbound Fulton Ave @ 12 St
* `54689` – Northbound 11 St @ Fulton Ave
* `54690` – Northbound 11 St @ Gordon Ave
* `54691` – Westbound Inglewood Ave @ 12 St
* `54692` – Westbound Inglewood Ave @ 13 St
* `54693` – Westbound Inglewood Ave @ 14 St
* `54694` – Westbound Inglewood Ave @ 15 St
* `54695` – Westbound Inglewood Ave @ 17 St
* `54696` – Westbound Inglewood Ave @ 20 St
* `54697` – Northbound 20 St @ Kings Ave
* `54698` – Westbound Mathers Ave @ 20 St
* `54699` – Westbound Mathers Ave @ 22 St
* `54700` – Westbound Mathers Ave @ 23 St
* `54701` – Westbound Mathers Ave @ 24 St
* `54702` – Southbound 25 St @ Mathers Ave
* `54703` – Southbound 25 St @ Kings Ave
* `54704` – Eastbound Caulfeild Dr @ Headland Dr
* `54705` – Eastbound Caulfeild Dr @ Estevan Place
* `54706` – Westbound Caulfeild Dr @ Willow Creek Rd
* `54707` – Westbound Caulfeild Dr @ Piccadilly North
* `54708` – Westbound Caulfeild Dr @ Cherbourg Dr
* `54709` – Eastbound Cherbourg Dr @ Keith Rd
* `54710` – Eastbound Keith Rd @ 4400 Block
* `54711` – Southbound Keith Rd @ Rockridge Rd
* `54712` – Southbound Keith Rd @ Stone Cres
* `54715` – Westbound Thompson Cres @ Mathers Ave
* `54722` – Westbound Westridge Ave @ Southridge Ave West
* `54723` – Westbound Westridge Ave @ 3900 Block
* `54724` – Northbound Almondel Rd @ Ripple Rd
* `54725` – Westbound Woodcrest Rd @ Almondel Rd
* `54726` – Westbound Woodcrest Rd @ Woodgreen Dr
* `54727` – Northbound Woodgreen Dr @ Woodgreen Place
* `54728` – Westbound Woodburn Rd @ Woodgreen Dr
* `54729` – Southbound Northwood Dr @ Woodburn Rd
* `54730` – Northbound Taylor Way @ Keith Rd
* `54731` – Northbound Taylor Way @ Inglewood Ave
* `54733` – Westbound Southborough Dr @ Taylor Way
* `54734` – Northbound Southborough Dr @ Parkside Rd
* `54735` – Northbound Eyremount Dr @ Southborough Dr
* `54736` – Westbound Cross Creek Rd @ Highland Dr
* `54737` – Westbound Cross Creek Rd @ Chartwell Dr
* `54738` – Northbound Chartwell Dr @ 1300 Block
* `54739` – Northbound Chartwell Dr @ Whitby Rd
* `54740` – Northbound Chartwell Dr @ Burnside Rd
* `54741` – Northbound Chartwell Dr @ Chippendale Rd
* `54742` – Northbound Chartwell Dr @ Sandhurst Place
* `54743` – Northbound Crestwell Rd @ Chartwell Dr
* `54744` – Eastbound Eyremount Dr @ Crestline Rd
* `54745` – Eastbound Eyremount Dr @ Highland Dr
* `54746` – Eastbound Eyremount Dr @ Hillside Rd
* `54747` – Southbound Eyremount Dr @ Elveden Row
* `54748` – Southbound Eyremount Dr @ Groveland Rd
* `54749` – Southbound Eyremount Dr @ Farmleigh Rd
* `54750` – Southbound Eyremount Dr @ King Georges Way
* `54751` – Southbound Eyremount Dr @ Fairmile Rd
* `54752` – Eastbound Fairmile Rd @ 600 Block
* `54753` – Eastbound Fairmile Rd @ Southborough Dr
* `54754` – Northbound Southborough Dr @ Knockmaroon Rd
* `54755` – Northbound Southborough Dr @ King Georges Way
* `54756` – Northbound Robin Hood Rd @ King Georges Way
* `54757` – Northbound St. Andrews Rd @ Barnham Rd
* `54758` – Northbound St. Andrews Rd @ Hawstead Place
* `54759` – Northbound St. Andrews Rd @ Greenwood Rd
* `54760` – Northbound St. Andrews Rd @ St. Andrews Place
* `54761` – Northbound St. Andrews Rd @ Ballantree Rd
* `54762` – Northbound St. Andrews Rd @ 500 Block
* `54763` – Northbound Bonnymuir Dr @ Unit Block
* `54764` – Southbound Bonnymuir Dr @ St. Giles Rd
* `54765` – Southbound Bonnymuir Dr @ Glenmore Dr
* `54766` – Southbound Stevens Dr @ Deep Dene Rd
* `54767` – Southbound Stevens Dr @ 100 Block
* `54768` – Southbound Stevens Dr @ Normanby Cres
* `54769` – Southbound Stevens Dr @ Onslow Place
* `54770` – Southbound Stevens Dr @ 300 Block
* `54771` – Southbound Stevens Dr @ Bury Lane
* `54772` – Southbound Stevens Dr @ Bury Lane
* `54773` – Southbound Stevens Dr @ Hidhurst Place
* `54774` – Southbound Stevens Dr @ Hadden Dr
* `54776` – Southbound Taylor Way @ Anderson Cres
* `54777` – Southbound Taylor Way @ Keith Rd
* `54778` – Westbound E 15 St @ Grand Blvd East
* `54784` – Southbound Tidewater Way @ Kelvin Grove Way
* `54785` – Southbound Hwy 99 @ Lawrence Way
* `54786` – Southbound Hwy 99 Offramp @ Ansell Place
* `54787` – Southbound Horseshoe Bay Dr @ Pasco Rd
* `54792` – Westbound Mt Seymour Parkway @ Lytton St
* `54797` – Westbound 72 Ave @ 116 St
* `54798` – Westbound 72 Ave @ 115 St
* `54799` – Westbound 72 Ave @ 113 St
* `54860` – Eastbound 72 Ave @ 11200 Block
* `54861` – Eastbound 72 Ave @ 113 St
* `54862` – Eastbound 72 Ave @ 114A St
* `54864` – Eastbound 72 Ave @ 118 St
* `54872` – Southbound Scott Rd @ Sunwood Dr
* `54881` – Northbound Scott Rd @ Hwy 10
* `54882` – Northbound 120 St @ Boundary Park Gate
* `54890` – Northbound 112 St @ 72A Ave
* `54891` – Northbound 112 St @ 73A Ave
* `54892` – Northbound 112 St @ Glenbrook Place
* `54893` – Northbound 112 St @ Bridlington Dr
* `54894` – Northbound 112 St @ 78A Ave
* `54895` – Northbound 112 St @ 80 Ave
* `54896` – Northbound 112 St @ 82 Ave
* `54897` – Northbound 112 St @ 84 Ave
* `54898` – Northbound 112 St @ 86 Ave
* `54899` – Northbound 112 St @ 88 Ave
* `54900` – Northbound 112 St @ 90 Ave
* `54901` – Northbound 112 St @ 91 Ave
* `54902` – Eastbound 92A Ave @ 112 St
* `54903` – Northbound 114 St @ 92A Ave
* `54904` – Northbound 114 St @ 94 Ave
* `54905` – Northbound 114 St @ 95 Ave
* `54906` – Eastbound 96 Ave @ 114 St
* `54907` – Eastbound 96 Ave @ 116 St
* `54908` – Eastbound 96 Ave @ 118 St
* `54911` – Northbound Scott Rd @ 98 Ave
* `54912` – Northbound Scott Rd @ 99 Ave
* `54913` – Northbound Scott Rd @ 103A Ave
* `54918` – Southbound Scott Rd @ 103A Ave
* `54919` – Southbound Scott Rd @ 99 Ave
* `54921` – Westbound 96 Ave @ 117B St
* `54922` – Westbound 96 Ave @ 116 St
* `54923` – Southbound 114 St @ 95A Ave
* `54924` – Southbound 114 St @ Dawson Cres
* `54925` – Southbound 114 St @ 92A Ave
* `54926` – Southbound 112 St @ 92A Ave
* `54927` – Southbound 112 St @ 91 Ave
* `54928` – Southbound 112 St @ 90 Ave
* `54929` – Southbound 112 St @ 8800 Block
* `54930` – Southbound 112 St @ Nordel Way
* `54931` – Southbound 112 St @ 86 Ave
* `54932` – Southbound 112 St @ 84 Ave
* `54933` – Southbound 112 St @ 82 Ave
* `54934` – Southbound 112 St @ 80 Ave
* `54935` – Southbound 112 St @ 78A Ave
* `54936` – Southbound 112 St @ Bridlington Dr
* `54937` – Southbound 112 St @ 74A Ave
* `54942` – Northbound 122 St @ 7300 Block
* `54948` – Westbound River Rd @ 92A Ave
* `54963` – Southbound Brooke Rd @ Dunlop Rd
* `54964` – Southbound Brooke Rd @ 85A Ave
* `54966` – Eastbound 84 Ave @ 110A St
* `54968` – Eastbound 84 Ave @ 114 St
* `54970` – Eastbound 84 Ave @ 117B St
* `54971` – Eastbound 84 Ave @ 118A St
* `54972` – Northbound Scott Rd @ 86 Ave
* `54974` – Northbound Scott Rd @ 90 Ave
* `54976` – Northbound Scott Rd @ Holt Rd
* `54978` – Eastbound 96 Ave @ 122 St
* `54979` – Eastbound 96 Ave @ 124 St
* `54980` – Eastbound 96 Ave @ 125 St
* `54981` – Eastbound 96 Ave @ 126 St
* `54982` – Eastbound 96 Ave @ 128 St
* `54986` – Eastbound 96 Ave @ 134 St
* `54998` – Westbound 96 Ave @ King George Blvd
* `54999` – Westbound 96 Ave @ 134 St
* `55001` – Westbound 96 Ave @ 130 St
* `55002` – Westbound 96 Ave @ Prince Charles Blvd
* `55003` – Westbound 96 Ave @ 128 St
* `55004` – Westbound 96 Ave @ 126 St
* `55006` – Westbound 96 Ave @ 122 St
* `55007` – Southbound 124 St @ 96 Ave
* `55008` – Southbound 124 St @ 95 Ave
* `55009` – Southbound 124 St @ Iona Place
* `55010` – Southbound 124 St @ 93 Ave
* `55011` – Westbound 92 Ave @ 124 St
* `55012` – Westbound 92 Ave @ 122 St
* `55013` – Westbound 92 Ave @ Scott Rd
* `55016` – Southbound Scott Rd @ 86 Ave
* `55017` – Westbound 84 Ave @ Scott Rd
* `55018` – Westbound 84 Ave @ 118A St
* `55019` – Westbound 84 Ave @ 117B St
* `55021` – Westbound 84 Ave @ 114 St
* `55023` – Westbound 84 Ave @ 110A St
* `55025` – Northbound Brooke Rd @ 85A Ave
* `55042` – Southbound 122 St @ 7300 Block
* `55052` – Northbound 116 St @ 84 Ave
* `55053` – Northbound 116 St @ 86 Ave
* `55054` – Northbound 116 St @ 88 Ave
* `55055` – Northbound 116 St @ 90 Ave
* `55056` – Northbound 116 St @ 9000 Block
* `55057` – Northbound 116 St @ 92 Ave
* `55058` – Northbound 116 St @ 94 Ave
* `55060` – Northbound 123A St @ 98 Ave
* `55061` – Northbound 123A St @ 99 Ave
* `55079` – Southbound 123A St @ 99 Ave
* `55080` – Southbound 123A St @ 98 Ave
* `55081` – Southbound 116 St @ 96 Ave
* `55082` – Southbound 116 St @ 94 Ave
* `55083` – Southbound 116 St @ 92 Ave
* `55084` – Southbound 116 St @ 9000 Block
* `55085` – Southbound 116 St @ 90 Ave
* `55086` – Southbound 116 St @ 87 Ave
* `55087` – Southbound 116 St @ 86 Ave
* `55088` – Southbound 116 St @ 84 Ave
* `55090` – Southbound 116 St @ 80 Ave
* `55091` – Southbound 116 St @ 78A Ave
* `55092` – Southbound 116 St @ 77A Ave
* `55093` – Southbound 116 St @ 76A Ave
* `55094` – Southbound 116 St @ 75A Ave
* `55130` – Southbound 152 St @ 105 Ave
* `55134` – Northbound 150 St @ 107A Ave
* `55145` – Westbound Grosvenor Rd @ 142 St
* `55194` – Northbound Scott Rd @ 75A Ave
* `55195` – Northbound Scott Rd @ 76A Ave
* `55197` – Northbound Scott Rd @ 82 Ave
* `55198` – Northbound Scott Rd @ 84 Ave
* `55204` – Southbound Scott Rd @ 84 Ave
* `55205` – Southbound Scott Rd @ 82 Ave
* `55207` – Southbound Scott Rd @ 7800 Block
* `55208` – Southbound Scott Rd @ 76A Ave
* `55220` – Eastbound 104 Ave @ 148 St
* `55230` – Southbound 152 St @ 94 Ave
* `55231` – Southbound 152 St @ 93 Ave
* `55240` – Eastbound Fraser Hwy @ 158 St
* `55241` – Eastbound Fraser Hwy @ Venture Way
* `55245` – Eastbound Fraser Hwy @ 164 St
* `55246` – Eastbound Fraser Hwy @ 166 St
* `55254` – Southbound 176 St @ 60 Ave
* `55261` – Eastbound Hwy 10 @ 182 St
* `55268` – Northbound 177B St @ 57 Ave
* `55269` – Northbound 177B St @ 58 Ave
* `55270` – Northbound 177B St @ 59A Ave
* `55272` – Eastbound 60 Ave @ 182 St
* `55274` – Eastbound 60 Ave @ 185B St
* `55276` – Eastbound 60 Ave @ 189A St
* `55292` – Eastbound 53 Ave @ 203 St
* `55293` – Eastbound 53 Ave @ 204 St
* `55294` – Eastbound 51B Ave @ 206 St
* `55295` – Eastbound 51B Ave @ 207 St
* `55296` – Northbound 208 St @ 51B Ave
* `55297` – Westbound 56 Ave @ Langley Bypass
* `55298` – Westbound 56 Ave @ Logan Ave
* `55300` – Eastbound 56 Ave @ 206 St
* `55301` – Eastbound 56 Ave @ 208 St
* `55303` – Westbound 51B Ave @ 207 St
* `55304` – Westbound 51B Ave @ 206 St
* `55305` – Westbound 53 Ave @ 53A Ave
* `55306` – Westbound 53 Ave @ 204 St
* `55307` – Northbound 203 St @ 53 Ave
* `55318` – Westbound Hwy 10 @ 18600 Block
* `55320` – Westbound Hwy 10 @ 182 St
* `55327` – Northbound 197 St @ Willowbrook Dr
* `55333` – Westbound 60 Ave @ 190 St
* `55335` – Westbound 60 Ave @ 186 St
* `55337` – Westbound 60 Ave @ 182 St
* `55339` – Southbound 177B St @ 59A Ave
* `55340` – Southbound 177B St @ 58A Ave
* `55342` – Northbound 177B St @ Hwy 10
* `55350` – Westbound Fraser Hwy @ 176 St
* `55351` – Westbound Fraser Hwy @ 168 St
* `55352` – Westbound Fraser Hwy @ 166 St
* `55353` – Westbound Fraser Hwy @ 164 St
* `55356` – Westbound Fraser Hwy @ 158 St
* `55358` – Northbound 152 St @ 91 Ave
* `55360` – Northbound 152 St @ 94 Ave
* `55364` – Northbound 152 St @ 101 Ave
* `55366` – Westbound 104 Ave @ 150 St
* `55367` – Westbound 104 Ave @ 148 St
* `55376` – Northbound King George Blvd @ 164 St
* `55378` – Westbound 16 Ave @ King George Blvd
* `55381` – Westbound 16 Ave @ 156 St
* `55387` – Northbound 152 St @ 22 Ave
* `55389` – Northbound 152 St @ 26 Ave
* `55391` – Northbound King George Blvd @ 152 St
* `55394` – Northbound King George Blvd @ 34 Ave
* `55396` – Northbound King George Blvd @ 40 Ave
* `55397` – Northbound King George Blvd @ 44 Ave
* `55415` – Northbound King George Blvd @ 78 Ave
* `55417` – Northbound King George Blvd @ 81 Ave
* `55418` – Northbound King George Blvd @ 8200 Block
* `55420` – Northbound King George Blvd @ 8600 Block
* `55422` – Northbound King George Blvd @ 91 Ave
* `55424` – Northbound King George Blvd @ 9300 Block
* `55425` – Northbound King George Blvd @ 94A Ave
* `55427` – Northbound King George Blvd @ 105A Ave
* `55429` – Northbound King George Blvd @ 108 Ave
* `55431` – Northbound King George Blvd @ Bolivar Rd
* `55433` – Westbound King George Blvd @ 128 St
* `55444` – Southbound King George Blvd @ 8900 Block
* `55445` – Southbound King George Blvd @ 88 Ave
* `55446` – Southbound King George Blvd @ 8600 Block
* `55448` – Southbound King George Blvd @ 8200 Block
* `55449` – Southbound King George Blvd @ 8100 Block
* `55451` – Southbound King George Blvd @ 77 Ave
* `55462` – Southbound King George Blvd @ 6500 Block
* `55470` – Southbound King George Blvd @ 40 Ave
* `55472` – Southbound King George Blvd @ 34 Ave
* `55473` – Eastbound 32 Ave Diversion @ King George Blvd
* `55475` – Southbound King George Blvd @ 2900 Block
* `55478` – Southbound 152 St @ 28 Ave
* `55480` – Southbound 152 St @ 26 Ave
* `55482` – Southbound 152 St @ 22 Ave
* `55486` – Southbound Martin Dr @ W Southmere Cres
* `55491` – Eastbound North Bluff Rd @ Finlay St
* `55494` – Southbound King George Blvd @ 16 Ave
* `55495` – Southbound King George Blvd @ 14 Ave
* `55506` – Southbound 128 St @ 66 Ave
* `55508` – Southbound 128 St @ 62 Ave
* `55514` – Southbound Scott Rd @ McKee Dr
* `55515` – Eastbound New McLellan Rd @ Hillside Rd
* `55516` – Eastbound New McLellan Rd @ 123 St
* `55518` – Eastbound 56 Ave @ 125A St
* `55520` – Eastbound 56 Ave @ Coulthard Rd
* `55521` – Eastbound Hwy 10 @ 13400 Block
* `55522` – Southbound 136 St @ 5700 Block
* `55523` – Westbound 56 Ave @ 13300 Block
* `55525` – Northbound 132 St @ Hwy 10
* `55527` – Northbound 132 St @ 62 Ave
* `55528` – Eastbound 64 Ave @ 128 St
* `55529` – Eastbound 64 Ave @ 130 St
* `55531` – Northbound 132 St @ 66A Ave
* `55534` – Northbound 132 St @ 69A Ave
* `55535` – Northbound 132 St @ 70B Ave
* `55537` – Eastbound 72 Ave @ 134 St
* `55542` – Southbound 132 St @ 70B Ave
* `55543` – Southbound 132 St @ 69A Ave
* `55545` – Southbound 132 St @ 65B Ave
* `55547` – Southbound 132 St @ 62 Ave
* `55549` – Eastbound Hwy 10 @ 132 St
* `55552` – Westbound 56 Ave @ Coulthard Rd
* `55554` – Westbound 56 Ave @ 125A St
* `55556` – Westbound New McLellan Rd @ 123 St
* `55557` – Westbound New McLellan Rd @ Hillside Rd
* `55559` – Northbound Scott Rd @ 56 Ave
* `55565` – Northbound 128 St @ 62 Ave
* `55566` – Westbound 64 Ave @ 132 St
* `55567` – Westbound 64 Ave @ 130 St
* `55569` – Northbound 128 St @ 66 Ave
* `55571` – Northbound 128 St @ 69A Ave
* `55572` – Northbound 128 St @ 7100 Block
* `55573` – Northbound 128 St @ 72 Ave
* `55581` – Northbound 138 St @ 76 Ave
* `55583` – Westbound 76 Ave @ King George Blvd
* `55584` – Westbound 76 Ave @ 134A St
* `55585` – Westbound 76 Ave @ 13300 Block
* `55587` – Westbound 76 Ave @ 13000 Block
* `55588` – Westbound 76 Ave @ 129A St
* `55590` – Northbound 128 St @ 78 Ave
* `55592` – Northbound 128 St @ 8200 Block
* `55597` – Northbound 128 St @ Prince Charles Blvd
* `55599` – Northbound 128 St @ 93 Ave
* `55600` – Northbound 128 St @ 9400 Block
* `55624` – Southbound 128 St @ 9400 Block
* `55625` – Southbound 128 St @ 93 Ave
* `55627` – Southbound 128 St @ 90 Ave
* `55632` – Southbound 128 St @ 8200 Block
* `55634` – Southbound 128 St @ 78 Ave
* `55636` – Eastbound 76 Ave @ 130 St
* `55637` – Eastbound 76 Ave @ 13000 Block
* `55639` – Eastbound 76 Ave @ 13300 Block
* `55641` – Eastbound 76 Ave @ 134A St
* `55643` – Southbound 138 St @ 75A Ave
* `55644` – Southbound 138 St @ 74 Ave
* `55646` – Southbound King George Blvd @ 72 Ave
* `55647` – Southbound King George Blvd @ 6900 Block
* `55650` – Westbound 68 Ave @ 134 St
* `55652` – Northbound 132 St @ Comber Way
* `55654` – Northbound 132 St @ 78A Ave
* `55656` – Northbound 132 St @ 81A Ave
* `55657` – Northbound 132 St @ 82A Ave
* `55659` – Northbound 132 St @ Shakespeare Place
* `55661` – Northbound 132 St @ 90 Ave
* `55663` – Northbound 132 St @ Huntley Ave
* `55671` – Southbound 132 St @ Huntley Ave
* `55673` – Southbound 132 St @ Queen Mary Blvd
* `55675` – Southbound 132 St @ 86 Ave
* `55677` – Southbound 132 St @ 82A Ave
* `55678` – Southbound 132 St @ 81 Ave
* `55680` – Southbound 132 St @ 78A Ave
* `55683` – Southbound 132 St @ 73A Ave
* `55685` – Eastbound 68 Ave @ 134 St
* `55686` – Northbound King George Blvd @ 68 Ave
* `55687` – Northbound King George Blvd @ 6900 Block
* `55691` – Northbound 140 St @ 74 Ave
* `55693` – Northbound 140 St @ 77A Ave
* `55694` – Northbound 140 St @ 78 Ave
* `55696` – Northbound 140 St @ Bear Creek Dr
* `55697` – Northbound 140 St @ 82 Ave
* `55699` – Northbound 140 St @ 85 Ave
* `55700` – Northbound 140 St @ 86A Ave
* `55702` – Northbound 140 St @ 90 Ave
* `55704` – Northbound 140 St @ 93A Ave
* `55705` – Northbound 140 St @ 94A Ave
* `55723` – Southbound 140 St @ 94A Ave
* `55725` – Southbound 140 St @ 90 Ave
* `55727` – Southbound 140 St @ 86A Ave
* `55728` – Southbound 140 St @ 85 Ave
* `55730` – Southbound 140 St @ 8300 Block
* `55731` – Southbound 140 St @ Bear Creek Dr
* `55734` – Southbound 140 St @ 78A Ave
* `55735` – Southbound 140 St @ 77 Ave
* `55736` – Southbound 140 St @ 74 Ave
* `55751` – Northbound 156 St @ 90 Ave
* `55753` – Northbound 156 St @ 94A Ave
* `55768` – Southbound 156 St @ 94 Ave
* `55770` – Southbound 156 St @ 89B Ave
* `55799` – Eastbound 112 Ave @ 136 St
* `55814` – Eastbound 84 Ave @ Scott Rd
* `55815` – Eastbound 84 Ave @ 121A St
* `55816` – Eastbound 84 Ave @ 122A St
* `55817` – Eastbound 84 Ave @ 124 St
* `55818` – Southbound 124 St @ 82 Ave
* `55819` – Southbound 124 St @ 80 Ave
* `55820` – Southbound 124 St @ 78 Ave
* `55821` – Southbound 124 St @ 76 Ave
* `55822` – Westbound 75 Ave @ 123A St
* `55823` – Westbound 75 Ave @ 122A St
* `55824` – Southbound 121A St @ 75 Ave
* `55826` – Eastbound 75 Ave @ 122A St
* `55827` – Northbound 124 St @ 75 Ave
* `55828` – Northbound 124 St @ 76 Ave
* `55829` – Northbound 124 St @ 78 Ave
* `55830` – Northbound 124 St @ 80 Ave
* `55831` – Northbound 124 St @ 82 Ave
* `55832` – Westbound 84 Ave @ 124 St
* `55833` – Westbound 84 Ave @ 123 St
* `55834` – Westbound 84 Ave @ 122 St
* `55860` – Southbound 160 St @ 93A Ave
* `55861` – Southbound 160 St @ 91 Ave
* `55866` – Northbound 156 St @ 80 Ave
* `55868` – Westbound 84 Ave @ 154 St
* `55870` – Westbound 84 Ave @ 150 St
* `55872` – Westbound 84 Ave @ 146 St
* `55874` – Southbound 144 St @ 82 Ave
* `55875` – Southbound 144 St @ 77A Ave
* `55878` – Southbound 144 St @ 7300 Block
* `55883` – Northbound 144 St @ 74A Ave
* `55885` – Northbound 144 St @ 77A Ave
* `55886` – Northbound 144 St @ 79 Ave
* `55888` – Eastbound 84 Ave @ 146 St
* `55889` – Eastbound 84 Ave @ 148 St
* `55890` – Eastbound 84 Ave @ 150 St
* `55891` – Westbound 84 Ave @ 152 St
* `55892` – Eastbound 84 Ave @ 154 St
* `55896` – Northbound 148 St @ Spenser Dr
* `55897` – Northbound 148 St @ 86B Ave
* `55900` – Southbound 148 St @ 86B Ave
* `55901` – Southbound 148 St @ Spenser Dr
* `55902` – Eastbound 80 Ave @ 156 St
* `55906` – Northbound 160 St @ 89A Ave
* `55907` – Northbound 160 St @ 91 Ave
* `55909` – Northbound 160 St @ 93A Ave
* `55937` – Eastbound Kittson Parkway @ McKenzie Dr
* `55938` – Eastbound Kittson Parkway @ Summit Cres
* `55939` – Eastbound 64 Ave @ Wade Rd
* `55940` – Eastbound 64 Ave @ Sunwood Dr
* `55943` – Eastbound 72 Ave @ 130 St
* `55947` – Northbound 144 St @ 82A Ave
* `55949` – Northbound 148 St @ 90 Ave
* `55951` – Northbound 148 St @ 94 Ave
* `55959` – Southbound 144 St @ 70A Ave
* `55961` – Southbound 144 St @ 66 Ave
* `55965` – Westbound 57 Ave @ 142 St
* `55966` – Northbound 142 St @ 60 Ave
* `55968` – Eastbound 60 Ave @ 146 St
* `55970` – Eastbound 60 Ave @ 150 St
* `55971` – Northbound 152 St @ 60 Ave
* `55972` – Northbound 152 St @ 6200 Block
* `55974` – Southbound 168 St @ 63 Ave
* `55976` – Eastbound 60 Ave @ 170 St
* `55978` – Eastbound 60 Ave @ 174 St
* `55980` – Southbound 176A St @ 60 Ave
* `55981` – Southbound 176A St @ 58A Ave
* `55982` – Eastbound 58 Ave @ 176A St
* `55986` – Westbound 60 Ave @ 174 St
* `55987` – Westbound 60 Ave @ 173B St
* `55988` – Westbound 60 Ave @ 172 St
* `55989` – Westbound 60 Ave @ 170 St
* `55992` – Northbound 168 St @ 63 Ave
* `55994` – Southbound 152 St @ 62A Ave
* `55996` – Westbound 60 Ave @ 150 St
* `55998` – Westbound 60 Ave @ 146 St
* `56000` – Southbound 142 St @ 60 Ave
* `56001` – Eastbound 57 Ave @ 142 St
* `56009` – Southbound 148 St @ 94 Ave
* `56011` – Southbound 148 St @ 90 Ave
* `56016` – Northbound 144 St @ 66 Ave
* `56018` – Northbound 144 St @ 70A Ave
* `56021` – Westbound 72 Ave @ 130 St
* `56024` – Westbound 64 Ave @ Wade Rd
* `56025` – Westbound Kittson Parkway @ Summit Cres
* `56026` – Westbound Kittson Parkway @ McKenzie Dr
* `56027` – Westbound Kittson Parkway @ Lyon Rd
* `56033` – Northbound King George Blvd @ 20 Ave
* `56038` – Northbound 152 St @ 34 Ave
* `56041` – Northbound 152 St @ 4400 Block
* `56042` – Northbound 152 St @ 48 Ave
* `56046` – Northbound 152 St @ 58A Ave
* `56049` – Northbound 152 St @ 72 Ave
* `56050` – Northbound 152 St @ 76 Ave
* `56051` – Northbound 152 St @ 81 Ave
* `56053` – Northbound 152 St @ 84 Ave
* `56054` – Northbound 152 St @ 86 Ave
* `56057` – Southbound 152 St @ 86 Ave
* `56060` – Southbound 152 St @ 7900 Block
* `56061` – Southbound 152 St @ 76 Ave
* `56062` – Southbound 152 St @ 72 Ave
* `56064` – Southbound 152 St @ 66A Ave
* `56066` – Southbound 152 St @ 60 Ave
* `56070` – Southbound 152 St @ 48 Ave
* `56071` – Southbound 152 St @ 4400 Block
* `56072` – Southbound 152 St @ 40 Ave
* `56074` – Southbound 152 St @ 34 Ave
* `56079` – Southbound King George Blvd @ 20 Ave
* `56083` – Southbound McBride Ave @ Alexandra St
* `56084` – Eastbound Beecher St @ Taylor Lane
* `56085` – Eastbound Beecher St @ Sullivan St
* `56086` – Eastbound Bayview St @ Beecher St
* `56087` – Eastbound Crescent Rd @ Tulloch Rd
* `56088` – Southbound 128 St @ Crescent Rd
* `56090` – Southbound 128 St @ 25 Ave
* `56091` – Southbound 128 St @ 24 Ave
* `56092` – Southbound 128 St @ 22 Ave
* `56093` – Southbound 128 St @ 20 Ave
* `56094` – Southbound 128 St @ 18 Ave
* `56098` – Eastbound 16 Ave @ 13300 Block
* `56099` – Eastbound North Bluff Rd @ Bergstrom Rd
* `56100` – Eastbound North Bluff Rd @ 13700 Block
* `56101` – Eastbound North Bluff Rd @ Lancaster St
* `56103` – Eastbound North Bluff Rd @ Bishop Rd
* `56104` – Eastbound North Bluff Rd @ Phoenix St
* `56105` – Eastbound North Bluff Rd @ Kerfoot Rd
* `56107` – Eastbound North Bluff Rd @ High Street
* `56108` – Eastbound North Bluff Rd @ Anderson St
* `56109` – Southbound Oxford St @ North Bluff Rd
* `56110` – Southbound Oxford St @ Russell Ave
* `56111` – Eastbound Thrift Ave @ Oxford St
* `56112` – Eastbound Thrift Ave @ Vidal St
* `56113` – Eastbound Thrift Ave @ Foster St
* `56114` – Northbound Johnston Rd @ Thrift Ave
* `56118` – Eastbound Crescent Rd @ 130 St
* `56119` – Eastbound Crescent Rd @ 132 St
* `56120` – Eastbound Crescent Rd @ 32 Ave
* `56121` – Eastbound Crescent Rd @ 136 St
* `56122` – Eastbound Crescent Rd @ 13700 Block
* `56123` – Eastbound Crescent Rd @ 140 St
* `56124` – Eastbound Crescent Rd @ 142A St
* `56127` – Westbound Crescent Rd @ King George Blvd
* `56128` – Westbound Crescent Rd @ Nico Wynd Dr
* `56129` – Westbound Crescent Rd @ 140 St
* `56130` – Westbound Crescent Rd @ 13700 Block
* `56131` – Westbound Crescent Rd @ 136 St
* `56132` – Westbound Crescent Rd @ 32 Ave
* `56133` – Westbound Crescent Rd @ 132 St
* `56134` – Westbound Crescent Rd @ 130 St
* `56137` – Westbound 16 Ave @ 148 St
* `56138` – Westbound Thrift Ave @ Johnston Rd
* `56139` – Westbound Thrift Ave @ Foster St
* `56140` – Westbound Thrift Ave @ Blackwood St
* `56142` – Northbound Oxford St @ Russell Ave
* `56143` – Westbound 16 Ave @ 146 St
* `56144` – Westbound 16 Ave @ 145 St
* `56146` – Westbound 16 Ave @ Kerfoot Rd
* `56147` – Westbound 16 Ave @ 142 St
* `56148` – Westbound 16 Ave @ Bishop Rd
* `56150` – Westbound 16 Ave @ Lancaster St
* `56151` – Westbound 16 Ave @ 13700 Block
* `56153` – Westbound 16 Ave @ 13300 Block
* `56156` – Northbound 128 St @ 18 Ave
* `56157` – Northbound 128 St @ 20 Ave
* `56158` – Northbound 128 St @ 22 Ave
* `56159` – Northbound 128 St @ 24 Ave
* `56160` – Northbound 128 St @ 25 Ave
* `56162` – Eastbound Crescent Rd @ 128 St
* `56163` – Westbound Crescent Rd @ 128 St
* `56165` – Westbound Bayview St @ Beecher St
* `56170` – Westbound 20 Ave @ 148 St
* `56176` – Eastbound 20 Ave @ Rotary Way
* `56180` – Eastbound Russell Ave @ George St
* `56181` – Eastbound Russell Ave @ Merklin St
* `56182` – Eastbound Russell Ave @ Hospital St
* `56183` – Eastbound Russell Ave @ Finlay St
* `56184` – Eastbound Russell Ave @ Lee St
* `56185` – Eastbound Russell Ave @ Kent St
* `56186` – Eastbound Russell Ave @ Habgood St
* `56187` – Southbound Stayte Rd @ Thrift Ave
* `56188` – Southbound Stayte Rd @ Roper Ave
* `56189` – Southbound Stayte Rd @ Buena Vista Ave
* `56190` – Southbound Stayte Rd @ Cliff Ave
* `56191` – Southbound Stayte Rd @ Pacific Ave
* `56192` – Southbound Stayte Rd @ Columbia Ave
* `56193` – Westbound Marine Dr @ Stevens St
* `56194` – Westbound Marine Dr @ Keil St
* `56195` – Westbound Marine Dr @ Lee St
* `56196` – Westbound Marine Dr @ Maple St
* `56197` – Westbound Marine Dr @ Balsam St
* `56198` – Westbound Marine Dr @ Dolphin St
* `56202` – Westbound Marine Dr @ Oxford St
* `56203` – Westbound Marine Dr @ Anderson St
* `56204` – Westbound Marine Dr @ Bay St
* `56205` – Westbound Marine Dr @ Magdalen Cres
* `56206` – Westbound Marine Dr @ Bishop Rd
* `56207` – Westbound Marine Dr @ Nichol Rd
* `56208` – Westbound Marine Dr @ 13800 Block
* `56209` – Westbound Marine Dr @ Bergstrom Rd
* `56210` – Westbound Marine Dr @ 13400 Block
* `56211` – Westbound Marine Dr @ 133A St
* `56212` – Westbound Marine Dr @ 132B St
* `56213` – Westbound Marine Dr @ 131 St
* `56217` – Northbound 130 St @ 14 Ave
* `56218` – Northbound 130 St @ 15 Ave
* `56219` – Westbound 16 Ave @ 130 St
* `56221` – Southbound 128 St @ 14B Ave
* `56222` – Eastbound Marine Dr @ 128A St
* `56223` – Eastbound Marine Dr @ 129A St
* `56224` – Eastbound Marine Dr @ 131 St
* `56225` – Eastbound Marine Dr @ 132B St
* `56226` – Eastbound Marine Dr @ 133A St
* `56227` – Eastbound Marine Dr @ 13400 Block
* `56228` – Eastbound Marine Dr @ 136 St
* `56229` – Eastbound Marine Dr @ 13800 Block
* `56230` – Eastbound Marine Dr @ Nichol Rd
* `56231` – Eastbound Marine Dr @ Bishop Rd
* `56232` – Eastbound Marine Dr @ Magdalen Cres
* `56233` – Eastbound Marine Dr @ Magdalen Cres
* `56234` – Eastbound Marine Dr @ Duprez St
* `56235` – Eastbound Marine Dr @ Anderson St
* `56236` – Eastbound Marine Dr @ Oxford St
* `56237` – Eastbound Marine Dr @ Elm St
* `56240` – Eastbound Marine Dr @ Fir St
* `56241` – Eastbound Marine Dr @ Dolphin St
* `56242` – Eastbound Marine Dr @ Balsam St
* `56243` – Eastbound Marine Dr @ Finlay St
* `56244` – Eastbound Marine Dr @ Lee St
* `56245` – Eastbound Marine Dr @ Kent St
* `56246` – Eastbound Marine Dr @ Stevens St
* `56247` – Northbound 160 St @ 9th Ave
* `56248` – Northbound 160 St @ 10 Ave
* `56249` – Northbound 160 St @ Cliff Ave
* `56252` – Northbound 160 St @ 14 Ave
* `56254` – Westbound Russell Ave @ Habgood St
* `56255` – Westbound Russell Ave @ Kent St
* `56256` – Westbound Russell Ave @ Lee St
* `56257` – Westbound Russell Ave @ Finlay St
* `56258` – Westbound Russell Ave @ Hospital St
* `56260` – Westbound Russell Ave @ Fir St
* `56261` – Southbound Stayte Rd @ North Bluff Rd
* `56267` – Westbound Buena Vista Ave @ Stayte Rd
* `56268` – Westbound Buena Vista Ave @ Habgood St
* `56269` – Westbound Buena Vista Ave @ Kent St
* `56270` – Westbound Buena Vista Ave @ Lee St
* `56272` – Westbound Buena Vista Ave @ Dolphin St
* `56275` – Westbound Columbia Ave @ Maple St
* `56276` – Westbound Columbia Ave @ 15500 Block
* `56277` – Westbound Columbia Ave @ 15300 Block
* `56278` – Northbound Centre St @ Royal Ave
* `56279` – Northbound Johnston Rd @ Pacific Ave
* `56281` – Westbound 16 Ave @ Martin Dr
* `56282` – Westbound 16 Ave @ Everall St
* `56283` – Northbound 148 St @ 16 Ave
* `56291` – Southbound 148 St @ 20 Ave
* `56292` – Southbound 148 St @ 18A Ave
* `56294` – Eastbound North Bluff Rd @ Oxford St
* `56295` – Eastbound North Bluff Rd @ Everall St
* `56296` – Eastbound North Bluff Rd @ Blackwood St
* `56297` – Southbound Johnston Rd @ Thrift Ave
* `56299` – Southbound Centre St @ Royal Ave
* `56300` – Eastbound Columbia Ave @ Centre St
* `56301` – Eastbound Columbia Ave @ 15400 Block
* `56302` – Eastbound Columbia Ave @ Maple St
* `56306` – Westbound 24 Ave @ 128 St
* `56307` – Westbound 24 Ave @ 124B St
* `56308` – Southbound 124 St @ 24 Ave
* `56309` – Southbound 124 St @ 23 Ave
* `56310` – Southbound 124 St @ Indian Fort Dr
* `56311` – Southbound Ocean Park Rd @ 18 Ave
* `56313` – Eastbound 20 Ave @ 128 St
* `56314` – Eastbound 20 Ave @ 129 St
* `56315` – Eastbound 20 Ave @ 131 St
* `56316` – Eastbound 20 Ave @ Amble Greene Dr
* `56317` – Eastbound 20 Ave @ 134 St
* `56318` – Eastbound 20 Ave @ 136 St
* `56319` – Eastbound 20 Ave @ 138 St
* `56320` – Eastbound 20 Ave @ 140 St
* `56321` – Eastbound 20 Ave @ 142 St
* `56322` – Westbound 20 Ave @ 144 St
* `56323` – Westbound 20 Ave @ 142 St
* `56324` – Westbound 20 Ave @ 140 St
* `56325` – Westbound 20 Ave @ 138 St
* `56326` – Westbound 20 Ave @ 136 St
* `56327` – Westbound 20 Ave @ 134 St
* `56328` – Westbound 20 Ave @ 132A St
* `56329` – Westbound 20 Ave @ 131 St
* `56330` – Westbound 20 Ave @ 129 St
* `56365` – Westbound 24 Ave @ 134 St
* `56366` – Westbound 24 Ave @ 132 St
* `56367` – Westbound 24 Ave @ 130 St
* `56368` – Eastbound 24 Ave @ 128 St
* `56369` – Eastbound 24 Ave @ 130 St
* `56370` – Eastbound 24 Ave @ 132 St
* `56371` – Eastbound 24 Ave @ 134 St
* `56372` – Eastbound 24 Ave @ 136 St
* `56389` – Westbound Buena Vista Ave @ Foster St
* `56390` – Westbound Buena Vista Ave @ Blackwood St
* `56391` – Westbound Buena Vista Ave @ Beach View Ave
* `56392` – Westbound Buena Vista Ave @ Oxford St
* `56398` – Westbound 64 Ave @ King George Blvd
* `56399` – Westbound 64 Ave @ 134 St
* `56400` – Northbound 128 St @ 74 Ave
* `56401` – Southbound 128 St @ 76 Ave
* `56403` – Eastbound 64 Ave @ 132 St
* `56404` – Eastbound 64 Ave @ 134 St
* `56409` – Eastbound Fraser Hwy @ 148 St
* `56410` – Eastbound Fraser Hwy @ 150 St
* `56412` – Eastbound 64 Ave @ 198 St
* `56422` – Northbound No. 1 Rd @ Chatham St
* `56423` – Northbound No. 1 Rd @ Richmond St
* `56424` – Northbound No. 1 Rd @ Georgia St
* `56426` – Northbound No. 1 Rd @ Fundy Gate
* `56427` – Northbound No. 1 Rd @ 10200 Block
* `56429` – Northbound No. 1 Rd @ Peterson Gate
* `56430` – Northbound No. 1 Rd @ Osmond Ave
* `56432` – Northbound No. 1 Rd @ Youngmore Rd
* `56434` – Northbound No. 1 Rd @ Blundell Rd
* `56435` – Northbound No. 1 Rd @ Moresby Dr
* `56445` – Eastbound Westminster Hwy @ 6300 Block
* `56458` – Eastbound Alderbridge Way @ Kwantlen St
* `56462` – Eastbound Bridgeport Rd @ Sexsmith Rd
* `56472` – Southbound Garden City Rd @ Odlin Rd
* `56481` – Westbound Westminster Hwy @ No. 3 Rd
* `56495` – Southbound No. 1 Rd @ Moresby Dr
* `56498` – Southbound No. 1 Rd @ Youngmore Rd
* `56500` – Southbound No. 1 Rd @ Osmond Ave
* `56501` – Southbound No. 1 Rd @ Desmond Ave
* `56503` – Southbound No. 1 Rd @ 10200 Block
* `56504` – Southbound No. 1 Rd @ Springfield Dr
* `56506` – Westbound Steveston Hwy @ 2nd Ave
* `56507` – Southbound 4th Ave @ Steveston Hwy
* `56508` – Southbound 4th Ave @ Pleasant St
* `56509` – Southbound 4th Ave @ Garry St
* `56510` – Southbound 4th Ave @ Broadway St
* `56512` – Southbound No. 1 Rd @ Steveston Hwy
* `56513` – Eastbound Steveston Hwy @ No. 1 Rd
* `56514` – Southbound No. 1 Rd @ Georgia St
* `56516` – Southbound No. 1 Rd @ Broadway St
* `56518` – Eastbound Moncton St @ No. 1 Rd
* `56519` – Eastbound Moncton St @ 4300 Block
* `56520` – Eastbound Moncton St @ Phoenix Dr
* `56522` – Eastbound Moncton St @ Trites Rd
* `56523` – Eastbound Moncton St @ No. 2 Rd
* `56524` – Northbound No. 2 Rd @ Kittiwake Dr
* `56525` – Northbound No. 2 Rd @ 11300 Block
* `56527` – Northbound No. 2 Rd @ Spender Dr
* `56528` – Northbound No. 2 Rd @ Wallace Rd
* `56530` – Northbound No. 2 Rd @ Woodwards Rd
* `56531` – Northbound No. 2 Rd @ Maple Rd
* `56533` – Northbound No. 2 Rd @ Colville Rd
* `56536` – Eastbound Blundell Rd @ Cheviot Place
* `56537` – Eastbound Blundell Rd @ 6400 Block
* `56538` – Eastbound Blundell Rd @ Minler Rd
* `56540` – Eastbound Blundell Rd @ Moffatt Rd
* `56543` – Northbound No. 3 Rd @ General Currie Rd
* `56544` – Northbound No. 3 Rd @ Bennett Rd
* `56547` – Eastbound Granville Ave @ No. 3 Rd
* `56563` – Westbound Cambie Rd @ Hazelbridge Way
* `56564` – Aberdeen Station @ Bay 2
* `56574` – Westbound Blundell Rd @ Minoru Blvd
* `56575` – Westbound Blundell Rd @ Moffatt Rd
* `56577` – Westbound Blundell Rd @ Curzon St
* `56578` – Westbound Blundell Rd @ 6300 Block
* `56579` – Westbound Blundell Rd @ Cheviot Place
* `56581` – Southbound No. 2 Rd @ Colville Rd
* `56583` – Southbound No. 2 Rd @ Maple Rd
* `56584` – Southbound No. 2 Rd @ Woodwards Rd
* `56586` – Southbound No. 2 Rd @ Wallace Rd
* `56588` – Southbound No. 2 Rd @ 11500 Block
* `56589` – Southbound No. 2 Rd @ Kittiwake Dr
* `56591` – Westbound Moncton St @ Trites Rd
* `56593` – Westbound Moncton St @ Phoenix Dr
* `56594` – Westbound Moncton St @ Easthope Ave
* `56595` – Northbound No. 1 Rd @ Moncton St
* `56599` – Eastbound Horseshoe Way @ No. 5 Rd
* `56602` – Westbound Steveston Hwy @ Shell Rd
* `56603` – Westbound Steveston Hwy @ Southport Rd
* `56604` – Westbound Steveston Hwy @ Swinton Cres
* `56605` – Westbound Steveston Hwy @ Southgate Rd
* `56606` – Westbound Steveston Hwy @ Southridge Rd
* `56607` – Westbound Steveston Hwy @ Mortfield Gate
* `56608` – Westbound Steveston Hwy @ 8700 Block
* `56609` – Westbound Steveston Hwy @ Roseland Gate
* `56610` – Northbound No. 3 Rd @ Steveston Hwy
* `56611` – Northbound No. 3 Rd @ Rosewell Ave
* `56612` – Northbound No. 3 Rd @ Ryan Rd
* `56613` – Northbound No. 3 Rd @ Williams Rd
* `56614` – Northbound No. 3 Rd @ Saunders Rd
* `56615` – Northbound No. 3 Rd @ Broadmoor Blvd
* `56621` – Westbound Lansdowne Rd @ Cooney Rd
* `56626` – Southbound No. 3 Rd @ Francis Rd
* `56627` – Southbound No. 3 Rd @ Broadmoor Blvd
* `56628` – Southbound No. 3 Rd @ Saunders Rd
* `56629` – Southbound No. 3 Rd @ Williams Rd
* `56630` – Southbound No. 3 Rd @ Ryan Rd
* `56631` – Southbound No. 3 Rd @ Goldstream Dr
* `56632` – Southbound No. 3 Rd @ Rosewell Ave
* `56634` – Eastbound Steveston Hwy @ Roseland Gate
* `56635` – Eastbound Steveston Hwy @ 9000 Block
* `56636` – Eastbound Steveston Hwy @ Mortfield Gate
* `56637` – Eastbound Steveston Hwy @ Southridge Rd
* `56638` – Eastbound Steveston Hwy @ No. 4 Rd
* `56640` – Eastbound Steveston Hwy @ Southport Rd
* `56641` – Eastbound Steveston Hwy @ Shell Rd
* `56645` – Eastbound Steveston Hwy @ Sidaway Rd
* `56650` – Eastbound Granville Ave @ St. Albans Rd
* `56651` – Eastbound Granville Ave @ 8700 Block
* `56652` – Eastbound Granville Ave @ Garden City Rd
* `56654` – Eastbound Granville Ave @ Bridge St
* `56655` – Southbound No. 4 Rd @ Granville Ave
* `56656` – Southbound No. 4 Rd @ 7500 Block
* `56657` – Southbound No. 4 Rd @ Blundell Rd
* `56658` – Southbound No. 4 Rd @ Dayton Ave
* `56659` – Southbound No. 4 Rd @ Francis Rd
* `56660` – Southbound No. 4 Rd @ Glendower Gate
* `56661` – Southbound No. 4 Rd @ Saunders Rd
* `56662` – Southbound No. 4 Rd @ Williams Rd
* `56663` – Southbound No. 4 Rd @ Mortfield Rd
* `56675` – Northbound No. 4 Rd @ Steveston Hwy
* `56676` – Northbound No. 4 Rd @ Wilkinson Rd
* `56677` – Northbound No. 4 Rd @ Williams Rd
* `56678` – Northbound No. 4 Rd @ Saunders Rd
* `56679` – Northbound No. 4 Rd @ Amethyst Ave
* `56680` – Northbound No. 4 Rd @ Francis Rd
* `56681` – Northbound No. 4 Rd @ Dayton Ave
* `56682` – Northbound No. 4 Rd @ Blundell Rd
* `56683` – Northbound No. 4 Rd @ 7400 Block
* `56684` – Westbound Granville Ave @ 9700 Block
* `56685` – Westbound Granville Ave @ Ash St
* `56686` – Westbound Granville Ave @ Garden City Rd
* `56687` – Westbound Granville Ave @ Cooney Rd
* `56691` – Eastbound Westminster Hwy @ No. 4 Rd
* `56692` – Eastbound Westminster Hwy @ 10500 Block
* `56693` – Eastbound Westminster Hwy @ 11000 Block
* `56694` – Southbound No. 5 Rd @ Westminster Hwy
* `56695` – Southbound No. 5 Rd @ 6400 Block
* `56696` – Southbound No. 5 Rd @ Granville Ave
* `56697` – Southbound No. 5 Rd @ 7400 Block
* `56698` – Southbound No. 5 Rd @ Blundell Rd
* `56699` – Southbound No. 5 Rd @ 8500 Block
* `56700` – Southbound No. 5 Rd @ Kingsbridge Dr
* `56701` – Southbound No. 5 Rd @ King Rd
* `56705` – Southbound No. 5 Rd @ Horseshoe Way
* `56707` – Southbound Horseshoe Way @ Machrina Way
* `56708` – Westbound Horseshoe Way @ Horseshoe Place
* `56709` – Northbound Hammersmith Way @ Horseshoe Way
* `56710` – Northbound Hammersmith Way @ Hammersmith Gate
* `56711` – Northbound Hammersmith Way @ Silversmith Place
* `56712` – Eastbound Coppersmith Way @ Coppersmith Place
* `56713` – Westbound Machrina Way @ No. 5 Rd
* `56714` – Eastbound Machrina Way @ No. 5 Rd
* `56719` – Northbound No. 5 Rd @ King Rd
* `56720` – Northbound No. 5 Rd @ Kingsbridge Dr
* `56721` – Northbound No. 5 Rd @ 8500 Block
* `56722` – Northbound No. 5 Rd @ Blundell Rd
* `56723` – Northbound No. 5 Rd @ 7300 Block
* `56724` – Northbound No. 5 Rd @ Granville Ave
* `56725` – Northbound No. 5 Rd @ 6400 Block
* `56726` – Northbound No. 5 Rd @ 6100 Block
* `56727` – Westbound Westminster Hwy @ No. 5 Rd
* `56728` – Westbound Westminster Hwy @ 11000 Block
* `56729` – Westbound Westminster Hwy @ 10500 Block
* `56730` – Westbound Westminster Hwy @ No. 4 Rd
* `56731` – Westbound Chatham St @ 5th Ave
* `56732` – Northbound 7th Ave @ Chatham St
* `56733` – Northbound 7th Ave @ Richmond St
* `56734` – Northbound 7th Ave @ Regent St
* `56735` – Eastbound Steveston Hwy @ 4th Ave
* `56736` – Eastbound Steveston Hwy @ 2nd Ave
* `56738` – Eastbound Steveston Hwy @ Ransford Gate
* `56741` – Eastbound Steveston Hwy @ Lassam Rd
* `56742` – Eastbound Steveston Hwy @ Kingfisher Dr
* `56744` – Eastbound Steveston Hwy @ Housman St
* `56745` – Eastbound Steveston Hwy @ Constable Gate
* `56748` – Northbound Springmont Gate @ Steveston Hwy
* `56752` – Eastbound Williams Rd @ Springmont Dr
* `56753` – Eastbound Williams Rd @ 4th Ave
* `56754` – Eastbound Williams Rd @ 2nd Ave
* `56756` – Eastbound Williams Rd @ Fortune Ave
* `56757` – Eastbound Williams Rd @ Freshwater Dr
* `56759` – Northbound Railway Ave @ Woodwards Rd
* `56760` – Northbound Railway Ave @ Maple Rd
* `56762` – Northbound Railway Ave @ 8600 Block
* `56763` – Northbound Railway Ave @ Colbeck Rd
* `56769` – Eastbound Granville Ave @ Ledway Rd
* `56772` – Eastbound Granville Ave @ Livingstone Gate
* `56781` – Westbound Granville Ave @ Ledway Rd
* `56787` – Southbound Railway Ave @ Colbeck Rd
* `56788` – Southbound Railway Ave @ 8600 Block
* `56790` – Southbound Railway Ave @ Maple Rd
* `56791` – Southbound Railway Ave @ Woodwards Rd
* `56792` – Westbound Williams Rd @ Fortune Ave
* `56794` – Westbound Williams Rd @ Elkmond Rd
* `56795` – Westbound Williams Rd @ Stilmond Rd
* `56796` – Southbound Springmont Dr @ Springhill Cres
* `56798` – Southbound Springmont Gate @ Springmont Dr
* `56799` – Southbound 7th Ave @ Steveston Hwy
* `56800` – Southbound 7th Ave @ Regent St
* `56801` – Southbound 7th Ave @ Garry St
* `56803` – Northbound 4th Ave @ Chatham St
* `56804` – Northbound 4th Ave @ Richmond St
* `56805` – Northbound 4th Ave @ Pleasant St
* `56807` – Northbound Gilbert Rd @ 10500 Block
* `56808` – Northbound Gilbert Rd @ Bamberton Dr
* `56810` – Northbound Gilbert Rd @ Gilhurst Gate
* `56811` – Northbound Gilbert Rd @ Broadmoor Blvd
* `56813` – Northbound Gilbert Rd @ 8600 Block
* `56814` – Northbound Gilbert Rd @ Lucas Rd
* `56815` – Northbound Gilbert Rd @ 8200 Block
* `56819` – Northbound Gilbert Rd @ Granville Ave
* `56821` – Northbound Gilbert Rd @ Azure Rd
* `56822` – Northbound Gilbert Rd @ 6300 Block
* `56824` – Southbound Gilbert Rd @ 6400 Block
* `56825` – Southbound Gilbert Rd @ Azure Rd
* `56826` – Southbound Gilbert Rd @ 6800 Block
* `56831` – Southbound Gilbert Rd @ 8200 Block
* `56832` – Southbound Gilbert Rd @ Lucas Rd
* `56833` – Southbound Gilbert Rd @ 8600 Block
* `56835` – Southbound Gilbert Rd @ Broadmoor Blvd
* `56836` – Southbound Gilbert Rd @ Woodwards Rd
* `56838` – Southbound Gilbert Rd @ Bamberton Dr
* `56839` – Southbound Gilbert Rd @ Kimberley Dr
* `56841` – Westbound Steveston Hwy @ Constable Gate
* `56842` – Westbound Steveston Hwy @ Housman St
* `56844` – Westbound Steveston Hwy @ Kingfisher Dr
* `56845` – Westbound Steveston Hwy @ Lassam Rd
* `56847` – Westbound Steveston Hwy @ Bonavista Gate
* `56848` – Westbound Steveston Hwy @ Ransford Gate
* `56849` – Westbound Horseshoe Way @ No. 5 Rd
* `56850` – Westbound King Rd @ No. 5 Rd
* `56855` – Westbound Williams Rd @ Aragon Rd
* `56857` – Westbound Williams Rd @ Ash St
* `56859` – Northbound Garden City Rd @ Saunders Rd
* `56861` – Northbound Garden City Rd @ Francis Rd
* `56873` – Southbound Garden City Rd @ Glenallan Gate
* `56874` – Southbound Garden City Rd @ Saunders Rd
* `56875` – Eastbound Williams Rd @ Garden City Rd
* `56876` – Eastbound Williams Rd @ Severn Dr
* `56878` – Eastbound Williams Rd @ Aquila Rd
* `56885` – Eastbound Cambie Rd @ Stolberg St
* `56911` – Eastbound Ewen Ave @ Hume St
* `56914` – Eastbound Ewen Ave @ Jardine St
* `56933` – Westbound Cambie Rd @ 10200 Block
* `56935` – Westbound Cambie Rd @ Stolberg St
* `56948` – Eastbound Bridgeport Rd @ Simpson Rd
* `56971` – Westbound Bridgeport Rd @ Simpson Rd
* `56974` – Westbound Bridgeport Rd @ McLeod Ave
* `56980` – Northbound Viking Way @ Crestwood Place
* `56995` – Southbound 208 St @ 40 Ave
* `56996` – Westbound 40 Ave @ 208 St
* `56997` – Westbound 40 Ave @ 206A St
* `56998` – Northbound 204 St @ 40 Ave
* `56999` – Eastbound 42 Ave @ 204 St
* `57000` – Eastbound 42 Ave @ 205A St
* `57001` – Eastbound 42 Ave @ 207 St
* `57002` – Northbound 208 St @ 42 Ave
* `57003` – Northbound 208 St @ 44 Ave
* `57004` – Northbound 208 St @ 45A Ave
* `57005` – Westbound Grade Cres @ 208 St
* `57006` – Westbound Grade Cres @ 20400 Block
* `57008` – Northbound 203 St @ Grade Cres
* `57009` – Northbound 203 St @ 50 Ave
* `57010` – Westbound 40 Ave @ 204 St
* `57011` – Westbound 40 Ave @ 202 St
* `57012` – Westbound 40 Ave @ 200A St
* `57014` – Northbound 200 St @ 42 Ave
* `57015` – Northbound 200 St @ 44 Ave
* `57016` – Northbound 200 St @ 45A Ave
* `57017` – Northbound 200 St @ 48 Ave
* `57020` – Eastbound 53 Ave @ 201A St
* `57021` – Northbound 204 St @ 53 Ave
* `57022` – Northbound 204 St @ 54 Ave
* `57023` – Northbound 204 St @ Park Ave
* `57038` – Eastbound 72 Ave @ 200 St
* `57039` – Northbound 202A St @ 7800 Block
* `57042` – Northbound 200 St @ 80 Ave
* `57068` – Eastbound 80 Ave @ 200 St
* `57080` – Southbound 203 St @ Michaud Cres
* `57082` – Southbound 203 St @ 53 Ave
* `57083` – Southbound 203 St @ 50 Ave
* `57084` – Eastbound Grade Cres @ 203 St
* `57085` – Eastbound Grade Cres @ 206 St
* `57086` – Eastbound Grade Cres @ 207B St
* `57087` – Southbound 208 St @ 46A Ave
* `57088` – Southbound 208 St @ 44 Ave
* `57089` – Southbound 208 St @ 42 Ave
* `57090` – Eastbound Fraser Hwy @ 176 St
* `57093` – Eastbound Fraser Hwy @ 194 St
* `57094` – Eastbound Fraser Hwy @ 195A St
* `57098` – Southbound 204 St @ Douglas Cres
* `57099` – Southbound 204 St @ 54 Ave
* `57100` – Westbound 53 Ave @ 203 St
* `57101` – Westbound 53 Ave @ 201A St
* `57103` – Southbound 200 St @ 50 Ave
* `57104` – Southbound 200 St @ 48 Ave
* `57105` – Southbound 200 St @ 45A Ave
* `57106` – Southbound 200 St @ 44 Ave
* `57107` – Southbound 200 St @ 42 Ave
* `57109` – Eastbound 40 Ave @ 200A St
* `57110` – Eastbound 40 Ave @ 202 St
* `57115` – Westbound 36 Ave @ 202A St
* `57116` – Southbound 200 St @ 36 Ave
* `57117` – Southbound 200 St @ 33A Ave
* `57118` – Southbound 200 St @ 32 Ave
* `57119` – Southbound 200 St @ 30 Ave
* `57122` – Southbound 198 St @ 24 Ave
* `57123` – Eastbound 20 Ave @ 198 St
* `57124` – Northbound 200 St @ 20 Ave
* `57125` – Northbound 200 St @ 24 Ave
* `57126` – Northbound 200 St @ 28 Ave
* `57127` – Northbound 200 St @ 30 Ave
* `57129` – Northbound 200 St @ 33A Ave
* `57130` – Eastbound 36 Ave @ 200 St
* `57131` – Eastbound 36 Ave @ 203 St
* `57139` – Westbound Fraser Hwy @ 194 St
* `57146` – Eastbound 48 Ave @ 208 St
* `57147` – Eastbound 48 Ave @ 210 St
* `57148` – Eastbound 48 Ave @ 214A St
* `57150` – Eastbound 48 Ave @ 219 St
* `57151` – Eastbound 48 Ave @ 221 St
* `57152` – Northbound 222 St @ 48 Ave
* `57154` – Eastbound Fraser Hwy @ 232 St
* `57155` – Eastbound Fraser Hwy @ 240 St
* `57156` – Eastbound Fraser Hwy @ 244 St
* `57158` – Eastbound Fraser Hwy @ 256 St
* `57161` – Westbound 29 Ave @ 266B St
* `57172` – Westbound Fraser Hwy @ 256 St
* `57173` – Westbound Fraser Hwy @ 248 St
* `57174` – Westbound Fraser Hwy @ 244 St
* `57175` – Westbound Fraser Hwy @ 240 St
* `57176` – Westbound Fraser Hwy @ 232 St
* `57178` – Westbound 48 Ave @ 222 St
* `57179` – Westbound 48 Ave @ 221 St
* `57180` – Westbound 48 Ave @ 219 St
* `57182` – Westbound 48 Ave @ 214A St
* `57183` – Westbound 48 Ave @ 210 St
* `57184` – Westbound 48 Ave @ 209 St
* `57192` – Northbound Glover Rd @ Worrell Cres
* `57194` – Northbound Glover Rd @ 216 St
* `57195` – Northbound Glover Rd @ Smith Cres
* `57197` – Northbound Glover Rd @ 79 Ave
* `57198` – Northbound Glover Rd @ 88 Ave
* `57201` – Southbound Trattle St @ 96 Ave
* `57202` – Southbound Trattle St @ 9100 Block
* `57203` – Southbound Trattle St @ Houston Ave
* `57204` – Westbound 88 Ave @ Trattle St
* `57205` – Westbound 88 Ave @ Wright St
* `57206` – Westbound 88 Ave @ 222A St
* `57207` – Westbound 88 Ave @ 22000 Block
* `57209` – Westbound 88 Ave @ 214B St
* `57210` – Westbound 88 Ave @ 212 St
* `57211` – Westbound 88 Ave @ 210 St
* `57213` – Eastbound Walnut Grove Dr @ 212 St
* `57218` – Westbound 96 Ave @ 208 St
* `57219` – Westbound 96 Ave @ 206 St
* `57220` – Westbound 96 Ave @ 205 St
* `57221` – Southbound 204 St @ 96 Ave
* `57222` – Southbound 204 St @ 93B Ave
* `57223` – Southbound 204 St @ 91B Ave
* `57224` – Southbound 204 St @ 90A Ave
* `57225` – Northbound 204 St @ 88 Ave
* `57226` – Northbound 204 St @ 90 Cres
* `57227` – Northbound 204 St @ 91A Ave
* `57228` – Northbound 204 St @ 93A Ave
* `57229` – Northbound 204 St @ 96 Ave
* `57230` – Eastbound 96 Ave @ 205 St
* `57231` – Eastbound 96 Ave @ 208 St
* `57236` – Westbound Walnut Grove Dr @ 212 St
* `57237` – Southbound Walnut Grove Dr @ 8900 Block
* `57239` – Eastbound 88 Ave @ Walnut Grove Dr
* `57240` – Eastbound 88 Ave @ 212 St
* `57241` – Eastbound 88 Ave @ 214B St
* `57243` – Eastbound 88 Ave @ 22000 Block
* `57244` – Eastbound 88 Ave @ 222A St
* `57245` – Eastbound 88 Ave @ Wright St
* `57246` – Northbound Trattle St @ 88 Ave
* `57247` – Northbound Trattle St @ Houston Ave
* `57248` – Northbound Trattle St @ 9000 Block
* `57249` – Eastbound 96 Ave @ Trattle St
* `57252` – Southbound Glover Rd @ 88 Ave
* `57253` – Southbound Glover Rd @ 79 Ave
* `57255` – Southbound Glover Rd @ Crush Cres
* `57260` – Eastbound 96 Ave @ 210 St
* `57261` – Eastbound 96 Ave @ 21200 Block
* `57262` – Southbound 213 St @ 95 Ave
* `57263` – Southbound 213 St @ 93B Ave
* `57264` – Southbound 213 St @ 92B Ave
* `57265` – Southbound 216 St @ 88 Ave
* `57266` – Southbound 216 St @ 8600 Block
* `57267` – Westbound 86A Cres @ Telegraph Trail
* `57268` – Westbound 86A Cres @ 215 St
* `57269` – Northbound 212 St @ 86A Cres
* `57270` – Eastbound 92 Ave @ 212 St
* `57272` – Northbound 213 St @ 93B Ave
* `57273` – Northbound 213 St @ 95 Ave
* `57274` – Westbound 96 Ave @ Yeomans Cres
* `57275` – Southbound 204 St @ 88 Ave
* `57276` – Eastbound 56 Ave @ Langley Bypass
* `57279` – Eastbound 52 Ave @ 216 St
* `57280` – Eastbound 52 Ave @ 217A St
* `57281` – Eastbound 52 Ave @ 219 St
* `57282` – Southbound 221A St @ Langley Memorial Hospital
* `57283` – Eastbound 50 Ave @ 221A St
* `57284` – Southbound 222 St @ Fraser Hwy
* `57305` – Eastbound Fraser Hwy @ 268 St
* `57306` – Eastbound Fraser Hwy @ 270 St
* `57307` – Eastbound 32 Ave @ 272 St
* `57309` – Eastbound 32 Ave @ 275A St
* `57310` – Southbound 276 St @ 31A Ave
* `57312` – Southbound 276 St @ Springfield Dr
* `57313` – Westbound 28 Ave @ 27400 Block
* `57314` – Westbound 28 Ave @ 273 St
* `57315` – Northbound 272 St @ 28 Ave
* `57316` – Westbound 29 Ave @ 272 St
* `57317` – Westbound 29 Ave @ 270A St
* `57318` – Westbound 29 Ave @ 268A St
* `57319` – Westbound 29 Ave @ 264A St
* `57339` – Westbound 50 Ave @ 221A St
* `57340` – Northbound 221A St @ Langley Memorial Hospital
* `57341` – Westbound 52 Ave @ 219 St
* `57342` – Westbound 52 Ave @ 218 St
* `57343` – Westbound 52 Ave @ 216 St
* `57347` – Westbound 56 Ave @ 210A St
* `57348` – Westbound 56 Ave @ 208 St
* `57411` – Southbound 53 St @ 3000 Block
* `57463` – Southbound 56 St @ 6th Ave
* `57471` – Eastbound 1st Ave @ English Bluff Rd
* `57472` – Eastbound 1st Ave @ 50 St
* `57473` – Eastbound 1st Ave @ Deerfield Dr
* `57474` – Eastbound 1st Ave @ 53A St
* `57475` – Eastbound 1st Ave @ Diefenbaker Wynd
* `57482` – Eastbound 16 Ave @ 56 St
* `57483` – Eastbound 16 Ave @ Gillespie Rd
* `57484` – Eastbound 16 Ave @ Farrell Cres
* `57485` – Eastbound 16 Ave @ Enderby Ave
* `57486` – Southbound Beach Grove Rd @ 16 Ave
* `57487` – Southbound Beach Grove Rd @ Kirkwood Rd
* `57488` – Southbound Beach Grove Rd @ Morris Cres
* `57497` – Southbound English Bluff Rd @ 4th Ave
* `57498` – Southbound English Bluff Rd @ 2nd Ave
* `57571` – Eastbound Lougheed Hwy @ Oxford St
* `57572` – Eastbound Lougheed Hwy @ Coast Meridian Rd
* `57579` – Southbound Harris Rd @ 119 Ave
* `57580` – Eastbound Hammond Rd @ Harris Rd
* `57581` – Eastbound Hammond Rd @ Blakely Rd
* `57582` – Eastbound Hammond Rd @ Bonson Rd
* `57583` – Eastbound Hammond Rd @ Wildwood Cres
* `57584` – Eastbound Hammond Rd @ Albertan St
* `57588` – Eastbound Dewdney Trunk Rd @ 202 St
* `57590` – Eastbound Dewdney Trunk Rd @ 206 St
* `57591` – Eastbound Dewdney Trunk Rd @ 207A St
* `57594` – Eastbound Dewdney Trunk Rd @ 214 St
* `57595` – Eastbound Dewdney Trunk Rd @ 216 St
* `57596` – Eastbound Dewdney Trunk Rd @ Wicklow Way
* `57597` – Eastbound Dewdney Trunk Rd @ Dover St
* `57598` – Eastbound Dewdney Trunk Rd @ Dunbar St
* `57600` – Eastbound Dewdney Trunk Rd @ 223 St
* `57603` – Haney Place @ Bay 10
* `57605` – Southbound 225 St @ North Ave
* `57606` – Southbound 225 St @ Brickwood Close
* `57607` – Eastbound 116 Ave @ 22500 Block
* `57608` – Eastbound 116 Ave @ 227 St
* `57609` – Eastbound Dewdney Trunk Rd @ 227 St
* `57610` – Eastbound Dewdney Trunk Rd @ 228 St
* `57611` – Eastbound Dewdney Trunk Rd @ 230 St
* `57612` – Eastbound Dewdney Trunk Rd @ 232 St
* `57613` – Eastbound Dewdney Trunk Rd @ 234 St
* `57614` – Eastbound Dewdney Trunk Rd @ 236 St
* `57615` – Eastbound Dewdney Trunk Rd @ 237A St
* `57617` – Eastbound Dewdney Trunk Rd @ 243 St
* `57621` – Northbound 250 St @ Dewdney Trunk Rd
* `57624` – Westbound Dewdney Trunk Rd @ 244 St
* `57625` – Westbound Dewdney Trunk Rd @ 243 St
* `57626` – Westbound Dewdney Trunk Rd @ 240 St
* `57627` – Westbound Dewdney Trunk Rd @ 237 St
* `57628` – Westbound Dewdney Trunk Rd @ 236 St
* `57629` – Westbound Dewdney Trunk Rd @ 234 St
* `57630` – Westbound Dewdney Trunk Rd @ 232 St
* `57631` – Westbound Dewdney Trunk Rd @ 230 St
* `57632` – Westbound Dewdney Trunk Rd @ Burnett St
* `57633` – Westbound Dewdney Trunk Rd @ 228 St
* `57634` – Westbound Dewdney Trunk Rd @ 227 St
* `57635` – Southbound 227 St @ Haney Bypass
* `57636` – Northbound 225 St @ Haney Bypass
* `57637` – Northbound 225 St @ Brickwood Close
* `57639` – Northbound 226 St @ Lougheed Hwy
* `57642` – Westbound Dewdney Trunk Rd @ Dover St
* `57643` – Westbound Dewdney Trunk Rd @ Hall St
* `57644` – Westbound Dewdney Trunk Rd @ 216 St
* `57645` – Westbound Dewdney Trunk Rd @ 214 St
* `57647` – Westbound Dewdney Trunk Rd @ 210 St
* `57648` – Westbound Dewdney Trunk Rd @ 207 St
* `57649` – Westbound Dewdney Trunk Rd @ 205 St
* `57651` – Westbound Dewdney Trunk Rd @ 201B St
* `57655` – Westbound Hammond Rd @ Springdale Dr
* `57656` – Westbound Hammond Rd @ Greenhaven Court
* `57657` – Westbound Hammond Rd @ Bonson Rd East
* `57658` – Westbound Hammond Rd @ Bonson Rd West
* `57659` – Westbound Hammond Rd @ Blakely Rd
* `57660` – Northbound Harris Rd @ Hammond Rd
* `57662` – Northbound Harris Rd @ Ford Rd
* `57672` – Southbound 203 St @ Lougheed Hwy
* `57673` – Southbound 203 St @ Stanton Ave
* `57674` – Southbound 203 St @ Thorne Ave
* `57675` – Eastbound Maple Cres @ 203 St
* `57676` – Eastbound Maple Cres @ Westfield Ave
* `57677` – Southbound Lorne Ave @ Eltham St
* `57680` – Northbound Ditton St @ Maple Cres
* `57681` – Eastbound Maple Cres @ Dartford St
* `57682` – Northbound 207 St @ Lorne Ave
* `57683` – Northbound 207 St @ Golf Lane
* `57684` – Northbound 207 St @ River Rd
* `57685` – Eastbound 117 Ave @ Graves St
* `57686` – Eastbound 117 Ave @ 210 St
* `57687` – Eastbound 117 Ave @ Riverwynd St
* `57689` – Southbound Laity St @ River Rd
* `57690` – Eastbound River Rd @ Pine St
* `57691` – Eastbound River Rd @ Darby St
* `57692` – Eastbound River Rd @ 216 St
* `57693` – Eastbound River Rd @ Carr St
* `57694` – Eastbound River Rd @ River Bend
* `57696` – Northbound 227 St @ Haney Bypass
* `57697` – Westbound 116 Ave @ 227 St
* `57698` – Westbound 116 Ave @ 22500 Block
* `57699` – Westbound North Ave @ 225 St
* `57700` – Northbound 224 St @ North Ave
* `57701` – Northbound 224 St @ 119 Ave
* `57704` – Westbound River Rd @ River Bend
* `57705` – Westbound River Rd @ Carr St
* `57706` – Westbound River Rd @ 216 St
* `57707` – Westbound River Rd @ Darby St
* `57708` – Westbound River Rd @ Pine St
* `57709` – Westbound River Rd @ Laity St
* `57711` – Westbound 117 Ave @ Laity St
* `57712` – Westbound 117 Ave @ Fraserview St
* `57713` – Westbound 117 Ave @ 210 St
* `57714` – Westbound 117 Ave @ Graves St
* `57715` – Southbound 207 St @ River Rd West
* `57717` – Southbound 207 St @ Lorne Ave
* `57718` – Westbound Maple Cres @ Dartford St
* `57719` – Westbound Maple Cres @ 206 St
* `57720` – Southbound Ditton St @ Maple Cres
* `57721` – Southbound Ditton St @ Princess St
* `57722` – Westbound Princess St @ Hampton St
* `57723` – Northbound Lorne Ave @ Princess St
* `57724` – Northbound Lorne Ave @ Eltham St
* `57725` – Westbound Maple Cres @ Westfield Ave
* `57726` – Westbound Maple Cres @ 115 Ave
* `57727` – Northbound 203 St @ Hammond Rd
* `57728` – Northbound 203 St @ Thorne Ave
* `57730` – Northbound 203 St @ Lougheed Hwy
* `57750` – Westbound Dewdney Trunk Rd @ 252 St
* `57754` – Northbound 203 St @ Dewdney Trunk Rd
* `57755` – Northbound 203 St @ Telep Ave
* `57756` – Eastbound 123 Ave @ 203 St
* `57757` – Eastbound 123 Ave @ 206 St
* `57758` – Eastbound 123 Ave @ Skillen St
* `57759` – Eastbound 123 Ave @ 210 St
* `57760` – Eastbound 123 Ave @ 212 St
* `57761` – Eastbound 123 Ave @ Laity St
* `57762` – Eastbound 123 Ave @ 21500 Block
* `57763` – Northbound 216 St @ 123 Ave
* `57764` – Eastbound 124 Ave @ 217 St
* `57765` – Eastbound 124 Ave @ Davison St
* `57766` – Eastbound 124 Ave @ Gray St
* `57767` – Eastbound 124 Ave @ 223 St
* `57771` – Northbound 227 St @ Balabanian Circle
* `57772` – Northbound 227 St @ 127 Ave
* `57773` – Eastbound 128 Ave @ 227 St
* `57774` – Eastbound 128 Ave @ 228 St
* `57775` – Eastbound 128 Ave @ 229 St
* `57776` – Eastbound 128 Ave @ 230 St
* `57777` – Northbound 232 St @ 128 Ave
* `57778` – Northbound 232 St @ Dogwood Ave
* `57782` – Southbound 232 St @ Dogwood Ave
* `57783` – Southbound 232 St @ 128 Ave
* `57784` – Southbound 232 St @ 126 Ave
* `57786` – Southbound 232 St @ 122 Ave
* `57787` – Southbound 232 St @ Dewdney Trunk Rd
* `57788` – Southbound 232 St @ 118 Ave
* `57790` – Westbound 116 Ave @ Lougheed Hwy
* `57791` – Westbound Lougheed Hwy @ 228 St
* `57794` – Eastbound Lougheed Hwy @ 228 St
* `57795` – Eastbound 116 Ave @ Lougheed Hwy
* `57796` – Eastbound 116 Ave @ 231B St
* `57797` – Northbound 232 St @ 118 Ave
* `57798` – Northbound 232 St @ Dewdney Trunk Rd
* `57799` – Northbound 232 St @ 122 Ave
* `57801` – Northbound 232 St @ 126 Ave
* `57802` – Westbound 128 Ave @ 232 St
* `57803` – Westbound 128 Ave @ 229 St
* `57804` – Westbound 128 Ave @ 228 St
* `57805` – Southbound 227 St @ 127 Ave
* `57806` – Southbound 227 St @ Kendrick Loop
* `57809` – Westbound 124 Ave @ 224 St
* `57810` – Westbound 124 Ave @ Gray St
* `57811` – Westbound 124 Ave @ 219 St
* `57812` – Westbound 124 Ave @ 217 St
* `57813` – Southbound 216 St @ 124 Ave
* `57814` – Westbound 123 Ave @ 216 St
* `57815` – Westbound 123 Ave @ 214 St
* `57816` – Westbound 123 Ave @ Laity St
* `57817` – Westbound 123 Ave @ 212 St
* `57818` – Westbound 123 Ave @ 210 St
* `57819` – Westbound 123 Ave @ 209 St
* `57820` – Westbound 123 Ave @ 20600 Block
* `57821` – Westbound 123 Ave @ 206 St
* `57822` – Westbound 123 Ave @ 203 St
* `57823` – Southbound 203 St @ 122 Ave
* `57824` – Southbound 203 St @ 121 Ave
* `57828` – Eastbound Dewdney Trunk Rd @ 26500 Block (Flag)
* `57829` – Eastbound Dewdney Trunk Rd @ 26900 Block (Flag)
* `57831` – Northbound McNutt Rd @ 12400 Block (Flag)
* `57832` – Eastbound On 128 Ave @ McNutt Rd (Flag)
* `57834` – Westbound On Sayers Cres @ Garibaldi St (Flag)
* `57835` – Southbound On Garibaldi St @ 12000 Block (Flag)
* `57836` – Eastbound Dewdney Trunk Rd @ 27800 Block (Flag)
* `57839` – Southbound 284 St @ 11600 Block (Flag)
* `57840` – Southbound 280 St @ 11400 Block (Flag)
* `57841` – Southbound 280 St @ 11300 Block (Flag)
* `57843` – Southbound 280 St @ 11100 Block (Flag)
* `57845` – Southbound 280 St @ 10500 Block (Flag)
* `57846` – Southbound 280 St @ 10300 Block (Flag)
* `57847` – Southbound 280 St @ 10000 Block (Flag)
* `57848` – Southbound 280 St @ 9800 Block (Flag)
* `57856` – Northbound On River Rd @ 27100 Block (Flag)
* `57857` – Northbound 272 St @ 9800 Block (Flag)
* `57858` – Northbound 272 St @ 10000 Block (Flag)
* `57859` – Northbound 272 St @ 10300 Block (Flag)
* `57860` – Northbound 272 St @ 10500 Block (Flag)
* `57862` – Northbound 272 St @ 11000 Block (Flag)
* `57863` – Northbound 272 St @ 11300 Block (Flag)
* `57865` – Northbound 272 St @ 11700 Block (Flag)
* `57867` – Northbound On Garibaldi St @ 12000 Block (Flag)
* `57868` – Eastbound On Sayers Cres @ Garibaldi St (Flag)
* `57870` – Southbound McNutt Rd @ 12200 Block (Flag)
* `57871` – Southbound McNutt Rd @ 12000 Block (Flag)
* `57872` – Westbound Dewdney Trunk Rd @ 26900 Block (Flag)
* `57873` – Westbound Dewdney Trunk Rd @ 26400 Block (Flag)
* `57882` – Southbound 240 St @ Hill Ave
* `57885` – Eastbound Tamarack Lane @ Lougheed Hwy
* `57886` – Southbound Tamarack Lane @ 108B Ave
* `57887` – Southbound Tamarack Lane @ 108 Ave
* `57888` – Southbound Tamarack Lane @ 106 Ave
* `57889` – Westbound 105 Ave @ 23400 Block
* `57890` – Eastbound River Rd @ 23200 Block (Flag)
* `57891` – Eastbound River Rd @ 23300 Block (Flag)
* `57892` – Eastbound River Rd @ 23400 Block (Flag)
* `57893` – Waterfront Station @ West Coast Express
* `57894` – Eastbound River Rd @ 23900 Block
* `57905` – Westbound 100 Ave @ 26400 Block (Flag)
* `57906` – Westbound 100 Ave @ 26100 Block (Flag)
* `57907` – Westbound 100 Ave @ 25800 Block (Flag)
* `57908` – Westbound 100 Ave @ 25500 Block (Flag)
* `57909` – Westbound 100 Ave @ 25300 Block (Flag)
* `57910` – Westbound 100 Ave @ 24900 Block (Flag)
* `57911` – Northbound On Jackson Rd @ 101 Ave (Flag)
* `57919` – Northbound 208 St @ 70 Ave
* `57920` – Southbound 208 St @ 70 Ave
* `57926` – Westbound River Rd @ 23400 Block (Flag)
* `57927` – Westbound River Rd @ 23200 Block (Flag)
* `57928` – Eastbound 105 Ave @ 23400 Block
* `57929` – Northbound Tamarack Lane @ 105 Ave
* `57930` – Northbound Tamarack Lane @ Tamarack Cres
* `57931` – Northbound Tamarack Lane @ 108 Ave
* `57932` – Northbound Tamarack Lane @ 108B Ave
* `57933` – Westbound Tamarack Lane @ Lougheed Hwy
* `57938` – Eastbound University Blvd @ Acadia Rd
* `57943` – Southbound Jones Ave @ W 21 St
* `57944` – Waterfront Station Eastbound
* `57945` – Moody Centre Station Eastbound
* `57946` – Coquitlam Central Station Eastbound
* `57947` – Port Coquitlam Station Eastbound
* `57948` – Pitt Meadows Station Eastbound
* `57949` – Maple Meadows Station Eastbound
* `57950` – Port Haney Station Eastbound
* `57951` – Mission City Station Unload Only
* `57952` – Mission City Station Westbound
* `57953` – Port Haney Station Westbound
* `57954` – Maple Meadows Station Westbound
* `57955` – Pitt Meadows Station Westbound
* `57956` – Port Coquitlam Station Westbound
* `57957` – Coquitlam Central Station Westbound
* `57958` – Moody Centre Station Westbound
* `57959` – Waterfront Station Unload Only
* `57960` – Waterfront Station Northbound
* `57961` – Lonsdale Quay Northbound
* `57962` – Lonsdale Quay Southbound
* `57963` – Waterfront Station Southbound
* `57964` – Waterfront Station @ Platform 2
* `57966` – Granville Station @ Platform 2
* `57967` – Stadium-Chinatown Station @ Platform 2
* `57968` – Main Street-Science World Station @ Platform 2
* `57969` – Commercial-Broadway Station @ Platform 4
* `57970` – Nanaimo Station @ Platform 2
* `57971` – 29th Avenue Station @ Platform 2
* `57972` – Joyce-Collingwood Station @ Platform 2
* `57973` – Patterson Station @ Platform 2
* `57974` – Metrotown Station @ Platform 2
* `57975` – Royal Oak Station @ Platform 2
* `57976` – Edmonds Station @ Platform 2
* `57977` – 22nd Street Station @ Platform 2
* `57978` – New Westminster Station @ Platform 2
* `57979` – Columbia Station @ Platform 2
* `57980` – Scott Road Station @ Platform 2
* `57981` – Gateway Station @ Platform 2
* `57982` – Surrey Central Station @ Platform 2
* `57983` – King George Station @ Platform 2
* `57984` – King George Station @ Platform 1
* `57985` – Surrey Central Station @ Platform 1
* `57986` – Gateway Station @ Platform 1
* `57987` – Scott Road Station @ Platform 1
* `57988` – Columbia Station @ Platform 1
* `57989` – New Westminster Station @ Platform 1
* `57990` – 22nd Street Station @ Platform 1
* `57991` – Edmonds Station @ Platform 1
* `57992` – Royal Oak Station @ Platform 1
* `57993` – Metrotown Station @ Platform 1
* `57994` – Patterson Station @ Platform 1
* `57995` – Joyce-Collingwood Station @ Platform 1
* `57996` – 29th Avenue Station @ Platform 1
* `57997` – Nanaimo Station @ Platform 1
* `57998` – Commercial-Broadway Station @ Platform 3
* `57999` – Main Street-Science World Station @ Platform 1
* `58000` – Stadium-Chinatown Station @ Platform 1
* `58001` – Granville Station @ Platform 1
* `58002` – Burrard Station @ Platform 1
* `58003` – Waterfront Station @ Platform 1
* `58010` – Westbound @ Bowen Bay (Flag)
* `58011` – Tunstall Blvd @ Whitesails Rd  (Flag)
* `58012` – Westbound Adams Rd @ Cowan (Flag)
* `58015` – Eastbound Adams Rd @ Cowan (Flag)
* `58016` – Eastbound @ Bowen Bay (Flag)
* `58017` – Westbound Scarborough Rd @ Eagle Cliff Rd (Flag)
* `58020` – Eastbound Scarborough Rd @ Eagle Cliff Rd (Flag)
* `58026` – Westbound Cottonwood Ave @ Robinson St
* `58029` – Eastbound Ravine Dr @ 100 Block
* `58038` – Northbound 208 St @ 80 Ave
* `58042` – Kootenay Loop @ Bay 2
* `58044` – Westbound E Broadway @ Lillooet St
* `58045` – Westbound Steveston Hwy @ Seaward Gate
* `58072` – Northbound Kwantlen St @ Lansdowne Rd
* `58080` – Southbound Holly Dr @ Sorrel St
* `58081` – Northbound Holly Dr @ Sorrel St
* `58094` – Westbound Glen Dr @ The High Street
* `58095` – Eastbound Glen Dr @ The High Street
* `58100` – Northbound Springmont Dr @ Springhill Cres
* `58102` – Northbound 208 St @ 82 Ave
* `58105` – Southbound Mariner Way @ Mara Dr
* `58106` – Southbound Mariner Way @ Como Lake Ave
* `58108` – Eastbound Lougheed Hwy @ Woolridge St
* `58126` – Eastbound Falcon Dr @ Peregrine Place
* `58147` – Northbound Willard St @ Marine Dr
* `58154` – Southbound No. 3 Rd @ Browngate Rd
* `58165` – Northbound 1st St @ 16 Ave
* `58166` – Southbound Lonsdale Ave @ W Keith Rd
* `58169` – Eastbound Keefer Place @ Taylor St
* `58174` – Eastbound E 8th Ave @ Chilliwack St
* `58175` – Guildford Exchange @ Bay 3
* `58180` – Southbound Cariboo Rd @ Avalon Ave
* `58183` – Eastbound SE Marine Dr @ 2600 Block
* `58192` – Southbound 116 St @ 88 Ave
* `58198` – Aberdeen Station @ Bay 1
* `58209` – Southbound No. 2 Rd @ Spender Dr
* `58210` – Eastbound Bridgeport Rd @ St. Edwards Dr
* `58214` – Eastbound Vulcan Way @ Viking Way
* `58216` – Westbound Bridgeport Rd @ No. 6 Rd
* `58218` – Southbound No. 6 Rd @ 2500 Block
* `58223` – Eastbound 88 Ave @ 156 St
* `58224` – Eastbound St. Johns St @ Kyle St
* `58228` – Southbound No. 6 Rd @ Vulcan Way
* `58233` – Northbound Cooney Rd @ Westminster Hwy
* `58236` – Northbound 128 St @ 26B Ave
* `58238` – Northbound 116 St @ Nordel Way
* `58241` – Northbound No. 4 Rd @ Alexandra Rd
* `58242` – Northbound No. 4 Rd @ Fisher Gate
* `58243` – Northbound No. 4 Rd @ Odlin Rd
* `58244` – Southbound No. 4 Rd @ Odlin Rd
* `58245` – Southbound No. 4 Rd @ Cambie Rd
* `58246` – Southbound No. 4 Rd @ Alexandra Rd
* `58248` – Southbound 140 St @ 75 Ave
* `58264` – Eastbound Broadway @ Ellerslie Ave
* `58268` – Eastbound W 41 Ave @ Oak St
* `58270` – Eastbound Como Lake Ave @ Baker Dr
* `58271` – Northbound Mariner Way @ Austin Ave
* `58272` – Northbound Mariner Way @ 500 Block
* `58279` – Eastbound Cook Rd @ No. 3 Rd
* `58280` – Westbound Terminal Ave @ 300 Block
* `58289` – Southbound Lansdowne Dr @ Mccoomb Dr
* `58295` – Southbound Citadel Dr @ Fortress Dr
* `58298` – Southbound Johnson St @ Parkway Blvd
* `58303` – Southbound 208 St @ 82 Ave
* `58315` – Eastbound 20 Ave @ 144 St
* `58317` – Westbound Gilpin St @ Cedarwood St
* `58318` – Eastbound Gilpin St @ Cedarwood St
* `58323` – Southbound No. 3 Rd @ General Currie Rd
* `58324` – Southbound No. 3 Rd @ Jones Rd
* `58325` – Southbound King George Blvd @ 94A Ave
* `58328` – Southbound Delbrook Ave @ W Windsor Rd
* `58329` – Southbound Deep Cove Rd @ 1400 Block
* `58330` – Westbound Dollarton Hwy @ 2400 Block
* `58331` – Eastbound Dollarton Hwy @ 2600 Block
* `58332` – Eastbound Plymouth Dr @ Fairfield Rd
* `58334` – Southbound 144 St @ 78A Ave
* `58336` – Eastbound 96 Ave @ 206 St
* `58337` – Westbound Hastings St @ Holdom Ave
* `58338` – Northbound 200 St @ 22 Ave
* `58339` – Southbound Shaughnessy St @ Patricia Ave
* `58359` – Westbound Enterprise St @ Underhill Ave
* `58364` – Northbound Lake City Way @ Venture St
* `58368` – Northbound Arden Ave @ Meadowood Dr
* `58369` – Southbound Arden Ave @ Meadowood Dr
* `58371` – Eastbound Greystone Dr @ Pinehurst Dr
* `58374` – Northbound Sperling Ave @ Union St
* `58392` – Eastbound Henning Dr @ Boundary Rd
* `58393` – Westbound Hastings St @ Duthie Ave
* `58394` – Northbound Gaglardi Way @ Broadway
* `58403` – Northbound Schoolhouse St @ Austin Ave
* `58404` – Northbound Schoolhouse St @ King Albert Ave
* `58405` – Eastbound Winslow Ave @ Schoolhouse St
* `58406` – Southbound Schoolhouse St @ Foster Ave
* `58408` – Southbound Mariner Way @ Chilko Dr
* `58416` – Westbound Winslow Ave @ Poirier St
* `58417` – Eastbound Winslow Ave @ Poirier St
* `58418` – Southbound Poirier St @ King Albert Ave
* `58419` – Southbound Mariner Way @ 500 Block
* `58421` – Northbound 208 St @ 84 Ave
* `58444` – Westbound Austin Ave @ Poirier St
* `58445` – Westbound Austin Ave @ Decaire St
* `58446` – Westbound Austin Ave @ Schoolhouse St
* `58447` – Eastbound Austin Ave @ Decaire St
* `58448` – Westbound Cliveden Ave @ 1400 Block
* `58449` – Westbound Cliveden Ave @ 800 Block
* `58450` – Eastbound Derwent Way @ Audley Blvd
* `58451` – Eastbound Broadway @ Duthie Ave
* `58453` – Southbound 208 St @ 84 Ave
* `58455` – Westbound Dollarton Hwy @ Forester St
* `58456` – Eastbound Dollarton Hwy @ Forester St
* `58457` – Eastbound Dollarton Hwy @ Riverside Dr
* `58458` – Northbound Riverside Dr @ Dollarton Hwy
* `58459` – Eastbound Old Dollarton Rd @ Riverside Dr
* `58460` – Northbound Lake City Way @ Express St
* `58462` – Westbound 64 Ave @ 166 St
* `58463` – Eastbound 64 Ave @ 166 St
* `58465` – Northbound 124 St @ Seacrest Dr
* `58466` – Eastbound 25 Ave @ 126 St
* `58467` – Westbound 25 Ave @ 126 St
* `58468` – Southbound 124 St @ Seacrest Dr
* `58469` – Northbound 124 St @ 20 Ave
* `58470` – Northbound 124 St @ 22 Ave
* `58471` – Northbound 124 St @ 24 Ave
* `58472` – Northbound Ocean Park Rd @ 16 Ave
* `58473` – Northbound Ocean Park Rd @ 18 Ave
* `58474` – Northbound Best St @ Vine Ave
* `58481` – Northbound 148 St @ 17 Ave
* `58482` – Southbound 148 St @ 17 Ave
* `58483` – Northbound 148 St @ 18A Ave
* `58487` – Southbound 124 St @ 21A Ave
* `58492` – Eastbound Old Dollarton Rd @ Front St
* `58498` – Westbound Marine Dr @ Kerfoot Rd
* `58509` – Southbound 222 St @ 50 Ave
* `58511` – Westbound Winston St @ Bainbridge Ave
* `58513` – Northbound 222 St @ Fraser Hwy
* `58515` – Westbound 91 Ave @ 152 St
* `58518` – Southbound 216 St @ 52 Ave
* `58520` – Westbound Fraser Hwy @ 221 St
* `58521` – Northbound 202 St @ 88 Ave
* `58523` – Westbound 88 Ave @ 208 St
* `58524` – Southbound 208 St @ 88 Ave
* `58533` – Westbound Dollarton Hwy @ Riverside Dr
* `58534` – Northbound Mundy St @ Gale Ave
* `58536` – Westbound 46 Ave @ 220 St
* `58537` – Westbound 46 Ave @ 217B St
* `58538` – Westbound 46 Ave @ 216 St
* `58539` – Northbound 216 St @ 47A Ave
* `58540` – Southbound 208 St @ Grade Cres
* `58541` – Southbound 208 St @ 45A Ave
* `58542` – Southbound 208 St @ 42 Ave
* `58543` – Westbound 42 Ave @ 207 St
* `58544` – Westbound 42 Ave @ 205A St
* `58545` – Southbound 204 St @ 42 Ave
* `58546` – Southbound 204 St @ 40 Ave
* `58547` – Southbound 204 St @ 38A Ave
* `58548` – Southbound 204 St @ 36 Ave
* `58549` – Northbound 200 St @ 36 Ave
* `58550` – Northbound 200 St @ 38A Ave
* `58554` – Eastbound Michaud Cres @ 200 St
* `58556` – Westbound Michaud Cres @ 201A St
* `58557` – Westbound Michaud Cres @ 200 St
* `58558` – Westbound Grade Cres @ 203 St
* `58559` – Westbound Grade Cres @ 200 St
* `58561` – Southbound 200 St @ 38A Ave
* `58562` – Northbound 204 St @ 36 Ave
* `58563` – Northbound 204 St @ 38 Ave
* `58564` – Northbound 208 St @ 47 Ave
* `58565` – Southbound 216 St @ 47A Ave
* `58566` – Eastbound 46 Ave @ 216 St
* `58567` – Eastbound 46 Ave @ 217B St
* `58568` – Eastbound 46 Ave @ 220 St
* `58569` – Westbound Old Yale Rd @ 220 St
* `58570` – Westbound Fraser Hwy @ 216 St
* `58571` – Eastbound Old Yale Rd @ 220 St
* `58573` – Southbound 144 St @ 6200 Block
* `58574` – Northbound 144 St @ 6200 Block
* `58578` – Eastbound 26 Ave @ 12800 Block
* `58586` – Eastbound 17 Ave @ 12900 Block
* `58587` – Eastbound Panorama Dr @ 2900 Block
* `58595` – Southbound Maple St @ McAllister Ave
* `58599` – Northbound Gamma Ave @ Albert St
* `58600` – Westbound 32 Ave @ 152 St
* `58609` – Northbound Mundy St @ Paradise Ave
* `58610` – Northbound Mundy St @ Hillside Ave
* `58611` – Eastbound Marine Dr @ 5500 Block
* `58614` – Eastbound E Broadway St @ Fraser St
* `58625` – Northbound Lonsdale Ave @ E 19 St
* `58636` – Northbound Shaughnessy St @ Mary Hill Bypass
* `58637` – Southbound Shaughnessy St @ Mary Hill Bypass
* `58644` – Eastbound 40 Ave @ 204 St
* `58645` – Eastbound 40 Ave @ 206A St
* `58646` – Northbound 208 St @ 40 Ave
* `58649` – Northbound Mundy St @ Palliser Ave
* `58650` – Eastbound W 41 Ave @ Crown St
* `58652` – Westbound E Hastings St @ Lakewood Dr
* `58657` – Southbound 128 St @ 74 Ave
* `58658` – Southbound 128 St @ 69A Ave
* `58660` – Eastbound Fraser Hwy @ Fleetwood Way
* `58662` – Southbound Derwent Way @ Belgrave Way
* `58663` – Southbound Pinetree Way @ Bobcat Place
* `58664` – Northbound Pinetree Way @ Glen Dr
* `58665` – Southbound Pinetree Way @ Grizzly Place
* `58667` – Northbound Plateau Blvd @ Bristlecone Court
* `58668` – Eastbound Plateau Blvd @ Robson Dr
* `58669` – Eastbound Plateau Blvd @ Timber Court
* `58674` – Southbound Coast Meridian Rd @ Robertson Ave
* `58678` – Eastbound Mason Ave @ Oxford St
* `58680` – Southbound Oxford St @ Westminster Ave
* `58681` – Eastbound Prairie Ave @ Fremont St
* `58682` – Westbound Prairie Ave @ Fremont St
* `58683` – Eastbound Prairie Ave @ Skeena St
* `58685` – Eastbound Riverside Dr @ Elbow Place
* `58687` – Westbound Riverside Dr @ Nechako Cres
* `58688` – Southbound Riverside Dr @ Parana Dr
* `58689` – Northbound Riverside Dr @ Riverwood Gate
* `58690` – Eastbound Riverside Dr @ Skeena St
* `58691` – Westbound Riverside Dr @ Skeena St
* `58694` – Southbound Scott Rd @ Nordel Way
* `58695` – Northbound Coast Meridian Rd @ Birchland Ave
* `58697` – Southbound Fremont St @ Riverside Dr
* `58701` – Northbound Shaughnessy St @ Flynn Cres
* `58702` – Northbound Shaughnessy St @ Keith Dr
* `58703` – Southbound Oxford St @ Forestgate Place
* `58704` – Westbound W Georgia St @ Bute St
* `58716` – Northbound 200 St @ 37 Ave
* `58717` – Southbound 200 St @ 37A Ave
* `58718` – Northbound Kensington Ave @ Joe Sakic Way
* `58726` – Southbound Marmont St @ Alderson Ave
* `58729` – Southbound Deer Lake Parkway @ Canada Way
* `58730` – Northbound Marmont St @ Quadling Ave
* `58731` – Westbound 88 Ave @ 204 St
* `58736` – Eastbound Harbourside Dr @ Harbourside Place
* `58737` – Southbound Fell Ave @ AutoMall Dr
* `58738` – Northbound Fell Ave @ AutoMall Dr
* `58740` – Northbound Cumberland St @ E 7th Ave
* `58742` – Northbound Richmond St @ Miner St
* `58743` – Northbound Jamieson Court @ Richmond St
* `58751` – Southbound Marmont St @ Walls Ave
* `58752` – Northbound Ravine Dr @ Brackenridge Place
* `58755` – Westbound Forest Park Way @ Aspenwood Dr
* `58756` – Northbound Aspenwood Dr @ Forest Park Way
* `58757` – Eastbound Ravine Dr @ Arrow Wood Place
* `58758` – Southbound Belcarra Bay Rd @ Salish Rd
* `58761` – Northbound Byrnepark Dr @ Marine Dr
* `58763` – Eastbound Riverside Dr @ Ottawa St
* `58764` – Eastbound Riverside Dr @ Nile Gate
* `58765` – Eastbound 88 Ave @ 204 St
* `58766` – Eastbound Grafton Rd @ Artisan Square (Flag)
* `58767` – Northbound Mt Gardner Rd @ Killarney Lake (Flag)
* `58771` – Westbound Kingsway Ave @ 1900 Block
* `58772` – Eastbound Kingsway Ave @ 1900 Block
* `58773` – Westbound Kingsway Ave @ Tyner St
* `58774` – Eastbound Kingsway Ave @ Tyner St
* `58775` – Northbound Shaughnessy St @ Prairie Ave
* `58776` – Northbound Shaughnessy St @ Lodge Dr
* `58777` – Southbound Ottawa St @ Riverside Dr
* `58783` – Northbound 160 St @ 28 Ave
* `58785` – Eastbound David Ave @ Oxford St
* `58786` – Northbound Oxford St @ Laurier Ave
* `58788` – Southbound Oxford St @ Lincoln Ave
* `58793` – Northbound Oxford St @ Prairie Ave
* `58794` – Westbound Pitt River Rd @ Shaughnessy St
* `58795` – Eastbound Pitt River Rd @ Shaughnessy St
* `58796` – Southbound Eastern Dr @ Penny Place
* `58797` – Northbound Wellington St @ Greenmount Ave
* `58798` – Southbound Wellington St @ Canterbury Lane
* `58799` – Northbound Wellington St @ Canterbury Lane
* `58800` – Westbound Pitt River Rd @ Mary Hill Rd
* `58801` – Southbound Oxford St @ Laurier Ave
* `58803` – Southbound Coast Meridian Rd @ Lincoln Ave
* `58804` – Westbound Oxford Connector @ 2000 Block
* `58806` – Westbound Oxford Connector @ Oxford St
* `58808` – Southbound Eastern Dr @ Langan Ave
* `58809` – Southbound Eastern Dr @ Lamprey Dr
* `58810` – Southbound Eastern Dr @ Oughton Dr
* `58811` – Northbound Shaughnessy St @ Dorset Ave
* `58812` – Northbound Shaughnessy St @ Centennial Ave
* `58813` – Southbound 228 St @ Selkirk Ave
* `58815` – Southbound Pipeline Rd @ Dayanee Springs Blvd
* `58816` – Northbound Pipeline Rd @ El Casa Court
* `58817` – Southbound Robson Dr @ Blackwater Place
* `58818` – Westbound Robson Dr @ Kenney St
* `58820` – Northbound Pinetree Way @ Tanager Court
* `58823` – Northbound Pinetree Way @ Robson Dr
* `58828` – Southbound 160 St @ 28 Ave
* `58831` – Southbound Pinetree Way @ Douglas College
* `58832` – Northbound Pinetree Way @ Douglas College
* `58834` – Eastbound Parkway Blvd @ 1400 Block
* `58835` – Eastbound Parkway Blvd @ Johnson St
* `58836` – Westbound Panorama Dr @ Johnson St
* `58837` – Westbound Panorama Dr @ 2900 Block
* `58838` – Southbound Parkway Blvd @ Panorama Dr
* `58839` – Westbound Panorama Dr @ Parkway Blvd
* `58840` – Southbound Parkway Blvd @ Tanglewood Lane
* `58841` – Southbound Parkway Blvd @ Salal Cres South
* `58842` – Southbound Parkway Blvd @ Salal Cres North
* `58843` – Southbound Parkway Blvd @ 1600 Block
* `58844` – Southbound Parkway Blvd @ Turnberry Lane
* `58845` – Southbound Parkway Blvd @ Braeside Place
* `58846` – Westbound Parkway Blvd @ Plateau Blvd
* `58847` – Southbound Panorama Dr @ Forest Park Way
* `58849` – Southbound Forest Park Way @ Fernway Dr
* `58850` – Northbound Paddock Dr @ 1700 Block
* `58851` – Northbound Paddock Dr @ Plateau Blvd
* `58852` – Northbound Plateau Blvd @ Chartwell Green
* `58853` – Eastbound Panorama Dr @ Stoneridge Lane
* `58854` – Westbound Panorama Dr @ Stoneridge Lane
* `58856` – Westbound Panorama Dr @ Greenstone Court
* `58858` – Northbound Forest Park Way @ Aspenwood Drive South
* `58861` – Northbound Forest Park Way @ Aspenwood Dr
* `58863` – Northbound Panorama Dr @ Panorama Place
* `58864` – Southbound Panorama Dr @ Panorama Place
* `58866` – Westbound Panorama Dr @ Eagle Mountain Dr
* `58867` – Westbound Panorama Dr @ Noons Creek Dr
* `58868` – Southbound Noons Creek Dr @ Sandstone Cres
* `58872` – Eastbound Noons Creek Dr @ Thurston Terrace
* `58875` – Northbound Ravine Dr @ Deerwood Place
* `58876` – Eastbound Ravine Dr @ Creekstone Place
* `58877` – Westbound Ravine Dr @ Buckhorn Place
* `58878` – Southbound Noons Creek Dr @ Dogwood Place
* `58879` – Southbound Noons Creek Dr @ Heather Place
* `58880` – Westbound Noons Creek Dr @ Britton Dr
* `58881` – Southbound Noons Creek Dr @ Noons Creek Close
* `58883` – Westbound Johnson St @ Robson Dr
* `58884` – Southbound Forest Park Way @ Firview Place
* `58885` – Northbound Forest Park Way @ Firview Place
* `58886` – Northbound Plateau Blvd @ Camelback Lane
* `58887` – Westbound Johnson St @ 1400 Block
* `58888` – Westbound David Ave @ 1300 Block
* `58889` – Eastbound David Ave @ 1300 Block
* `58890` – Northbound Heritage Mountain Blvd @ 1200 Block
* `58891` – Southbound Heritage Mountain Blvd @ Foxwood Dr
* `58892` – Westbound Ravine Dr @ Turner Creek Dr
* `58893` – Westbound W Pender St @ Burrard St
* `58897` – Northbound 160 St @ 30 Ave
* `58898` – Northbound Lansdowne Dr @ Runnel Dr
* `58902` – Southbound 227 St @ Lee Ave
* `58903` – Southbound 227 St @ 121 Ave
* `58905` – Northbound 224 St @ 121 Ave
* `58907` – Southbound Turner Creek Dr @ Ravine Dr
* `58910` – Northbound 53A Ave @ 51B Ave
* `58911` – Southbound 53A Ave @ 205 St
* `58912` – Northbound 206 St @ 5400 Block
* `58913` – Southbound 206 St @ Douglas Cres
* `58914` – Eastbound Douglas Cres @ 206 St
* `58915` – Westbound Douglas Cres @ 207 St
* `58917` – Southbound 208 St @ Fraser Hwy
* `58918` – Eastbound Douglas Cres @ 208 St
* `58919` – Northbound 208 St @ Fraser Hwy
* `58920` – Southbound 208 St @ 56 Ave
* `58921` – Eastbound 56 Ave @ Langley Bypass
* `58927` – Southbound 160 St @ 30 Ave
* `58931` – Eastbound 64 Ave @ 20400 Block
* `58932` – Westbound 64 Ave @ 20300 Block
* `58935` – Southbound 203 St @ 65 Ave
* `58940` – Westbound 32 Ave @ 160 St
* `58946` – Westbound Fraser Hwy @ 159 St
* `58947` – Westbound E 6th Ave @ Glenbrook Dr
* `58953` – Westbound E 10 Ave @ Kingsway
* `58954` – Southbound Fraser St @ E 31 Ave
* `58955` – Northbound Fraser St @ E 31 Ave
* `58960` – Southbound Belcarra Bay Rd @ Whiskey Cove Lane
* `58961` – Mall Access Rd @ Willowbrook Mall
* `58972` – Westbound On Sunnyside Rd @ 2200 Block (Flag)
* `58973` – Eastbound On Sunnyside Rd @ 2200 Block (Flag)
* `58974` – Westbound Bedwell Bay Rd @ 3000 Block (Flag)
* `58975` – Eastbound Bedwell Bay Rd @ 3000 Block (Flag)
* `58977` – Eastbound Bedwell Bay Rd @ 2000 Block (Flag)
* `58980` – Westbound Bedwell Bay Rd @ 1000 Block (Flag)
* `58981` – Eastbound Bedwell Bay Rd @ 1000 Block (Flag)
* `58982` – Southbound 160 St @ 32 Ave
* `58983` – Westbound Queens Park Care Centre @ Blackberry Dr
* `58987` – Southbound Riverside Dr @ Riverwood Gate
* `58989` – Eastbound Falcon Dr @ Noons Creek Dr
* `58990` – Westbound Ioco Rd @ Alderside Rd
* `58991` – Eastbound Ioco Rd @ Alderside Rd
* `59001` – Westbound Cape Horn Ave @ Mundy St
* `59006` – Westbound Citadel Dr @ Capital Court
* `59026` – Eastbound United Blvd @ Fawcett
* `59027` – Northbound Golden Dr @ 100 Block
* `59028` – Northbound Golden Dr @ Rocket Way
* `59029` – Eastbound Golden Dr @ North Bend St
* `59030` – Westbound Canoe Ave @ 2400 Block
* `59031` – Westbound Shuswap Ave @ North Bend St
* `59043` – Eastbound Greenmount Ave @ Wellington St
* `59046` – Westbound 64 Ave @ 168 St
* `59049` – Northbound Shell Rd @ Steveston Hwy
* `59050` – Westbound Williams Rd @ Garden City Rd
* `59051` – Westbound Williams Rd @ Pigott Dr
* `59052` – Eastbound Williams Rd @ Pigott Dr
* `59053` – Westbound Williams Rd @ No. 3 Rd
* `59054` – Eastbound Williams Rd @ No. 3 Rd
* `59055` – Westbound Williams Rd @ Greenlees Rd
* `59056` – Eastbound Williams Rd @ Dunoon Dr
* `59057` – Westbound Williams Rd @ Deagle Rd
* `59058` – Eastbound Williams Rd @ Deagle Rd
* `59061` – Westbound Williams Rd @ Sheridan Rd
* `59063` – Westbound Williams Rd @ Swift Lane
* `59064` – Eastbound Williams Rd @ Swift Lane
* `59065` – Westbound Williams Rd @ No. 2 Rd
* `59066` – Eastbound Williams Rd @ No. 2 Rd
* `59067` – Westbound Williams Rd @ Lassam Rd
* `59068` – Eastbound Williams Rd @ Lassam Rd
* `59069` – Southbound Railway Ave @ Williams Rd
* `59070` – Eastbound Williams Rd @ Railway Ave
* `59071` – Southbound Railway Ave @ Hollymount Gate
* `59072` – Northbound Railway Ave @ Hollymount Gate
* `59073` – Northbound Railway Ave @ Steveston Hwy
* `59075` – Southbound Railway Ave @ 11200 Block
* `59076` – Northbound Railway Ave @ 11100 Block
* `59077` – Northbound Railway Ave @ Garry St
* `59078` – Southbound Railway Ave @ Garry St
* `59079` – Northbound Railway Ave @ Moncton St
* `59081` – Northbound Shell Rd @ Maddocks Rd
* `59087` – Northbound King George Blvd @ 148 St
* `59089` – Southbound Shell Rd @ Maddocks Rd
* `59091` – Northbound 152 St @ 93 Ave
* `59093` – Southbound Pipeline Rd @ 1300 Block
* `59100` – Southbound Heritage Mountain Blvd @ Parkside Dr
* `59101` – Southbound Heritage Mountain Blvd @ Ravine Dr
* `59102` – Westbound W 49 Ave @ Maple St
* `59103` – Eastbound W 49 Ave @ Maple St
* `59104` – Westbound 32 Ave @ 156A St
* `59105` – Westbound Wilson Ave @ Bury Ave
* `59106` – Southbound Reeve St @ Wilson Ave
* `59110` – Eastbound 32 Ave @ 156A St
* `59111` – Westbound Main St @ Harbour Ave
* `59117` – Eastbound Como Lake Ave @ North Rd
* `59119` – Westbound 32 Ave @ 154 St
* `59120` – Westbound Horseshoe Way @ 12300 Block
* `59121` – Eastbound Steveston Hwy @ Trimaran Gate
* `59124` – Westbound Como Lake Ave @ Tyndall St
* `59128` – Westbound Canada Way @ Smith Ave
* `59133` – Westbound Kincaid St @ Ingleton Ave
* `59140` – Southbound Westwood St @ Crabbe Ave
* `59141` – Eastbound 88 Ave @ 202 St
* `59145` – Westbound McLean Ave @ Taylor St
* `59146` – Eastbound McLean Ave @ Taylor St
* `59148` – Northbound 203 St @ Dewdney Trunk Rd
* `59149` – Southbound 203 St @ Lougheed Hwy
* `59150` – Southbound Merivale St @ Royal Ave
* `59152` – Northbound 240 St @ Hill Ave
* `59155` – Westbound 102 Ave @ 243 St
* `59157` – Southbound 240 St @ 104 Ave
* `59158` – Westbound 104 Ave @ 240 St
* `59159` – Northbound 240 St @ 10600 Block
* `59160` – Southbound 240 St @ McClure Dr
* `59161` – Northbound 240 St @ McClure Dr
* `59162` – Southbound 240 St @ 110 Ave
* `59163` – Westbound 112 Ave @ 240 St
* `59165` – Westbound Kanaka Way @ 238 St
* `59166` – Eastbound Kanaka Way @ 238 St
* `59167` – Westbound Kanaka Way @ Creekside St
* `59168` – Eastbound Kanaka Way @ Creekside St
* `59170` – Eastbound Kanaka Way @ 236 St
* `59171` – Southbound Gilker Hill Rd @ 11100 Block
* `59173` – Southbound 236 St @ 113A Ave
* `59174` – Northbound 236 St @ 112B Ave
* `59176` – Northbound 236 St @ 114A Ave
* `59177` – Eastbound 32 Ave  @ Croydon Dr
* `59178` – Eastbound 32 Ave @ 152 St
* `59179` – Westbound 116 Ave @ 23700 Block
* `59180` – Eastbound 116 Ave @ 236B St
* `59181` – Southbound Creekside St @ 11600 Block
* `59182` – Northbound Creekside St @ 116 Ave
* `59183` – Southbound 238B St @ 118 Ave
* `59184` – Northbound 238B St @ 118 Ave
* `59185` – Westbound Dewdney Trunk Rd @ 238B St
* `59186` – Southbound 238B St @ Dewdney Trunk Rd
* `59187` – Eastbound Dewdney Trunk Rd @ 238B St
* `59190` – Southbound 208 Street Causeway @ Fraser Hwy
* `59191` – Northbound 208 St Causeway @ Fraser Hwy
* `59192` – Eastbound 104 Ave @ 240 St
* `59193` – Northbound 240 St @ 109 Ave
* `59198` – Westbound 102 Ave @ 241A St
* `59199` – Eastbound 102 Ave @ 241A St
* `59200` – Westbound 102 Ave @ 244 St
* `59201` – Eastbound 102 Ave @ 244 St
* `59202` – Southbound Jackson Rd @ 103 Ave
* `59204` – Southbound Jackson Rd @ 104 Ave
* `59206` – Westbound 104 Ave @ 244 St
* `59207` – Eastbound 104 Ave @ 244 St
* `59208` – Westbound 104 Ave @ 245 St
* `59209` – Eastbound 104 Ave @ 245 St
* `59213` – Eastbound 116 Ave @ Burnett St
* `59214` – Westbound 116 Ave @ Burnett St
* `59217` – Westbound 105 Ave @ 23500 Block
* `59218` – Eastbound 105 Ave @ 23500 Block
* `59219` – Southbound 225 St @ Haney Bypass
* `59220` – Northbound 222 St @ Lougheed Hwy
* `59221` – Southbound 222 St @ Selkirk Ave
* `59222` – Southbound 222 St @ 119 Ave
* `59223` – Northbound 222 St @ 119 Ave
* `59224` – Southbound 222 St @ Dewdney Trunk Rd
* `59226` – Eastbound E Keith Rd @ Grand Blvd East
* `59227` – Eastbound Chatham St @ 4th Ave
* `59237` – Westbound 105 Ave @ Lincoln Dr
* `59239` – Southbound Hilton Rd @ Bentley Rd
* `59259` – Eastbound Williams Rd @ Sheridan Rd
* `59260` – Southbound Mariner Way @ Austin Ave
* `59264` – Southbound Pinetree Way @ 1300 Block
* `59278` – Westbound David Ave @ Shaughnessy St
* `59279` – Eastbound David Ave @ Shaughnessy St
* `59284` – Eastbound David Ave @ Coast Meridian Rd
* `59285` – Southbound Coast Meridian Rd @ David Ave
* `59295` – Westbound River Rd @ 23900 Block
* `59296` – Southbound 236 St @ 116 Ave
* `59297` – Northbound Panorama Dr @ Forest Park Way
* `59299` – Eastbound North Fraser Way @ 5000 Block
* `59304` – Westbound Dewdney Trunk Rd @ Fraser St
* `59309` – Southbound East Rd @ Blackberry Dr
* `59310` – Northbound East Rd @ Blackberry Dr
* `59311` – Southbound Turner Creek Dr @ Heritage Mountain Blvd
* `59318` – Westbound E 49 Ave @ Tyne St
* `59320` – Southbound Parkway Blvd @ 1700 Block
* `59322` – Westbound Broadway @ Gaglardi Way
* `59323` – Southbound No. 5 Rd @ Cambie Rd
* `59324` – Northbound No. 5 Rd @ Woodhead Rd
* `59325` – Southbound No. 5 Rd @ Woodhead Rd
* `59326` – Northbound No. 5 Rd @ McNeely Dr
* `59327` – Eastbound McNeely Dr @ No. 5 Rd
* `59328` – Westbound Jack Bell Dr @ McNeely Dr
* `59329` – Eastbound Jack Bell Dr @ McNeely Dr
* `59330` – Westbound Jack Bell Dr @ 12500 Block
* `59331` – Eastbound Jack Bell Dr @ 12500 Block
* `59332` – Northbound Jack Bell Dr @ Wyne Cres North
* `59333` – Southbound Jack Bell Dr @ Wyne Cres North
* `59334` – Westbound Jack Bell Dr @ Jacombs Rd
* `59335` – Northbound Jacombs Rd @ Vanier Place
* `59336` – Southbound Jacombs Rd @ Cambie Rd
* `59341` – Southbound Cooney Rd @ Ackroyd Rd
* `59345` – Southbound E Columbia St @ 600 Block
* `59346` – Northbound North Rd @ 200 Block
* `59378` – Southbound Duthie Ave @ Union St
* `59385` – Westbound Citadel Dr @ Pitt River Rd
* `59399` – Westbound David Ave @ Pipeline Rd
* `59400` – Eastbound David Ave @ Pipeline Rd
* `59405` – Westbound River Rd @ 236 St
* `59406` – Eastbound River Rd @ 236 St
* `59408` – Westbound Hickey Dr @ Dartmoor Dr
* `59413` – Northbound 208 St @ 76 Ave
* `59422` – Eastbound Burley Dr @ Braeside St
* `59423` – Westbound 116 Ave @ 231B St
* `59425` – Eastbound Queens Ave @ 18 St
* `59426` – Eastbound Burley Dr @ Kings Ave
* `59427` – Northbound 27 St @ Marine Dr
* `59431` – Eastbound W Broadway @ Spruce St
* `59434` – Westbound E 49 Ave @ Vivian St
* `59439` – Eastbound North Bluff Rd @ Kent St
* `59446` – Southbound Production Way @ Thunderbird Cres
* `59467` – Northbound Maple St @ McAllister Ave
* `59471` – Westbound Buena Vista Ave @ Merklin St
* `59481` – Southbound Broadway St @ Cameron Ave
* `59482` – Southbound Broadway St @ Kingsway Ave
* `59483` – Westbound Robson Dr @ Pinetree Way
* `59484` – Westbound Robson Dr @ Madrona Place
* `59486` – Southbound Underhill Ave @ 2700 Block
* `59487` – Northbound Glover Rd @ Hwy 10 Bypass
* `59488` – Southbound Glover Rd @ Hwy 10 Bypass
* `59489` – Eastbound SW Marine Dr @ Wesbrook Mall
* `59492` – Northbound Lansdowne Dr @ Glen Dr
* `59495` – Northbound Pinetree Way @ 1300 Block
* `59496` – Northbound Pinetree Way @ Silver Springs Blvd
* `59497` – Westbound Murray St @ Klahanie Dr East
* `59502` – Southbound 116 St @ 82 Ave
* `59503` – Southbound Broadway St @ Mary Hill Bypass
* `59505` – Eastbound Johnson St @ 1400 Block
* `59506` – Southbound Pinetree Way @ Robson Dr
* `59509` – Southbound Horseshoe Way @ Blacksmith Place
* `59513` – Eastbound W Georgia St @ Hamilton St
* `59518` – Northbound Banbury Rd @ Gallant Ave
* `59528` – Southbound Falcon Dr @ Guildford Way
* `59538` – Southbound Main St @ E 59 Ave
* `59560` – Eastbound Marine Dr @ 4900 Block
* `59562` – Eastbound Rochester Ave @ Goyer Court
* `59566` – Southbound 128 St @ 26 Ave
* `59570` – Westbound Foster Ave @ Fairview St
* `59571` – Westbound 60 Ave @ 126 St
* `59572` – Eastbound 60 Ave @ 126 St
* `59573` – Northbound 124 St @ 58A Ave
* `59580` – Southbound Main St @ E 2nd Ave
* `59581` – Northbound Main St @ E 16 Ave
* `59582` – Northbound Main St @ E 14 Ave
* `59585` – Eastbound Foster Ave @ Fairview St
* `59591` – Westbound 58 Ave @ 177B St
* `59592` – Northbound 203 St @ 65 Ave
* `59604` – Southbound Riverside Dr @ Riverside Close
* `59609` – Eastbound Park Rd @ 193 St
* `59611` – Eastbound Park Rd @ Mcmyn Ave
* `59612` – Northbound Park Rd @ Somerset Dr
* `59615` – Southbound 203 St @ Dewdney Trunk Rd
* `59616` – Eastbound Mall Access Rd @ Meadowtown Centre
* `59617` – Northbound Mall Access Rd @ Meadowtown Centre
* `59620` – Eastbound Fraser Hwy @ 84 Ave
* `59622` – Southbound Garden City Rd @ Alderbridge Way
* `59624` – Eastbound 122 Ave @ 189A St
* `59625` – Eastbound 122 Ave @ 190 St
* `59626` – Eastbound 122 Ave @ 191B St
* `59627` – Northbound 189A St @ Ford Rd
* `59628` – Westbound Ford Rd @ 189B St
* `59629` – Westbound Ford Rd @ 190A St
* `59632` – Southbound Harris Rd @ Civic Centre
* `59633` – Northbound Blakely Rd @ 117 Ave
* `59634` – Westbound 116B Ave @ Blakely Rd
* `59635` – Westbound 116B Ave @ Bonson Rd
* `59636` – Northbound Bonson Rd @ S Wildwood Cres
* `59637` – Westbound S Wildwood Cres @ 19600 Block
* `59638` – Southbound N Wildwood Cres @ Hammond Rd
* `59639` – Southbound N Wildwood Cres @ Wildwood Place
* `59640` – Southbound N Wildwood Cres @ 19800 Block
* `59641` – Southbound Wildwood Cres @ 116A Ave
* `59642` – Westbound S Wildwood Cres @ 19800 Block
* `59645` – Southbound West St @ Maple Meadows Station
* `59646` – Northbound West St @ Maple Meadows Station
* `59647` – Westbound Hammond Rd @ 20100 Block
* `59648` – Eastbound Hammond Rd @ 20100 Block
* `59649` – Westbound Hammond Rd @ 20200 Block
* `59650` – Southbound Laity St @ 12200 Block
* `59652` – Southbound Laity St @ 122 Ave
* `59653` – Northbound Laity St @ 121 Ave
* `59654` – Southbound Laity St @ 12100 Block
* `59656` – Southbound Laity St @ Cook Ave
* `59657` – Northbound Laity St @ Cook Ave
* `59658` – Northbound Laity St @ Lougheed Hwy
* `59659` – Southbound Laity St @ Cutler Place
* `59660` – Northbound Laity St @ 117 Ave
* `59661` – Westbound Barnet Rd @ Cariboo Rd
* `59662` – Eastbound Barnet Rd @ Cariboo Rd
* `59664` – Joyce Station @ Bay 7
* `59671` – Southbound Nelson Ave @ Rumble St
* `59688` – Northbound Gilley Ave @ Ewart St
* `59692` – Southbound 240 St @ Dewdney Trunk Rd
* `59693` – Northbound 240 St @ 118 Ave
* `59695` – Northbound 240 St @ 11500 Block
* `59696` – Southbound 240 St @ 11500 Block
* `59697` – Northbound 240 St @ 112 Ave
* `59698` – Southbound 240 St @ Kanaka Way
* `59701` – Westbound Lougheed Hwy @ Madison Ave
* `59705` – Westbound Fraser Hwy @ 84 Ave
* `59706` – Southbound 224 St @ 124 Ave
* `59707` – Southbound 224 St @ 122 Ave
* `59708` – Eastbound Brown Ave @ 224 St
* `59710` – Westbound Dewdney Trunk Rd @ 223 St
* `59711` – Southbound Edge St @ Brown Ave
* `59713` – Southbound 240 St @ 118 Ave
* `59721` – Westbound North Fraser Way @ Glenlyon Parkway West
* `59733` – Southbound 124 St @ 58A Ave
* `59734` – Eastbound Murray St @ Klahanie Dr East
* `59773` – Westbound Lougheed Hwy @ 224 St
* `59774` – Westbound Lougheed Hwy @ 226 St
* `59775` – Southbound 228 St @ Dewdney Trunk Rd
* `59776` – Northbound 227 St @ 122 Ave
* `59777` – Northbound 227 St @ 121 Ave
* `59778` – Northbound 227 St @ Dewdney Trunk Rd
* `59786` – Northbound Pitt River Rd @ Pooley Ave
* `59795` – Westbound 58 Ave @ 176A St
* `59807` – Northbound Falcon Dr @ Runnel Dr
* `59819` – Northbound Shaughnessy St @ Shaughnessy Place
* `59822` – Southbound Falcon Dr @ 1100 Block
* `59838` – Southbound Main St @ E 39 Ave
* `59841` – Northbound 240 St @ 109 Ave
* `59842` – Northbound Main St @ E 46 Ave
* `59843` – Northbound Main St @ 54 Ave
* `59844` – Northbound Main St @ E 62 Ave
* `59846` – Eastbound Derwent Way @ Humber Place
* `59847` – Westbound 32 Ave Diversion @ 32 Ave
* `59851` – Westbound 20 Ave @ Rotary Way
* `59853` – Northbound Glover Rd @ Rawlison Cres
* `59854` – Southbound Glover Rd @ Rawlison Cres
* `59855` – Westbound Fraser Hwy @ 182 St
* `59883` – Eastbound Lansdowne Rd @ Kwantlen St
* `59887` – Eastbound Dewdney Trunk Rd @ 240 St
* `59889` – Kootenay Loop @ Bay 1
* `59891` – Westbound 64 Ave @ 176 St
* `59892` – Westbound 64 Ave @ 174A St
* `59893` – Eastbound 64 Ave @ 173A St
* `59894` – Westbound 64 Ave @ 172 St
* `59895` – Eastbound 64 Ave @ 16900 Block
* `59896` – Westbound 64 Ave @ 16900 Block
* `59897` – Eastbound 64 Ave @ 168 St
* `59907` – Southbound Lougheed Hwy @ 116 Ave
* `59911` – Eastbound Mary Hill Bypass @ Broadway St
* `59912` – Westbound Mary Hill Bypass @ Kingsway Ave
* `59950` – Southbound King George Blvd @ 32 Ave
* `59951` – Westbound Ioco Rd @ Walton Way
* `59957` – Eastbound NW Marine Dr @ East Mall
* `59958` – Northbound Minoru Blvd @ Granville Ave
* `59959` – Northbound Minoru Blvd @ Minoru Gate
* `59961` – Westbound Dominion Ave @ Fremont Connector
* `59962` – Northbound Minoru Blvd @ Westminster Hwy
* `59971` – Southbound Fremont St @ Dominion Ave
* `59982` – Southbound Buswell St @ Cook Rd
* `59984` – Eastbound Fraser Hwy @ 48 Ave
* `59986` – Westbound 28 Ave @ 200 St
* `59987` – Westbound 28 Ave @ 197A St
* `59988` – Eastbound 24 Ave @ 196 St
* `59990` – Northbound Capilano Rd @ Fullerton Ave
* `60000` – Eastbound Davison Rd @ Harris Rd
* `60022` – Westbound E 49 Ave @ Doman St
* `60023` – Southbound Arlington St @ E 49 Ave
* `60024` – Southbound Champlain Cres @ E 54 Ave
* `60025` – Northbound Champlain Cres @ E 54 Ave
* `60026` – Eastbound Dominion Ave @ Ottawa St
* `60027` – Westbound Dominion Ave @ Ottawa St
* `60028` – Westbound Dominion Ave @ Avon Place
* `60029` – Eastbound Dominion Ave @ Hawkins St
* `60030` – Westbound Dominion Ave @ 500 Block
* `60031` – Eastbound Dominion Ave @ Fremont St
* `60032` – Southbound Fremont Connector @ Seaborne Place
* `60033` – Northbound Fremont Connector @ Seaborne Place
* `60034` – Southbound Fremont Connector @ Nicola Ave
* `60035` – Northbound Fremont Connector @ Nicola Ave
* `60036` – Westbound Sherling Ave @ Fremont Connector
* `60037` – Northbound Fremont Connector @ Sherling Ave
* `60038` – Westbound Sherling Ave @ Hawkins St
* `60039` – Eastbound Sherling Ave @ Hawkins St
* `60046` – Eastbound Cook Rd @ Buswell St
* `60052` – Eastbound David Ave @ Soball St
* `60053` – Eastbound David Ave @ Kingston St
* `60054` – Eastbound David Ave @ Riley St
* `60056` – Northbound Princeton Ave @ David Ave
* `60057` – Westbound Princeton Ave @ Kingston St
* `60058` – Westbound Princeton Ave @ Dayton St
* `60060` – Southbound Coast Meridian Rd @ Princeton Ave
* `60073` – Southbound Coast Meridian Rd @ Millard Ave
* `60074` – Southbound Coast Meridian Rd @ Galloway Ave
* `60075` – Northbound Pipeline Rd @ Windsor Gate
* `60076` – Southbound Pipeline Rd @ Windsor Gate
* `60077` – Southbound Pipeline Rd @ Glen Dr
* `60084` – Lougheed Town Centre Station @ Platform 3
* `60085` – Burquitlam Station @ Platform 1
* `60086` – Burquitlam Station @ Platform 2
* `60087` – Moody Centre Station @ Platform 1
* `60088` – Moody Centre Station @ Platform 2
* `60089` – Inlet Centre Station @ Platform 1
* `60090` – Inlet Centre Station @ Platform 2
* `60091` – Coquitlam Central Station @ Platform 1
* `60092` – Coquitlam Central Station @ Platform 2
* `60093` – Lincoln Station @ Platform 1
* `60094` – Lincoln Station @ Platform 2
* `60095` – Lafarge Lake-Douglas Station @ Platform 1
* `60096` – Lafarge Lake-Douglas Station @ Platform 2
* `60105` – Moody Centre Station @ West Coast Express
* `60106` – Moody Centre Station @ West Coast Express
* `60107` – Coquitlam Central Station @ West Coast Express
* `60108` – Coquitlam Central Station @ West Coast Express
* `60110` – Westbound W Queens Rd @ Del Rio Dr
* `60111` – Eastbound W Queens Rd @ Del Rio Dr
* `60114` – Northbound Riverside Dr @ Windridge Dr
* `60115` – Westbound E Keith Rd @ Sutherland Ave
* `60118` – Northbound 184 St @ Fraser Hwy
* `60119` – Eastbound 72 Ave @ 184 St
* `60120` – Southbound 184 St @ 72 Ave
* `60121` – Westbound W 2nd St @ Bewicke Ave
* `60131` – Eastbound 72 Ave @ 192 St
* `60132` – Westbound 72 Ave @ 194A St
* `60133` – Eastbound 72 Ave @ 194A St
* `60134` – Westbound 72 Ave @ 196 St
* `60135` – Westbound 68 Ave @ 18700 Block
* `60143` – Eastbound Princess St @ Ditton St
* `60144` – Northbound 202B St @ 70A Ave
* `60145` – Southbound 202B St @ 70A Ave
* `60149` – Southbound 160 St @ 20 Ave
* `60150` – Southbound 160 St @ 19 Ave
* `60154` – Westbound Alderbridge Way @ May Dr
* `60155` – Eastbound Alderbridge Way @ May Dr
* `60169` – Northbound 232 St @ 132 Ave
* `60170` – Southbound 232 St @ 132 Ave
* `60171` – Northbound 232 St @ 136 Ave
* `60172` – Southbound 232 St @ 136 Ave
* `60173` – Westbound 136 Ave @ 229A St
* `60174` – Westbound 136 Ave @ 228A St
* `60175` – Eastbound Foreman Dr @ Haley St
* `60176` – Eastbound Foreman Dr @ 229B St
* `60177` – Eastbound Foreman Dr @ 232 St
* `60178` – Northbound 232 St @ Anderson Creek Dr
* `60179` – Southbound Silver Valley Rd @ 141 Ave
* `60180` – Southbound Silver Valley Rd @ 13800 Block
* `60181` – Northbound Minoru Blvd @ Murdoch Ave
* `60182` – Westbound Silver Valley Rd @ 232A St
* `60184` – Eastbound Larch Ave @ Balsam St
* `60185` – Eastbound 133 Ave @ 236 St
* `60186` – Eastbound 133 Ave @ Rock Ridge Dr
* `60187` – Eastbound 133 Ave @ 23800 Block
* `60188` – Southbound 239B St @ 133 Ave
* `60189` – Southbound 239B St @ 132 Ave
* `60190` – Westbound 130 Ave @ 239B St
* `60192` – Westbound Fern Cres @ Balsam St
* `60201` – Southbound Pinetree Way @ David Ave
* `60204` – Westbound Oxford Connector @ 1900 Block
* `60209` – Westbound Elmbridge Way @ Gilbert Rd
* `60215` – Eastbound Adams Rd @ Bowen Bay Rd
* `60219` – Westbound University Blvd @ Western Parkway
* `60220` – Eastbound Lougheed Hwy @ 227 St
* `60222` – Northbound 228 St @ Lougheed Hwy
* `60224` – Southbound 140 St @ 93A Ave
* `60236` – Northbound Windjammer Rd @ Arbutus Point Rd (Flag)
* `60239` – Westbound Miller Rd @ Scarborough Rd (Flag)
* `60240` – Lenora Rd @ Melmore Rd (Flag)
* `60247` – Southbound 184 St @ 71 Ave
* `60269` – Northbound Burrard St @ Smithe St
* `60283` – Southbound No. 3 Rd @ Yaohan Access Rd
* `60284` – Northbound No. 3 Rd @ Yaohan Access Rd
* `60297` – Northbound 168 St @ 78A Ave
* `60310` – Westbound 84 Ave @ Venture Way
* `60313` – Eastbound Ioco Rd @ Beach Ave
* `60315` – Eastbound 64 Ave @ 165 St
* `60316` – Westbound 64 Ave @ 165 St
* `60320` – Westbound Marine Dr @ 14900 Block
* `60322` – Westbound Brunette @ Pare Court
* `60332` – Westbound Kingsway @ Gilley Ave
* `60335` – Eastbound E King Edward Ave @ Ontario St
* `60342` – Southbound Cambie St @ W Georgia St
* `60343` – Southbound Orchid Dr @ Holly Dr
* `60344` – Northbound Orchid Dr @ Palm Terrace
* `60366` – Westbound Oakland St @ Dufferin Ave
* `60367` – Eastbound Westminster Hwy @ No. 7 Rd
* `60376` – Westbound Marine Dr @ Capilano Rd
* `60380` – Southbound Lonsdale Ave @ W 3rd St
* `60389` – Northbound 202A St @ 76 Ave
* `60401` – Eastbound 64 Ave @ 172 St
* `60405` – Eastbound Marine Dr @ Pemberton Ave
* `60406` – Westbound W 3rd St @ Lonsdale Ave
* `60419` – Eastbound 105 Ave @ 150 St
* `60420` – Northbound King George Blvd @ 6700 Block
* `60422` – Northbound Jackson Rd @ 103 Ave
* `60423` – Eastbound Robson St @ Cardero St
* `60429` – Southbound Boundary Bay Rd @ 400 Block
* `60431` – Westbound W 41 Ave @ Churchill St
* `60433` – Westbound Kingsway @ Gladstone
* `60434` – Westbound Westminster Hwy @ No. 7 Rd
* `60435` – Westbound E 29 St @ Duchess Ave
* `60462` – Eastbound Panorama Dr @ Sandstone Cres
* `60472` – Eastbound Sawmill Cres @ River District Crossing
* `60473` – Lonsdale Quay @ Bay 9
* `60476` – Northbound Grand Blvd East @ E 13 St
* `60477` – Northbound Grand Blvd East @ E 15 St
* `60480` – King George Station @ Bay 5
* `60492` – Eastbound W Cordova St @ Carrall St
* `60494` – Eastbound W Broadway @ Granville St
* `60495` – Southbound Arbutus St @ 4200 Block
* `60498` – Westbound E Hastings St @ Heatley Ave
* `60500` – Eastbound University Dr @ Mattson Centre
* `60502` – Northbound Denman St @ Comox St
* `60507` – Northbound E Columbia St @ Sherbrooke St
* `60515` – Brentwood Station @ Bay 4
* `60517` – Southbound Main St @ E Broadway
* `60521` – Northbound 180 St @ 55 Ave
* `60523` – Southbound Griffiths Ave @ Elwell St
* `60524` – Northbound Griffiths Ave @ Elwell St
* `60525` – Eastbound Pacific St @ Thurlow St
* `60534` – Broadway-City Hall Station @ Bay 6
* `60535` – Broadway-City Hall Station @ Bay 1
* `60540` – Westbound Kingsway @ Dumfries St
* `60547` – Southbound West Blvd @ W 46th Ave
* `60551` – Northbound Fraser St @ E 56 Ave
* `60552` – Westbound W Broadway @ Hemlock St
* `60553` – Eastbound W 6th Ave @ Alder St
* `60554` – Eastbound E 2nd Ave @ Lorne St
* `60560` – Southbound 168 St @ 92A Ave
* `60561` – Northbound 168 St @ 92A Ave
* `60566` – Westbound Williams Rd @ 5100 Block
* `60567` – Eastbound W Georgia St @ Burrard St
* `60579` – Westbound E Broadway @ Quebec
* `60580` – Westbound Broadway @ Camrose Dr
* `60585` – Eastbound Broadway @ Camrose Dr
* `60586` – Westbound Hastings St @ Willingdon Ave-
* `60587` – Westbound Hastings St @ Willingdon Ave
* `60589` – Westbound SE Marine Dr @ Prince Edward St
* `60590` – Eastbound Government Rd @ Phillips Ave
* `60592` – Northbound Granville St @ Park Dr
* `60596` – Westbound Vine Ave @ Anderson St
* `60597` – Southbound Glover Rd @ Eastleigh Cres
* `60598` – Westbound Bedwell Bay Rd @ Crystal Creek Dr (Flag)
* `60599` – Eastbound Bedwell Bay Rd @ Crystal Creek Dr (Flag)
* `60600` – Kootenay Loop @ Bay 8-
* `60661` – Westbound 60 Ave @ 179 St
* `60681` – Southbound Knight St @ E 19 Ave
* `60702` – Southbound Nestor St @ Tory Ave
* `60703` – Westbound Dunkirk Ave @ Nestor St
* `60707` – Westbound Fraser Hwy @ 260 St
* `60708` – Eastbound Fraser Hwy @ 260 St
* `60714` – Eastbound Fraser Hwy @ 68 Ave
* `60715` – Westbound Fraser Hwy @ 68 Ave
* `60716` – Westbound Fraser Hwy @ 18900 Block
* `60717` – Eastbound Fraser Hwy @ 19000 Block
* `60718` – Eastbound 84 Ave @ 158 St
* `60720` – Eastbound Indian River Dr @ Mt Seymour Rd
* `60721` – Westbound Dunn Ave @ Maple Meadows Way
* `60723` – Southbound 200 St @ Maple Meadows Way
* `60743` – Westbound Fraser Hwy @ 48 Ave
* `60746` – Westbound W 49 Ave @ Yew St
* `60750` – Northbound 184 St @ 60 Ave
* `60751` – Northbound 184 St @ 6200 Block
* `60752` – Eastbound 64 Ave @ 184 St
* `60753` – Eastbound 64 Ave @ 186 St
* `60754` – Northbound 188 St @ 64 Ave
* `60755` – Northbound 188 St @ Fraser Hwy
* `60756` – Northbound 188 St @ 70 Ave
* `60757` – Eastbound 72 Ave @ 188 St
* `60758` – Eastbound 72 Ave @ 190 St
* `60759` – Southbound 192 St @ 72 Ave
* `60760` – Southbound 192 St @ 70 Ave
* `60763` – Northbound 198 St @ Willowbrook Dr
* `60766` – Northbound 192 St @ 70 Ave
* `60767` – Westbound 72 Ave @ 192 St
* `60768` – Westbound 72 Ave @ 190 St
* `60769` – Westbound 72 Ave @ 188 St
* `60770` – Southbound 188 St @ 70 Ave
* `60771` – Southbound 188 St @ Fraser Hwy
* `60772` – Westbound 64 Ave @ 188 St
* `60773` – Westbound 64 Ave @ 186 St
* `60774` – Southbound 184 St @ 64 Ave
* `60775` – Southbound 184 St @ 63 Ave
* `60778` – Northbound 176A St @ 60 Ave
* `60779` – Northbound 176A St @ 58A Ave
* `60781` – Westbound 57 Ave @ 177B St
* `60782` – Eastbound 57 Ave @ 176A St
* `60784` – Southbound 188 St @ 68 Ave
* `60785` – Northbound 188 St @ 68 Ave
* `60789` – Westbound 64 Ave @ 184 St
* `60790` – Eastbound 64 Ave @ 176 St
* `60791` – Westbound 64 Ave @ 177B St
* `60792` – Eastbound 64 Ave @ 177B St
* `60793` – Westbound 64 Ave @ 179 St
* `60794` – Eastbound 64 Ave @ 179 St
* `60795` – Westbound 64 Ave @ 181A St
* `60796` – Eastbound 64 Ave @ 181A St
* `60797` – Eastbound 64 Ave @ 188 St
* `60798` – Eastbound 64 Ave @ 190 St
* `60799` – Westbound 64 Ave @ 190 St
* `60800` – Sapperton Station @ Platform 1
* `60801` – Sapperton Station @ Platform 2
* `60802` – Braid Station @ Platform 1
* `60803` – Braid Station @ Platform 2
* `60804` – Lougheed Town Centre Station @ Platform 2
* `60805` – Lougheed Town Centre Station @ Platform 1
* `60806` – Production Way-University Station @ Platform 1
* `60807` – Production Way-University Station @ Platform 2
* `60808` – Lake City Way Station @ Platform 1
* `60809` – Lake City Way Station @ Platform 2
* `60810` – Sperling-Burnaby Lake Station @ Platform 1
* `60811` – Sperling-Burnaby Lake Station @ Platform 2
* `60812` – Holdom Station @ Platform 1
* `60813` – Holdom Station @ Platform 2
* `60814` – Brentwood Town Centre Station @ Platform 1
* `60815` – Brentwood Town Centre Station @ Platform 2
* `60816` – Gilmore Station @ Platform 1
* `60817` – Gilmore Station @ Platform 2
* `60818` – Rupert Station @ Platform 1
* `60819` – Rupert Station @ Platform 2
* `60820` – Renfrew Station @ Platform 1
* `60821` – Renfrew Station @ Platform 2
* `60822` – Commercial-Broadway Station @ Platform 1
* `60823` – Commercial-Broadway Station @ Platform 2
* `60824` – VCC-Clark Station @ Platform 1
* `60825` – VCC-Clark Station @ Platform 2
* `60826` – Burrard Station @ Platform 2
* `60854` – Southbound 200 St @ 82 Ave
* `60861` – Northbound 201A St @ Michaud Cres
* `60866` – Westbound 56 Ave @ 213A St
* `60867` – Eastbound 56 Ave @ 213A St
* `60884` – Southbound Arborlynn Dr @ Mountain Hwy
* `60886` – Westbound Hwy One Overpass @ Marine Dr
* `60887` – Caulfeild Village @ Safeway
* `60888` – Southbound Northwood Dr @ Westport Rd
* `60891` – Northbound 27 St @ Ottawa Ave
* `60892` – Northbound 27 St @ Ottawa Ave
* `60893` – Northbound 27 St @ Mathers Ave
* `60894` – Westbound Marine Dr @ 27 St
* `60895` – Eastbound Queens Ave @ 26 St
* `60897` – Northbound 24 St @ Jefferson Ave
* `60899` – Eastbound Queens Ave @ Orchard Way
* `60900` – Eastbound Mathers Ave @ 22 St
* `60902` – Southbound 20 St @ Mathers Ave
* `60904` – Westbound Inglewood Ave @ Sinclair St
* `60905` – Eastbound Inglewood Ave @ 17 St
* `60908` – Eastbound Queens Ave @ 15 St
* `60910` – Eastbound Mathers Ave @ 14 St
* `60911` – Eastbound Mathers Ave @ 1200 Block (Flag)
* `60912` – Eastbound Inglewood Ave @ 15 St
* `60913` – Southbound 17 St @ Duchess Ave
* `60914` – Westbound Fulton Ave @ 15 St
* `60915` – Eastbound Inglewood Ave @ 14 St
* `60916` – Eastbound Inglewood Ave @ 12 St
* `60917` – Westbound Fulton Ave @ 13 St
* `60918` – Westbound Fulton Ave @ 12 St
* `60921` – Southbound 11 St @ Inglewood Ave
* `60922` – Southbound 11 St @ Gordon Ave
* `60923` – Southbound 11 St @ Fulton Ave
* `60924` – Southbound 11 St @ Haywood Ave
* `60925` – Westbound Mathers Ave @ Braeside St
* `60928` – Southbound Taylor Way @ Marine Dr
* `60929` – Eastbound Stevens Dr @ Rabbit Lane
* `60930` – Eastbound Stevens Dr @ Hadden Dr
* `60931` – Southbound Stevens Dr @ Taylor Way
* `60932` – Eastbound Southborough Dr @ Westcot Rd
* `60933` – Southbound Southborough Dr @ Highland Dr
* `60935` – Westbound Eyremount Dr @ 700 Block
* `60936` – Westbound Cross Creek Rd @ Eyremount Dr
* `60937` – Eastbound Cross Creek Rd @ Highland Dr
* `60938` – Eastbound Chartwell Dr @ 1200 Block
* `60939` – Eastbound Chartwell Dr @ 1300 Block
* `60940` – Southbound Chartwell Dr @ Bramwell Rd
* `60941` – Westbound Eyremount Dr @ Crestline Rd
* `60942` – Westbound Eyremount Dr @ Highland Dr
* `60943` – Westbound Eyremount Dr @ Hillside Rd
* `60944` – Northbound Eyremount Dr @ Elveden Row
* `60945` – Westbound Eyremount Dr @ Kenwood Rd
* `60946` – Northbound Eyremount Dr @ Farmleigh Rd
* `60947` – Northbound Eyremount Dr @ King Georges Way
* `60948` – Eastbound Eyremount Dr @ Kenwood Rd
* `60949` – Northbound Eyremount Dr @ Fairmile Rd
* `60950` – Westbound Fairmile Rd @ Southborough Dr
* `60951` – Southbound Southborough Dr @ Knockmaroon Rd
* `60952` – Eastbound Stevens Dr @ 300 Block
* `60953` – Northbound Stevens Dr @ 300 Block
* `60954` – Northbound Stevens Dr @ 100 Block
* `60955` – Northbound Bonnymuir Dr @ Glenmore Dr
* `60956` – Northbound Bonnymuir Dr @ St. Giles Rd
* `60957` – Westbound St. Andrews Rd @ 500 Block
* `60958` – Southbound St. Andrews Rd @ Ballantree Rd
* `60959` – Southbound St. Andrews Rd @ St. Andrews Place
* `60960` – Southbound St. Andrews Rd @ Greenwood Rd
* `60961` – Southbound St. Andrews Rd @ Hawstead Place
* `60962` – Southbound Robin Hood Rd @ Kenwood Rd
* `60963` – Southbound Robin Hood Rd @ King Georges Way
* `60964` – Northbound Robin Hood Rd @ Kenwood Rd
* `60965` – Westbound Fulton Ave @ 14 St
* `60966` – Eastbound Inglewood Ave @ 13 St
* `60967` – Eastbound Mathers Ave @ 2000 Block
* `60968` – Northbound Eyremount Dr @ Groveland Rd
* `60969` – Eastbound Mathers Ave @ 11 St
* `60970` – Westbound Bridgeport Rd @ Smith St
* `60973` – Southbound 152 St @ 62 Ave
* `60995` – Eastbound Chippendale Rd @ Westhill Dr (Flag)
* `60998` – Northbound 21 St @ Inglewood Ave (Flag)
* `60999` – Northbound On 21 St @ Ottawa Ave (Flag)
* `61000` – Westbound On Skilift Rd @ Folkestone Way (Flag)
* `61001` – Northbound On Folkestone Way @ Chippendale Rd (Flag)
* `61002` – Southbound On Westhill Dr @ 2000 Block (Flag)
* `61003` – Southbound 21 St @ Ottawa Ave (Flag)
* `61004` – Southbound On 21 St @ Inglewood Ave (Flag)
* `61006` – Southbound Ravine Dr @ Wildwood Dr East
* `61049` – Eastbound 103A Ave @ Grace Rd
* `61050` – Westbound Grace Rd @ 103A Ave
* `61078` – Eastbound Oxford Connector @ 1900 Block
* `61081` – Westbound Grafton Rd @ Artisan Square (Flag)
* `61082` – Southbound Mt Gardner Rd @ Killarney Lake (Flag)
* `61084` – Northbound Miller Rd @ Deep Bay (Flag)
* `61085` – Southbound Miller Rd @ Deep Bay (Flag)
* `61103` – Eastbound E 33 Ave @ Prince Edward St
* `61139` – Eastbound McClure Dr @ 240 St
* `61151` – Southbound Windjammer Rd @ Captains Way Rd (Flag)
* `61152` – Northbound Windjammer Rd @ Captains Way Rd (Flag)
* `61153` – Westbound Adams Rd @ 1400 Block (Flag)
* `61155` – Westbound Grafton Rd @ 900 Block (Flag)
* `61156` – Eastbound Grafton Rd @ 900 Block (Flag)
* `61157` – Northbound Mt Gardner Rd @ Jones Rd (Flag)
* `61158` – Southbound Mt Gardner Rd @ Jones Rd (Flag)
* `61159` – Millers Landing @ 1200 Block (Flag)
* `61161` – Southbound Eagle Cliff Rd @ Upland Trail (Flag)
* `61162` – Northbound Eagle Cliff Rd @ Upland Trail (Flag)
* `61164` – Eastbound 64 Ave @ 122 St
* `61165` – Westbound 64 Ave @ 122 St
* `61166` – Eastbound 64 Ave @ 124 St
* `61167` – Westbound 64 Ave @ 124 St
* `61168` – Eastbound 64 Ave @ 126 St
* `61169` – Westbound 64 Ave @ 126 St
* `61170` – Westbound 64 Ave @ 128 St
* `61171` – Eastbound 64 Ave @ King George Blvd
* `61172` – Eastbound 64 Ave @ 138 St
* `61173` – Westbound 64 Ave @ 138 St
* `61174` – Eastbound 64 Ave @ 140 St
* `61175` – Westbound 64 Ave @ 140 St
* `61176` – Westbound 64 Ave @ 142 St
* `61177` – Eastbound 64 Ave @ 142 St
* `61178` – Westbound 64 Ave @ 144 St
* `61179` – Eastbound 64 Ave @ 144 St
* `61180` – Eastbound 64 Ave @ 146 St
* `61181` – Westbound 64 Ave @ 146 St
* `61182` – Eastbound 64 Ave @ 148 St
* `61183` – Westbound 64 Ave @ 148 St
* `61188` – Eastbound 64 Ave @ 202 St
* `61244` – Eastbound Machrina Way @ Horseshoe Way
* `61246` – Southbound Cedar Dr @ Inverness St
* `61247` – Eastbound 102 Ave @ 242B St
* `61250` – Southbound Ross Dr @ 200 Block
* `61251` – Northbound Ross Dr @ 200 Block
* `61260` – Spuraway Lodge (Flag)
* `61262` – Southbound On Jackson Rd @ 101 Ave (Flag)
* `61263` – Eastbound 100 Ave @ 24900 Block (Flag)
* `61264` – Eastbound 100 Ave @ 25300 Block (Flag)
* `61265` – Eastbound 100 Ave @ 25500 Block (Flag)
* `61266` – Eastbound 98 Ave @ 25800 Block (Flag)
* `61267` – Eastbound 98 Ave @ 26100 Block (Flag)
* `61269` – Northbound Mountain Hwy @ E 27 St
* `61273` – Eastbound Dominion Ave @ Archbishop Carney Secondary
* `61283` – Westbound East Rd @ 2400 Block
* `61284` – Eastbound East Rd @ 2300 Block
* `61294` – Eastbound David Ave @ Genest Way
* `61295` – Northbound 232 St @ Fern Cres
* `61297` – Northbound Cessna Dr @ 3800 Block
* `61298` – Southbound Brunette Ave @ Hillside Ave
* `61302` – Westbound Indian River Dr @ Mt Seymour Rd
* `61305` – Northbound 203 St @ 118 Ave
* `61310` – Southbound 196 St @ Fraser Hwy
* `61311` – Southbound East Rd @ Wyndham Cres
* `61312` – Northbound East Rd @ Wyndham Cres
* `61313` – Northbound East Rd @ Strong Rd
* `61314` – Southbound East Rd @ Strong Rd
* `61316` – Northbound Hwy 99 Onramp @ Seascape Dr
* `61318` – Southbound Hwy 99 @ Ocean Point Dr
* `61319` – Southbound Hwy 99 @ Strachan Point Rd
* `61336` – Eastbound Larson Rd @ Fir St
* `61341` – Westbound Elmbridge Way @ Minoru Blvd
* `61342` – Westbound Elmbridge Way @ Alderbridge Way
* `61344` – Westbound River Rd @ Hollybridge Way
* `61346` – Southbound Lynas Lane @ Dover Cres
* `61347` – Southbound Minoru Blvd @ Westminster Hwy
* `61348` – Southbound Minoru Blvd @ Murdoch Ave
* `61349` – Southbound Minoru Blvd @ Minoru Gate
* `61353` – Northbound 224 St @ 12200 Block
* `61363` – Vancouver City Centre Station @ Platform 2
* `61364` – Vancouver City Centre Station @ Platform 1
* `61365` – Yaletown-Roundhouse Station @ Platform 2
* `61366` – Yaletown-Roundhouse Station @ Platform 1
* `61367` – Olympic Village Station @ Platform 2
* `61368` – Olympic Village Station @ Platform 1
* `61369` – Broadway-City Hall Station @ Platform 2
* `61370` – Broadway-City Hall Station @ Platform 1
* `61371` – King Edward Station @ Platform 2
* `61372` – King Edward Station @ Platform 1
* `61373` – Oakridge-41st Avenue Station @ Platform 2
* `61374` – Oakridge-41st Avenue Station @ Platform 1
* `61375` – Langara-49th Avenue Station @ Platform 2
* `61376` – Langara-49th Avenue Station @ Platform 1
* `61377` – Marine Drive Station @ Platform 2
* `61378` – Marine Drive Station @ Platform 1
* `61379` – Bridgeport Station @ Platform 2
* `61380` – Bridgeport Station @ Platform 1
* `61381` – Aberdeen Station @ Platform 2
* `61382` – Aberdeen Station @ Platform 1
* `61383` – Landsdowne Station @ Platform 2
* `61384` – Landsdowne Station @ Platform 1
* `61385` – Richmond-Brighouse Station @ Canada Line
* `61386` – Richmond-Brighouse Station @ Platform 1
* `61387` – Templeton Station @ Platform 2
* `61388` – Templeton Station @ Platform 1
* `61389` – Sea Island Centre Station @ Platform 2
* `61390` – Sea Island Centre Station @ Platform 1
* `61391` – YVR-Airport Station @ Canada Line
* `61392` – YVR-Airport Station @ Platform 1
* `61393` – Waterfront Station Canada Line @ Platform 2
* `61394` – Waterfront Station @ Canada Line
* `61396` – Southbound 232 St @ Fern Cres
* `61414` – Eastbound 68 Ave @ 192 St
* `61419` – Northbound Renaissance Square @ Quayside Dr
* `61420` – Westbound North Fraser Way @ 5100 Block
* `61421` – Eastbound North Fraser Way @ 5100 Block
* `61423` – Eastbound North Fraser Way @ Glenlyon Parkway East
* `61427` – Northbound Oxford St @ Goggs Ave
* `61436` – Northbound Westwood St @ Kitchener Ave
* `61439` – Southbound Bonson Rd @ Pitt Meadows Athletic Park
* `61440` – Southbound Bonson Rd @ Bruce Dr
* `61441` – Southbound Bonson Rd @ Hoffmann Way
* `61442` – Eastbound Fraser Way @ Bay Mill Rd
* `61445` – Westbound Brunswick Rd @ Hwy 99 Overpass
* `61452` – Westbound Lougheed Hwy @ Old Dewdney Trunk Rd
* `61480` – Southbound No. 5 Rd @ Rice Mill Rd
* `61489` – Eastbound Ovens Ave @ Colborne St
* `61510` – Northbound Capilano Rd @ Curling Rd
* `61515` – Eastbound United Blvd @ King Edward St
* `61516` – Eastbound 96 Ave @ Prince Charles Blvd
* `61518` – Westbound E Pender St @ Main St
* `61526` – Northbound No. 3 Rd @ Jones Rd
* `61527` – Northbound Horseshoe Bay Dr @ Pasco Rd
* `61528` – Northbound Hwy 99 Onramp @ Kelvin Grove Way
* `61531` – Southbound Isleview Place @ Lions Bay Ave
* `61541` – Northbound 194 St @ 64 Ave
* `61548` – Southbound 208 St @ 52A Ave
* `61550` – Westbound 60 Ave @ 194A St
* `61551` – Eastbound 60 Ave @ Enterprise Way
* `61552` – Eastbound McLean Ave @ Kingsway Ave
* `61567` – Eastbound Fraser Hwy @ 248 St
* `61573` – Westbound North Fraser Way @ 4300 Block
* `61575` – Westbound Westminster Hwy @ Gilley Rd
* `61578` – Northbound 152 St @ 66A Ave
* `61580` – Northbound Wesbrook Mall @ Birney Ave
* `61582` – Westbound W 3rd St @ Mission Rd
* `61585` – Southbound Coast Meridian Rd @ Mason Ave
* `61586` – Northbound Coast Meridian Rd @ Gislason Ave
* `61595` – Westbound Lougheed Hwy @ Sherling Ave
* `61613` – Eastbound Lougheed Hwy @ Woolridge Connector
* `61617` – Westbound United Blvd @ King Edward St
* `61625` – Southbound 144 St @ 67A Ave
* `61632` – Westbound 24 Ave @ 161A St
* `61634` – Westbound 24 Ave @ 164 St
* `61636` – Westbound 24 Ave @ 168 St
* `61640` – Southbound 192 St @ 26 Ave
* `61660` – Eastbound W Broadway @ Yew St
* `61667` – Westbound 32 Ave @ 200 St
* `61669` – Westbound North Fraser Way @ Northbrook Court
* `61670` – Westbound North Fraser Way @ Fraserwood Court
* `61675` – Westbound E Royal Ave @ McBride Blvd
* `61676` – Eastbound E Royal Ave @ McBride Blvd
* `61685` – Eastbound Lougheed Hwy @ Kennedy Rd
* `61690` – Northbound East Rd @ 1400 Block
* `61691` – Southbound East Rd @ 1400 Block
* `61703` – Newton Exchange @ Bay 11
* `61731` – Westbound 16 Ave @ Kent St
* `61745` – Eastbound Como Lake Ave @ Banting St
* `61746` – Westbound Como Lake Ave @ Banting St
* `61763` – Northbound 192 St @ 68 Ave
* `61764` – Westbound Fraser Hwy @ 150 St
* `61776` – Eastbound Marine Dr @ South Mall Access
* `61785` – Westbound Westminster Hwy @ No. 8th Rd
* `61789` – Northbound 152 St @ 40 Ave
* `61791` – Westbound 24 Ave @ 180 St
* `61793` – Westbound 24 Ave @ 184 St
* `61800` – Victoria Dr @ 54th Avenue Loop
* `61807` – Southbound Willingdon Ave @ Halifax St
* `61832` – Southbound East Mall @ Eagles Dr
* `61837` – Southbound Noons Creek Dr @ David Ave
* `61838` – Northbound Noons Creek Dr @ David Ave
* `61844` – Northbound Coast Meridian Rd @ Apel Dr
* `61845` – Southbound Coast Meridian Rd @ Chelsea Ave
* `61846` – Northbound Coast Meridian Rd @ Lincoln Dr
* `61847` – Northbound Coast Meridian Rd @ Angelo Ave
* `61848` – Northbound 192A St @ Davison Rd
* `61849` – Westbound Park Rd @ 193 St
* `61850` – Westbound Park Rd @ 194B St
* `61851` – Southbound Park Rd @ Somerset Dr
* `61852` – Westbound 122 Ave @ 191B St
* `61853` – Westbound 122 Ave @ 190 St
* `61856` – Eastbound Ford Rd @ 189B St
* `61857` – Southbound 190A St @ 119B Ave
* `61858` – Eastbound 119 Ave @ 190A St
* `61859` – Northbound 190A St @ 119 Ave
* `61860` – Northbound 190A St @ 119B Ave
* `61861` – Westbound 119 Ave @ 19100 Block
* `61862` – Southbound Blakely Rd @ Hammond Rd
* `61863` – Southbound Blakely Rd @ 116B Ave
* `61864` – Eastbound 116B Ave @ 195A St
* `61865` – Southbound Bonson Rd @ 116A Ave
* `61866` – Northbound Bonson Rd @ 114B Ave
* `61867` – Eastbound S Wildwood Cres @ 19800 Block
* `61868` – Northbound Wildwood Cres @ Wildwood Place
* `61869` – Westbound N Wildwood Cres @ 19800 Block
* `61872` – Eastbound McClure Dr @ 24100 Block
* `61873` – Westbound McClure Dr @ 106 Ave
* `61874` – Westbound McClure Dr @ 243 St
* `61875` – Eastbound McClure Dr @ 243 St
* `61876` – Westbound McClure Dr @ 244 St
* `61877` – Eastbound McClure Dr @ 244 St
* `61878` – Eastbound McClure Dr @ 245 St
* `61879` – Westbound McClure Dr @ 245 St
* `61880` – Eastbound McClure Dr @ 245B St
* `61881` – Westbound McClure Dr @ 245B St
* `61882` – Eastbound McClure Dr @ 24700 Block
* `61883` – Westbound McClure Dr @ Kimola Dr
* `61884` – Northbound 248 St @ 106B Ave
* `61885` – Southbound 248 St @ 106B Ave
* `61887` – Northbound Jackson Rd @ 105A Ave
* `61893` – Westbound Ross Dr @ Birney Ave
* `61897` – Westbound River Rd @ 6100 Block
* `61898` – Northbound Shaughnessy St @ Central Ave
* `61902` – Westbound 72 Ave @ 146 St
* `61903` – Eastbound 72 Ave @ 146 St
* `61905` – Westbound 24 Ave @ 172 St
* `61906` – Westbound 32 Ave @ 196 St
* `61908` – Westbound 24 Ave @ 176 St
* `61925` – Waterfront Station
* `61928` – Southbound Glover Rd @ Telegraph Trail
* `61929` – Northbound Glover Rd @ Telegraph Trail
* `61931` – Eastbound Oxford Connector @ Oxford St
* `61938` – Westbound Quayside Dr @ Reliance Court
* `61939` – Southbound 128 St @ 7000 Block
* `61944` – Westbound Mufford Cres @ 62 Ave
* `61945` – Eastbound Mufford Cres @ 62 Ave
* `61947` – Westbound 54 Ave @ Production Blvd
* `61948` – Westbound 54 Ave @ 192 St
* `61949` – Eastbound Queens Ave @ 27 St
* `61951` – Eastbound Queens Ave @ 21 St
* `61953` – Northbound Maple St @ Elgin Ave
* `61956` – Eastbound 105 Ave @ Lincoln Dr
* `61970` – Westbound North Fraser Way @ 8200 Block
* `61971` – Eastbound North Fraser Way @ 8200 Block
* `61975` – Eastbound North Fraser Way @ 8000 Block
* `61976` – Westbound North Fraser Way @ Tillicum St
* `61977` – Eastbound North Fraser Way @ Tillicum St
* `61982` – Southbound 168 St @ 66 Ave

## Existing stops (4448)
* `50001` – Westbound Davie St @ Bidwell St
* `50011` – Eastbound Davie St @ Howe St
* `50013` – Eastbound Pacific St @ Richards St
* `50014` – Eastbound Pacific Blvd @ Homer St
* `50015` – Eastbound Pacific Blvd @ Drake St
* `50016` – Yaletown-Roundhouse Station @ Bay 4
* `50021` – Eastbound E Pender St @ Carrall St
* `50022` – Northbound Columbia St @ E Pender St
* `50023` – Yaletown-Roundhouse Station @ Bay 1
* `50024` – Westbound Davie St @ Richards St
* `50025` – Westbound Davie St @ Granville St
* `50026` – Westbound Davie St @ Howe St
* `50030` – Northbound Burrard St @ Robson St
* `50031` – Burrard Station @ Bay 7
* `50034` – Northbound Granville St @ W Hastings St
* `50035` – Waterfront Station @ Bay 3
* `50036` – Eastbound W Cordova St @ Homer St
* `50038` – Westbound Powell St @ Columbia St
* `50040` – Waterfront Station @ Bay 2
* `50043` – Burrard Station @ Bay 1
* `50044` – Southbound Burrard St @ W Georgia St
* `50045` – Southbound Burrard St @ Robson St
* `50046` – Southbound Burrard St @ Barclay St
* `50047` – Southbound Burrard St @ Nelson St
* `50048` – Westbound Davie St @ Burrard St
* `50049` – Westbound Davie St @ Thurlow St
* `50050` – Westbound Davie St @ Bute St
* `50052` – Westbound Davie St @ Broughton St
* `50053` – Westbound Davie St @ Cardero St
* `50054` – Eastbound W 16 Ave @ Trutch St
* `50055` – Eastbound W 16 Ave @ Carnarvon St
* `50056` – Northbound Macdonald St @ W 16 Ave
* `50058` – Northbound Macdonald St @ W 12 Ave
* `50060` – Northbound Macdonald St @ W Broadway
* `50061` – Northbound Macdonald St @ W 7th Ave
* `50063` – Northbound Macdonald St @ W 4th Ave
* `50064` – Northbound Macdonald St @ W 2nd Ave
* `50067` – Eastbound Cornwall Ave @ Balsam St
* `50069` – Eastbound Cornwall Ave @ Yew St
* `50070` – Eastbound Cornwall Ave @ Arbutus St
* `50072` – Eastbound Cornwall Ave @ Cypress St
* `50073` – Northbound Burrard St @ Pacific St
* `50074` – Eastbound Davie St @ Burrard St
* `50075` – Northbound Burrard St @ Davie St
* `50076` – Eastbound W Pender St @ Burrard St
* `50077` – Eastbound W Pender St @ Granville St
* `50078` – Eastbound W Pender St @ Seymour St
* `50080` – Eastbound W Pender St @ Hamilton St
* `50081` – Southbound Cambie St @ Dunsmuir St
* `50085` – Yaletown-Roundhouse Station @ Bay 3
* `50086` – Westbound Pacific Blvd @ Drake St
* `50087` – Westbound Pacific St @ Richards St
* `50091` – Westbound W Pender St @ Carrall St
* `50093` – Westbound W Pender St @ Abbott St
* `50094` – Westbound W Pender St @ Hamilton St
* `50095` – Westbound W Pender St @ Seymour St
* `50096` – Westbound W Pender St @ Granville St
* `50097` – Westbound W Pender St @ Howe St
* `50098` – Southbound Burrard St @ Davie St
* `50101` – Westbound Cornwall Ave @ Cypress St
* `50103` – Westbound Cornwall Ave @ Arbutus St
* `50104` – Westbound Cornwall Ave @ Yew St
* `50105` – Westbound Cornwall Ave @ Point Grey Rd
* `50106` – Westbound Cornwall Ave @ Trafalgar St
* `50107` – Southbound Macdonald St @ W 1st Ave
* `50109` – Southbound Macdonald St @ W 6th Ave
* `50110` – Southbound Macdonald St @ W Broadway
* `50111` – Southbound Macdonald St @ W 11 Ave
* `50112` – Southbound Macdonald St @ W 13 Ave
* `50113` – Southbound Macdonald St @ W 16 Ave
* `50114` – Westbound W 16 Ave @ Macdonald St
* `50115` – Westbound W 16 Ave @ Mackenzie St
* `50116` – Westbound W 16 Ave @ Balaclava St
* `50119` – Southbound Macdonald St @ W 21 Ave
* `50121` – Southbound Macdonald St @ Alamein Ave
* `50123` – Westbound W King Edward Ave @ Puget Dr
* `50124` – Southbound Quesnel Dr @ W King Edward Ave
* `50126` – Southbound Mackenzie St @ W 29 Ave
* `50128` – Southbound Mackenzie St @ W 33 Ave
* `50130` – Southbound Mackenzie St @ W 37 Ave
* `50132` – Eastbound W 41 Ave @ Mackenzie St
* `50133` – Westbound W 41st Ave @ Willow St
* `50134` – Eastbound W 41 Ave @ Willow St
* `50136` – Oakridge-41st Ave Station @ Bay 2
* `50137` – Eastbound W 41 Ave @ Cambie St
* `50138` – Eastbound W 41 Ave @ Columbia St
* `50139` – Eastbound W 41 Ave @ Manitoba St
* `50140` – Eastbound E 41 Ave @ Ontario St
* `50142` – Northbound Main St @ SE Marine Dr
* `50147` – Northbound Main St @ E 57 Ave
* `50151` – Northbound Main St @ E 49 Ave
* `50155` – Northbound Main St @ E 41 Ave
* `50156` – Westbound E 41 Ave @ Main St
* `50157` – Westbound W 41 Ave @ Ontario St
* `50158` – Westbound W 41 Ave @ Columbia St
* `50160` – Oakridge-41st Ave Station @ Bay 5
* `50163` – Northbound Main St @ E 36 Ave
* `50165` – Northbound Main St @ E 33 Ave
* `50166` – Northbound Main St @ E 30 Ave
* `50167` – Northbound Main St @ E 28 Ave
* `50169` – Northbound Main St @ E King Edward Ave
* `50174` – Northbound Main St @ E 12 Ave
* `50176` – Northbound Main St @ E Broadway
* `50177` – Northbound Main St @ E 6th Ave
* `50178` – Northbound Main St @ E 5th Ave
* `50179` – Northbound Main St @ E 2nd Ave
* `50180` – Northbound Main St @ Industrial Ave
* `50181` – Main Street-Science World Station @ Bay 2
* `50182` – Northbound Main St @ National Ave
* `50184` – Northbound Main St @ E Georgia St
* `50185` – Westbound E Hastings St @ Main St
* `50186` – Westbound W Hastings St @ Carrall St
* `50187` – Westbound W Hastings St @ Abbott St
* `50188` – Westbound W Hastings St @ Hamilton St
* `50189` – Southbound Richards St @ W Hastings St
* `50195` – Westbound W 41 Ave @ Oak St
* `50196` – Westbound W 41 Ave @ Selkirk St
* `50197` – Westbound W 41 Ave @ Cartier St
* `50198` – Westbound W 41 Ave @ Granville St
* `50199` – Eastbound W 41 Ave @ Granville St
* `50200` – Eastbound W 41 Ave @ Selkirk St
* `50201` – Eastbound W 41 Ave @ Osler St
* `50203` – Northbound Granville St @ W 38 Ave
* `50204` – Northbound Granville St @ W 36 Ave
* `50205` – Northbound Granville St @ W 34 Ave
* `50206` – Northbound Granville St @ Minto Cres
* `50207` – Northbound Granville St @ N Connaught Dr
* `50208` – Northbound Granville St @ Nanton Ave
* `50209` – Northbound Granville St @ W King Edward Ave
* `50210` – Northbound Granville St @ Balfour Ave
* `50211` – Northbound Granville St @ Matthews Ave
* `50212` – Northbound Granville St @ W 16 Ave
* `50214` – Northbound Granville St @ W 12 Ave
* `50217` – Northbound Granville St @ W 7th Ave
* `50218` – Northbound Granville St @ W 5th Ave
* `50220` – Northbound Granville St @ Davie St
* `50228` – Eastbound E Cordova St @ Main St
* `50229` – Southbound Main St @ E Hastings St
* `50230` – Southbound Main St @ Keefer St
* `50232` – Southbound Main St @ National Ave
* `50233` – Main Street-Science World Station @ Bay 1
* `50234` – Southbound Main St @ Switchmen St
* `50236` – Southbound Main St @ E 5th Ave
* `50240` – Southbound Main St @ E 12 Ave
* `50241` – Southbound Main St @ E 14 Ave
* `50244` – Southbound Main St @ E 20 Ave
* `50245` – Southbound Main St @ E 22 Ave
* `50246` – Southbound Main St @ E King Edward Ave
* `50249` – Southbound Main St @ E 33 Ave
* `50250` – Southbound Main St @ E 36 Ave
* `50253` – Eastbound E 41 Ave @ Main St
* `50260` – Southbound Main St @ E 56 Ave
* `50268` – Eastbound University Blvd @ Allison Rd
* `50271` – Eastbound University Blvd @ 5100 Block
* `50272` – Northbound Blanca St @ W 10 Ave
* `50273` – Blanca Loop @ Bay 1
* `50280` – Eastbound W 10 Ave @ Camosun St
* `50281` – Eastbound W 10 Ave @ Crown St
* `50282` – Eastbound W 10 Ave @ Wallace St
* `50283` – Eastbound W 10 Ave @ Highbury St
* `50284` – Westbound W 41 Ave @ Granville St
* `50285` – Westbound W 41 Ave @ Marguerite St
* `50286` – Westbound W 41 Ave @ Angus Dr
* `50288` – Westbound W 41 Ave @ Maple St
* `50290` – Westbound W 41 Ave @ Vine St
* `50291` – Westbound W 41 Ave @ Larch St
* `50292` – Westbound W 41 Ave @ Macdonald St
* `50293` – Westbound W 41 Ave @ Mackenzie St
* `50294` – Westbound W 41 Ave @ Carnarvon St
* `50296` – Westbound W 41 Ave @ Blenheim St
* `50297` – Westbound W 41 Ave @ Collingwood St
* `50298` – Dunbar Loop @ Bay 6
* `50299` – Northbound Dunbar St @ W 39 Ave
* `50300` – Northbound Dunbar St @ W 38 Ave
* `50301` – Northbound Dunbar St @ W 36 Ave
* `50302` – Northbound Dunbar St @ W 34 Ave
* `50303` – Northbound Dunbar St @ W 31 Ave
* `50304` – Northbound Dunbar St @ W 30 Ave
* `50305` – Northbound Dunbar St @ W 28 Ave
* `50307` – Northbound Dunbar St @ W King Edward Ave
* `50308` – Northbound Dunbar St @ W 22 Ave
* `50310` – Northbound Dunbar St @ W 18 Ave
* `50312` – Northbound Dunbar Diversion @ W 14 Ave
* `50313` – Northbound Alma St @ W 12 Ave
* `50314` – Northbound Alma St @ W 10 Ave
* `50315` – Eastbound W Broadway @ Alma St
* `50319` – Eastbound W Broadway @ Macdonald St
* `50320` – Eastbound W Broadway @ Trafalgar St
* `50321` – Eastbound W Broadway @ Larch St
* `50322` – Eastbound W Broadway @ Vine St
* `50323` – Eastbound W Broadway @ Arbutus St
* `50325` – Eastbound W Broadway @ Burrard St
* `50327` – Eastbound W Broadway @ Hemlock St
* `50328` – Eastbound W Broadway @ Alder St
* `50331` – Southbound Oak St @ W 12 Ave
* `50333` – Southbound Oak St @ W 16 Ave
* `50335` – Southbound Oak St @ Douglas Cres
* `50336` – Southbound Oak St @ Balfour Ave
* `50337` – Southbound Oak St @ W King Edward Ave
* `50339` – Southbound Oak St @ Devonshire Cres
* `50341` – Southbound Oak St @ W 33 Ave
* `50342` – Southbound Oak St @ W 37 Ave
* `50347` – Eastbound W 4th Ave @ Tolmie St
* `50349` – Eastbound W 4th Ave @ Trimble St
* `50350` – Eastbound W 4th Ave @ 4100 Block
* `50352` – Eastbound W 4th Ave @ Wallace St
* `50354` – Eastbound W 4th Ave @ Alma St
* `50355` – Southbound Alma St @ W 4th Ave
* `50356` – Southbound Alma St @ W 6th Ave
* `50357` – Southbound Alma St @ W 10 Ave
* `50359` – Southbound Alma St @ W 12 Ave
* `50360` – Southbound Dunbar Diversion @ W 14 Ave
* `50361` – Southbound Dunbar St @ W 15 Ave
* `50362` – Southbound Dunbar St @ W 17 Ave
* `50364` – Southbound Dunbar St @ W 21 Ave
* `50366` – Southbound Dunbar St @ W King Edward Ave
* `50367` – Southbound Dunbar St @ W 27 Ave
* `50368` – Southbound Dunbar St @ W 29 Ave
* `50369` – Southbound Dunbar St @ W 31 Ave
* `50370` – Southbound Dunbar St @ W 33 Ave
* `50371` – Southbound Dunbar St @ W 35 Ave
* `50372` – Southbound Dunbar St @ W 37 Ave
* `50374` – Eastbound W 41 Ave @ Collingwood St
* `50375` – Eastbound W 41 Ave @ Blenheim St
* `50377` – Eastbound W 41 Ave @ Carnarvon St
* `50378` – Eastbound W 41 Ave @ Macdonald St
* `50379` – Eastbound W 41 Ave @ Elm St
* `50380` – Eastbound W 41 Ave @ Balsam St
* `50381` – Eastbound W 41 Ave @ Yew St
* `50383` – Eastbound W 41 Ave @ Maple St
* `50384` – Eastbound W 41 Ave @ Cypress St
* `50385` – Eastbound W 41 Ave @ Wiltshire St
* `50386` – Eastbound W 41 Ave @ Adera St
* `50388` – Eastbound W 41 Ave @ Granville St
* `50389` – Eastbound W 4th Ave @ Collingwood St
* `50393` – Eastbound W 4th Ave @ Macdonald St
* `50394` – Eastbound W 4th Ave @ Trafalgar St
* `50396` – Eastbound W 4th Ave @ Balsam St
* `50400` – Eastbound W 4th Ave @ Burrard St
* `50401` – Eastbound W 4th Ave @ Fir St
* `50403` – Westbound W 5th Ave @ Granville St
* `50405` – Burrard Station @ Bay 2
* `50406` – Eastbound W Pender St @ Thurlow St
* `50407` – Northbound Homer St @ W Pender St
* `50408` – Northbound Seymour St @ W Hastings St
* `50410` – Southbound Cambie St @ W Hastings St
* `50411` – Eastbound Nelson St @ Cambie St
* `50412` – Southbound Cambie St @ W 7th Ave
* `50414` – Southbound Cambie St @ W 12 Ave
* `50415` – Southbound Cambie St @ W 16 Ave
* `50416` – Southbound Cambie St @ W 18 Ave
* `50417` – Southbound Cambie St @ W 20 Ave
* `50418` – Southbound Cambie St @ W 22 Ave
* `50419` – King Edward Station @ Bay 4
* `50421` – Southbound Cambie St @ W 31 Ave
* `50422` – Southbound Cambie St @ W 33 Ave
* `50423` – Southbound Cambie St @ W 35 Ave
* `50424` – Southbound Cambie St @ W 37 Ave
* `50425` – Eastbound E Cordova St @ Main St
* `50426` – Eastbound E Cordova St @ Dunlevy Ave
* `50427` – Eastbound E Cordova St @ Princess Ave
* `50429` – Eastbound Powell St @ Glen Dr
* `50431` – Eastbound Powell St @ Clark Dr
* `50433` – Eastbound Powell St @ Commercial Dr
* `50437` – Eastbound Dundas St @ Templeton Dr
* `50440` – Northbound N Nanaimo St @ Eton St
* `50442` – Eastbound McGill St @ N Penticton St
* `50445` – Westbound Eton St @ N Renfrew St
* `50446` – Eastbound McGill St @ N Renfrew St
* `50447` – Phibbs Exchange @ Bay 3
* `50448` – Phibbs Exchange @ Bay 8
* `50449` – Westbound McGill St @ N Renfrew St
* `50451` – Northbound Oak St @ W 38 Ave
* `50454` – Northbound Oak St @ Devonshire Cres
* `50455` – Northbound Oak St @ W 27 Ave
* `50456` – Northbound Oak St @ W King Edward Ave
* `50457` – Northbound Oak St @ W 22 Ave
* `50458` – Northbound Oak St @ W 20 Ave
* `50459` – Northbound Oak St @ W 17 Ave
* `50464` – Westbound W Broadway @ Spruce St
* `50465` – Westbound W Broadway @ Alder St
* `50467` – Oakridge-41st Ave Station @ Bay 6
* `50469` – Northbound Cambie St @ W 37 Ave
* `50471` – Northbound Cambie St @ W 33 Ave
* `50475` – King Edward Station @ Bay 2
* `50476` – Northbound Cambie St @ W 22 Ave
* `50477` – Northbound Cambie St @ W 20 Ave
* `50478` – Northbound Cambie St @ W 18 Ave
* `50479` – Northbound Cambie St @ W 16 Ave
* `50480` – Northbound Cambie St @ W 14 Ave
* `50481` – Northbound Cambie St @ W 12 Ave
* `50483` – Northbound Cambie St @ W 7th Ave
* `50489` – Westbound McGill St @ N Kaslo St
* `50490` – Westbound McGill St @ N Penticton St
* `50491` – Southbound N Nanaimo St @ McGill St
* `50493` – Westbound Dundas St @ N Garden Dr
* `50494` – Westbound Dundas St @ N Templeton Dr
* `50495` – Westbound Dundas St @ Wall St
* `50499` – Westbound E Hastings St @ Commercial Dr
* `50500` – Westbound E Hastings St @ McLean Dr
* `50501` – Westbound E Hastings St @ Clark Dr
* `50502` – Westbound E Hastings St @ Glen Dr
* `50503` – Westbound E Hastings St @ Campbell Ave
* `50506` – Westbound E Hastings St @ Jackson Ave
* `50507` – Westbound E Hastings St @ Dunlevy Ave
* `50508` – Westbound Powell St @ Commercial Dr
* `50512` – Westbound Powell St @ Glen Dr
* `50513` – Westbound Powell St @ Hawks Ave
* `50514` – Westbound Powell St @ Heatley Ave
* `50515` – Westbound Powell St @ Jackson Ave
* `50516` – Westbound Powell St @ Gore Ave
* `50520` – Eastbound E Pender St @ Main St
* `50526` – Southbound Granville St @ W Pender St
* `50532` – Southbound Granville St @ Nelson St
* `50535` – Southbound Granville St @ Davie St
* `50536` – Southbound Granville St @ Drake St
* `50537` – Westbound W Cloverleaf @ Granville St
* `50539` – Southbound Granville St @ W 7th Ave
* `50540` – Westbound W Broadway @ Granville St
* `50542` – Southbound Granville St @ W 11 Ave
* `50544` – Southbound Granville St @ W 15 Ave
* `50545` – Southbound Granville St @ Angus Dr
* `50546` – Southbound Granville St @ Matthews Ave
* `50547` – Southbound Granville St @ Laurier Ave
* `50550` – Southbound Granville St @ N Connaught Dr
* `50551` – Southbound Granville St @ W 32 Ave
* `50553` – Southbound Granville St @ W 35 Ave
* `50554` – Southbound Granville St @ W 37 Ave
* `50555` – Southbound Granville St @ S Connaught Dr
* `50557` – Westbound W Broadway @ Pine St
* `50558` – Westbound W Broadway @ Burrard St
* `50561` – Westbound W Broadway @ Yew St
* `50562` – Westbound W Broadway @ Balsam St
* `50563` – Westbound W Broadway @ Trafalgar St
* `50564` – Westbound W Broadway @ Macdonald St
* `50565` – Westbound W Broadway @ Bayswater St
* `50566` – Westbound W Broadway @ Balaclava St
* `50567` – Westbound W Broadway @ Blenheim St
* `50568` – Westbound W Broadway @ Collingwood St
* `50571` – Westbound W 10 Ave @ Wallace St
* `50572` – Westbound W 10 Ave @ Crown St
* `50573` – Westbound W 10 Ave @ Camosun St
* `50574` – Westbound W 10 Ave @ Courtenay St
* `50575` – Westbound W 10 Ave @ Discovery St
* `50576` – Westbound W 10 Ave @ Trimble St
* `50577` – Westbound W 10 Ave @ Tolmie St
* `50578` – Westbound W 4th Ave @ Fir St
* `50579` – Westbound W 4th Ave @ Burrard St
* `50580` – Westbound W 4th Ave @ Maple St
* `50583` – Westbound W 4th Ave @ Balsam St
* `50584` – Westbound W 4th Ave @ Trafalgar St
* `50585` – Westbound W 4th Ave @ Macdonald St
* `50587` – Westbound W 4th Ave @ Balaclava St
* `50588` – Westbound W 4th Ave @ Blenheim St
* `50589` – Westbound W 4th Ave @ Collingwood St
* `50590` – Westbound W 4th Ave @ Alma St
* `50591` – Westbound W 4th Ave @ Wallace St
* `50593` – Westbound W 4th Ave @ 4100 Block
* `50594` – Westbound W 4th Ave @ NW Marine Dr
* `50595` – Westbound W 4th Ave @ Trimble St
* `50596` – Westbound W 4th Ave @ Sasamat St
* `50598` – Southbound Blanca St @ W 4th Ave
* `50600` – Blanca Loop @ Bay 2
* `50601` – Westbound University Blvd @ Blanca St
* `50603` – Westbound University Blvd @ 5300 Block
* `50604` – Westbound University Blvd @ Acadia Rd
* `50607` – Westbound Davie St @ Denman St
* `50610` – Northbound Denman St @ Haro St
* `50611` – Eastbound Robson St @ Bidwell St
* `50614` – Eastbound Robson St @ Bute St
* `50621` – Southbound Cambie St @ W 41 Ave
* `50622` – Westbound Robson St @ Hamilton St
* `50624` – Westbound Robson St @ Burrard St
* `50626` – Westbound Robson St @ Bute St
* `50627` – Westbound Robson St @ Broughton St
* `50628` – Westbound Robson St @ Cardero St
* `50629` – Southbound Denman St @ Robson St
* `50630` – Southbound Denman St @ Barclay St
* `50631` – Southbound Denman St @ Comox St
* `50633` – Eastbound Davie St @ Denman St
* `50634` – Eastbound Davie St @ Cardero St
* `50635` – Eastbound Davie St @ Nicola St
* `50636` – Eastbound Davie St @ Jervis St
* `50637` – Eastbound Davie St @ Bute St
* `50638` – Eastbound Davie St @ Thurlow St
* `50640` – Dunbar Loop @ Bay 2
* `50642` – Eastbound E 41 Ave @ Sophia St
* `50643` – Eastbound E 41 Ave @ St. George St
* `50644` – Eastbound E 41 Ave @ Fraser St
* `50645` – Eastbound E 41 Ave @ Prince Albert St
* `50646` – Eastbound E 41 Ave @ Windsor St
* `50647` – Eastbound E 41 Ave @ Sherbrooke St
* `50650` – Eastbound E 41 Ave @ Fleming St
* `50651` – Eastbound E 41 Ave @ Bruce St
* `50652` – Northbound Victoria Dr @ E 41 Ave
* `50653` – Northbound Victoria Dr @ E 39 Ave
* `50654` – Northbound Victoria Dr @ E 38 Ave
* `50655` – Northbound Victoria Dr @ E 36 Ave
* `50656` – Northbound Victoria Dr @ E 33 Ave
* `50658` – Northbound Victoria Dr @ Kingsway
* `50659` – Eastbound Kingsway @ Victoria Dr
* `50660` – Eastbound Kingsway @ Nanaimo St
* `50661` – Eastbound Kingsway @ Slocan St
* `50662` – Northbound Slocan St @ Kingsway
* `50663` – Northbound Slocan St @ Ward St
* `50664` – Northbound Slocan St @ Euclid Ave
* `50665` – Westbound E 22 Ave @ Slocan St
* `50668` – Northbound Alma St @ W 7th Ave
* `50671` – Southbound Nanaimo St @ E Hastings St
* `50675` – Southbound Nanaimo St @ William St
* `50678` – Southbound Nanaimo St @ E 1st Ave
* `50680` – Southbound Nanaimo St @ E 5th Ave
* `50682` – Westbound E Broadway @ Nanaimo St
* `50683` – Southbound Nanaimo St @ E Broadway
* `50685` – Southbound Nanaimo St @ E 17 Ave
* `50686` – Southbound Nanaimo St @ E 20 Ave
* `50687` – Southbound Nanaimo St @ E 22 Ave
* `50688` – Nanaimo Station @ Bay 3
* `50689` – Westbound E Pender St @ Main St
* `50690` – Eastbound E 22 Ave @ Nanaimo St
* `50692` – Southbound Slocan St @ E 29 Ave
* `50693` – Southbound Slocan St @ Norquay St
* `50694` – Westbound Kingsway @ Slocan St
* `50695` – Westbound Kingsway @ Nanaimo St
* `50697` – Southbound Victoria Dr @ Kingsway
* `50699` – Southbound Victoria Dr @ E 33 Ave
* `50700` – Southbound Victoria Dr @ E 36 Ave
* `50702` – Westbound E 41 Ave @ Victoria Dr
* `50703` – Westbound E 41 Ave @ Commercial St
* `50706` – Westbound E 41 Ave @ Knight St
* `50707` – Westbound E 41 Ave @ Inverness St
* `50708` – Westbound E 41 Ave @ Windsor St
* `50709` – Westbound E 41 Ave @ St. Catherines St
* `50710` – Westbound E 41 Ave @ Chester St
* `50711` – Westbound E 41 Ave @ Fraser St
* `50712` – Westbound E 41 Ave @ St. George St
* `50713` – Westbound E 41 Ave @ Prince Edward St
* `50715` – Northbound Nanaimo St @ E 22 Ave
* `50717` – Northbound Nanaimo St @ E 17 Ave
* `50718` – Northbound Nanaimo St @ S Grandview Hwy
* `50720` – Northbound Nanaimo St @ E 11 Ave
* `50721` – Northbound Nanaimo St @ E Broadway
* `50722` – Northbound Nanaimo St @ E 6th Ave
* `50724` – Northbound Nanaimo St @ E 1st Ave
* `50726` – Northbound Nanaimo St @ Charles St
* `50730` – Westbound E Hastings St @ Nanaimo St
* `50731` – Westbound E Hastings St @ Templeton Dr
* `50733` – Westbound E Hastings St @ Victoria Dr
* `50735` – Northbound Nanaimo St @ E Hastings St
* `50737` – Dunbar Loop @ Bay 1
* `50738` – Marpole Loop @ Bay 1
* `50742` – Northbound Oak St @ W 70 Ave
* `50751` – Northbound Oak St @ W 49 Ave
* `50753` – Northbound Oak St @ W 43 Ave
* `50754` – Northbound Oak St @ W 41 Ave
* `50755` – Westbound SW Marine Dr @ Montcalm St
* `50756` – Westbound SW Marine Dr @ Granville St
* `50757` – Northbound Granville St @ W 71 Ave
* `50758` – Northbound Granville St @ W 70 Ave
* `50759` – Northbound Granville St @ W 67 Ave
* `50760` – Northbound Granville St @ W 64 Ave
* `50761` – Northbound Granville St @ W 62 Ave
* `50763` – Northbound Granville St @ W 57 Ave
* `50764` – Northbound Granville St @ W 55 Ave
* `50767` – Northbound Granville St @ W 49 Ave
* `50769` – Northbound Granville St @ W 46 Ave
* `50770` – Northbound Granville St @ W 43 Ave
* `50771` – Eastbound Kingsway @ Main St
* `50772` – Eastbound E Broadway @ Kingsway
* `50773` – Eastbound E Broadway @ Prince Edward St
* `50774` – Eastbound E Broadway @ St. George St
* `50779` – Southbound Fraser St @ E 19 Ave
* `50780` – Southbound Fraser St @ E 21 Ave
* `50781` – Southbound Fraser St @ E 23 Ave
* `50786` – Southbound Fraser St @ E 33 Ave
* `50789` – Southbound Fraser St @ E 39 Ave
* `50790` – Southbound Fraser St @ E 41 Ave
* `50791` – Southbound Fraser St @ E 43 Ave
* `50792` – Southbound Fraser St @ E 45 Ave
* `50793` – Southbound Fraser St @ E 47 Ave
* `50794` – Southbound Fraser St @ E 49 Ave
* `50795` – Southbound Fraser St @ E 51 Ave
* `50801` – Southbound Fraser St @ E 63 Ave
* `50802` – Southbound Fraser St @ E 65 Ave
* `50804` – Eastbound W 41 Ave @ Oak St
* `50805` – Southbound Oak St @ W 41 Ave
* `50806` – Southbound Oak St @ W 43 Ave
* `50808` – Southbound Oak St @ W 49 Ave
* `50816` – Southbound Oak St @ W 70 Ave
* `50820` – Northbound Fraser St @ E 65 Ave
* `50822` – Northbound Fraser St @ E 60 Ave
* `50824` – Northbound Fraser St @ E 54 Ave
* `50825` – Northbound Fraser St @ E 52 Ave
* `50828` – Northbound Fraser St @ E 47 Ave
* `50829` – Northbound Fraser St @ E 45 Ave
* `50830` – Northbound Fraser St @ E 43 Ave
* `50831` – Northbound Fraser St @ E 41 Ave
* `50832` – Northbound Fraser St @ E 39 Ave
* `50833` – Northbound Fraser St @ E 37 Ave
* `50834` – Northbound Fraser St @ E 35 Ave
* `50835` – Northbound Fraser St @ E 33 Ave
* `50840` – Northbound Fraser St @ E King Edward Ave
* `50841` – Northbound Fraser St @ E 22 Ave
* `50848` – Westbound E Broadway @ St. George St
* `50849` – Westbound E Broadway @ Prince Edward St
* `50850` – Westbound E Broadway @ Scotia St
* `50852` – Westbound W Hastings St @ Richards St
* `50853` – Southbound Granville St @ W 41 Ave
* `50854` – Southbound Granville St @ W 43 Ave
* `50855` – Southbound Granville St @ W 47 Ave
* `50856` – Southbound Granville St @ W 49 Ave
* `50857` – Southbound Granville St @ W 52 Ave
* `50858` – Southbound Granville St @ W 54 Ave
* `50859` – Southbound Granville St @ W 57 Ave
* `50860` – Southbound Granville St @ W 59 Ave
* `50861` – Southbound Granville St @ W 61 Ave
* `50862` – Southbound Granville St @ W 63 Ave
* `50863` – Southbound Granville St @ W 65 Ave
* `50864` – Southbound Granville St @ W 68 Ave
* `50865` – Southbound Granville St @ W 70 Ave
* `50866` – Southbound Granville St @ SW Marine Dr
* `50873` – Eastbound W Broadway @ Willow St
* `50878` – Eastbound E Broadway @ Ontario St
* `50880` – Eastbound E Broadway @ Kingsway St
* `50883` – Eastbound E Broadway @ St. Catherines St
* `50884` – Eastbound E Broadway @ Glen Dr
* `50885` – Eastbound E Broadway @ Clark Dr
* `50886` – Eastbound E Broadway @ Woodland Dr
* `50888` – Eastbound E Broadway @ Victoria Dr
* `50889` – Eastbound E Broadway @ Lakewood Dr
* `50890` – Eastbound E Broadway @ Garden Dr
* `50891` – Eastbound E Broadway @ Nanaimo St
* `50892` – Eastbound E Broadway @ Penticton St
* `50893` – Eastbound E Broadway @ Slocan St
* `50894` – Eastbound E Broadway @ Kaslo St
* `50895` – Eastbound E Broadway @ Renfrew St
* `50899` – Eastbound E Broadway @ Rupert St
* `50900` – Eastbound Lougheed Hwy @ Skeena St
* `50901` – Lougheed Hwy @ Boundary Loop
* `50902` – Westbound Lougheed Hwy @ Skeena St
* `50904` – Westbound E Broadway @ Rupert St
* `50907` – Westbound E Broadway @ Renfrew St
* `50908` – Westbound E Broadway @ Slocan St
* `50909` – Westbound E Broadway @ Penticton St
* `50910` – Westbound E Broadway @ Kamloops St
* `50911` – Westbound E Broadway @ Lakewood Dr
* `50913` – Commercial-Broadway Station @ Bay 5
* `50914` – Westbound E Broadway @ Woodland Dr
* `50915` – Westbound E Broadway @ McLean Dr
* `50916` – Westbound E Broadway @ Clark Dr
* `50917` – Westbound E Broadway @ Glen Dr
* `50919` – Westbound E Broadway @ Prince Albert St
* `50920` – Westbound W Broadway @ Ontario St
* `50921` – Westbound W Broadway @ Columbia St
* `50924` – Westbound W Broadway @ Ash St
* `50925` – Westbound W Broadway @ Heather St
* `50926` – Westbound W Broadway @ Willow St
* `50930` – Eastbound W Hastings St @ Seymour St
* `50931` – Eastbound W Hastings St @ Homer St
* `50934` – Eastbound E Hastings St @ Main St
* `50935` – Eastbound E Hastings St @ Gore Ave
* `50936` – Eastbound E Hastings St @ Jackson Ave
* `50937` – Eastbound E Hastings St @ Princess Ave
* `50938` – Eastbound E Hastings St @ Hawks Ave
* `50939` – Eastbound E Hastings St @ Campbell Ave
* `50940` – Eastbound E Hastings St @ Glen Dr
* `50941` – Eastbound E Hastings St @ Clark Dr
* `50943` – Eastbound E Hastings St @ Commercial Dr
* `50944` – Eastbound E Hastings St @ Victoria Dr
* `50945` – Eastbound E Hastings St @ Lakewood Dr
* `50946` – Eastbound E Hastings St @ Templeton Dr
* `50947` – Eastbound E Hastings St @ Nanaimo St
* `50948` – Eastbound E Hastings St @ Kamloops St
* `50949` – Eastbound E Hastings St @ Slocan St
* `50950` – Eastbound E Hastings St @ Kaslo St
* `50951` – Eastbound E Hastings St @ Renfrew St
* `50952` – Eastbound E Hastings St @ Lillooet St
* `50953` – Eastbound E Hastings St @ Windermere St
* `50954` – Eastbound E Hastings St @ Cassiar St
* `50955` – Kootenay Loop @ Bay 5
* `50956` – Westbound E Hastings St @ Skeena St
* `50957` – Westbound E Hastings St @ Cassiar St
* `50958` – Westbound E Hastings St @ Windermere St
* `50959` – Westbound E Hastings St @ Renfrew St
* `50963` – Commercial-Broadway Station @ Bay 2
* `50968` – Southbound Victoria Dr @ Victoria Diversion
* `50970` – Southbound Victoria Dr @ E 22 Ave
* `50971` – Southbound Victoria Dr @ E 26 Ave
* `50972` – Eastbound W Georgia St @ Denman St
* `50973` – Eastbound W Georgia St @ Broughton St
* `50974` – Eastbound W Georgia St @ Bute St
* `50976` – Eastbound W Georgia St @ Granville St
* `50978` – Westbound W Hastings St @ Granville St
* `50980` – Lonsdale Quay @ Bay 5
* `50981` – Oakridge-41st Ave Station @ Bay 1
* `50983` – Southbound Cambie St @ W 45 Ave
* `50985` – Langara-49th Station @ Bay 4
* `50990` – Southbound Cambie St @ W 62 Ave
* `50992` – Northbound Cambie St @ W 64 Ave
* `50999` – Langara-49th Station @ Bay 1
* `51003` – Northbound Cambie St @ W 42 Ave
* `51004` – Northbound Seymour St @ Davie St
* `51006` – Northbound Seymour St @ Nelson St
* `51008` – Southbound Granville St @ W 45 Ave
* `51010` – Westbound W 64 Ave @ Adera St
* `51012` – Northbound Angus Dr @ W 61 Ave
* `51013` – Northbound West Blvd @ W 60 Ave
* `51014` – Northbound West Blvd @ W 57 Ave
* `51015` – Northbound West Blvd @ W 54 Ave
* `51016` – Northbound West Blvd @ W 51 Ave
* `51017` – Northbound Arbutus St @ W 51 Ave
* `51018` – Northbound West Blvd @ W 49 Ave
* `51019` – Northbound West Blvd @ W 46 Ave
* `51020` – Northbound West Blvd @ W 45 Ave
* `51021` – Northbound West Blvd @ W 43 Ave
* `51022` – Northbound West Blvd @ W 41 Ave
* `51023` – Northbound West Blvd @ W 39 Ave
* `51024` – Northbound West Blvd @ W 37 Ave
* `51027` – Northbound Arbutus St @ W 33 Ave
* `51028` – Northbound Arbutus St @ Valley Dr
* `51029` – Northbound Arbutus St @ Nanton Ave
* `51030` – Northbound Arbutus St @ W King Edward Ave
* `51032` – Northbound Arbutus St @ W 20 Ave
* `51034` – Northbound Arbutus St @ W 15 Ave
* `51037` – Southbound Renfrew St @ E Hastings St
* `51038` – Southbound Renfrew St @ E Georgia St
* `51039` – Southbound Renfrew St @ Venables St
* `51042` – Southbound Renfrew St @ E 1st Ave
* `51043` – Southbound Renfrew St @ E 4th Ave
* `51044` – Southbound Renfrew St @ E 5th Ave
* `51045` – Southbound Renfrew St @ E 8th Ave
* `51046` – Southbound Renfrew St @ E Broadway
* `51048` – Southbound Renfrew St @ Grandview Hwy
* `51049` – Southbound Renfrew St @ E 15 Ave
* `51050` – Southbound Renfrew St @ E 17 Ave
* `51051` – Southbound Renfrew St @ E 20 Ave
* `51052` – Southbound Boyd Diversion @ E 22 Ave
* `51053` – Southbound Nootka St @ E 25 Ave
* `51054` – Southbound Nootka St @ E 27 Ave
* `51055` – Westbound E 29 Ave @ Nootka St
* `51056` – 29th Avenue Station @ Bay 3
* `51057` – Southbound Earles St @ E 29 Ave
* `51058` – Southbound Earles St @ Vanness Ave
* `51059` – Southbound Earles St @ Euclid Ave
* `51060` – Southbound Earles St @ Horley St
* `51061` – Northbound Earles St @ Kingsway
* `51062` – Westbound Kingsway @ Earles St
* `51063` – Eastbound 24 Ave @ 161A St
* `51065` – Southbound Clarendon St @ E 38 Ave
* `51066` – Southbound Clarendon St @ E 40 Ave
* `51067` – Westbound E 41 Ave @ Clarendon St
* `51068` – Westbound E 41 Ave @ Nanaimo St
* `51071` – Eastbound Kingsway @ Wales St
* `51072` – Northbound Earles St @ Horley St
* `51073` – Northbound Earles St @ Euclid Ave
* `51074` – Northbound Earles St @ Kings Ave
* `51075` – Northbound Nootka St @ E 26 Ave
* `51076` – Northbound Boyd Diversion @ Nootka St
* `51077` – Northbound Boyd Diversion @ E 22 Ave
* `51078` – Northbound Renfrew St @ E 21 Ave
* `51079` – Northbound Renfrew St @ E 17 Ave
* `51080` – Northbound Renfrew St @ E 16 Ave
* `51081` – Northbound Renfrew St @ Grandview Hwy
* `51082` – Renfrew Station @ Bay 3
* `51083` – Northbound Renfrew St @ E Broadway
* `51084` – Northbound Renfrew St @ E 7th Ave
* `51085` – Northbound Renfrew St @ E 5th Ave
* `51086` – Northbound Renfrew St @ E 3rd Ave
* `51087` – Northbound Renfrew St @ E 1st Ave
* `51088` – Northbound Renfrew St @ Grant St
* `51089` – Northbound Renfrew St @ Charles St
* `51090` – Northbound Renfrew St @ Venables St
* `51091` – Northbound Renfrew St @ E Georgia St
* `51092` – Northbound Renfrew St @ E Pender St
* `51094` – Southbound Arbutus St @ W 12 Ave
* `51095` – Southbound Arbutus St @ W 14 Ave
* `51096` – Southbound Arbutus St @ W 16 Ave
* `51097` – Southbound Arbutus St @ W 19 Ave
* `51098` – Southbound Arbutus St @ W 21 Ave
* `51099` – Southbound Arbutus St @ W King Edward Ave
* `51102` – Southbound Arbutus St @ Valley Dr
* `51103` – Southbound Arbutus St @ W 32 Ave
* `51104` – Southbound Arbutus St @ W 35 Ave
* `51105` – Southbound West Blvd @ W 37 Ave
* `51106` – Southbound West Blvd @ W 39 Ave
* `51107` – Southbound West Blvd @ W 41 Ave
* `51111` – Southbound Arbutus St @ W 49 Ave
* `51112` – Eastbound W 51 Ave @ Arbutus St
* `51114` – Southbound West Blvd @ W 57 Ave
* `51118` – Eastbound W 64 Ave @ East Blvd
* `51120` – Marpole Loop @ Bay 2
* `51121` – Eastbound E 41 Ave @ Victoria Dr
* `51122` – Eastbound E 41 Ave @ Gladstone St
* `51123` – Eastbound E 41 Ave @ Nanaimo St
* `51124` – Eastbound E 41 Ave @ Clarendon St
* `51125` – Eastbound E 41 Ave @ Wales St
* `51126` – Eastbound E 41 Ave @ Earles St
* `51127` – Eastbound E 41 Ave @ Killarney St
* `51128` – Eastbound E 41 Ave @ Rupert St
* `51129` – Eastbound E 41 Ave @ Kerr St
* `51130` – Eastbound E 41 Ave @ School Ave
* `51133` – Westbound Keefer St @ Columbia St
* `51134` – Eastbound Kingsway @ E Broadway
* `51135` – Eastbound Kingsway @ E 12 Ave
* `51136` – Eastbound Kingsway @ E 13 Ave
* `51137` – Eastbound Kingsway @ Carolina St
* `51140` – Eastbound Kingsway @ Glen Dr
* `51142` – Eastbound Kingsway @ Knight St
* `51143` – Eastbound Kingsway @ Dumfries St
* `51144` – Eastbound Kingsway @ Perry St
* `51147` – Eastbound Kingsway @ Earles St
* `51148` – Eastbound Kingsway @ Killarney St
* `51150` – Eastbound Kingsway @ McKinnon St
* `51151` – Eastbound Kingsway @ Joyce St
* `51152` – Eastbound Kingsway @ Wessex St
* `51153` – Eastbound Kingsway @ Tyne St
* `51154` – Eastbound Kingsway @ Melbourne St
* `51155` – Eastbound Kingsway @ Lincoln St
* `51156` – Eastbound Kingsway @ Boundary Rd
* `51158` – Eastbound Kingsway @ Inman Ave
* `51159` – Eastbound Kingsway @ Patterson Ave
* `51160` – Eastbound Kingsway @ Olive Ave
* `51161` – Eastbound Kingsway @ Wilson Ave
* `51162` – Southbound Willingdon Ave @ Kemp St
* `51163` – Eastbound Central Blvd @ Willingdon Ave
* `51164` – Metrotown Station @ Bay 2
* `51165` – Northbound Willingdon Ave @ Central Blvd
* `51166` – Westbound Kingsway @ Willingdon Ave
* `51167` – Westbound Kingsway @ Wilson Ave
* `51168` – Westbound Kingsway @ Barker Ave
* `51169` – Westbound Kingsway @ Inman Ave
* `51170` – Westbound Kingsway @ Smith Ave
* `51171` – Westbound Kingsway @ Boundary Rd
* `51172` – Westbound Kingsway @ Boundary Rd
* `51173` – Westbound Kingsway @ Lincoln St
* `51174` – Westbound Kingsway @ Melbourne St
* `51176` – Westbound Kingsway @ Wessex St
* `51177` – Westbound Kingsway @ Joyce St
* `51178` – Southbound Joyce St @ Kingsway
* `51179` – Westbound E 41 Ave @ School Ave
* `51180` – Westbound E 41 Ave @ Kerr St
* `51181` – Westbound E 41 Ave @ Rupert St
* `51182` – Westbound E 41 Ave @ Killarney St
* `51183` – Westbound E 41 Ave @ Earles St
* `51184` – Westbound Kingsway @ McKinnon St
* `51185` – Westbound Kingsway @ Spencer St
* `51186` – Westbound Kingsway @ Rupert St
* `51187` – Westbound Kingsway @ Fairmont St
* `51189` – Westbound Kingsway @ Victoria Dr
* `51190` – Westbound Kingsway @ Miller St
* `51193` – Westbound Kingsway @ Knight St
* `51194` – Westbound Kingsway @ Inverness St
* `51195` – Westbound Kingsway @ Windsor St
* `51197` – Westbound Kingsway @ Fraser St
* `51198` – Westbound Kingsway @ St. George St
* `51199` – Westbound Kingsway @ E 12 Ave
* `51202` – Westbound Harrison Dr @ Victoria Dr
* `51203` – Northbound Victoria Dr @ Fraserview Dr
* `51204` – Northbound Victoria Dr @ E 61 Ave
* `51205` – Northbound Victoria Dr @ Brigadoon Ave
* `51206` – Northbound Victoria Dr @ Upland Dr
* `51207` – Northbound Victoria Dr @ E 54 Ave
* `51208` – Northbound Victoria Dr @ E 51 Ave
* `51209` – Northbound Victoria Dr @ E 49 Ave
* `51213` – Northbound Victoria Dr @ E 26 Ave
* `51215` – Northbound Victoria Dr @ E 22 Ave
* `51216` – Northbound Victoria Diversion @ Victoria Dr
* `51219` – Northbound Commercial Dr @ E 14 Ave
* `51220` – Northbound Commercial Dr @ E 12 Ave
* `51225` – Northbound Commercial Dr @ E 3rd Ave
* `51226` – Northbound Commercial Dr @ E 1st Ave
* `51227` – Northbound Commercial Dr @ Grant St
* `51228` – Northbound Commercial Dr @ Charles St
* `51229` – Northbound Commercial Dr @ Napier St
* `51232` – Northbound Commercial Dr @ E Georgia St
* `51233` – Northbound Commercial Dr @ E Pender St
* `51234` – Southbound Commercial Dr @ E Hastings St
* `51235` – Southbound Commercial Dr @ Frances St
* `51236` – Southbound Commercial Diversion @ Adanac St
* `51237` – Southbound Commercial Dr @ Parker St
* `51238` – Southbound Commercial Dr @ William St
* `51239` – Southbound Commercial Dr @ Kitchener St
* `51240` – Southbound Commercial Dr @ Graveley St
* `51241` – Southbound Commercial Dr @ E 2nd Ave
* `51242` – Southbound Commercial Dr @ E 4th Ave
* `51243` – Southbound Commercial Dr @ E 6th Ave
* `51244` – Southbound Victoria Dr @ E 41 Ave
* `51248` – Southbound Victoria Dr @ E 49 Ave
* `51249` – Southbound Victoria Dr @ E 51 Ave
* `51250` – Southbound Victoria Dr @ E 53 Ave
* `51251` – Southbound Victoria Dr @ Argyle Dr
* `51252` – Southbound Victoria Dr @ Upland Dr
* `51254` – Southbound Victoria Dr @ Newport Ave
* `51255` – Southbound Victoria Dr @ Thornhill Dr
* `51256` – Southbound Victoria Dr @ E 64 Ave
* `51259` – Northbound Mackenzie St @ W 38 Ave
* `51261` – Northbound Mackenzie St @ W 34 Ave
* `51263` – Northbound Mackenzie St @ W 30 Ave
* `51265` – Northbound Quesnel Dr @ W King Edward Ave
* `51266` – Northbound Macdonald St @ W King Edward Ave
* `51267` – Northbound Macdonald St @ Oliver Cres
* `51269` – Northbound Macdonald St @ W 22 Ave
* `51272` – Eastbound E Pender St @ Main St
* `51275` – Eastbound Terminal Ave @ Western St
* `51278` – Eastbound Prior St @ Dunlevy Ave
* `51279` – Eastbound Prior St @ Princess Ave
* `51280` – Eastbound Prior St @ Hawks Ave
* `51284` – Southbound Clark Dr @ Parker St
* `51285` – Southbound Clark Dr @ William St
* `51286` – Southbound Clark Dr @ Grant St
* `51287` – Southbound Clark Dr @ E 1st Ave
* `51288` – Southbound Clark Dr @ E 3rd Ave
* `51289` – Southbound Clark Dr @ E 6th Ave
* `51290` – Southbound Clark Dr @ E Broadway
* `51291` – Southbound Clark Dr @ E 12 Ave
* `51297` – Southbound Knight St @ E King Edward Ave
* `51299` – Southbound Knight St @ E 29 Ave
* `51301` – Southbound Knight St @ E 33 Ave
* `51305` – Southbound Knight St @ E 41 Ave
* `51309` – Southbound Knight St @ E 49 Ave
* `51310` – Southbound Knight St @ E 51 Ave
* `51311` – Southbound Knight St @ E 53 Ave
* `51313` – Southbound Knight St @ E 57 Ave
* `51314` – Southbound Knight St @ E 58 Ave
* `51315` – Southbound Knight St @ E 60 Ave
* `51316` – Southbound Knight St @ E 62 Ave
* `51317` – Southbound Knight St @ E 63 Ave
* `51318` – Knight St Underpass @ SE Marine Dr
* `51319` – Southbound Knight St @ Mitchell Rd
* `51320` – Southbound Viking Way @ Bridgeport Rd
* `51324` – Eastbound Bridgeport Rd @ Sweden Way
* `51325` – Northbound Knight St @ Mitchell Rd
* `51326` – Northbound Knight St @ E 63 Ave
* `51327` – Northbound Knight St @ E 61 Ave
* `51328` – Northbound Knight St @ E 59 Ave
* `51329` – Northbound Knight St @ E 57 Ave
* `51330` – Northbound Knight St @ E 53 Ave
* `51331` – Northbound Knight St @ E 51 Ave
* `51332` – Northbound Knight St @ E 49 Ave
* `51336` – Northbound Knight St @ E 41 Ave
* `51339` – Northbound Knight St @ E 33 Ave
* `51341` – Northbound Knight St @ E 30 Ave
* `51343` – Northbound Knight St @ E King Edward Ave
* `51344` – Northbound Knight St @ Kingsway
* `51347` – Northbound Knight St @ E 17 Ave
* `51349` – Northbound Clark Dr @ E 11 Ave
* `51350` – Northbound Clark Dr @ E Broadway
* `51352` – Northbound Clark Dr @ E 3rd Ave
* `51356` – Northbound Clark Dr @ E 1st Ave
* `51357` – Northbound Clark Dr @ Grant St
* `51358` – Northbound Clark Dr @ William St
* `51368` – Southbound Macdonald St @ W 4th Ave
* `51370` – Eastbound W Pender St @ Nicola St
* `51371` – Eastbound W Pender St @ Broughton St
* `51372` – Eastbound W Pender St @ Jervis St
* `51373` – Eastbound W Pender St @ Bute St
* `51374` – Eastbound W Hastings St @ Granville St
* `51377` – Eastbound Hastings St @ Ingleton Ave
* `51381` – Eastbound Hastings St @ Willingdon Ave
* `51382` – Southbound Willingdon Ave @ Pender St
* `51383` – Southbound Willingdon Ave @ Frances St
* `51384` – Southbound Willingdon Ave @ Union St
* `51385` – Southbound Willingdon Ave @ Parker St
* `51386` – Southbound Willingdon Ave @ William St
* `51389` – Brentwood Station @ Bay 7
* `51390` – Southbound Willingdon Ave @ Dawson St
* `51392` – Eastbound Canada Way @ Willingdon Ave
* `51395` – Southbound Canada Way @ Hardwick St
* `51398` – Eastbound Canada Way @ Deer Lake Parkway
* `51399` – Eastbound Canada Way @ Ledger Ave
* `51401` – Eastbound Canada Way @ Sperling Ave
* `51402` – Eastbound Canada Way @ Rugby St
* `51403` – Southbound Canada Way @ Haszard St
* `51410` – Southbound Canada Way @ Rosewood St
* `51411` – Southbound Canada Way @ Edmonds St
* `51412` – Southbound Canada Way @ 18 Ave
* `51413` – Southbound Canada Way @ 16 Ave
* `51414` – Southbound Canada Way @ 14 Ave
* `51415` – Southbound Canada Way @ 12 Ave
* `51416` – Southbound 8th St @ 10 Ave
* `51417` – Southbound 8th St @ 8th Ave
* `51418` – Southbound 8th St @ 7th Ave
* `51419` – Southbound 8th St @ 5th Ave
* `51420` – Southbound 8th St @ 4th Ave
* `51421` – Southbound 8th St @ 3rd Ave
* `51422` – Southbound 8th St @ Queens Ave
* `51423` – Southbound 8th St @ Royal Ave
* `51425` – Northbound 8th St @ Royal Ave
* `51426` – Northbound 8th St @ Queens Ave
* `51427` – Northbound 8th St @ 3rd Ave
* `51428` – Northbound 8th St @ 4th Ave
* `51429` – Northbound 8th St @ 5th Ave
* `51430` – Northbound 8th St @ 6th Ave
* `51432` – Northbound 8th St @ 8th Ave
* `51433` – Northbound 8th St @ 10 Ave
* `51434` – Northbound Canada Way @ 12 Ave
* `51435` – Northbound Canada Way @ 14 Ave
* `51436` – Northbound Canada Way @ 16 Ave
* `51437` – Northbound Canada Way @ 18 Ave
* `51439` – Northbound Canada Way @ Wedgewood St
* `51440` – Northbound Canada Way @ Elwell St
* `51448` – Westbound Canada Way @ Ledger Ave
* `51449` – Westbound Canada Way @ Norland Ave
* `51455` – Westbound Canada Way @ Wayburne Dr
* `51456` – Westbound Canada Way @ Beta Ave
* `51457` – Westbound Canada Way @ Willingdon Ave
* `51458` – Northbound Willingdon Ave @ Juneau St
* `51463` – Northbound Willingdon Ave @ Parker St
* `51467` – Westbound Hastings St @ Gilmore Ave
* `51469` – Westbound Hastings St @ Esmond Ave
* `51470` – Kootenay Loop @ Bay 6
* `51471` – Westbound W Pender St @ Thurlow St
* `51472` – Westbound W Pender St @ Bute St
* `51473` – Westbound W Pender St @ Broughton St
* `51474` – Westbound W Georgia St @ Cardero St
* `51475` – Westbound W Georgia St @ Denman St
* `51476` – Westbound W Georgia St @ Gilford St
* `51477` – Westbound Stanley Park Dr @ Pipeline Rd
* `51479` – Southbound Wesbrook Mall @ Agronomy Rd
* `51480` – Southbound Wesbrook Mall @ Thunderbird Blvd
* `51483` – Southbound Wesbrook Mall @ Hampton Place
* `51484` – Eastbound W 16 Ave @ Wesbrook Mall
* `51485` – Eastbound W 16 Ave @ Pacific Spirit Park
* `51486` – Eastbound W 16 Ave @ Blanca St
* `51487` – Eastbound W 16 Ave @ Tolmie St
* `51488` – Eastbound W 16 Ave @ Trimble St
* `51491` – Eastbound W 16 Ave @ Crown St
* `51492` – Eastbound W 16 Ave @ Highbury St
* `51493` – Eastbound W King Edward Ave @ Dunbar St
* `51495` – Eastbound W King Edward Ave @ Blenheim St
* `51498` – Eastbound W King Edward Ave @ Puget Dr
* `51499` – Eastbound W King Edward Ave @ Brakenridge St
* `51500` – Eastbound W King Edward Ave @ Valley Dr
* `51502` – Eastbound W King Edward Ave @ Arbutus St
* `51507` – Eastbound W King Edward Ave @ Granville St
* `51510` – Eastbound W King Edward Ave @ Oak St
* `51513` – King Edward Station @ Bay 3
* `51518` – Eastbound E King Edward Ave @ Main St
* `51520` – Eastbound E King Edward Ave @ St. George St
* `51521` – Eastbound E King Edward Ave @ Fraser St
* `51523` – Eastbound E King Edward Ave @ Glen Dr
* `51525` – Eastbound E King Edward Ave @ Knight St
* `51526` – Northbound Nanaimo St @ Kingsway
* `51527` – Northbound Nanaimo St @ Brock St
* `51530` – Eastbound E 22 Ave @ Slocan St
* `51532` – Eastbound E 22 Ave @ Boyd Diversion
* `51533` – Eastbound E 22 Ave @ Lillooet St
* `51534` – Eastbound E 22 Ave @ Rupert St
* `51535` – Eastbound E 22 Ave @ Cassiar St
* `51537` – Eastbound Kincaid St @ Boundary Rd
* `51538` – Eastbound Kincaid St @ Smith Ave
* `51539` – Eastbound Kincaid St @ MacDonald Ave
* `51541` – Eastbound Sanderson Way @ Mathissi Place
* `51542` – Northbound Willingdon Ave @ Goard Way
* `51543` – Northbound Willingdon Ave @ Canada Way
* `51545` – Southbound Willingdon Ave @ Canada Way
* `51546` – Southbound Willingdon Ave @ Goard Way
* `51547` – Southbound Willingdon Ave @ Sanderson Way
* `51548` – Westbound Sanderson Way @ Gilmore Way
* `51549` – Westbound Kincaid St @ Gilmore Way
* `51551` – Westbound Kincaid St @ Smith Ave
* `51552` – Westbound Kincaid St @ Boundary Rd
* `51553` – Westbound E 22 Ave @ Boundary Rd
* `51555` – Westbound E 22 Ave @ Rupert St
* `51556` – Westbound E 22 Ave @ Windermere St
* `51558` – Westbound E 22 Ave @ Renfrew St
* `51560` – Nanaimo Station @ Bay 2
* `51561` – Southbound Nanaimo St @ E 28 Ave
* `51562` – Southbound Nanaimo St @ Brock St
* `51563` – Westbound E King Edward Ave @ Kingsway
* `51564` – Westbound E King Edward Ave @ Knight St
* `51565` – Westbound E King Edward Ave @ Inverness St
* `51566` – Westbound E King Edward Ave @ Windsor St
* `51568` – Westbound E King Edward Ave @ Fraser St
* `51569` – Westbound E King Edward Ave @ St. George St
* `51570` – Westbound E King Edward Ave @ Prince Edward St
* `51571` – Westbound E King Edward Ave @ Main St
* `51572` – Westbound W King Edward Ave @ Ontario St
* `51573` – Westbound W King Edward Ave @ Columbia St
* `51574` – King Edward Station @ Bay 1
* `51576` – Westbound W King Edward Ave @ Laurel St
* `51577` – Westbound W King Edward Ave @ Oak St
* `51580` – Westbound W King Edward Ave @ Granville St
* `51582` – Westbound W King Edward Ave @ Pine Cres
* `51583` – Westbound W King Edward Ave @ Maple Cres
* `51584` – Westbound W King Edward Ave @ Arbutus St
* `51585` – Westbound W King Edward Ave @ Yew St
* `51586` – Westbound W King Edward Ave @ Valley Dr
* `51588` – Westbound W King Edward Ave @ Trafalgar St
* `51589` – Westbound W King Edward Ave @ Balaclava St
* `51590` – Westbound W King Edward Ave @ Blenheim St
* `51592` – Westbound W 16 Ave @ Alma St
* `51593` – Westbound W 16 Ave @ Wallace St
* `51594` – Westbound W 16 Ave @ Camosun St
* `51595` – Westbound W 16 Ave @ Discovery St
* `51596` – Westbound W 16 Ave @ Sasamat St
* `51597` – Westbound W 16 Ave @ Blanca St
* `51598` – Westbound W 16 Ave @ Pacific Spirit Park
* `51600` – Northbound Wesbrook Mall @ 2900 Block
* `51603` – 29th Avenue Station @ Bay 2
* `51604` – Southbound Rupert St @ School Ave
* `51605` – Southbound Rupert St @ E 41 Ave
* `51606` – Southbound Rupert St @ E 43 Ave
* `51607` – Southbound Rupert St @ E 45 Ave
* `51608` – Southbound Rupert St @ Kerr St
* `51610` – Southbound Kerr St @ E 49 Ave
* `51611` – Southbound Kerr St @ E 52 Ave
* `51612` – Southbound Kerr St @ E 54 Ave
* `51613` – Southbound Kerr St @ E 56 Ave
* `51614` – Southbound Kerr St @ E 59 Ave
* `51615` – Eastbound Rosemont Dr @ Kerr St
* `51617` – Southbound Butler St @ Rosemont Dr
* `51618` – Eastbound Maquinna Dr @ Stikine Place
* `51619` – Southbound Champlain Cres @ Langford Ave
* `51620` – Southbound Champlain Cres @ Marquette Cres
* `51621` – Northbound Matheson Cres @ Champlain Cres
* `51625` – Northbound Matheson Cres @ Borham Cres
* `51626` – Eastbound Arbor Ave @ Matheson Cres
* `51627` – Northbound Boundary Rd @ Arbor Ave
* `51628` – Northbound Boundary Rd @ Hurst Ave
* `51629` – Westbound E 49 Ave @ Boundary Rd
* `51630` – Westbound E 49 Ave @ Arlington St
* `51636` – Northbound Joyce St @ Kingsway
* `51638` – Northbound Joyce St @ Euclid Ave
* `51640` – Southbound Joyce St @ Euclid Ave
* `51641` – Southbound Joyce St @ Cherry St
* `51642` – Southbound Joyce St @ E 41 Ave
* `51647` – Eastbound E 49 Ave @ Tyne St
* `51648` – Eastbound E 49 Ave @ Arlington St
* `51649` – Southbound Frontenac St @ Hennepin Ave
* `51650` – Southbound Frontenac St @ Hurst Ave
* `51651` – Eastbound Hurst Ave @ Boundary Rd
* `51652` – Westbound Arbor Ave @ Boundary Rd
* `51653` – Southbound Matheson Cres @ Borham Cres
* `51656` – Westbound Champlain Cres @ Matheson Cres
* `51658` – Northbound Champlain Cres @ Langford Ave
* `51659` – Westbound Maquinna Dr @ Stikine Place
* `51660` – Westbound Rosemont Dr @ Butler St
* `51662` – Westbound Rosemont Dr @ Kerr St
* `51663` – Northbound Kerr St @ E 58 Ave
* `51665` – Northbound Kerr St @ E 51 Ave
* `51666` – Northbound Kerr St @ E 49 Ave
* `51667` – Northbound Rupert St @ Kerr St
* `51668` – Northbound Rupert St @ E 45 Ave
* `51669` – Northbound Rupert St @ E 43 Ave
* `51670` – Northbound Rupert St @ E 41 Ave
* `51671` – Joyce Station @ Bay 2
* `51672` – Westbound Wellington Ave @ Joyce St
* `51673` – Westbound Wellington Ave @ McHardy St
* `51674` – Northbound Rupert St @ Wellington Ave
* `51675` – Northbound Rupert St @ E 29 Ave
* `51678` – Northbound Rupert St @ E 22 Ave
* `51679` – Northbound Rupert St @ Seaforth Dr
* `51680` – Northbound Rupert St @ E 17 Ave
* `51681` – Northbound Rupert St @ Worthington Dr
* `51682` – Northbound Rupert St @ Grandview Hwy
* `51683` – Northbound Rupert St @ E Broadway
* `51684` – Northbound Rupert St @ E 7th Ave
* `51685` – Northbound Rupert St @ E 5th Ave
* `51686` – Northbound Rupert St @ E 3rd Ave
* `51687` – Northbound Rupert St @ Graveley St
* `51691` – Eastbound Adanac St @ Rupert St
* `51692` – Eastbound Adanac St @ Skeena St
* `51693` – Eastbound Adanac St @ Kootenay St
* `51694` – Northbound Boundary Rd @ Georgia St
* `51695` – Kootenay Loop @ Bay 3
* `51696` – Southbound Boundary Rd @ E Pender St
* `51697` – Southbound Boundary Rd @ E Georgia St
* `51698` – Westbound Adanac St @ Skeena St
* `51699` – Southbound Rupert St @ Adanac St
* `51703` – Southbound Rupert St @ E 1st Ave
* `51704` – Southbound Rupert St @ E 3rd Ave
* `51705` – Southbound Rupert St @ E 5th Ave
* `51706` – Southbound Rupert St @ E 7th Ave
* `51707` – Southbound Rupert St @ E Broadway
* `51710` – Southbound Rupert St @ Grandview Hwy
* `51711` – Southbound Rupert St @ E 16 Ave
* `51712` – Southbound Rupert St @ E 18 Ave
* `51713` – Southbound Rupert St @ E 20 Ave
* `51714` – Southbound Rupert St @ E 22 Ave
* `51715` – Southbound Rupert St @ E 24 Ave
* `51716` – Southbound Rupert St @ E 26 Ave
* `51717` – Southbound Rupert St @ E 28 Ave
* `51719` – Eastbound Wellington Ave @ Rupert St
* `51720` – Eastbound Wellington Ave @ McHardy St
* `51721` – Southbound Joyce St @ Wellington Ave
* `51722` – Joyce Station @ Bay 1 Unload Only
* `51723` – Eastbound Vanness Ave @ Melbourne St
* `51724` – Northbound Boundary Rd @ Vanness Ave
* `51725` – Eastbound Burke St @ Boundary Rd
* `51726` – Northbound Smith Ave @ Burke St
* `51727` – Northbound Smith Ave @ Brandon St
* `51728` – Northbound Smith Ave @ Price St
* `51729` – Northbound Smith Ave @ Moscrop St
* `51730` – Northbound Smith Ave @ Fir St
* `51731` – Northbound Smith Ave @ Spruce St
* `51732` – Northbound Ingleton Ave @ Sunset St
* `51733` – Northbound Smith Ave @ Avondale St
* `51736` – Northbound Boundary Rd @ Dominion St
* `51737` – Northbound Boundary Rd @ Grandview Hwy
* `51740` – Northbound Boundary Rd @ Lougheed Hwy
* `51741` – Northbound Boundary Rd @ 2nd Ave
* `51742` – Northbound Boundary Rd @ 1st Ave
* `51743` – Northbound Boundary Rd @ Kitchener St
* `51745` – Northbound Boundary Rd @ Venables St
* `51747` – Phibbs Exchange @ Bay 5
* `51749` – Northbound Mountain Hwy @ Crown St
* `51752` – Northbound Lillooet Rd @ 1000 Block
* `51755` – Southbound Lillooet Rd @ Purcell Way
* `51756` – Southbound Lillooet Rd @ Mt Seymour Parkway
* `51757` – Southbound Mountain Hwy @ Fern St
* `51758` – Southbound Mountain Hwy @ Charlotte Rd
* `51759` – Southbound Mountain Hwy @ Rupert St
* `51761` – Phibbs Exchange @ Bay 1
* `51762` – Southbound Boundary Rd @ Douglas Rd
* `51764` – Southbound Boundary Rd @ Kitchener St
* `51765` – Southbound Boundary Rd @ E 1st Ave
* `51769` – Southbound Boundary Rd @ Regent St
* `51771` – Southbound Boundary Rd @ Grandview Hwy
* `51773` – Eastbound Laurel St @ Boundary Rd
* `51774` – Southbound Smith Ave @ Laurel St
* `51775` – Southbound Smith Ave @ Avondale St
* `51776` – Southbound Smith Ave @ Nithsdale St
* `51777` – Southbound Smith Ave @ Sunset St
* `51779` – Southbound Smith Ave @ Kincaid St
* `51780` – Southbound Smith Ave @ Spruce St
* `51781` – Eastbound 24 Ave @ 188 St
* `51784` – Southbound Boundary Rd @ Burke St
* `51785` – Westbound Vanness Ave @ Boundary Rd
* `51786` – Westbound Vanness Ave @ Melbourne St
* `51787` – Joyce Station @ Bay 4
* `51794` – Westbound SE Marine Dr @ Matheson Cres
* `51795` – Westbound SE Marine Dr @ Fieldstone Ave
* `51796` – Westbound SE Marine Dr @ Kerr St
* `51797` – Westbound SE Marine Dr @ Jellicoe St
* `51798` – Westbound SE Marine Dr @ Elliott St
* `51800` – Westbound SE Marine Dr @ Gladstone St
* `51803` – Northbound Nanaimo St @ Fraserview Dr
* `51804` – Northbound Muirfield Dr @ Scarboro Ave
* `51805` – Northbound Muirfield Dr @ Bobolink Ave
* `51806` – Northbound Muirfield Dr @ Lynbrook Dr
* `51807` – Eastbound Ashburn St @ Elliott St
* `51808` – Northbound Elliott St @ E 54 Ave
* `51809` – Northbound Elliott St @ E 52 Ave
* `51810` – Northbound Elliott St @ E 49 Ave
* `51811` – Northbound Elliott St @ E 46 Ave
* `51812` – Northbound Elliott St @ Waverley Ave
* `51813` – Northbound Clarendon St @ E 43 Ave
* `51814` – Northbound Clarendon St @ E 41 Ave
* `51815` – Northbound Clarendon St @ E 39 Ave
* `51816` – Northbound Clarendon St @ E 37 Ave
* `51818` – 29th Avenue Station @ Bay 1
* `51819` – Southbound Clarendon St @ E 41 Ave
* `51821` – Southbound Elliott St @ E 45 Ave
* `51822` – Southbound Elliott St @ E 47 Ave
* `51824` – Southbound Elliott St @ E 49 Ave
* `51825` – Southbound Elliott St @ E 51 Ave
* `51827` – Southbound Elliott St @ E 56 Ave
* `51828` – Southbound Muirfield Dr @ Ashburn St
* `51829` – Southbound Muirfield Dr @ Hoylake Ave
* `51830` – Southbound Muirfield Dr @ Bobolink Ave
* `51831` – Southbound Muirfield Dr @ Scarboro Ave
* `51832` – Eastbound SE Marine Dr @ Victoria Dr
* `51833` – Eastbound SE Marine Dr @ Gladstone St
* `51834` – Eastbound SE Marine Dr @ Nanaimo St
* `51836` – Eastbound SE Marine Dr @ Jellicoe St
* `51837` – Eastbound SE Marine Dr @ Kerr St
* `51838` – Eastbound SE Marine Dr @ Fieldstone Ave
* `51842` – Burrard Station @ Bay 4
* `51843` – Burrard Station @ Bay 3
* `51844` – Eastbound Hastings St @ Alpha
* `51845` – Eastbound Hastings St @ Beta Ave
* `51846` – Eastbound Hastings St @ Hythe Ave
* `51848` – Eastbound Hastings St @ Howard Ave
* `51849` – Eastbound Hastings St @ Holdom Ave
* `51851` – Eastbound Hastings St @ Kensington Ave
* `51856` – Eastbound Hastings St @ Duthie Ave
* `51857` – Eastbound Burnaby Mountain Parkway @ Burnwood Ave
* `51858` – Eastbound Burnaby Mountain Parkway @ Curtis St
* `51861` – SFU Transit Exchange @ Bay 1
* `51862` – Westbound S Campus Rd @ Science Rd
* `51863` – SFU Transportation Centre @ Bay 1
* `51864` – Westbound University Dr W @ W Campus Rd
* `51865` – Westbound Burnaby Mountain Parkway @ Curtis St
* `51866` – Westbound Burnaby Mountain Parkway @ Burnwood Ave
* `51869` – Westbound Hastings St @ Cliff Ave
* `51871` – Westbound Hastings St @ Sperling Ave
* `51872` – Westbound Hastings St @ Kensington Ave
* `51873` – Westbound Hastings St @ Fell Ave
* `51874` – Westbound Hastings St @ Holdom Ave
* `51875` – Westbound Hastings St @ Howard Ave
* `51876` – Westbound Hastings St @ Springer Ave
* `51877` – Westbound Hastings St @ Hythe Ave
* `51878` – Westbound Hastings St @ Gamma Ave
* `51879` – Westbound Hastings St @ Alpha Ave
* `51881` – Northbound Wesbrook Mall @ Triumf Centre
* `51882` – Westbound W 16 Ave @ Wesbrook Mall
* `51888` – Eastbound W 41 Ave @ Crown St
* `51889` – Eastbound W 41 Ave @ Wallace St
* `51890` – Eastbound W 41 Ave @ Highbury St
* `51892` – Westbound E 41 Ave @ Wales St
* `51893` – Dunbar Loop @ Bay 5
* `51894` – Westbound W 41 Ave @ Olympic St
* `51895` – Westbound W 41 Ave @ Holland St
* `51897` – Westbound SW Marine Dr @ Camosun St
* `51913` – Westbound W 4th Ave @ Blanca St
* `51916` – Westbound Chancellor Blvd @ Hamber Rd
* `51917` – Westbound Chancellor Blvd @ Allison Rd
* `51928` – Eastbound Chancellor Blvd @ Western Parkway
* `51930` – Eastbound Chancellor Blvd @ Hamber Rd
* `51941` – Northbound Burrard St @ W 3rd Ave
* `51942` – Southbound Burrard St @ W 3rd Ave
* `51951` – Eastbound W 49 Ave @ Marine Cres
* `51952` – Eastbound W 49 Ave @ Balsam St
* `51953` – Eastbound W 49 Ave @ Yew St
* `51954` – Eastbound W 49 Ave @ Arbutus St
* `51955` – Eastbound W 49 Ave @ East Blvd
* `51962` – Eastbound W 49 Ave @ Montgomery St
* `51963` – Eastbound W 49 Ave @ Oak St
* `51966` – Eastbound W 49 Ave @ Tisdall St
* `51967` – Langara-49th Station @ Bay 3
* `51969` – Eastbound W 49 Ave @ Manitoba St
* `51970` – Eastbound E 49 Ave @ Quebec St
* `51975` – Eastbound E 49 Ave @ Fraser St
* `51979` – Eastbound E 49 Ave @ Knight St
* `51980` – Eastbound E 49 Ave @ Dumfries St
* `51981` – Eastbound E 49 Ave @ Argyle St
* `51982` – Eastbound E 49 Ave @ Commercial St
* `51983` – Eastbound E 49 Ave @ Victoria Dr
* `51984` – Eastbound E 49 Ave @ Gladstone St
* `51985` – Eastbound E 49 Ave @ Nanaimo St
* `51987` – Eastbound E 49 Ave @ Elliott St
* `51988` – Eastbound E 49 Ave @ Vivian St
* `51989` – Eastbound E 49 Ave @ Killarney St
* `51992` – Eastbound E 49 Ave @ Frontenac St
* `51993` – Eastbound Imperial St @ Boundary Rd
* `51994` – Eastbound Imperial St @ Mandy Ave
* `51998` – Northbound Willingdon Ave @ Maywood St
* `51999` – Metrotown Station @ Bay 3
* `52003` – Westbound Imperial St @ McKay Ave
* `52011` – Westbound E 49 Ave @ Kerr St
* `52015` – Westbound E 49 Ave @ Elliott St
* `52016` – Westbound E 49 Ave @ Nanaimo St
* `52017` – Westbound E 49 Ave @ Gladstone St
* `52018` – Westbound E 49 Ave @ Victoria Dr
* `52019` – Westbound E 49 Ave @ Beatrice St
* `52020` – Westbound E 49 Ave @ Bruce St
* `52021` – Westbound E 49 Ave @ Fleming St
* `52022` – Westbound E 49 Ave @ Knight St
* `52027` – Westbound E 49 Ave @ Fraser St
* `52032` – Westbound W 49 Ave @ Manitoba St
* `52033` – Westbound W 49 Ave @ Alberta St
* `52035` – Westbound W 49 Ave @ Tisdall St
* `52037` – Westbound W 49 Ave @ Oak St
* `52038` – Westbound W 49 Ave @ Montgomery St
* `52039` – Westbound W 49 Ave @ Hudson St
* `52041` – Westbound W 49 Ave @ Granville St
* `52045` – Westbound W 49 Ave @ West Blvd
* `52057` – Heather Square @ Bay 1
* `52058` – Westbound Lameys Mill Rd @ Sitka Square
* `52059` – Westbound Lameys Mill Rd @ Alder Crossing
* `52061` – Westbound W 2nd Ave @ Anderson St
* `52062` – Southbound Fir St @ W 2nd Ave
* `52065` – Eastbound W 2nd Ave @ Fir St
* `52067` – Eastbound Lameys Mill Rd @ Fountain Way Court
* `52068` – Eastbound Lameys Mill Rd @ Alder Crossing
* `52069` – Eastbound Lameys Mill Rd @ Sitka Square
* `52076` – Eastbound Cook Rd @ Buswell St
* `52077` – Eastbound Cook Rd @ Eckersley Rd
* `52078` – Eastbound Cook Rd @ Pimlico Way
* `52079` – Northbound Garden City Rd @ Ferndale Rd
* `52080` – Northbound Garden City Rd @ Westminster Hwy
* `52083` – Northbound Garden City Rd @ Odlin Rd
* `52084` – Northbound Garden City Rd @ Cambie Rd
* `52085` – Northbound Garden City Rd @ Capstan Way
* `52087` – Northbound Fir St @ W 4th Ave
* `52094` – Commercial-Broadway Station @ Bay 4
* `52095` – Westbound W Broadway @ Macdonald St
* `52099` – Eastbound W Broadway @ Heather St
* `52101` – Eastbound E Broadway @ Main St
* `52102` – Commercial-Broadway Station @ Bay 1
* `52103` – Eastbound Lougheed Hwy @ Boundary Rd
* `52105` – Eastbound Lougheed Hwy @ Government St
* `52108` – Eastbound Miller Rd @ Airport Access Rd
* `52109` – Eastbound Miller Rd @ 5000 Block
* `52110` – Eastbound Miller Rd @ 5500 Block
* `52111` – Eastbound Miller Rd @ Jericho Rd
* `52112` – Eastbound Miller Rd @ Aurora Connector
* `52113` – Eastbound Miller Rd @ Aylmer Rd
* `52114` – Eastbound Miller Rd @ Templeton St
* `52115` – Eastbound Miller Rd @ 7000 Block
* `52117` – Northbound Granville St @ W 71 Ave
* `52118` – Eastbound W 70 Ave @ French St
* `52119` – Southbound Hudson St @ W 70 Ave
* `52123` – Marpole Loop @ Bay 5
* `52126` – Eastbound SW Marine Dr @ Heather St
* `52127` – Eastbound SW Marine Dr @ W 70 Ave
* `52131` – Eastbound SW Marine Dr @ Manitoba St
* `52132` – Eastbound SE Marine Dr @ Ontario St
* `52136` – Eastbound SE Marine Dr @ Fraser St
* `52138` – Eastbound SE Marine Dr @ Ross St
* `52140` – Eastbound SE Marine Dr @ Knight St
* `52142` – Eastbound SE Marine Dr @ Beatrice St
* `52143` – Eastbound SE Marine Dr @ Matheson Cres
* `52144` – Eastbound SE Marine Dr @ City Edge Place
* `52145` – Eastbound Marine Dr @ Joffre Ave
* `52146` – Eastbound Marine Dr @ Greenall Ave
* `52147` – Eastbound Marine Dr @ Patterson Ave
* `52148` – Eastbound Marine Dr @ Sussex Ave
* `52150` – Eastbound Marine Dr @ Nelson Ave
* `52152` – Eastbound Marine Dr @ Royal Oak Ave
* `52154` – Eastbound Marine Dr @ Gilley Ave
* `52155` – Eastbound Marine Dr @ Willard St
* `52156` – Eastbound Marine Dr @ 14 Ave
* `52158` – Eastbound Marine Dr @ Trapp Rd
* `52164` – Eastbound Trapp Rd @ Marine Dr
* `52167` – Eastbound 6th Ave @ 16 St
* `52168` – Eastbound 6th Ave @ 14 St
* `52170` – Southbound 12 St @ 5th Ave
* `52171` – Southbound 12 St @ 4th Ave
* `52172` – Southbound 12 St @ 3rd Ave
* `52173` – Southbound 12 St @ Queens Ave
* `52176` – Westbound Columbia St @ 10 St
* `52177` – Westbound Columbia St @ Royal Ave
* `52182` – Westbound Marine Way @ 20 St
* `52183` – 22nd Street Station @ Bay 8
* `52184` – Westbound Marine Dr @ 7th Ave
* `52185` – Westbound Marine Dr @ 10 Ave
* `52186` – Westbound Marine Dr @ 14 Ave
* `52187` – Westbound Trapp Rd @ Marine Dr
* `52194` – Westbound Marine Dr @ Gilley Ave
* `52196` – Westbound Marine Dr @ Royal Oak Ave
* `52198` – Westbound Marine Dr @ Nelson Ave
* `52199` – Westbound Marine Dr @ Sussex Ave
* `52200` – Westbound Marine Dr @ Patterson Ave
* `52201` – Westbound Marine Dr @ Greenall Ave
* `52202` – Westbound SE Marine Dr @ City Edge Place
* `52203` – Westbound SE Marine Dr @ Victoria Dr
* `52205` – Westbound SE Marine Dr @ Knight St
* `52206` – Westbound SE Marine Dr @ Inverness St
* `52207` – Westbound SE Marine Dr @ Ross St
* `52209` – Westbound SE Marine Dr @ Chester St
* `52212` – Westbound SE Marine Dr @ Main St
* `52213` – Westbound SW Marine Dr @ Ontario St
* `52214` – Westbound SW Marine Dr @ Manitoba St
* `52217` – Westbound SW Marine Dr @ Ash St
* `52226` – Westbound Miller Rd @ Templeton St
* `52228` – Westbound North Service Rd @ Air Canada Operations Centre
* `52229` – Southbound Aviation Ave @ North Service Rd
* `52230` – 22nd Street Station @ Bay 4
* `52232` – Eastbound 8th Ave @ 22 St
* `52234` – Eastbound 8th Ave @ 20 St
* `52235` – Eastbound 8th Ave @ 19 St
* `52236` – Eastbound 8th Ave @ 18 St
* `52237` – Eastbound 8th Ave @ 16 St
* `52238` – Eastbound 8th Ave @ 14 St
* `52239` – Eastbound 8th Ave @ 12 St
* `52240` – Eastbound 8th Ave @ 10 St
* `52241` – Eastbound 6th Ave @ 8th St
* `52242` – Northbound 6th St @ 6th Ave
* `52243` – Northbound 6th St @ 7th Ave
* `52244` – Northbound 6th St @ 8th Ave
* `52246` – Northbound 6th St @ 10 Ave
* `52247` – Northbound 6th St @ 12 Ave
* `52248` – Eastbound 12 Ave @ 4th St
* `52251` – Northbound 1st St @ 12 Ave
* `52256` – Southbound Cumberland St @ 16 Ave
* `52261` – Northbound Cariboo Rd @ Armstrong Ave
* `52280` – Southbound Cariboo Rd @ Cariboo Place
* `52286` – Westbound Armstrong Ave @ Cariboo Rd
* `52290` – Westbound 16 Ave @ Cumberland St
* `52296` – Westbound 12 Ave @ 2nd St
* `52297` – Westbound 12 Ave @ 4th St
* `52298` – Southbound 6th St @ 12 Ave
* `52299` – Southbound 6th St @ 10 Ave
* `52301` – Southbound 6th St @ 8th Ave
* `52302` – Southbound 6th St @ 7th Ave
* `52303` – Westbound 6th Ave @ 6th St
* `52304` – Westbound 8th Ave @ 8th St
* `52305` – Westbound 8th Ave @ 10 St
* `52306` – Westbound 8th Ave @ 12 St
* `52307` – Westbound 8th Ave @ 14 St
* `52308` – Westbound 8th Ave @ 16 St
* `52309` – Westbound 8th Ave @ 18 St
* `52310` – Westbound 8th Ave @ 19 St
* `52311` – Westbound 8th Ave @ 20 St
* `52318` – Eastbound Columbia St @ 200 Block
* `52322` – Eastbound E 8th Ave @ McBride Blvd
* `52326` – Eastbound Cumberland St @ Glenbrook Dr
* `52331` – Columbia Station @ Bay 1
* `52332` – Westbound Columbia St @ 6th St
* `52335` – Eastbound Quayside Dr @ Laguna Court
* `52340` – Southbound Cliveden Ave @ 1600 Block
* `52342` – Eastbound Derwent Way @ 1600 Block
* `52343` – Eastbound Derwent Way @ Ridley Place
* `52354` – Northbound Boyd St @ Ewen Ave
* `52355` – Eastbound Duncan St @ 400 Block
* `52356` – Southbound Furness St @ Ewen Ave
* `52358` – Westbound Ewen Ave @ Fenton St
* `52360` – Westbound Ewen Ave @ Wood St
* `52361` – Westbound Ewen Ave @ McGillivray Place
* `52362` – Westbound Ewen Ave @ Howes St
* `52363` – Westbound Ewen Ave @ Jardine St
* `52366` – Westbound Westminster Hwy @ Boundary Rd
* `52367` – Westbound Westminster Hwy @ Smith Cres
* `52369` – Westbound Westminster Hwy @ Willett Ave
* `52372` – Northbound Howes St @ Ewen Ave
* `52373` – 22nd Street Station @ Bay 5
* `52375` – Eastbound Ewen Ave @ Howes St
* `52376` – Eastbound Ewen Ave @ Lawrence St
* `52377` – Eastbound Ewen Ave @ Wood St
* `52388` – Westbound Cliveden Ave @ Eaton Way
* `52389` – Westbound Cliveden Ave @ Hampstead Close
* `52390` – Metrotown Station @ Bay 7
* `52393` – Eastbound Kingsway @ Sussex Ave
* `52394` – Eastbound Kingsway @ Nelson Ave
* `52395` – Eastbound Kingsway @ Marlborough Ave
* `52396` – Eastbound Kingsway @ Royal Oak Ave
* `52397` – Eastbound Kingsway @ Grimmer St
* `52398` – Eastbound Kingsway @ Merritt Ave
* `52400` – Eastbound Kingsway @ Waltham Ave
* `52402` – Eastbound Kingsway @ Arcola St
* `52404` – Eastbound Kingsway @ Salisbury Ave
* `52405` – Eastbound Kingsway @ Hall Ave
* `52406` – Westbound Edmonds St @ Kingsway
* `52410` – Eastbound Edmonds St @ 16 St
* `52411` – Eastbound Edmonds St @ Linden Ave
* `52412` – Eastbound Edmonds St @ Humphries Ave
* `52413` – Eastbound Edmonds St @ Mary Ave
* `52414` – Eastbound Edmonds St @ New Vista Place
* `52415` – Eastbound Edmonds St @ 6th St
* `52417` – Southbound 6th St @ 16 Ave
* `52418` – Southbound 6th St @ 15 Ave
* `52419` – Southbound 6th St @ 13 Ave
* `52420` – Southbound 6th St @ 6th Ave
* `52421` – Southbound 6th St @ 5th Ave
* `52422` – Southbound 6th St @ 4th Ave
* `52424` – Southbound 6th St @ Queens Ave
* `52425` – Southbound 6th St @ Royal Ave
* `52431` – Columbia Station @ Bay 3
* `52438` – Northbound 6th St @ Royal Ave
* `52439` – Northbound 6th St @ Queens Ave
* `52440` – Northbound 6th St @ 3rd Ave
* `52441` – Northbound 6th St @ 4th Ave
* `52442` – Northbound 6th St @ 5th Ave
* `52443` – Northbound 6th St @ 13 Ave
* `52444` – Northbound 6th St @ 15 Ave
* `52445` – Northbound 6th St @ 16 Ave
* `52446` – Northbound 6th St @ 18 Ave
* `52447` – Westbound Edmonds St @ 7800 Block
* `52448` – Westbound Edmonds St @ Canada Way
* `52449` – Westbound Edmonds St @ New Vista Place
* `52450` – Westbound Edmonds St @ Mary Ave
* `52451` – Westbound Edmonds St @ Humphries Ave
* `52453` – Edmonds Station @ Bay 3
* `52455` – Westbound Kingsway @ Salisbury Ave
* `52460` – Westbound Kingsway @ Waltham Ave
* `52461` – Westbound Kingsway @ Imperial St
* `52462` – Westbound Kingsway @ Elgin Ave
* `52463` – Westbound Kingsway @ Denbigh Ave
* `52464` – Westbound Kingsway @ Royal Oak Ave
* `52466` – Westbound Kingsway @ McMurray Ave
* `52467` – Westbound Kingsway @ Sussex Ave
* `52468` – Southbound McKay Ave @ Kingsway
* `52469` – Southbound McKay Ave @ Kingsborough St
* `52483` – Westbound 8th Ave @ 2nd St
* `52484` – Westbound 8th Ave @ 4th St
* `52485` – Westbound 8th Ave @ 6th St
* `52487` – Eastbound 6th Ave @ 6th St
* `52488` – Eastbound 6th Ave @ 4th St
* `52491` – Southbound 2nd St @ 4th Ave
* `52492` – Southbound 2nd St @ 3rd Ave
* `52499` – Metrotown Station @ Bay 8
* `52500` – Eastbound Central Blvd @ 4500 Block
* `52501` – Northbound Bonsor Ave @ Central Blvd
* `52502` – Northbound Nelson Ave @ Bennett St
* `52503` – Northbound Nelson Ave @ Newton St
* `52504` – Northbound Nelson Ave @ Dover St
* `52505` – Northbound Nelson Ave @ Grafton St
* `52507` – Westbound Bond St @ Nelson Ave
* `52508` – Westbound Bond St @ Sussex Ave
* `52509` – Westbound Bond St @ Elsom Ave
* `52510` – Northbound Willingdon Ave @ Bond St
* `52513` – Northbound Willingdon Ave @ Price St
* `52514` – Westbound Beresford St @ Willingdon Ave
* `52515` – Patterson Station @ Bay 1
* `52516` – Northbound Patterson Ave @ Kingsway
* `52517` – Northbound Barker Ave @ Grange St
* `52518` – Northbound Patterson Ave @ Sardis St
* `52519` – Northbound Patterson Ave @ Bond St
* `52520` – Northbound Patterson Ave @ Burke St
* `52521` – Northbound Patterson Ave @ Hazelwood Cres
* `52522` – Northbound Patterson Ave @ Castlewood Cres
* `52523` – Northbound Patterson Ave @ Moscrop St
* `52525` – Eastbound Gilpin St @ Royal Oak Ave
* `52526` – Eastbound Gilpin St @ Chapple Cres
* `52528` – Northbound Norland Ave @ Canada Way
* `52529` – Northbound Norland Ave @ Ledger Ave
* `52531` – Eastbound Sprott St @ Norland Ave East
* `52534` – Eastbound Broadway @ Sperling Ave
* `52560` – Southbound Phillips Ave @ Government Rd
* `52568` – Westbound Broadway @ Sperling Ave
* `52569` – Westbound Sprott St @ Kensington Ave
* `52570` – Westbound Sprott St @ Norland Ave East
* `52571` – Southbound Norland Ave @ Sprott St West
* `52572` – Southbound Norland Ave @ Kincaid St
* `52577` – Westbound Deer Lake Parkway @ Garden Grove Dr
* `52579` – Southbound Patterson Ave @ Moscrop St
* `52580` – Southbound Patterson Ave @ Castlewood Cres
* `52581` – Southbound Patterson Ave @ Brandon St
* `52582` – Southbound Patterson Ave @ Burke St
* `52583` – Southbound Patterson Ave @ Bond St
* `52584` – Southbound Patterson Ave @ Sardis St
* `52585` – Southbound Patterson Ave @ Kingsway
* `52586` – Patterson Station @ Bay 2
* `52587` – Eastbound Kemp St @ Kathleen Ave
* `52588` – Southbound Willingdon Ave @ Moscrop St
* `52589` – Southbound Willingdon Ave @ Wildwood Cres
* `52590` – Southbound Willingdon Ave @ Price Cres
* `52591` – Southbound Willingdon Ave @ Burke St
* `52592` – Eastbound Bond St @ Willingdon Ave
* `52593` – Eastbound Bond St @ Elsom Ave
* `52594` – Eastbound Bond St @ Sussex Ave
* `52596` – Southbound Nelson Ave @ Shepherd St
* `52597` – Southbound Nelson Ave @ Grange St
* `52598` – Southbound Nelson Ave @ Hazel St
* `52599` – Westbound Bennett St @ Nelson Ave
* `52600` – Southbound Bonsor Ave @ Bennett St
* `52601` – Westbound Central Blvd @ Bonsor Ave
* `52602` – Westbound Central Blvd @ 4500 Block
* `52605` – Edmonds Station @ Bay 5
* `52606` – Southbound Griffiths Dr @ Southpoint Dr
* `52609` – Westbound 14 Ave @ 15 St
* `52612` – Southbound Kingsway @ 12 Ave
* `52613` – Southbound 12 St @ London St
* `52614` – Southbound 12 St @ 8th Ave
* `52615` – Southbound 12 St @ 7th Ave
* `52617` – Northbound 12 St @ Queens Ave
* `52618` – Northbound 12 St @ 3rd Ave
* `52619` – Northbound 12 St @ 4th Ave
* `52620` – Northbound 12 St @ 5th Ave
* `52621` – Northbound 12 St @ Nanaimo St
* `52622` – Northbound 12 St @ 7th Ave
* `52623` – Northbound 12 St @ 8th Ave
* `52624` – Northbound 12 St @ Dublin St
* `52625` – Northbound 12 St @ 10 Ave
* `52626` – Northbound Kingsway @ 12 Ave
* `52629` – Eastbound 14 Ave @ 15 St
* `52632` – Northbound Griffiths Dr @ Southpoint Dr
* `52635` – Northbound Nelson Ave @ Carson Place
* `52636` – Northbound Nelson Ave @ McKee Place
* `52637` – Northbound Nelson Ave @ Portland St
* `52638` – Eastbound Rumble St @ Nelson Ave
* `52639` – Eastbound Rumble St @ Arthur Ave
* `52640` – Northbound Royal Oak Ave @ Rumble St
* `52641` – Northbound Royal Oak Ave @ Sidley St
* `52646` – Metrotown Station @ Bay 5
* `52652` – Southbound Royal Oak Ave @ Rumble St
* `52653` – Eastbound Clinton St @ Royal Oak Ave
* `52654` – Eastbound Clinton St @ Roslyn Ave
* `52655` – Eastbound Clinton St @ Macpherson Ave
* `52656` – Eastbound Clinton St @ Plum Ave
* `52657` – Eastbound Clinton St @ Buller Ave
* `52659` – Southbound Gilley Ave @ Portland St
* `52661` – Southbound Gilley Ave @ Carson St
* `52662` – Southbound Gilley Ave @ Keith St
* `52665` – Eastbound Rumble St @ Hedley Ave
* `52666` – Eastbound Rumble St @ 6800 Block
* `52667` – Edmonds Station @ Bay 6
* `52668` – Westbound Rumble St @ 6800 Block
* `52669` – Westbound Rumble St @ Hedley Ave
* `52670` – Southbound Gilley Ave @ Rumble St
* `52673` – Westbound Clinton St @ Buller Ave
* `52675` – Westbound Clinton St @ Macpherson Ave
* `52676` – Westbound Clinton St @ Roslyn Ave
* `52677` – Northbound Royal Oak Ave @ Neville St
* `52678` – Northbound Joffre Ave @ Maple Tree Lane
* `52680` – Northbound Joffre Ave @ Clinton St
* `52681` – Eastbound Rumble St @ Joffre Ave
* `52682` – Eastbound Rumble St @ Greenall Ave
* `52683` – Eastbound Rumble St @ Patterson Ave
* `52684` – Eastbound Rumble St @ McKay Ave
* `52685` – Northbound Sussex Ave @ Rumble St
* `52686` – Northbound Sussex Ave @ Watling St
* `52687` – Northbound Sussex Ave @ Victory St
* `52688` – Northbound Sussex Ave @ Hurst St
* `52692` – Metrotown Station @ Bay 1
* `52693` – Westbound Imperial St @ Waverley Ave
* `52694` – Westbound Imperial St @ Frederick Ave
* `52695` – Westbound Imperial St @ Dow Ave
* `52696` – Southbound Sussex Ave @ Imperial St
* `52697` – Southbound Sussex Ave @ Hurst St
* `52698` – Southbound Sussex Ave @ Victory St
* `52699` – Southbound Sussex Ave @ Irmin St
* `52700` – Southbound Sussex Ave @ Rumble St
* `52701` – Westbound Portland St @ Sussex Ave
* `52702` – Westbound Portland St @ McKay Ave
* `52703` – Southbound Patterson Ave @ Winnifred St
* `52704` – Southbound Roseberry Ave @ Keith St
* `52705` – Southbound Boundary Rd @ E Kent Ave
* `52712` – Eastbound W Hastings St @ Abbott St
* `52713` – Eastbound E Hastings St @ Main St
* `52714` – Eastbound E Hastings St @ Commercial Dr
* `52716` – Bus Loop @ Burnaby City Hall
* `52717` – Kootenay Loop @ Bay 7
* `52718` – Westbound E Hastings St @ Renfrew St
* `52719` – Westbound Moscrop St @ Inman Ave
* `52720` – Northbound Gilmore Way @ Sanderson Way
* `52723` – Northbound Gilmore Diversion @ Myrtle St
* `52731` – Southbound Gilmore Diversion @ Myrtle St
* `52732` – Southbound Gilmore Diversion @ Manor St
* `52735` – Eastbound Moscrop St @ Smith Ave
* `52736` – Metrotown Station @ Bay 4
* `52738` – Northbound Willingdon Ave @ Grafton St
* `52739` – Northbound Willingdon Ave @ Deer Lake Parkway
* `52740` – Northbound Willingdon Ave @ Sanderson Way
* `52741` – Southbound Willingdon Ave @ Sardis St
* `52742` – Southbound Gilmore Ave @ Hastings St
* `52743` – Southbound Gilmore Ave @ Georgia St
* `52744` – Southbound Gilmore Ave @ Venables St
* `52745` – Southbound Gilmore Ave @ William St
* `52754` – Southbound Douglas Rd @ Still Creek Ave
* `52760` – Westbound Burris St @ Canada Way
* `52763` – Westbound Burris St @ Malvern Ave
* `52771` – Edmonds Station @ Bay 4
* `52779` – Eastbound Burris St @ Leibly Ave
* `52782` – Southbound Deer Lake Parkway @ Canada Way
* `52792` – Westbound Lougheed Hwy @ Beta Ave
* `52798` – Northbound Gilmore Ave @ 1st Ave
* `52801` – Northbound Gilmore Ave @ Parker St
* `52802` – Northbound Gilmore Ave @ Union St
* `52804` – Metrotown Station @ Bay 6
* `52806` – SFU Transportation Centre @ Bay 2
* `52807` – SFU Transit Exchange @ Bay 3
* `52812` – Northbound Delta Ave @ Brentlawn Dr
* `52816` – Eastbound Parker St @ Delta Ave
* `52818` – Eastbound Parker St @ Holdom Ave
* `52824` – Southbound Duthie Ave @ Curtis St
* `52829` – Westbound Halifax St @ Phillips Ave
* `52830` – Westbound Halifax St @ Augusta Ave
* `52831` – Southbound Duthie Ave @ Halifax St
* `52834` – Southbound Duthie Ave @ Broadway
* `52840` – Westbound Lougheed Hwy @ Gaglardi Way
* `52847` – Eastbound Forest Grove Dr @ 8700 Block
* `52848` – Eastbound Forest Grove Dr @ Maple Grove Cres East
* `52849` – Eastbound Forest Grove Dr @ Birch Grove Place
* `52850` – Westbound Forest Grove Dr @ 9200 Block
* `52851` – Westbound Forest Grove Dr @ Goldhurst Terrace East
* `52852` – Eastbound Eastlake Dr @ Production Way
* `52854` – Eastbound Beaverbrook Dr @ Eastlake Dr
* `52855` – Eastbound Beaverbrook Dr @ Centaurus Dr
* `52856` – Eastbound Beaverbrook Dr @ Noel Dr
* `52859` – Eastbound Cameron St @ Morrey Court
* `52861` – Northbound North Rd @ Austin Ave
* `52862` – Westbound Cameron St @ North Rd
* `52863` – Westbound Cameron St @ Morrey Court
* `52864` – Westbound Cameron St @ Bartlett Court
* `52865` – Northbound Beaverbrook Dr @ Cameron St
* `52866` – Westbound Beaverbrook Dr @ Centaurus Dr
* `52867` – Northbound Eastlake Dr @ Beaverbrook Dr
* `52868` – Northbound Eastlake Dr @ Centaurus Dr
* `52869` – Northbound Production Way @ Eastlake Dr
* `52871` – Eastbound Forest Grove Dr @ 9200 Block
* `52874` – Westbound Forest Grove Dr @ 8700 Block
* `52878` – Westbound Forest Grove Dr @ Underhill Ave
* `52883` – Northbound Duthie Ave @ Broadway
* `52885` – Northbound Duthie Ave @ Sutliff St
* `52886` – Eastbound Halifax St @ Pepperidge Court
* `52897` – Westbound Parker St @ Holdom Ave
* `52899` – Southbound Delta Ave @ Parker St
* `52902` – Southbound Delta Ave @ Brentlawn Dr
* `52907` – Southbound Duthie Ave @ Hastings St
* `52908` – Southbound Holdom Ave @ Hastings St
* `52910` – Southbound Holdom Ave @ Parker St
* `52912` – Eastbound Halifax St @ Holdom Ave
* `52917` – Eastbound Halifax St @ Sperling Ave
* `52924` – Westbound Halifax St @ Duthie Ave
* `52928` – Westbound Halifax St @ Sperling Ave
* `52933` – Southbound Fell Ave @ Sumas St
* `52937` – Northbound Holdom Ave @ Halifax St
* `52940` – Northbound Holdom Ave @ Curtis St
* `52943` – Westbound Pender St @ Willingdon Ave
* `52965` – Northbound Boundary Rd @ Triumph St
* `52966` – Northbound N Boundary Rd @ Oxford St
* `52967` – Southbound N Esmond Ave @ McGill St
* `52968` – Eastbound Eton St @ N Ingleton Ave
* `52969` – Eastbound Eton St @ N MacDonald Ave
* `52970` – Eastbound Eton St @ N Gilmore Ave
* `52971` – Eastbound Eton St @ N Carleton Ave
* `52975` – Southbound N Willingdon Ave @ Cambridge St
* `52976` – Southbound Willingdon Ave @ Dundas St
* `52977` – Southbound Willingdon Ave @ Pandora St
* `52978` – Northbound Willingdon Ave @ Albert St
* `52979` – Northbound Willingdon Ave @ Triumph St
* `52980` – Northbound N Willingdon Ave @ Penzance Dr
* `52983` – Westbound Eton St @ N Madison Ave
* `52984` – Westbound Eton St @ N Carleton Ave
* `52985` – Westbound Eton St @ N Gilmore Ave
* `52986` – Westbound Eton St @ N MacDonald Ave
* `52988` – Southbound N Boundary Rd @ Eton St
* `52989` – Southbound N Boundary Rd @ Cambridge St
* `52990` – Southbound Boundary Rd @ Dundas St
* `52991` – Southbound Boundary Rd @ Pandora St
* `52992` – Edmonds Station @ Bay 2
* `52993` – Northbound Duthie Ave @ Halifax St
* `52994` – Northbound Duthie Ave @ Curtis St
* `52998` – SFU Transit Exchange @ Bay 4
* `53000` – Eastbound Como Lake Ave @ Dogwood St
* `53003` – Eastbound Como Lake Ave @ Blue Mountain St
* `53004` – Eastbound Como Lake Ave @ Porter St
* `53005` – Eastbound Como Lake Ave @ Gatensbury St
* `53006` – Eastbound Como Lake Ave @ Schoolhouse St
* `53008` – Eastbound Como Lake Ave @ Poirier St
* `53009` – Eastbound Como Lake Ave @ Linton St
* `53012` – Eastbound Como Lake Ave @ Thermal Dr
* `53013` – Eastbound Como Lake Ave @ Seymour Dr
* `53015` – Northbound Mariner Way @ Lighthouse Court
* `53016` – Northbound Mariner Way @ 2900 Block
* `53018` – Northbound Mariner Way @ Hawser Ave
* `53019` – Eastbound Mariner Way @ Windward Dr
* `53021` – Coquitlam Central Station @ Bay 3
* `53022` – Eastbound Lougheed Hwy @ Westwood St
* `53023` – Eastbound Lougheed Hwy @ Hastings St
* `53024` – Southbound Shaughnessy St @ Lions Way
* `53029` – Westbound Wilson Ave @ Kingsway Ave
* `53032` – Northbound Shaughnessy St @ Lions Way
* `53034` – Westbound Lougheed Hwy @ Westwood St
* `53035` – Coquitlam Central Station @ Bay 4
* `53036` – Westbound Dewdney Trunk Rd @ Lougheed Hwy
* `53054` – Westbound Como Lake Ave @ Gatensbury St
* `53056` – Westbound Como Lake Ave @ Blue Mountain St
* `53059` – Westbound Como Lake Ave @ Dogwood St
* `53061` – Eastbound Dover St @ Nelson Ave
* `53063` – Eastbound Oakland St @ Denbigh Ave
* `53064` – Eastbound Oakland St @ Dufferin Ave
* `53069` – Northbound Deer Lake Ave @ Heritage Village
* `53072` – Northbound Sperling Ave @ Broadway
* `53075` – Northbound Sperling Ave @ Halifax St
* `53080` – Southbound Sperling Ave @ Curtis St
* `53084` – Southbound Sperling Ave @ Halifax St
* `53088` – Southbound Deer Lake Ave @ Shadbolt Centre
* `53089` – Westbound Oakland St @ Walker Ave
* `53095` – Westbound Dover St @ Royal Oak Ave
* `53096` – SFU Transit Exchange @ Bay 2
* `53097` – Eastbound Lougheed Hwy @ Gaglardi Way
* `53100` – Southbound E Columbia St @ Braid St
* `53101` – Southbound E Columbia St @ Sherbrooke St
* `53104` – Scott Road Station @ Bay 1
* `53107` – Northbound E Columbia St @ Braid St
* `53110` – Northbound E Columbia St @ Alberta St
* `53111` – Northbound E Columbia St @ Keary St
* `53121` – Northbound North Rd @ Cottonwood Ave
* `53122` – Burquitlam Station @ Bay 4
* `53123` – Northbound Clarke Rd @ Como Lake Ave
* `53147` – Eastbound St. Johns St @ Moody St
* `53148` – Eastbound St. Johns St @ Williams St
* `53149` – Eastbound St. Johns St @ Buller St
* `53150` – Eastbound St. Johns St @ Moray St
* `53151` – Eastbound St. Johns St @ Clearview Dr
* `53161` – Coquitlam Central Station @ Bay 6
* `53164` – Eastbound Guildford Way @ Johnson St
* `53165` – Eastbound Guildford Way @ Pacific St
* `53170` – Coquitlam Central Station @ Bay 7
* `53173` – Westbound Barnet Hwy @ Ioco Rd
* `53180` – Westbound St. Johns St @ Clearview Dr
* `53181` – Westbound St. Johns St @ Moray St
* `53183` – Westbound St. Johns St @ Williams St
* `53184` – Westbound St. Johns St @ Moody St
* `53185` – Westbound St. Johns St @ Queens St
* `53186` – Westbound St. Johns St @ Elgin St
* `53208` – Burquitlam Station @ Bay 6
* `53210` – Southbound North Rd @ Cottonwood Ave
* `53214` – Southbound North Rd @ Austin Rd
* `53218` – Southbound E Columbia St @ Keary St
* `53219` – Southbound E Columbia St @ Alberta St
* `53227` – Westbound Ungless Way @ Guildford Way
* `53242` – Northbound Sunnyside Rd @ East Rd
* `53244` – Southbound Sunnyside Rd @ Anmore Grocery Store
* `53246` – Southbound Sunnyside Rd @ East Rd
* `53253` – Eastbound Bedwell Bay Rd @ 3900 Block
* `53257` – Eastbound Ioco Rd @ Barber St
* `53258` – Eastbound Ioco Rd @ Bentley Rd
* `53260` – Eastbound Ioco Rd @ San Remo Dr
* `53262` – Eastbound Ioco Rd @ Metta Lane
* `53264` – Eastbound Ungless Way @ Ioco Rd
* `53267` – Inlet Centre Station @ Bay 1
* `53281` – Westbound Guildford Way @ Eagle Ridge Hospital
* `53282` – Northbound Ioco Rd @ Newport Dr
* `53283` – Northbound Heritage Mountain Blvd @ Ungless Way
* `53291` – Southbound Ravine Dr @ Creekstone Place
* `53298` – Eastbound Guildford Way @ Lansdowne Dr
* `53299` – Coquitlam Central Station @ Bay 1
* `53301` – Eastbound Lougheed Hwy @ Douglas Rd
* `53302` – Eastbound Lougheed Hwy @ Holdom Ave
* `53304` – Eastbound Lougheed Hwy @ Bainbridge Ave
* `53305` – Eastbound Lougheed Hwy @ Lake City Way
* `53309` – Westbound Lougheed Hwy @ Holdom Ave
* `53314` – Eastbound Lougheed Hwy @ North Rd
* `53320` – Eastbound Brunette Ave @ Nelson St
* `53322` – Eastbound Brunette Ave @ Begin St
* `53323` – Eastbound Brunette Ave @ Laval St
* `53324` – Eastbound Brunette Ave @ Casey St
* `53325` – Eastbound Brunette Ave @ Schoolhouse St
* `53343` – Eastbound Mariner Way @ Dewdney Trunk Rd
* `53348` – Port Coquitlam Station @ Bay 6
* `53349` – Eastbound Spuraway Ave @ Pinnacle St
* `53350` – Eastbound Spuraway Ave @ Cape Court
* `53357` – Coquitlam Central Station @ Bay 9
* `53358` – Westbound Dewdney Trunk Rd @ Irvine St
* `53363` – Westbound Spuraway Ave @ Armada St
* `53391` – Westbound E Hastings St @ Main St
* `53392` – Westbound W Hastings St @ Abbott St
* `53394` – Eastbound Austin Ave @ 500 Block
* `53406` – Northbound Poirier St @ Austin Ave
* `53426` – Northbound Holly Dr @ Rose Way
* `53430` – Northbound Mariner Way @ Crawley Ave
* `53440` – Port Coquitlam Station @ Bay 2
* `53441` – Westbound Wilson Ave @ Mary Hill Rd
* `53469` – Southbound Mariner Way @ Crawley Ave
* `53488` – Westbound Rochester Ave @ Guilby St
* `53490` – Northbound Guilby St @ Dansey Ave
* `53491` – Westbound Austin Ave @ Whiting Way
* `53493` – Northbound Mariner Way @ Mara Dr
* `53494` – Northbound Mariner Way @ Spuraway Ave
* `53495` – Coquitlam Central Station @ Bay 8
* `53497` – 22nd Street Station @ Bay 3
* `53498` – Eastbound 6th Ave @ 12 St
* `53499` – Eastbound 6th Ave @ 10 St
* `53502` – Eastbound 8th Ave @ 4th St
* `53503` – Eastbound 8th Ave @ Park Cres
* `53504` – Eastbound E 8th Ave @ Cumberland St
* `53510` – Eastbound Braid St @ E Columbia St
* `53580` – Westbound E 8th Ave @ Cumberland St
* `53581` – Westbound 8th Ave @ McBride Blvd
* `53584` – Westbound 6th Ave @ 10 St
* `53585` – Westbound 6th Ave @ 12 St
* `53586` – Westbound 6th Ave @ 14 St
* `53587` – Westbound 6th Ave @ 16 St
* `53588` – Westbound 6th Ave @ 18 St
* `53589` – Eastbound 6th Ave @ 2nd St
* `53592` – Eastbound E 6th Ave @ McBride Blvd
* `53596` – Eastbound Hospital St @ Richmond St
* `53597` – Eastbound Hospital St @ E Columbia St
* `53613` – Westbound Hospital St @ E Columbia St
* `53614` – Westbound Hospital St @ Blair Ave
* `53617` – Westbound Cumberland St @ Glenbrook Dr
* `53621` – Westbound 6th Ave @ 1st St
* `53622` – Westbound 6th Ave @ 2nd St
* `53623` – Westbound 6th Ave @ 4th St
* `53643` – Northbound Nancy Greene Way @ Woodchuck Place
* `53673` – Northbound Shaughnessy St @ Citadel Dr
* `53682` – Eastbound Kebet Way @ Mustang Place
* `53730` – Coquitlam Central Station @ Bay 12
* `53740` – Port Coquitlam Station @ Bay 4
* `53743` – Lafarge Lake-Douglas Station @ Bay 3
* `53744` – Westbound Guildford Way @ Pacific St
* `53760` – Southbound Inlet Dr @ Bayview Dr
* `53762` – Eastbound Glen Dr @ Johnson St
* `53764` – Southbound Westwood St @ Lincoln Ave
* `53767` – Port Coquitlam Station @ Bay 5
* `53777` – Port Coquitlam Station @ Bay 1
* `53783` – Port Coquitlam Station @ Bay 3
* `53786` – Eastbound Lougheed Hwy @ Shaughnessy St
* `53795` – Northbound Coast Meridian Rd @ Laurier Ave
* `53803` – Westbound David Ave @ Oxford St
* `53808` – Eastbound Victoria Dr @ 900 Block
* `53809` – Eastbound Victoria Dr @ Rocklin St
* `53814` – Westbound Prairie Ave @ Finley St
* `53816` – Westbound Prairie Ave @ Toronto St
* `53838` – Eastbound Prairie Ave @ Coast Meridian Rd
* `53845` – Westbound Victoria Dr @ Rocklin St
* `53857` – Southbound Coast Meridian Rd @ Patricia Ave
* `53860` – Westbound Coquitlam Ave @ Coast Meridian Rd
* `53864` – Coquitlam Central Station @ Bay 10
* `53866` – Coquitlam Central Station @ Bay 5
* `53868` – Lafarge Lake-Douglas Station @ Bay 2
* `53872` – Northbound Pipeline Rd @ Ozada Ave
* `53887` – Coquitlam Central Station @ Bay 11
* `53895` – Westbound Noons Creek Dr @ Thurston Terrace
* `53902` – Eastbound Panorama Dr @ Noons Creek Dr
* `53903` – Southbound Lansdowne Dr @ Panorama Dr
* `53905` – Southbound Lansdowne Dr @ Briarcliffe Dr North
* `53922` – Northbound Lansdowne Dr @ 1400 Block
* `53929` – Southbound Johnson St @ Walton Ave
* `53955` – Eastbound Cotton Rd @ Brooksbank Ave
* `53958` – Westbound Cotton Rd @ Brooksbank Ave
* `53964` – Northbound Mountain Hwy @ Arborlynn Dr
* `53970` – Northbound Boulevard Cres @ E 19 St
* `53971` – Eastbound Lynn Valley Rd @ Morgan Rd
* `53972` – Eastbound Lynn Valley Rd @ William Ave
* `53973` – Eastbound Lynn Valley Rd @ Mollie Nye Way
* `53974` – Eastbound Lynn Valley Rd @ E 27 St
* `53975` – Eastbound Lynn Valley Rd @ E 29 St
* `53977` – Lynn Valley @ Bay 3
* `53985` – Eastbound McNair Dr @ Ramsay Rd
* `53989` – Northbound Underwood Ave @ Evelyn St
* `53994` – Westbound McNair Dr @ Ramsay Rd
* `54002` – Southbound Mountain Hwy @ Lynn Valley Rd
* `54003` – Lynn Valley @ Bay 4
* `54004` – Southbound Mountain Hwy @ E 27 St
* `54005` – Southbound Mountain Hwy @ Emery Place
* `54006` – Southbound Mountain Hwy @ Kirkstone Rd
* `54007` – Southbound Mountain Hwy @ E 17 St
* `54008` – Southbound Mountain Hwy @ E 15 St
* `54009` – Southbound Mountain Hwy @ Arborlynn Dr
* `54011` – Eastbound E Keith Rd @ Mountain Hwy
* `54012` – Westbound Lynn Valley Rd @ Ross Rd
* `54019` – Southbound Grand Blvd East @ E 17 St
* `54026` – Phibbs Exchange @ Bay 4
* `54031` – Eastbound Mt Seymour Parkway @ Browning Place
* `54032` – Eastbound Mt Seymour Parkway @ Berkley Rd
* `54033` – Eastbound Mt Seymour Parkway @ Lytton St
* `54039` – Eastbound Mt Seymour Parkway @ Roche Point Dr
* `54046` – Eastbound Dollar Rd @ N Dollarton Hwy
* `54048` – Northbound N Dollarton Hwy @ Mt Seymour Parkway
* `54057` – Southbound N Dollarton Hwy @ Mt Seymour Parkway
* `54059` – Westbound Dollar Rd @ N Dollarton Hwy
* `54066` – Westbound Mt Seymour Parkway @ Mt Seymour Rd
* `54077` – Phibbs Exchange @ Bay 7
* `54089` – Southbound N Dollarton Hwy @ Dollar Rd
* `54100` – Phibbs Exchange @ Bay 2
* `54119` – Phibbs Exchange @ Bay 9
* `54124` – Northbound Mt Seymour Rd @ Mt Seymour Parkway
* `54134` – Lonsdale Quay @ Bay 6
* `54142` – Northbound Lonsdale Ave @ E 2nd St
* `54143` – Northbound Lonsdale Ave @ E 4th St
* `54147` – Eastbound E 15 St @ Lonsdale Ave
* `54149` – Westbound E 3rd St @ Ridgeway Ave
* `54151` – Westbound E 3rd St @ St. Andrews Ave
* `54152` – Westbound E 3rd St @ St. Georges Ave
* `54158` – Southbound Lonsdale Ave @ W 5th St
* `54159` – Southbound Lonsdale Ave @ W 3rd St
* `54160` – Southbound Lonsdale Ave @ W 1st St
* `54167` – Eastbound E 3rd St @ Lonsdale Ave
* `54168` – Eastbound E 3rd St @ St. Georges Ave
* `54169` – Eastbound E 3rd St @ St. Andrews Ave
* `54180` – Northbound Grand Blvd East @ E 17 St
* `54182` – Lynn Valley @ Bay 2
* `54196` – Lonsdale Quay @ Bay 2
* `54217` – Southbound Duval Rd @ Draycott Rd
* `54226` – Southbound Arborlynn Dr @ Appin Rd
* `54227` – Northbound Arborlynn Dr @ Mountain Hwy
* `54228` – Northbound Arborlynn Dr @ Appin Rd
* `54229` – Northbound Arborlynn Dr @ Avonlynn Cres
* `54270` – Northbound Lonsdale Ave @ E Braemar Rd
* `54271` – Northbound Lonsdale Ave @ E Rockland Rd
* `54272` – Southbound Prospect Rd @ W Rockland Rd
* `54273` – Southbound Lonsdale Ave @ W Balmoral Rd
* `54275` – Southbound Lonsdale Ave @ Carisbrooke Cres
* `54279` – Westbound W 3rd St @ Chesterfield Ave
* `54290` – Grouse Mountain @ Skyride
* `54291` – Southbound Nancy Greene Way @ N Grousewoods Dr
* `54292` – Southbound Nancy Greene Way @ S Grousewoods Dr
* `54293` – Southbound Nancy Greene Way @ Blue Grouse Way
* `54299` – Southbound Capilano Rd @ Teviot Place
* `54300` – Southbound Capilano Rd @ Handsworth Rd
* `54301` – Southbound Capilano Rd @ Edgewood Rd
* `54304` – Northbound Capilano Rd @ 3600 Block
* `54305` – Eastbound Edgemont Blvd @ Riviere Place
* `54333` – Phibbs Exchange @ Bay 11
* `54336` – Westbound E Keith Rd @ Brooksbank Ave
* `54362` – Westbound Edgemont Blvd @ Capilano Rd
* `54365` – Northbound Capilano Rd @ Edgemont Blvd
* `54367` – Northbound Capilano Rd @ Eldon Rd
* `54368` – Northbound Capilano Rd @ Edgewood Rd
* `54369` – Northbound Capilano Rd @ Handsworth Rd
* `54370` – Northbound Capilano Rd @ Montroyal Blvd
* `54371` – Northbound Nancy Greene Way @ Prospect Ave
* `54373` – Northbound Nancy Greene Way @ S Grousewoods Dr
* `54374` – Northbound Nancy Greene Way @ N Grousewoods Dr
* `54378` – Westbound Marine Dr @ Bewicke Ave
* `54391` – Northbound Capilano Rd @ Woods Dr
* `54393` – Eastbound W 3rd St @ Forbes Ave
* `54394` – Eastbound W 3rd St @ Mahon Ave
* `54397` – Southbound Capilano Rd @ 3500 Block
* `54401` – Eastbound W 22 St @ Pemberton Ave
* `54402` – Eastbound W 22 St @ Lloyd Ave
* `54406` – Southbound Lloyd Ave @ W 17 St
* `54407` – Eastbound Marine Dr @ Hamilton Ave
* `54408` – Eastbound Marine Dr @ Fell Ave
* `54409` – Eastbound W Esplanade @ Semisch Ave
* `54410` – Lonsdale Quay @ Bay 7
* `54411` – Park Royal @ Bay 5
* `54413` – Eastbound Marine Dr @ Capilano Rd
* `54416` – Eastbound Marine Dr @ Bridgman Ave
* `54417` – Eastbound Marine Dr @ Pemberton Ave
* `54418` – Eastbound Marine Dr @ Lloyd Ave
* `54421` – Lonsdale Quay @ Bay 1
* `54422` – Northbound Lillooet Rd @ Mt Seymour Parkway
* `54423` – Westbound Purcell Way @ Capilano University
* `54424` – Phibbs Exchange @ Bay 10
* `54429` – Westbound Marine Dr @ Lloyd Ave
* `54435` – Westbound Marine Dr @ Pemberton Ave
* `54439` – Westbound Marine Dr @ McGuire Ave
* `54440` – Westbound Marine Dr @ McGuire Ave
* `54441` – Park Royal @ Bay 2
* `54442` – Eastbound Marine Dr @ 1000 Block
* `54444` – Westbound W Georgia St @ Homer St
* `54445` – Westbound W Georgia St @ Seymour St
* `54446` – Westbound W Georgia St @ Burrard St
* `54447` – Westbound W Georgia St @ Bute St
* `54448` – Westbound W Georgia St @ Broughton St
* `54450` – Eastbound W Keith Rd @ Bewicke Ave
* `54457` – Eastbound E 15 St @ St. Georges Ave
* `54464` – Westbound W 15 St @ Lonsdale Ave
* `54465` – Westbound W 15 St @ Chesterfield Ave
* `54473` – Eastbound W Georgia St @ Richards St
* `54485` – Northbound Highland Blvd @ Woodbine Dr
* `54516` – Lonsdale Quay @ Bay 8
* `54547` – Northbound Capilano Rd @ Ridgewood Dr
* `54556` – Keith Rd @ Horseshoe Bay Ferry Terminal
* `54557` – Southbound Nelson Ave @ Bay St
* `54559` – Eastbound Marine Dr @ Nelson Ave
* `54562` – Southbound Marine Dr @ Orchill Rd
* `54573` – Eastbound Marine Dr @ Beacon Lane
* `54575` – Eastbound Marine Dr @ McKenzie Dr
* `54578` – Eastbound Marine Dr @ 4100 Block
* `54584` – Eastbound Marine Dr @ Creery Ave
* `54587` – Eastbound Marine Dr @ 31 St
* `54593` – Eastbound Marine Dr @ 24 St
* `54595` – Eastbound Marine Dr @ 22 St
* `54596` – Eastbound Marine Dr @ 21 St
* `54600` – Eastbound Marine Dr @ 17 St
* `54601` – Eastbound Marine Dr @ 15 St
* `54602` – Eastbound Marine Dr @ 14 St
* `54607` – Westbound Marine Dr @ Capilano River
* `54608` – Park Royal @ Bay 3
* `54611` – Westbound Marine Dr @ 13 St
* `54617` – Westbound Marine Dr @ 21 St
* `54618` – Westbound Marine Dr @ 22 St
* `54620` – Westbound Marine Dr @ 23 St
* `54621` – Westbound Marine Dr @ 24 St
* `54622` – Westbound Marine Dr @ 25 St
* `54634` – Westbound Marine Dr @ Rose Cres
* `54635` – Westbound Marine Dr @ Sherman St
* `54636` – Westbound Marine Dr @ 4200 Block
* `54637` – Westbound Marine Dr @ Morgan Cres
* `54638` – Westbound Marine Dr @ N Piccadilly Rd
* `54639` – Westbound Marine Dr @ McKenzie Dr
* `54642` – Northbound Marine Dr @ Crossway
* `54644` – Northbound Marine Dr @ Greentree Rd
* `54645` – Northbound Marine Dr @ Greenleaf Rd
* `54646` – Northbound Marine Dr @ Westport Rd
* `54647` – Westbound Marine Dr @ Cranley Dr
* `54648` – Northbound Marine Dr @ Primrose Place
* `54652` – Northbound Marine Dr @ Orchill Rd
* `54656` – Northbound Royal Ave @ Argyle Ave
* `54657` – Eastbound Bruce St @ Royal Ave
* `54713` – Northbound 31 St @ Dickinson Cres
* `54714` – Westbound Mathers Ave @ 31 St
* `54716` – Westbound Westmount Rd @ Benbow Rd
* `54717` – Westbound Westmount Rd @ 3300 Block
* `54718` – Westbound Westmount Rd @ Rockview Place
* `54719` – Westbound Westmount Rd @ Cedaridge Place
* `54720` – Westbound Westridge Ave @ Southridge Ave East
* `54721` – Westbound Westridge Ave @ 3800 Block
* `54732` – Northbound Taylor Way @ Hwy One Overpass
* `54775` – Southbound Taylor Way @ Hwy One Overpass
* `54780` – Westbound Marine Dr @ 14 St
* `54793` – Scottsdale Exchange @ Bay 6
* `54794` – Southbound Scott Rd @ 7300 Block
* `54795` – Westbound 72 Ave @ Scott Rd
* `54796` – Westbound 72 Ave @ 118 St
* `54802` – Northbound Nordel Way @ Swenson Way
* `54803` – Northbound Nordel Way @ Nordel Court
* `54805` – Westbound River Rd @ 9500 Block
* `54806` – Westbound River Rd @ 9200 Block
* `54807` – Westbound River Rd @ 8700 Block
* `54808` – Westbound River Rd @ Alexander Rd
* `54809` – Westbound River Rd @ Webster Rd
* `54811` – Westbound Vantage Way @ 80 St
* `54812` – Westbound Vantage Way @ Vantage Place
* `54813` – Southbound 76 St @ Vantage Way
* `54814` – Westbound Progress Way @ 76 St
* `54815` – Westbound Progress Way @ Brown St
* `54816` – Westbound Progress Way @ Honeyman St
* `54818` – Northbound 72 St @ Vantage Way
* `54819` – Westbound River Rd @ Hopcott Rd
* `54820` – Westbound River Rd @ McDonald Rd
* `54821` – Southbound River Rd @ Deas Island Rd
* `54822` – Westbound Hwy 17A @ 60 Ave
* `54824` – Marpole Loop @ Bay 4
* `54826` – Northbound Granville St @ W 10 Ave
* `54828` – Southbound Howe St @ W Pender St
* `54830` – Southbound Howe St @ W Georgia St
* `54831` – Southbound Howe St @ Robson St
* `54832` – Southbound Howe St @ Nelson St
* `54835` – Southbound Granville St @ W Cloverleaf
* `54839` – Northbound 62B St @ 60 Ave
* `54840` – Northbound River Rd @ Deas Island Rd
* `54841` – Eastbound River Rd @ 6400 Block
* `54842` – Southbound 72 St @ River Rd
* `54843` – Eastbound Progress Way @ 72 St
* `54844` – Eastbound Progress Way @ Honeyman St
* `54845` – Eastbound Progress Way @ Brown St
* `54846` – Northbound 76 St @ Progress Way
* `54847` – Eastbound Vantage Way @ 76 St
* `54848` – Eastbound Vantage Way @ Vantage Place
* `54849` – Eastbound Vantage Way @ 7800 Block
* `54851` – Eastbound River Rd @ Huston Rd
* `54852` – Eastbound River Rd @ Webster Rd
* `54853` – Eastbound River Rd @ Alexander Rd
* `54854` – Eastbound River Rd @ 8800 Block
* `54855` – Eastbound River Rd @ 9300 Block
* `54856` – Eastbound River Rd @ 9500 Block
* `54858` – Southbound Nordel Way @ River Rd
* `54863` – Eastbound 72 Ave @ 116 St
* `54865` – Northbound Scott Rd @ 72 Ave
* `54866` – Southbound Scott Rd @ 72 Ave
* `54867` – Southbound Scott Rd @ 70 Ave
* `54869` – Southbound Scott Rd @ Wade Rd
* `54870` – Southbound Scott Rd @ 64 Ave
* `54871` – Southbound Scott Rd @ 62 Ave
* `54873` – Southbound Scott Rd @ Hwy 10
* `54874` – Westbound Hwy 10 @ 112 St
* `54875` – Westbound Hwy 10 @ 104 St
* `54876` – Westbound Hwy 10 @ 96 St
* `54877` – Eastbound Hwy 10 @ Hornby Dr
* `54878` – Eastbound Hwy 10 @ 96 St
* `54879` – Eastbound Hwy 10 @ 104 St
* `54880` – Eastbound Hwy 10 @ 112 St
* `54883` – Northbound Scott Rd @ 62 Ave
* `54884` – Northbound Scott Rd @ 64 Ave
* `54885` – Northbound Scott Rd @ 66 Ave
* `54886` – Northbound Scott Rd @ 68 Ave
* `54887` – Scottsdale Exchange @ Bay 3
* `54909` – Eastbound 96 Ave @ Scott Rd
* `54910` – Northbound Scott Rd @ 96 Ave
* `54914` – Northbound Scott Rd @ Old Yale Rd
* `54915` – Northbound Scott Rd @ Lien Rd
* `54917` – Southbound Scott Rd @ Old Yale Rd
* `54920` – Westbound 96 Ave @ Scott Rd
* `54939` – Eastbound 72 Ave @ Nicholson Rd
* `54945` – Westbound Grace Rd @ 117 St
* `54949` – Westbound River Rd @ 11000 Block
* `54950` – Westbound River Rd @ Russell Dr
* `54951` – Westbound River Rd @ Brooke Rd
* `54952` – Westbound River Rd @ Delwood Dr
* `54955` – Eastbound River Rd @ Centre St
* `54957` – Eastbound River Rd @ Delwood Dr
* `54965` – Eastbound 84 Ave @ Brooke Rd
* `54967` – Eastbound 84 Ave @ 112 St
* `54969` – Eastbound 84 Ave @ 116 St
* `54977` – Northbound Scott Rd @ 93A Ave
* `54984` – Eastbound 96 Ave @ 130A St
* `54985` – Eastbound 96 Ave @ 132 St
* `54987` – Southbound King George Blvd @ 96 Ave
* `54988` – Northbound King George Blvd @ 96 Ave
* `54990` – King George Station @ Bay 2
* `54993` – Surrey Central Station @ Bay 8
* `54994` – Southbound University Dr @ 10100 Block
* `54995` – Eastbound Old Yale Rd @ University Dr
* `54996` – King George Station @ Bay 1
* `54997` – Southbound King George Blvd @ 98 Ave
* `55000` – Westbound 96 Ave @ 132 St
* `55014` – Southbound Scott Rd @ 90 Ave
* `55020` – Westbound 84 Ave @ 116 St
* `55022` – Westbound 84 Ave @ 112 St
* `55024` – Northbound Brooke Rd @ 84 Ave
* `55026` – Southbound Dunlop Rd @ Byron Rd
* `55027` – Southbound Dunlop Rd @ Delnova Dr
* `55028` – Southbound Dunlop Rd @ Sunset Dr
* `55029` – Southbound Dunlop Rd @ 10400 Block
* `55030` – Westbound Centre St @ Skagit Dr
* `55031` – Westbound Centre St @ Main St
* `55032` – Eastbound River Rd @ Brooke Rd
* `55033` – Eastbound River Rd @ Russell Dr
* `55034` – Eastbound River Rd @ 11000 Block
* `55035` – Eastbound River Rd @ 92A Ave
* `55036` – Eastbound River Rd @ Knudson Rd
* `55037` – Eastbound River Rd @ Regal Dr
* `55038` – Eastbound River Rd @ Millar Rd
* `55041` – Scottsdale Exchange @ Bay 2
* `55045` – Northbound 116 St @ 73A Ave
* `55046` – Northbound 116 St @ 75A Ave
* `55047` – Northbound 116 St @ 76A Ave
* `55048` – Northbound 116 St @ 77A Ave
* `55049` – Northbound 116 St @ 78A Ave
* `55050` – Northbound 116 St @ 80 Ave
* `55051` – Northbound 116 St @ 82 Ave
* `55059` – Northbound 123A St @ 96 Ave
* `55062` – Eastbound 100 Ave @ 123A St
* `55063` – Eastbound 100 Ave @ 124B St
* `55064` – Eastbound 100 Ave @ Helen Dr
* `55065` – Eastbound 100 Ave @ 128 St
* `55066` – Eastbound 100 Ave @ 130 St
* `55067` – Northbound 132 St @ 100 Ave
* `55068` – Eastbound Old Yale Rd @ 132 St
* `55069` – Northbound University Dr @ Old Yale Rd
* `55070` – Surrey Central Station @ Bay 9
* `55071` – Westbound Old Yale Rd @ University Dr
* `55072` – Southbound 132 St @ Old Yale Rd
* `55073` – Westbound 100 Ave @ 132 St
* `55074` – Westbound 100 Ave @ 129A St
* `55075` – Westbound 100 Ave @ 128 St
* `55076` – Westbound 100 Ave @ Helen Dr
* `55077` – Westbound 100 Ave @ 124B St
* `55078` – Southbound 123A St @ 100 Ave
* `55095` – Southbound 116 St @ 73A Ave
* `55096` – Scott Road Station @ Bay 7
* `55100` – Westbound 114 Ave @ 126A St
* `55101` – Westbound 114 Ave @ 124 St
* `55107` – Eastbound 114 Ave @ 132 St
* `55108` – Southbound 133A St @ 114 Ave
* `55109` – Southbound 133A St @ 112A Ave
* `55110` – Eastbound 112 Ave @ 13400 Block
* `55111` – Eastbound 113 Ave @ 136 St
* `55112` – Eastbound 113 Ave @ 138 St
* `55113` – Eastbound 113 Ave @ 140 St
* `55115` – Eastbound Grosvenor Rd @ Gladstone Dr
* `55116` – Northbound McBride Dr @ 114 Ave
* `55117` – Northbound McBride Dr @ King Rd
* `55118` – Eastbound 116A Ave @ 14500 Block
* `55119` – Southbound Surrey Rd @ 116A Ave
* `55120` – Southbound Surrey Rd @ Wellington Dr
* `55121` – Southbound Wallace Dr @ Roxburgh Rd
* `55122` – Southbound 148 St @ 111A Ave
* `55123` – Southbound 148 St @ 110 Ave
* `55124` – Eastbound 108 Ave @ 148 St
* `55125` – Eastbound 108 Ave @ Oriole Dr
* `55126` – Southbound 150 St @ 107A Ave
* `55127` – Southbound 150 St @ 10600 Block
* `55128` – Guildford Exchange @ Bay 1
* `55131` – Guildford Exchange @ Bay 2
* `55133` – Northbound 150 St @ 10600 Block
* `55135` – Westbound 108 Ave @ Oriole Dr
* `55136` – Northbound 148 St @ 108 Ave
* `55137` – Northbound 148 St @ 110 Ave
* `55138` – Northbound Wallace Dr @ Ellendale Dr
* `55139` – Northbound Surrey Rd @ Wallace Dr
* `55140` – Northbound Surrey Rd @ Wellington Dr
* `55141` – Northbound Surrey Rd @ 115A Ave
* `55142` – Westbound 116A Ave @ King Rd
* `55143` – Southbound McBride Dr @ King Rd
* `55144` – Southbound Grosvenor Rd @ 114 Ave
* `55146` – Northbound Kindersley Dr @ Grosvenor Rd
* `55147` – Westbound 113 Ave @ 140 St
* `55148` – Westbound 113 Ave @ 138 St
* `55149` – Westbound 113 Ave @ 137 St
* `55150` – Southbound 136 St @ 113 Ave
* `55151` – Westbound 112 Ave @ 13400 Block
* `55152` – Northbound 133A St @ 112A Ave
* `55153` – Northbound 133A St @ Crestview Dr
* `55154` – Westbound 114 Ave @ 132 St
* `55160` – Eastbound 114 Ave @ 124 St
* `55161` – Eastbound 114 Ave @ 126A St
* `55162` – Southbound 128 St @ 114 Ave
* `55164` – Ladner Exchange @ Bay 6
* `55165` – Northbound Harvest Dr @ Ladner Trunk Rd
* `55166` – Eastbound Hwy 10 @ Hwy 17A
* `55167` – Eastbound Hwy 10 @ 62 St
* `55168` – Eastbound Hwy 10 @ 64 St
* `55169` – Eastbound Hwy 10 @ 66 St
* `55170` – Eastbound Hwy 10 @ Anderson Place
* `55172` – Southbound 72 St @ Hwy 10
* `55173` – Eastbound Churchill St @ 72 St
* `55174` – Eastbound Churchill St @ King St
* `55175` – Eastbound Hwy 10 @ 80 St
* `55176` – Eastbound Hwy 10 @ 88 St
* `55177` – Northbound Scott Rd @ 70 Ave
* `55178` – Scottsdale Exchange @ Bay 7
* `55179` – Westbound Hwy 10 @ Hornby Dr
* `55180` – Westbound Hwy 10 @ 88 St
* `55182` – Southbound 80 St @ Ladner Trunk Rd
* `55183` – Westbound Churchill St @ King St
* `55184` – Westbound Churchill St @ 72 St
* `55185` – Westbound Hwy 10 @ 72 St
* `55186` – Westbound Hwy 10 @ Anderson Place
* `55187` – Westbound Hwy 10 @ 66 St
* `55188` – Westbound Hwy 10 @ 64 St
* `55189` – Westbound Hwy 10 @ 62 St
* `55190` – Westbound Ladner Trunk Rd @ Hwy 17A
* `55191` – Southbound Harvest Dr @ Ladner Trunk Rd
* `55192` – Scottsdale Exchange @ Bay 9
* `55193` – Northbound Scott Rd @ 74 Ave
* `55196` – Northbound Scott Rd @ 80 Ave
* `55201` – Southbound Scott Rd @ 96 Ave
* `55202` – Southbound Scott Rd @ 93A Ave
* `55203` – Southbound Scott Rd @ 92 Ave
* `55206` – Southbound Scott Rd @ 80 Ave
* `55209` – Southbound Scott Rd @ 75A Ave
* `55210` – Surrey Central Station @ Bay 2
* `55214` – Eastbound 104 Ave @ Whalley Blvd
* `55215` – Eastbound 104 Ave @ 139 St
* `55216` – Eastbound 104 Ave @ 140 St
* `55217` – Eastbound 104 Ave @ 142 St
* `55218` – Eastbound 104 Ave @ 144 St
* `55219` – Eastbound 104 Ave @ 146 St
* `55221` – Eastbound 104 Ave @ 149 St
* `55222` – Eastbound 104 Ave @ 150 St
* `55223` – Northbound 150 St @ 105 Ave
* `55225` – Southbound 152 St @ 102A Ave
* `55226` – Southbound 152 St @ 101 Ave
* `55227` – Southbound 152 St @ 100 Ave
* `55228` – Southbound 152 St @ 98 Ave
* `55229` – Southbound 152 St @ 96 Ave
* `55232` – Southbound 152 St @ 91A Ave
* `55233` – Eastbound Fraser Hwy @ 152 St
* `55235` – Westbound Fraser Hwy @ 156 St
* `55236` – Eastbound Fraser Hwy @ 156 St
* `55243` – Eastbound Fraser Hwy @ 160 St
* `55247` – Eastbound Fraser Hwy @ 168 St
* `55253` – Westbound 60 Ave @ 176 St
* `55258` – Southbound 177B St @ 56A Ave
* `55259` – Eastbound Hwy 10 @ 177B St
* `55260` – Eastbound Hwy 10 @ 180 St
* `55262` – Eastbound Hwy 10 @ 184 St
* `55263` – Eastbound Hwy 10 @ 18600 Block
* `55264` – Eastbound Hwy 10 @ 188 St
* `55265` – Eastbound 56 Ave @ Production Blvd
* `55266` – Eastbound 56 Ave @ Production Way
* `55267` – Eastbound 56 Ave @ 198 St
* `55271` – Eastbound 60 Ave @ 180 St
* `55273` – Eastbound 60 Ave @ 184 St
* `55275` – Eastbound 60 Ave @ 188 St
* `55282` – Eastbound 64 Ave @ 196 St
* `55283` – Southbound 197 St @ 64 Ave
* `55284` – Westbound Willowbrook Dr @ 197 St
* `55285` – Southbound Willowbrook Dr @ 19600 Block
* `55286` – Eastbound Fraser Hwy @ Langley Bypass
* `55287` – Eastbound Fraser Hwy @ Production Way
* `55288` – Southbound 200 St @ Fraser Hwy
* `55289` – Eastbound 56 Ave @ 200 St
* `55290` – Eastbound 56 Ave @ 201A St
* `55291` – Eastbound Douglas Cres @ 203 St
* `55299` – Langley Centre @ Bay 1
* `55308` – Northbound 203 St @ 54 Ave
* `55309` – Northbound 203 St @ Michaud Cres
* `55311` – Westbound 56 Ave @ 201A St
* `55312` – Westbound 56 Ave @ 200 St
* `55313` – Westbound 56 Ave @ 198 St
* `55314` – Westbound 56 Ave @ Production Way
* `55316` – Westbound Hwy 10 @ 192 St
* `55317` – Westbound Hwy 10 @ 188 St
* `55319` – Westbound Hwy 10 @ 184 St
* `55321` – Westbound Hwy 10 @ 180 St
* `55322` – Westbound Hwy 10 @ 17800 Block
* `55323` – Northbound 200 St @ 56 Ave
* `55324` – Westbound Fraser Hwy @ Production Way
* `55325` – Westbound Fraser Hwy @ Langley Bypass
* `55326` – Northbound Willowbrook Dr @ 19600 Block
* `55328` – Westbound 64 Ave @ 197 St
* `55329` – Westbound 64 Ave @ 194 St
* `55332` – Westbound 60 Ave @ 192 St
* `55334` – Westbound 60 Ave @ 188 St
* `55336` – Westbound 60 Ave @ 184 St
* `55338` – Westbound 60 Ave @ 180 St
* `55355` – Westbound Fraser Hwy @ 160 St
* `55357` – Northbound 152 St @ Fraser Hwy
* `55359` – Northbound 152 St @ 92 Ave
* `55361` – Northbound 152 St @ 96 Ave
* `55362` – Northbound 152 St @ 98 Ave
* `55363` – Northbound 152 St @ 100 Ave
* `55365` – Northbound 152 St @ 102A Ave
* `55368` – Westbound 104 Ave @ 146 St
* `55369` – Westbound 104 Ave @ 144 St
* `55370` – Westbound 104 Ave @ 142 St
* `55371` – Westbound 104 Ave @ 140 St
* `55372` – Westbound 104 Ave @ 138A St
* `55373` – Westbound 104 Ave @ Whalley Blvd
* `55375` – Northbound King George Blvd @ 1100 Block
* `55377` – Northbound King George Blvd @ 14 Ave
* `55379` – Westbound 16 Ave @ 160 St
* `55382` – Westbound 16 Ave @ 154 St
* `55383` – White Rock Centre @ Bay 8
* `55384` – Northbound 152 St @ 18 Ave
* `55385` – Northbound 152 St @ 19 Ave
* `55386` – Northbound 152 St @ 20 Ave
* `55388` – Northbound 152 St @ 24 Ave
* `55390` – Northbound 152 St @ King George Blvd
* `55392` – Northbound 152 St @ 28 Ave
* `55393` – Northbound King George Blvd @ 32 Ave Diversion
* `55398` – Northbound King George Blvd @ 5000 Block
* `55399` – Northbound King George Blvd @ Hwy 10
* `55401` – Northbound King George Blvd @ 60 Ave
* `55402` – Northbound King George Blvd @ 62 Ave
* `55403` – Northbound King George Blvd @ 64 Ave
* `55405` – Eastbound 68 Ave @ King George Blvd
* `55406` – Northbound 138 St @ Hyland Rd
* `55407` – Northbound 138 St @ 68 Ave
* `55408` – Northbound 138 St @ 70 Ave
* `55409` – Northbound 138 St @ 7100 Block
* `55410` – Westbound 72 Ave @ 138 St
* `55411` – Newton Exchange @ Bay 3
* `55412` – Northbound King George Blvd @ 72 Ave
* `55413` – Northbound King George Blvd @ 74 Ave
* `55414` – Northbound King George Blvd @ 76 Ave
* `55416` – Northbound King George Blvd @ 80 Ave
* `55419` – Northbound King George Blvd @ 84 Ave
* `55421` – Northbound King George Blvd @ 88 Ave
* `55423` – Northbound King George Blvd @ 92 Ave
* `55434` – Eastbound King George Blvd @ 128 St
* `55435` – Southbound King George Blvd @ 132 St
* `55437` – Southbound King George Blvd @ 108 Ave
* `55439` – Southbound King George Blvd @ 105A Ave
* `55441` – Surrey Central Station @ Bay 10
* `55443` – Southbound King George Blvd @ 92 Ave
* `55447` – Southbound King George Blvd @ 84 Ave
* `55450` – Southbound King George Blvd @ 80 Ave
* `55452` – Southbound King George Blvd @ 76 Ave
* `55453` – Southbound King George Blvd @ 73A Ave
* `55454` – Eastbound 72 Ave @ King George Blvd
* `55456` – Newton Exchange @ Bay 5
* `55457` – Southbound 138 St @ 72 Ave
* `55458` – Southbound 138 St @ 7100 Block
* `55459` – Southbound 138 St @ 70 Ave
* `55460` – Southbound 138 St @ 6800 Block
* `55461` – Southbound King George Blvd @ 68 Ave
* `55463` – Southbound King George Blvd @ 64 Ave
* `55464` – Southbound King George Blvd @ 62 Ave
* `55465` – Southbound King George Blvd @ 60 Ave
* `55466` – Southbound King George Blvd @ 58 Ave
* `55467` – Southbound King George Blvd @ Hwy 10
* `55468` – Southbound King George Blvd @ 4900 Block
* `55469` – Southbound King George Blvd @ 44 Ave
* `55471` – Southbound King George Blvd @ Crescent Rd
* `55474` – Southbound King George Blvd @ 32 Ave Diversion
* `55476` – Southbound King George Blvd @ 28 Ave
* `55477` – Southbound 152 St @ 29A Ave
* `55479` – Southbound 152 St @ King George Blvd
* `55481` – Southbound 152 St @ 24 Ave
* `55483` – Southbound 152 St @ 20 Ave
* `55484` – Southbound 152 St @ 19 Ave
* `55485` – Southbound Martin Dr @ E Southmere Cres
* `55487` – Southbound 152 St @ 18 Ave
* `55488` – White Rock Centre @ Bay 10
* `55490` – Eastbound North Bluff Rd @ Best St
* `55493` – Eastbound 16 Ave @ 160 St
* `55496` – Southbound King George Blvd @ 164 St
* `55497` – Northbound King George Blvd @ 8th Ave
* `55499` – Scottsdale Exchange @ Bay 5
* `55500` – Eastbound 72 Ave @ 122 St
* `55501` – Eastbound 72 Ave @ 124 St
* `55502` – Eastbound 72 Ave @ 126 St
* `55503` – Southbound 128 St @ 72 Ave
* `55505` – Southbound 128 St @ 68 Ave
* `55507` – Southbound 128 St @ 64 Ave
* `55509` – Southbound 128 St @ 60 Ave
* `55517` – Eastbound New McLellan Rd @ 124 St
* `55524` – Northbound 132 St @ 56 Ave
* `55526` – Northbound 132 St @ 60 Ave
* `55530` – Northbound 132 St @ 64 Ave
* `55533` – Northbound 132 St @ 68 Ave
* `55536` – Eastbound 72 Ave @ 132 St
* `55538` – Newton Exchange @ Bay 1
* `55539` – Westbound 72 Ave @ King George Blvd
* `55540` – Westbound 72 Ave @ Comber Way
* `55541` – Southbound 132 St @ 72 Ave
* `55544` – Southbound 132 St @ 68 Ave
* `55546` – Southbound 132 St @ 64 Ave
* `55548` – Southbound 132 St @ 60 Ave
* `55550` – Westbound 56 Ave @ 136 St
* `55551` – Westbound 56 Ave @ 132 St
* `55555` – Westbound New McLellan Rd @ 124 St
* `55564` – Northbound 128 St @ 60 Ave
* `55568` – Northbound 128 St @ 64 Ave
* `55570` – Northbound 128 St @ 68 Ave
* `55574` – Westbound 72 Ave @ 128 St
* `55575` – Westbound 72 Ave @ 126 St
* `55576` – Westbound 72 Ave @ 124 St
* `55577` – Westbound 72 Ave @ 122A St
* `55578` – Newton Exchange @ Bay 6
* `55579` – Northbound 138 St @ 72 Ave
* `55580` – Northbound 138 St @ 74 Ave
* `55582` – Westbound 76 Ave @ King George Blvd
* `55586` – Westbound 76 Ave @ 132 St
* `55589` – Northbound 128 St @ 76 Ave
* `55591` – Northbound 128 St @ 82 Ave
* `55593` – Northbound 128 St @ 84 Ave
* `55596` – Northbound 128 St @ 88 Ave
* `55598` – Northbound 128 St @ 92 Ave
* `55601` – Northbound 128 St @ 96 Ave
* `55602` – Northbound 128 St @ 98 Ave
* `55603` – Northbound 128 St @ 99 Ave
* `55604` – Northbound 128 St @ 100 Ave
* `55605` – Northbound 128 St @ 102 Ave
* `55606` – Eastbound 104 Ave @ 128 St
* `55607` – Eastbound 104 Ave @ Old Yale Rd
* `55608` – Eastbound 104 Ave @ 13000 Block
* `55609` – Eastbound 104 Ave @ 132 St
* `55610` – Eastbound 104 Ave @ University Dr
* `55611` – Southbound University Dr @ 104 Ave
* `55612` – Surrey Central Station @ Bay 4
* `55613` – Westbound 104 Ave @ University Dr
* `55614` – Westbound 104 Ave @ 133 St
* `55615` – Westbound 104 Ave @ 132 St
* `55616` – Westbound 104 Ave @ 13000 Block
* `55617` – Westbound 104 Ave @ Old Yale Rd
* `55618` – Southbound 128 St @ 104 Ave
* `55619` – Southbound 128 St @ 102 Ave
* `55620` – Southbound 128 St @ 100 Ave
* `55621` – Southbound 128 St @ 99 Ave
* `55622` – Southbound 128 St @ 97A Ave
* `55623` – Southbound 128 St @ 96 Ave
* `55626` – Southbound 128 St @ 92 Ave
* `55628` – Southbound 128 St @ 88 Ave
* `55631` – Southbound 128 St @ 84 Ave
* `55633` – Southbound 128 St @ 80 Ave
* `55635` – Eastbound 76 Ave @ 128 St
* `55638` – Eastbound 76 Ave @ 132 St
* `55640` – Northbound 132 St @ 7700 Block
* `55642` – Eastbound 76 Ave @ King George Blvd
* `55645` – Newton Exchange @ Bay 2
* `55648` – Westbound 68 Ave @ King George Blvd
* `55651` – Northbound 132 St @ 72 Ave
* `55653` – Northbound 132 St @ 76 Ave
* `55655` – Northbound 132 St @ 80 Ave
* `55658` – Northbound 132 St @ 84 Ave
* `55660` – Northbound 132 St @ 88 Ave
* `55662` – Northbound 132 St @ 92 Ave
* `55664` – Northbound 132 St @ 96 Ave
* `55665` – Northbound 132 St @ 98 Ave
* `55668` – Southbound 132 St @ 100 Ave
* `55669` – Southbound 132 St @ 98 Ave
* `55670` – Southbound 132 St @ 96 Ave
* `55672` – Southbound 132 St @ 92 Ave
* `55674` – Southbound 132 St @ 88 Ave
* `55676` – Southbound 132 St @ 84 Ave
* `55679` – Southbound 132 St @ 80 Ave
* `55681` – Southbound 132 St @ 7700 Block
* `55682` – Southbound 132 St @ 76 Ave
* `55684` – Eastbound 68 Ave @ 132 St
* `55688` – Newton Exchange @ Bay 7
* `55689` – Eastbound 72 Ave @ 138 St
* `55690` – Northbound 140 St @ 72 Ave
* `55692` – Northbound 140 St @ 75 Ave
* `55695` – Northbound 140 St @ 80 Ave
* `55698` – Northbound 140 St @ 84 Ave
* `55701` – Northbound 140 St @ 88 Ave
* `55703` – Northbound 140 St @ 92 Ave
* `55706` – Northbound 140 St @ 96 Ave
* `55707` – Northbound 140 St @ Fraser Hwy
* `55708` – Northbound 140 St @ Green Timbers Way
* `55710` – Westbound 102 Ave @ 140 St
* `55711` – Westbound 102 Ave @ 139 St
* `55712` – Westbound 102 Ave @ 137A St
* `55713` – Surrey Central Station @ Bay 5
* `55714` – Surrey Central Station @ Bay 6
* `55715` – Eastbound 102 Ave @ Whalley Blvd
* `55716` – Eastbound 102 Ave @ 137A St
* `55717` – Eastbound 102 Ave @ 139 St
* `55718` – Southbound 140 St @ 100A Ave
* `55719` – Southbound 140 St @ 100 Ave
* `55720` – Southbound 140 St @ Green Timbers Way
* `55721` – Southbound 140 St @ Fraser Hwy
* `55722` – Southbound 140 St @ 96 Ave
* `55724` – Southbound 140 St @ 92 Ave
* `55726` – Southbound 140 St @ 88 Ave
* `55729` – Southbound 140 St @ 84 Ave
* `55732` – Southbound 140 St @ 80 Ave
* `55737` – Westbound 72 Ave @ 140 St
* `55738` – Surrey Central Station @ Bay 7
* `55739` – Eastbound 96 Ave @ 137 St
* `55740` – Eastbound 96 Ave @ 138 St
* `55741` – Eastbound 96 Ave @ 139 St
* `55742` – Eastbound 88 Ave @ 140 St
* `55743` – Eastbound 88 Ave @ 142A St
* `55744` – Eastbound 88 Ave @ 144 St
* `55745` – Eastbound 88 Ave @ 146 St
* `55746` – Eastbound 88 Ave @ 148 St
* `55747` – Eastbound 88 Ave @ 150 St
* `55748` – Eastbound 88 Ave @ 152 St
* `55749` – Eastbound 88 Ave @ 154 St
* `55750` – Northbound 156 St @ 88 Ave
* `55752` – Northbound 156 St @ 92 Ave
* `55754` – Northbound 156 St @ 96 Ave
* `55755` – Northbound 156 St @ 98 Ave
* `55756` – Northbound 156 St @ 100 Ave
* `55757` – Northbound 156 St @ 102 Ave
* `55758` – Westbound 104 Ave @ 156 St
* `55759` – Westbound 104 Ave @ 154 St
* `55760` – Westbound 104 Ave @ 152 St
* `55761` – Eastbound 104 Ave @ 152 St
* `55762` – Eastbound 104 Ave @ 154 St
* `55763` – Southbound 156 St @ 104 Ave
* `55764` – Southbound 156 St @ 102 Ave
* `55765` – Southbound 156 St @ 100 Ave
* `55766` – Southbound 156 St @ 98 Ave
* `55767` – Southbound 156 St @ 96 Ave
* `55769` – Southbound 156 St @ 92 Ave
* `55771` – Westbound 88 Ave @ 156 St
* `55772` – Westbound 88 Ave @ 154A St
* `55773` – Westbound 88 Ave @ 152 St
* `55774` – Westbound 88 Ave @ 150 St
* `55775` – Westbound 88 Ave @ 148 St
* `55776` – Westbound 88 Ave @ 146 St
* `55777` – Westbound 88 Ave @ 144 St
* `55778` – Westbound 88 Ave @ 142A St
* `55779` – Westbound 96 Ave @ 140 St
* `55780` – Westbound 96 Ave @ 139 St
* `55781` – Westbound 96 Ave @ 137B St
* `55782` – Northbound 128 St @ 104 Ave
* `55783` – Northbound 128 St @ 106 Ave
* `55784` – Northbound 128 St @ 107A Ave
* `55785` – Eastbound 108 Ave @ 128 St
* `55786` – Eastbound 108 Ave @ 130 St
* `55787` – Eastbound 108 Ave @ 132 St
* `55788` – Eastbound 108 Ave @ 133 St
* `55789` – Gateway Station @ Bay 3
* `55790` – Eastbound 108 Ave @ King George Blvd
* `55791` – Eastbound Grosvenor Rd @ Hilton Rd
* `55792` – Eastbound Grosvenor Rd @ Berg Rd
* `55793` – Eastbound Grosvenor Rd @ Brentwood Cres
* `55797` – Northbound 143A St @ Gladstone Dr
* `55800` – Eastbound 112 Ave @ 138 St
* `55801` – Westbound Grosvenor Rd @ Hansen Rd
* `55802` – Westbound Grosvenor Rd @ Harper Rd
* `55803` – Westbound Grosvenor Rd @ Howey Rd
* `55805` – Westbound 108 Ave @ 13600 Block
* `55806` – Westbound 108 Ave @ King George Blvd
* `55807` – Gateway Station @ Bay 1
* `55808` – Westbound 108 Ave @ 133 St
* `55809` – Westbound 108 Ave @ 132 St
* `55810` – Westbound 108 Ave @ 130 St
* `55811` – Westbound 108 Ave @ 12800 Block
* `55812` – Southbound 128 St @ 107A Ave
* `55813` – Southbound 128 St @ 106 Ave
* `55835` – Scott Road Station @ Bay 9
* `55836` – Surrey Central Station @ Bay 3
* `55838` – Northbound University Dr @ 105A Ave
* `55839` – Eastbound 108 Ave @ Whalley Blvd
* `55840` – Eastbound 108 Ave @ 138 St
* `55841` – Eastbound 108 Ave @ 140 St
* `55842` – Eastbound 108 Ave @ 142 St
* `55843` – Eastbound 108 Ave @ 144 St
* `55844` – Eastbound 108 Ave @ 146 St
* `55845` – Westbound 108 Ave @ 148 St
* `55846` – Westbound 108 Ave @ 146 St
* `55847` – Westbound 108 Ave @ 144 St
* `55848` – Westbound 108 Ave @ 142 St
* `55849` – Westbound 108 Ave @ 140 St
* `55850` – Westbound 108 Ave @ Whalley Blvd
* `55852` – Southbound University Dr @ 105A Ave
* `55853` – Southbound 150 St @ 105 Ave
* `55854` – Eastbound 104 Ave @ 156 St
* `55858` – Southbound 160 St @ 98 Ave
* `55859` – Southbound 160 St @ 96 Ave
* `55862` – Southbound 160 St @ 88 Ave
* `55863` – Southbound 160 St @ Fraser Hwy
* `55864` – Westbound 84 Ave @ 160 St
* `55867` – Westbound 84 Ave @ 156 St
* `55869` – Eastbound 84 Ave @ 152 St
* `55871` – Westbound 84 Ave @ 148 St
* `55873` – Southbound 144 St @ 84 Ave
* `55877` – Southbound 144 St @ 74A Ave
* `55879` – Westbound 72 Ave @ 144 St
* `55880` – Westbound 72 Ave @ 142 St
* `55881` – Eastbound 72 Ave @ 140 St
* `55882` – Northbound 144 St @ 72 Ave
* `55884` – Northbound 144 St @ 76 Ave
* `55893` – Eastbound 84 Ave @ 156 St
* `55895` – Northbound 148 St @ 84 Ave
* `55898` – Northbound 152 St @ 88 Ave
* `55899` – Southbound 148 St @ 88 Ave
* `55904` – Northbound 160 St @ Fraser Hwy
* `55905` – Northbound 160 St @ 88 Ave
* `55908` – Northbound 160 St @ 92 Ave
* `55911` – Northbound 160 St @ 98 Ave
* `55916` – Eastbound 104 Ave @ Fraserglen Dr
* `55917` – Eastbound 104 Ave @ 164 St
* `55918` – Eastbound 104 Ave @ Oak Gate
* `55919` – Northbound 168 St @ 104 Ave
* `55920` – Northbound 168 St @ 106 Ave
* `55922` – Westbound 108 Ave @ 165B St
* `55924` – Westbound 108 Ave @ 164 St
* `55925` – Westbound 108 Ave @ 162 St
* `55926` – Westbound 108 Ave @ 160 St
* `55932` – Southbound 160 St @ 92 Ave
* `55933` – 22nd Street Station @ Bay 6
* `55934` – Southbound Hwy 91A Onramp @ Howes St
* `55935` – Southbound Hwy 91 Offramp @ Cliveden Ave
* `55936` – Eastbound Kittson Parkway @ 109A St
* `55941` – Scottsdale Exchange @ Bay 4
* `55942` – Eastbound 72 Ave @ 128 St
* `55944` – Newton Exchange @ Bay 8
* `55945` – Eastbound 72 Ave @ 142 St
* `55946` – Northbound 144 St @ 80 Ave
* `55948` – Northbound 148 St @ 88 Ave
* `55950` – Northbound 148 St @ Fraser Hwy
* `55952` – Northbound 148 St @ 96 Ave
* `55953` – Northbound 148 St @ 98 Ave
* `55954` – Eastbound 100 Ave @ 148 St
* `55955` – Northbound 150 St @ 100 Ave
* `55956` – Northbound 150 St @ 101 Ave
* `55957` – Northbound 150 St @ 102A Ave
* `55958` – Southbound 144 St @ 72 Ave
* `55962` – Southbound 144 St @ 64 Ave
* `55963` – Southbound 144 St @ 60 Ave
* `55964` – Southbound 144 St @ 57 Ave
* `55967` – Eastbound 60 Ave @ 144 St
* `55969` – Eastbound 60 Ave @ 148 St
* `55973` – Northbound 152 St @ 64 Ave
* `55975` – Eastbound 60 Ave @ 168 St
* `55977` – Eastbound 60 Ave @ 172 St
* `55979` – Eastbound 60 Ave @ 175A St
* `55991` – Northbound 168 St @ 60 Ave
* `55993` – Southbound 152 St @ 64 Ave
* `55995` – Westbound 60 Ave @ 152 St
* `55997` – Westbound 60 Ave @ 148 St
* `55999` – Westbound 60 Ave @ 144 St
* `56002` – Southbound 150 St @ 104 Ave
* `56003` – Southbound 150 St @ 102A Ave
* `56004` – Southbound 150 St @ 101 Ave
* `56005` – Westbound 100 Ave @ 150 St
* `56006` – Southbound 148 St @ 100 Ave
* `56007` – Southbound 148 St @ 98 Ave
* `56008` – Southbound 148 St @ 96 Ave
* `56010` – Southbound 148 St @ Fraser Hwy
* `56012` – Southbound 144 St @ 80 Ave
* `56013` – Northbound 144 St @ 57 Ave
* `56014` – Northbound 144 St @ 60 Ave
* `56015` – Northbound 144 St @ 64 Ave
* `56019` – Newton Exchange @ Bay 4
* `56020` – Westbound 72 Ave @ 132 St
* `56022` – Scottsdale Exchange @ Bay 8
* `56023` – Westbound 64 Ave @ Scott Rd
* `56028` – Northbound Hwy 91 Offramp @ Cliveden Ave
* `56029` – Northbound Hwy 91A Onramp @ Howes St
* `56030` – White Rock Centre @ Bay 2
* `56031` – Northbound King George Blvd @ 16 Ave
* `56034` – Northbound King George Blvd @ 156 St
* `56036` – Northbound 152 St @ 3000 Block
* `56037` – Northbound 152 St @ 32 Ave
* `56039` – Northbound 152 St @ 36 Ave
* `56044` – Northbound 152 St @ 54A Ave
* `56045` – Northbound 152 St @ Hwy 10
* `56048` – Northbound 152 St @ 68 Ave
* `56052` – Northbound 152 St @ 82 Ave
* `56055` – Southbound 152 St @ Fraser Hwy
* `56056` – Southbound 152 St @ 88 Ave
* `56058` – Southbound 152 St @ 84 Ave
* `56059` – Southbound 152 St @ 82 Ave
* `56063` – Southbound 152 St @ 68 Ave
* `56067` – Southbound 152 St @ Panorama Dr
* `56068` – Southbound 152 St @ Hwy 10
* `56073` – Northbound 160 St @ 24 Ave
* `56077` – Southbound King George Blvd @ 24 Ave
* `56078` – Southbound King George Blvd @ 156 St
* `56081` – White Rock Centre @ Bay 5
* `56082` – Westbound Sullivan St @ Kidd Rd
* `56095` – Eastbound 16 Ave @ 128 St
* `56096` – Eastbound 16 Ave @ 130 St
* `56097` – Eastbound 16 Ave @ 132 St
* `56102` – Eastbound North Bluff Rd @ Nichol Rd
* `56106` – Eastbound North Bluff Rd @ Archibald Rd
* `56115` – Northbound Johnston Rd @ Russell Ave
* `56116` – White Rock Centre @ Bay 6
* `56125` – Northbound Granville St @ W 41 Ave
* `56136` – White Rock Centre @ Bay 3
* `56145` – Westbound 16 Ave @ 144 St
* `56149` – Westbound 16 Ave @ 140 St
* `56152` – Westbound 16 Ave @ 136 St
* `56154` – Westbound 16 Ave @ Amble Greene Blvd
* `56155` – Northbound 128 St @ 16 Ave
* `56164` – Westbound Crescent Rd @ Beckett Rd
* `56199` – Westbound Marine Dr @ 15100 Block
* `56201` – Westbound Marine Dr @ Vidal St
* `56220` – Southbound 128 St @ 16 Ave
* `56238` – Eastbound Marine Dr @ Martin St
* `56239` – Eastbound Marine Dr @ 15000 Block
* `56250` – Northbound 160 St @ 12 Ave
* `56251` – Northbound 160 St @ 13 Ave
* `56253` – Westbound Russell Ave @ Stayte Rd
* `56264` – Eastbound Buena Vista Ave @ Maple St
* `56265` – Eastbound Buena Vista Ave @ Parker St
* `56266` – Eastbound Buena Vista Ave @ Habgood St
* `56280` – White Rock Centre @ Bay 1
* `56312` – Eastbound 16 Ave @ 126A St
* `56337` – Eastbound Cranley Dr @ 157A St
* `56338` – Southbound Cranley Dr @ Suffolk Rd
* `56339` – Southbound Cranley Dr @ Cumbria Dr
* `56342` – Northbound 160 St @ Park Dr
* `56343` – Northbound 160 St @ 20 Ave
* `56344` – Northbound Cranley Dr @ Cumbria Dr
* `56345` – Northbound Cranley Dr @ Suffolk Rd
* `56346` – Westbound Cranley Dr @ 157A St
* `56347` – Westbound 24 Ave @ 156 St
* `56358` – Westbound 24 Ave @ 153 St
* `56380` – Eastbound 24 Ave @ 152 St
* `56388` – White Rock Centre @ Bay 7
* `56394` – Scottsdale Exchange @ Bay 1
* `56395` – Surrey Central Station @ Bay 1 Unloading Only
* `56396` – Southbound King George Blvd @ 10 Ave
* `56406` – Surrey Central Station @ Bay 11
* `56411` – Eastbound 64 Ave @ 197 St
* `56413` – Eastbound 64 Ave @ 19900 Block
* `56414` – Westbound 64 Ave @ 200 St
* `56415` – Westbound 64 Ave @ 198 St
* `56421` – Steveston Exchange @ Bay 2
* `56425` – Northbound No. 1 Rd @ Steveston Hwy
* `56428` – Northbound No. 1 Rd @ Williams Rd
* `56431` – Northbound No. 1 Rd @ Francis Rd
* `56433` – Northbound No. 1 Rd @ Pacemore Ave
* `56436` – Northbound No. 1 Rd @ Granville Ave
* `56437` – Northbound No. 1 Rd @ Tucker Ave
* `56438` – Eastbound Westminster Hwy @ Forsyth Cres
* `56439` – Eastbound Westminster Hwy @ Gibbons Dr
* `56440` – Eastbound Westminster Hwy @ Riverdale Dr
* `56441` – Eastbound Westminster Hwy @ Mccallan Rd
* `56442` – Eastbound Westminster Hwy @ Tiffany Blvd
* `56443` – Eastbound Westminster Hwy @ Lynas Lane
* `56444` – Eastbound Westminster Hwy @ No. 2 Rd
* `56446` – Eastbound Westminster Hwy @ Elmbridge Way
* `56447` – Eastbound Westminster Hwy @ Gilbert Rd
* `56448` – Eastbound Westminster Hwy @ Alderbridge Way
* `56449` – Eastbound Westminster Hwy @ Minoru Blvd
* `56450` – Southbound No. 3 Rd @ Westminster Hwy
* `56451` – Eastbound Cook Rd @ No. 3 Rd
* `56452` – Northbound No. 3 Rd @ Saba Rd
* `56453` – Northbound No. 3 Rd @ Ackroyd Rd
* `56454` – Lansdowne Station @ Bay 2
* `56459` – Westbound Capstan Way @ Garden City Rd
* `56464` – Eastbound Bridgeport Rd @ Gage Rd
* `56465` – Eastbound Bridgeport Rd @ No. 4 Rd
* `56470` – Southbound Garden City Rd @ Capstan Way
* `56471` – Southbound Garden City Rd @ Cambie Rd
* `56474` – Southbound Garden City Rd @ Lansdowne Rd
* `56475` – Southbound Garden City Rd @ Westminster Hwy
* `56476` – Southbound Garden City Rd @ Ferndale Rd
* `56479` – Westbound Cook Rd @ Cooney Rd
* `56482` – Westbound Westminster Hwy @ Minoru Blvd
* `56483` – Westbound Westminster Hwy @ Alderbridge Way
* `56484` – Westbound Westminster Hwy @ Gilbert Rd
* `56485` – Westbound Westminster Hwy @ Elmbridge Way
* `56486` – Westbound Westminster Hwy @ No. 2 Rd
* `56487` – Westbound Westminster Hwy @ Lynas Lane
* `56488` – Westbound Westminster Hwy @ Tiffany Blvd
* `56489` – Westbound Westminster Hwy @ Mccallan Rd
* `56490` – Westbound Westminster Hwy @ Riverdale Dr
* `56491` – Westbound Westminster Hwy @ Gibbons Dr
* `56492` – Southbound No. 1 Rd @ Westminster Hwy
* `56493` – Southbound No. 1 Rd @ Tucker Ave
* `56494` – Southbound No. 1 Rd @ Granville Ave
* `56496` – Southbound No. 1 Rd @ Blundell Rd
* `56497` – Southbound No. 1 Rd @ Pacemore Ave
* `56499` – Southbound No. 1 Rd @ Francis Rd
* `56502` – Southbound No. 1 Rd @ Williams Rd
* `56505` – Westbound Steveston Hwy @ No. 1 Rd
* `56511` – Eastbound Chatham St @ 4th Ave
* `56517` – Steveston Exchange @ Bay 3
* `56521` – Eastbound Moncton St @ Railway Ave
* `56526` – Northbound No. 2 Rd @ Steveston Hwy
* `56529` – Northbound No. 2 Rd @ Williams Rd
* `56532` – Northbound No. 2 Rd @ Francis Rd
* `56534` – Northbound No. 2 Rd @ Blundell Rd
* `56535` – Eastbound Blundell Rd @ No. 2 Rd
* `56539` – Eastbound Blundell Rd @ Gilbert Rd
* `56541` – Eastbound Blundell Rd @ Minoru Blvd
* `56542` – Northbound No. 3 Rd @ Blundell Rd
* `56549` – Southbound No. 3 Rd @ Cook Rd
* `56550` – Northbound No. 3 Rd @ Alderbridge Way
* `56551` – Northbound No. 3 Rd @ Leslie Rd
* `56554` – Eastbound Cambie Rd @ Brown Rd
* `56560` – Eastbound Sea Island Way @ No. 3 Rd
* `56562` – Westbound Cambie Rd @ Sexsmith Rd
* `56566` – Southbound No. 3 Rd @ Leslie Rd
* `56567` – Southbound No. 3 Rd @ Alderbridge Way
* `56570` – Southbound No. 3 Rd @ Granville Ave
* `56573` – Westbound Blundell Rd @ No. 3 Rd
* `56576` – Westbound Blundell Rd @ Gilbert Rd
* `56580` – Southbound No. 2 Rd @ Blundell Rd
* `56582` – Southbound No. 2 Rd @ Francis Rd
* `56585` – Southbound No. 2 Rd @ Williams Rd
* `56587` – Southbound No. 2 Rd @ Steveston Hwy
* `56590` – Westbound Moncton St @ No. 2 Rd
* `56592` – Westbound Moncton St @ Railway Ave
* `56596` – Northbound Entertainment Blvd @ Steveston Hwy
* `56597` – Westbound Steveston Hwy @ Palmberg Rd
* `56598` – Westbound Steveston Hwy @ Hwy 99
* `56600` – Westbound Steveston Hwy @ No. 5 Rd
* `56616` – Northbound No. 3 Rd @ Francis Rd
* `56617` – Northbound No. 3 Rd @ Bowcock Rd
* `56618` – Northbound No. 3 Rd @ Lucas Rd
* `56619` – Northbound No. 3 Rd @ Sunnymede Gate
* `56620` – Westbound Lansdowne Rd @ Kwantlen St
* `56622` – Southbound No. 3 Rd @ Blundell Rd
* `56623` – Southbound No. 3 Rd @ Sunnymede Gate
* `56624` – Southbound No. 3 Rd @ Lucas Rd
* `56625` – Southbound No. 3 Rd @ Penny Lane
* `56633` – Eastbound Steveston Hwy @ No. 3 Rd
* `56639` – Eastbound Steveston Hwy @ Swinton Cres
* `56642` – Eastbound Steveston Hwy @ Coppersmith Place
* `56644` – Eastbound Steveston Hwy @ Hwy 99
* `56646` – Eastbound Steveston Hwy @ Palmberg Rd
* `56647` – Entertainment Blvd @ Riverport Recreation Complex
* `56649` – Eastbound Granville Ave @ Buswell St
* `56653` – Eastbound Granville Ave @ Heather St
* `56664` – Ladner Exchange @ Bay 1
* `56665` – Southbound Hwy 17A @ Ladner Trunk Rd
* `56666` – Southbound Hwy 17 @ 56 St
* `56667` – Southbound Hwy 17 @ 52 St
* `56668` – Southbound Hwy 17 @ Tsawwassen Dr
* `56669` – Tsawwassen Ferry Terminal @ Bay 1
* `56670` – Northbound Hwy 17 @ Tsawwassen Dr
* `56671` – Northbound Hwy 17 @ 52 St
* `56672` – Northbound Hwy 17 @ 56 St
* `56673` – Ladner Exchange @ Bay 4
* `56674` – Northbound Hwy 17A @ Hwy 10
* `56688` – Westbound Granville Ave @ Buswell St
* `56689` – Richmond-Brighouse Station @ Bay 3
* `56690` – Eastbound Westminster Hwy @ Garden City Rd
* `56702` – Southbound No. 5 Rd @ Williams Rd
* `56703` – Southbound No. 5 Rd @ Seacliff Rd
* `56704` – Southbound No. 5 Rd @ Steveston Hwy
* `56716` – Northbound No. 5 Rd @ Steveston Hwy
* `56717` – Northbound No. 5 Rd @ Seacliff Rd
* `56718` – Northbound No. 5 Rd @ Williams Rd
* `56737` – Eastbound Steveston Hwy @ No. 1 Rd
* `56740` – Eastbound Steveston Hwy @ Railway Ave
* `56743` – Eastbound Steveston Hwy @ No. 2 Rd
* `56749` – Northbound Springmont Dr @ Springmont Gate
* `56750` – Northbound Springmont Dr @ Springside Place
* `56755` – Eastbound Williams Rd @ No. 1 Rd
* `56758` – Northbound Railway Ave @ Williams Rd
* `56761` – Northbound Railway Ave @ Francis Rd
* `56764` – Northbound Railway Ave @ Blundell Rd
* `56765` – Northbound Railway Ave @ Lancing Rd
* `56766` – Northbound Railway Ave @ Linfield Gate
* `56767` – Eastbound Granville Ave @ Railway Ave
* `56768` – Eastbound Granville Ave @ Lynas Lane
* `56770` – Eastbound Granville Ave @ No. 2 Rd
* `56771` – Eastbound Granville Ave @ Azure Gate
* `56773` – Eastbound Granville Ave @ Gilbert Rd
* `56774` – Eastbound Granville Ave @ Moffatt Rd
* `56775` – Eastbound Granville Ave @ Minoru Blvd
* `56776` – Westbound Granville Ave @ Minoru Blvd
* `56778` – Westbound Granville Ave @ Gilbert Rd
* `56779` – Westbound Granville Ave @ Azure Gate
* `56780` – Westbound Granville Ave @ No. 2 Rd
* `56782` – Westbound Granville Ave @ Lynas Lane
* `56783` – Southbound Railway Ave @ Granville Ave
* `56784` – Southbound Railway Ave @ Linfield Gate
* `56785` – Southbound Railway Ave @ Lancing Rd
* `56786` – Southbound Railway Ave @ Blundell Rd
* `56789` – Southbound Railway Ave @ Francis Rd
* `56793` – Westbound Williams Rd @ No. 1 Rd
* `56797` – Southbound Springmont Dr @ Springside Place
* `56802` – Eastbound Chatham St @ 7th Ave
* `56806` – Northbound Gilbert Rd @ Kimberley Dr
* `56809` – Northbound Gilbert Rd @ Williams Rd
* `56812` – Northbound Gilbert Rd @ Francis Rd
* `56816` – Northbound Gilbert Rd @ Blundell Rd
* `56817` – Northbound Gilbert Rd @ Donald Rd
* `56818` – Northbound Gilbert Rd @ Mang Rd
* `56823` – Southbound Gilbert Rd @ Westminster Hwy
* `56827` – Southbound Gilbert Rd @ Granville Ave
* `56828` – Southbound Gilbert Rd @ Mang Rd
* `56829` – Southbound Gilbert Rd @ Donald Rd
* `56830` – Southbound Gilbert Rd @ Blundell Rd
* `56834` – Southbound Gilbert Rd @ Francis Rd
* `56837` – Southbound Gilbert Rd @ Williams Rd
* `56840` – Westbound Steveston Hwy @ Gilbert Rd
* `56843` – Westbound Steveston Hwy @ No. 2 Rd
* `56846` – Westbound Steveston Hwy @ Railway Ave
* `56851` – Westbound King Rd @ 11500 Block
* `56852` – Southbound Seacote Rd @ Seaport Ave
* `56853` – Westbound Williams Rd @ Seacote Rd
* `56854` – Westbound Williams Rd @ Shell Rd
* `56856` – Westbound Williams Rd @ No. 4 Rd
* `56858` – Northbound Garden City Rd @ Williams Rd
* `56860` – Northbound Garden City Rd @ Glenallan Gate
* `56862` – Northbound Garden City Rd @ Dayton Ave
* `56863` – Northbound Garden City Rd @ Blundell Rd
* `56864` – Northbound Garden City Rd @ Jones Rd
* `56865` – Northbound Garden City Rd @ General Currie Rd
* `56866` – Northbound Garden City Rd @ Bennett Rd
* `56867` – Southbound Garden City Rd @ Bennett Rd
* `56868` – Southbound Garden City Rd @ General Currie Rd
* `56869` – Southbound Garden City Rd @ Jones Rd
* `56870` – Southbound Garden City Rd @ Blundell Rd
* `56871` – Southbound Garden City Rd @ Bowcock Rd
* `56872` – Southbound Garden City Rd @ Francis Rd
* `56877` – Eastbound Williams Rd @ No. 4 Rd
* `56879` – Eastbound Williams Rd @ Shell Rd
* `56880` – Eastbound Williams Rd @ Shell Rd
* `56881` – Eastbound Williams Rd @ Seacote Rd
* `56883` – Eastbound Cambie Rd @ Odlin Cres
* `56884` – Eastbound Cambie Rd @ Garden City Rd
* `56886` – Eastbound Cambie Rd @ No. 4 Rd
* `56887` – Eastbound Cambie Rd @ 10200 Block
* `56888` – Eastbound Cambie Rd @ St. Edwards Dr
* `56889` – Eastbound Cambie Rd @ Vanguard Rd
* `56890` – Eastbound Cambie Rd @ Bargen Dr
* `56891` – Eastbound Cambie Rd @ No. 5 Rd
* `56892` – Eastbound Cambie Rd @ 12400 Block
* `56893` – Eastbound Cambie Rd @ Jacombs Rd
* `56894` – Eastbound Cambie Rd @ Viking Way
* `56895` – Southbound No. 6 Rd @ Cambie Rd
* `56896` – Southbound No. 6 Rd @ Mayfield Place
* `56897` – Southbound No. 6 Rd @ Maycrest Way
* `56898` – Northbound No. 6 Rd @ Sparwood Place
* `56899` – Northbound No. 6 Rd @ International Place
* `56900` – Northbound No. 6 Rd @ Commerce Parkway
* `56901` – Southbound Graybar Rd @ Westminster Hwy
* `56902` – Eastbound Gordon Way @ 21300 Block
* `56903` – Northbound Fraserwood Place @ Gordon Way
* `56904` – Eastbound Westminster Hwy @ Hwy 91
* `56905` – Eastbound Westminster Hwy @ McLean Ave
* `56906` – Eastbound Westminster Hwy @ Gilley Rd
* `56910` – Eastbound Westminster Hwy @ Willett Ave
* `56913` – Eastbound Ewen Ave @ Gifford St
* `56919` – 22nd Street Station @ Bay 7
* `56921` – Westbound Westminster Hwy @ McLean Ave
* `56922` – Westbound Westminster Hwy @ Hwy 91
* `56923` – Northbound No. 6 Rd @ Maycrest Way
* `56924` – Northbound No. 6 Rd @ Mayfield Place
* `56925` – Westbound Cambie Rd @ No. 6 Rd
* `56926` – Westbound Cambie Rd @ Viking Way
* `56927` – Westbound Cambie Rd @ Jacombs Rd
* `56928` – Westbound Cambie Rd @ 12300 Block
* `56929` – Westbound Cambie Rd @ No. 5 Rd
* `56930` – Westbound Cambie Rd @ Bargen Dr
* `56931` – Westbound Cambie Rd @ Vanguard Rd
* `56932` – Westbound Cambie Rd @ Shell Rd
* `56934` – Westbound Cambie Rd @ No. 4 Rd
* `56936` – Westbound Cambie Rd @ Garden City Rd
* `56946` – Eastbound Bridgeport Rd @ McLennan Ave
* `56947` – Eastbound Bridgeport Rd @ Shell Rd
* `56949` – Eastbound Bridgeport Rd @ Olafsen Ave
* `56950` – Eastbound Bridgeport Rd @ No. 5 Rd
* `56951` – Eastbound Bridgeport Rd @ 12400 Block
* `56952` – Northbound No. 5 Rd @ Bridgeport Rd
* `56953` – Eastbound Vulcan Way @ 12200 Block
* `56954` – Eastbound Vulcan Way @ Sweden Way
* `56955` – Eastbound Vulcan Way @ Viscount Way
* `56956` – Southbound Viking Way @ Vulcan Way
* `56957` – Southbound Viking Way @ Viking Place
* `56958` – Northbound Knight St Bridge Offramp @ SE Marine Dr
* `56959` – Westbound Bridgeport Rd @ Knight St Bridge
* `56960` – Westbound Bridgeport Rd @ Sweden Way
* `56961` – Westbound Bridgeport Rd @ Vickers Way
* `56962` – Northbound Viking Way @ Bridgeport Rd
* `56963` – Northbound Viking Way @ Viking Place
* `56964` – Westbound Vulcan Way @ Viking Way
* `56965` – Westbound Vulcan Way @ Viscount Way
* `56966` – Westbound Vulcan Way @ Sweden Way
* `56967` – Westbound Vulcan Way @ 12200 Block
* `56968` – Southbound No. 5 Rd @ Vulcan Way
* `56969` – Westbound Bridgeport Rd @ No. 5 Rd
* `56970` – Westbound Bridgeport Rd @ Olafsen Ave
* `56972` – Westbound Bridgeport Rd @ Shell Rd
* `56973` – Westbound Bridgeport Rd @ McLennan Ave
* `56975` – Westbound Bridgeport Rd @ No. 4 Rd
* `56976` – Westbound Bridgeport Rd @ Gage Rd
* `56979` – Northbound Viking Way @ Cambie Rd
* `56981` – Northbound Viking Way @ Bridgeport Rd
* `56982` – Southbound Agar Dr @ Inglis Dr
* `56983` – Northbound Cowley Cres @ Inglis Dr
* `56984` – Northbound Cowley Cres @ Airport Rd South
* `56985` – Southbound Cowley Cres @ Airport Rd South
* `56986` – Northbound Inglis Dr @ Airport Rd South
* `56990` – Southbound Inglis Dr @ Airport Rd South
* `56991` – Westbound Inglis Dr @ Bell Irving St
* `57013` – Northbound 200 St @ 41 Ave
* `57018` – Northbound 200 St @ 50 Ave
* `57019` – Eastbound 53 Ave @ 200 St
* `57024` – Westbound Douglas Cres @ 204 St
* `57025` – Northbound 203 St @ Fraser Hwy
* `57026` – Eastbound Logan Ave @ 203A St
* `57027` – Langley Centre @ Bay 3
* `57028` – Westbound Fraser Hwy @ 56 Ave
* `57029` – Westbound Fraser Hwy @ 203 St
* `57030` – Westbound Fraser Hwy @ 201A St
* `57031` – Westbound Fraser Hwy @ 200 St
* `57032` – Eastbound Willowbrook Dr @ 197 St
* `57033` – Eastbound Willowbrook Dr @ 198 St
* `57034` – Northbound 200 St @ 64 Ave
* `57035` – Northbound 200 St @ 66 Ave
* `57036` – Northbound 200 St @ 68 Ave
* `57037` – Northbound 200 St @ 70 Ave
* `57040` – Northbound 200 St @ 72 Ave
* `57041` – Northbound 200 St @ 7600 Block
* `57048` – Northbound 200 St @ 92A Ave
* `57049` – Westbound 96 Ave @ 199A St
* `57051` – Westbound 96 Ave @ 195 St
* `57052` – Westbound 96 Ave @ 192 St
* `57053` – Westbound 96 Ave @ 190 St
* `57054` – Westbound 96 Ave @ 188 St
* `57055` – Westbound 96 Ave @ 186 St
* `57057` – Eastbound 96 Ave @ 186 St
* `57058` – Eastbound 96 Ave @ 188 St
* `57059` – Eastbound 96 Ave @ 190 St
* `57060` – Eastbound 96 Ave @ 192 St
* `57061` – Eastbound 96 Ave @ 195 St
* `57062` – Eastbound 96 Ave @ 198 St
* `57069` – Southbound 202A St @ 7700 Block
* `57070` – Southbound 200 St @ 80 Ave
* `57072` – Southbound 200 St @ 72 Ave
* `57073` – Southbound 200 St @ 70 Ave
* `57074` – Southbound 200 St @ 68 Ave
* `57075` – Southbound 200 St @ Willoughby Way
* `57076` – Westbound Willowbrook Dr @ 200 St
* `57077` – Westbound Willowbrook Dr @ 198 St
* `57078` – Eastbound Fraser Hwy @ 200 St
* `57079` – Eastbound Fraser Hwy @ 201A St
* `57081` – Southbound 203 St @ 54 Ave
* `57091` – Eastbound Fraser Hwy @ 184 St
* `57092` – Eastbound Fraser Hwy @ 188 St
* `57097` – Langley Centre @ Bay 4
* `57102` – Southbound 200 St @ 53 Ave
* `57108` – Southbound 200 St @ 41 Ave
* `57128` – Northbound 200 St @ 32 Ave
* `57136` – Langley Centre @ Bay 2
* `57138` – Westbound Fraser Hwy @ 195A St
* `57140` – Westbound Fraser Hwy @ 188 St
* `57141` – Westbound Fraser Hwy @ 184 St
* `57142` – Northbound King George Blvd @ 102 Ave
* `57143` – Langley Centre @ Bay 5
* `57171` – Westbound Fraser Hwy @ 264 St
* `57188` – Langley Centre @ Bay 6
* `57189` – Northbound Glover Rd @ Eastleigh Cres
* `57190` – Northbound Glover Rd @ Kwantlen Cres
* `57196` – Northbound Glover Rd @ 76A Ave
* `57199` – Northbound Glover Rd @ St. Andrews Ave
* `57200` – Westbound 96 Ave @ Glover Rd
* `57208` – Westbound 88 Ave @ 216 St
* `57212` – Northbound Walnut Grove Dr @ 88 Ave
* `57214` – Northbound 212 St @ Walnut Grove Dr
* `57215` – Northbound 212 St @ 93 Ave
* `57217` – Northbound 210 St @ 95A Ave
* `57232` – Southbound 210 St @ 95A Ave
* `57233` – Southbound 210 St @ 93A Ave
* `57234` – Southbound 212 St @ 92 Ave
* `57235` – Southbound 212 St @ 91B Ave
* `57242` – Eastbound 88 Ave @ 216 St
* `57250` – Eastbound 96 Ave @ Glover Rd
* `57251` – Southbound Glover Rd @ St. Andrews Ave
* `57254` – Southbound Glover Rd @ Labonte Rd
* `57257` – Southbound Glover Rd @ 5900 Block
* `57271` – Northbound 213 St @ 92 Ave
* `57278` – Southbound 216 St @ 5300 Block
* `57304` – Eastbound Fraser Hwy @ 264 St
* `57308` – Eastbound 32 Ave @ 27400 Block
* `57311` – Southbound 276 St @ Fraser Hwy
* `57344` – Northbound 216 St @ 5300 Block
* `57349` – Westbound 1A Ave @ 67 St
* `57350` – Northbound 66A St @ 200 Block
* `57351` – Northbound 66A St @ 3rd Ave
* `57352` – Westbound 3rd Ave @ 6500 Block
* `57353` – Northbound Boundary Bay Rd @ 3rd Ave
* `57354` – Northbound Boundary Bay Rd @ 500 Block
* `57355` – Northbound Boundary Bay Rd @ Ironwood Place
* `57356` – Westbound 12 Ave @ Morris Cres
* `57357` – Westbound 12 Ave @ Jackson Way
* `57358` – Westbound 12 Ave @ Hunter Rd
* `57359` – Westbound 2nd Ave @ 52A St
* `57360` – Northbound 52A St @ 3rd Ave
* `57361` – Westbound Milsom Wynd @ 52 St
* `57362` – Westbound Milsom Wynd @ Galway Dr
* `57363` – Northbound Milsom Wynd @ 5th Ave
* `57364` – Eastbound 6th Ave @ Milsom Wynd
* `57365` – Northbound 52 St @ 6th Ave
* `57368` – Eastbound 8A Ave @ 52A St
* `57369` – Northbound 53A St @ 9th Ave
* `57370` – Northbound 53A St @ 10A Ave
* `57371` – Eastbound 12 Ave @ 53A St
* `57372` – Eastbound 12 Ave @ 5500 Block
* `57373` – Northbound 56 St @ 12 Ave
* `57374` – Northbound 56 St @ View Cres South
* `57375` – Northbound 56 St @ View Cres North
* `57376` – Northbound 56 St @ 16 Ave
* `57377` – South Delta Exchange @ Bay 1
* `57380` – Northbound 53 St @ 28 Ave
* `57381` – Northbound 53 St @ 3000 Block
* `57382` – Northbound Arthur Dr @ 34B Ave
* `57383` – Northbound Arthur Dr @ 36 Ave
* `57384` – Northbound Arthur Dr @ 3700 Block
* `57385` – Northbound Arthur Dr @ 3900 Block
* `57386` – Northbound Arthur Dr @ Whitworth Cres
* `57387` – Northbound Arthur Dr @ 4400 Block
* `57388` – Northbound Arthur Dr @ 45 Ave
* `57389` – Northbound Arthur Dr @ 47 Ave
* `57390` – Northbound Arthur Dr @ Ladner Trunk Rd
* `57391` – Eastbound Ladner Trunk Rd @ Central Ave
* `57392` – Eastbound Ladner Trunk Rd @ 53 St
* `57393` – Eastbound Ladner Trunk Rd @ Linden Dr
* `57394` – Eastbound Ladner Trunk Rd @ 55B St
* `57395` – Ladner Exchange @ Bay 2
* `57396` – Ladner Exchange @ Bay 5
* `57397` – Westbound Ladner Trunk Rd @ Harvest Dr
* `57398` – Westbound Ladner Trunk Rd @ 57 St
* `57399` – Westbound Ladner Trunk Rd @ 55B St
* `57400` – Westbound Ladner Trunk Rd @ 54A St
* `57401` – Westbound Ladner Trunk Rd @ 53 St
* `57402` – Westbound Ladner Trunk Rd @ 52A St
* `57403` – Southbound Arthur Dr @ 4700 Block
* `57404` – Southbound Arthur Dr @ 47 Ave
* `57405` – Southbound Arthur Dr @ 45 Ave
* `57406` – Southbound Arthur Dr @ 44 Ave
* `57407` – Southbound Arthur Dr @ Whitworth Cres
* `57408` – Southbound Arthur Dr @ 3700 Block
* `57409` – Southbound Arthur Dr @ 36 Ave
* `57410` – Southbound Arthur Dr @ 34B Ave
* `57415` – Southbound 56 St @ Hwy 17
* `57417` – Southbound 56 St @ 16 Ave
* `57418` – Southbound 56 St @ 14B Ave
* `57419` – Southbound 56 St @ View Cres
* `57420` – Eastbound 12 Ave @ 56 St
* `57421` – Eastbound 12 Ave @ Jackson Way
* `57422` – Eastbound 12 Ave @ Morris Cres
* `57423` – Southbound Boundary Bay Rd @ Ironwood Place
* `57425` – Eastbound 3rd Ave @ Boundary Bay Rd
* `57426` – Eastbound 3rd Ave @ 66A St
* `57427` – Southbound 67 St @ 3rd Ave
* `57428` – Westbound 12 Ave @ 56 St
* `57429` – Westbound 12 Ave @ 55 St
* `57430` – Southbound 53A St @ 11A Ave
* `57431` – Southbound 53A St @ 10 Ave
* `57432` – Southbound 53A St @ 9th Ave
* `57433` – Westbound 8A Ave @ 53A St
* `57434` – Southbound 52 St @ 8A Ave
* `57435` – Southbound 52 St @ Dennison Dr
* `57436` – Southbound 52 St @ 6th Ave
* `57437` – Southbound Milsom Wynd @ 6th Ave
* `57438` – Southbound Milsom Wynd @ Kerry Dr
* `57439` – Eastbound Milsom Wynd @ Galway Dr
* `57440` – Eastbound 4th Ave @ 52A St
* `57441` – Southbound 54 St @ 4th Ave
* `57443` – Westbound 1st Ave @ 49 St
* `57444` – Northbound English Bluff Rd @ 2nd Ave
* `57445` – Northbound English Bluff Rd @ 4th Ave
* `57446` – Northbound English Bluff Rd @ 5th Ave
* `57447` – Northbound English Bluff Rd @ 7A Ave
* `57448` – Northbound English Bluff Rd @ Gale Dr
* `57449` – Northbound English Bluff Rd @ 1000 Block
* `57450` – Eastbound 12 Ave @ English Bluff Rd
* `57451` – Eastbound 12 Ave @ Cliff Dr
* `57452` – Eastbound 12 Ave @ 52 St
* `57453` – Northbound Beach Grove Rd @ 12 Ave
* `57454` – Northbound Beach Grove Rd @ Whitcomb Place
* `57455` – Northbound Beach Grove Rd @ Grove Cres
* `57456` – Westbound 16 Ave @ Beach Grove Rd
* `57457` – Westbound 16 Ave @ Braid Rd
* `57458` – Westbound 16 Ave @ Farrell Ave
* `57459` – Westbound 16 Ave @ 57 St
* `57460` – Southbound 56 St @ 12 Ave
* `57461` – Southbound 56 St @ 10 Ave
* `57464` – Southbound 56 St @ 4th Ave
* `57465` – Southbound 56 St @ Morningside Dr
* `57466` – Westbound 1st Ave @ 56 St
* `57467` – Westbound 1st Ave @ 5400 Block
* `57468` – Westbound 1st Ave @ 53 St
* `57469` – Westbound 1st Ave @ Deerfield Dr
* `57470` – Westbound 1st Ave @ 50 St
* `57476` – Eastbound 1st Ave @ 55A St
* `57477` – Northbound 56 St @ 100 Block
* `57478` – Northbound 56 St @ 4th Ave
* `57479` – Northbound 56 St @ 6th Ave
* `57480` – Northbound 56 St @ 8A Ave
* `57481` – Northbound 56 St @ 10 Ave
* `57489` – Westbound 12 Ave @ 53A St
* `57490` – Westbound 12 Ave @ 52 St
* `57491` – Westbound 12 Ave @ Cliff Dr
* `57492` – Westbound 12 Ave @ Bayview Dr
* `57493` – Southbound English Bluff Rd @ 1000 Block
* `57494` – Southbound English Bluff Rd @ Wesley Dr
* `57495` – Southbound English Bluff Rd @ Glenwood Dr
* `57496` – Southbound English Bluff Rd @ 6th Ave
* `57499` – Northbound 46A St @ River Rd West
* `57500` – Eastbound River Rd @ 47A St
* `57501` – Eastbound 47A Ave @ 48B St
* `57503` – Northbound Central Ave @ Ladner Trunk Rd
* `57504` – Northbound Central Ave @ 4900 Block
* `57505` – Westbound Linden Dr @ Fenton Dr
* `57506` – Northbound Ferry Rd @ Westminster Ave
* `57507` – Northbound Ferry Rd @ Schooner Gate
* `57509` – Eastbound Commodore Dr @ Ferry Rd
* `57510` – Southbound Admiral Blvd @ Commodore Dr
* `57511` – Southbound Admiral Blvd @ Regatta Way
* `57512` – Southbound Westminster Ave @ Chamberlayne Ave
* `57513` – Eastbound Crescent Dr @ Wellburn Dr
* `57514` – Eastbound Crescent Dr @ Dowler Rd
* `57515` – Southbound 57 St @ Crescent Dr
* `57516` – Southbound 57 St @ Grove Ave
* `57517` – Southbound 57 St @ 49B Ave
* `57518` – Ladner Exchange @ Bay 7
* `57519` – Southbound Harvest Dr @ 4500 Block
* `57520` – Westbound Harvest Dr @ 57 St
* `57521` – Westbound 44 Ave @ Hutcherson Lane
* `57522` – Westbound 44 Ave @ 5400 Block
* `57523` – Westbound 44 Ave @ Maple Lane
* `57524` – Westbound 45 Ave @ Arthur Dr
* `57525` – Westbound 45 Ave @ 51 St
* `57526` – Westbound 45 Ave @ Garry St
* `57528` – Westbound 45 Ave @ 46B St
* `57529` – Southbound 46A St @ River Rd West
* `57530` – Eastbound 45 Ave @ 46A St
* `57531` – Eastbound 45 Ave @ 47A St
* `57532` – Eastbound 45 Ave @ 48B St
* `57533` – Eastbound 45 Ave @ 51 St
* `57534` – Eastbound 44 Ave @ 52A St
* `57535` – Eastbound 44 Ave @ 5400 Block
* `57536` – Eastbound 44 Ave @ 55B St
* `57537` – Eastbound Harvest Dr @ 57 St
* `57538` – Northbound Harvest Dr @ Mountain View Blvd
* `57539` – Northbound 57 St @ 48B Ave
* `57540` – Northbound 57 St @ 49B Ave
* `57541` – Northbound 57 St @ Grove Ave
* `57542` – Westbound Crescent Dr @ 57 St
* `57543` – Westbound Crescent Dr @ Dowler Rd
* `57544` – Westbound Crescent Dr @ Wellburn Dr
* `57545` – Northbound Westminster Ave @ Chamberlayne Ave
* `57546` – Westbound Admiral Blvd @ Regatta Way
* `57547` – Westbound Commodore Dr @ Admiral Blvd
* `57548` – Westbound Commodore Dr @ Clipper Rd
* `57549` – Southbound Ferry Rd @ Commodore Dr
* `57550` – Southbound Ferry Rd @ Brigantine Rd
* `57551` – Southbound Ferry Rd @ Schooner Gate
* `57552` – Southbound Ferry Rd @ Heron Bay Close
* `57553` – Eastbound Linden Dr @ Westminster Ave
* `57554` – Eastbound Linden Dr @ Central Ave
* `57555` – Southbound Central Ave @ 4900 Block
* `57556` – Southbound Central Ave @ Ladner Trunk Rd
* `57559` – Westbound 47A Ave @ 48B St
* `57560` – Westbound River Rd @ Ashbury Place
* `57562` – Ladner Exchange @ Bay 3
* `57563` – Eastbound River Rd @ 72 St
* `57564` – Eastbound River Rd @ Berg Rd
* `57565` – Eastbound River Rd @ East Mill Access Rd
* `57566` – Scott Road Station @ Bay 8
* `57568` – Westbound River Rd @ Huston Rd
* `57569` – Westbound River Rd @ Ross Rd
* `57570` – Coquitlam Central Station @ Bay 2
* `57573` – Eastbound Lougheed Hwy @ Ottawa St
* `57575` – Southbound Harris Rd @ McMyn Rd
* `57576` – Southbound Harris Rd @ Davison Rd
* `57577` – Pitt Meadows Station @ Bay 2
* `57585` – Maple Meadows Station @ Bay 3
* `57589` – Eastbound Dewdney Trunk Rd @ 203 St
* `57592` – Eastbound Dewdney Trunk Rd @ 210 St
* `57593` – Eastbound Dewdney Trunk Rd @ Laity St
* `57599` – Eastbound Dewdney Trunk Rd @ 222 St
* `57618` – Eastbound Dewdney Trunk Rd @ 246 St
* `57619` – Eastbound Dewdney Trunk Rd @ 248 St
* `57620` – Eastbound Dewdney Trunk Rd @ 24900 Block
* `57623` – Westbound Dewdney Trunk Rd @ 246 St
* `57641` – Westbound Dewdney Trunk Rd @ 222 St
* `57646` – Westbound Dewdney Trunk Rd @ Laity St
* `57650` – Westbound Dewdney  Trunk Rd @ 203 St
* `57652` – Southbound Maple Meadows Way @ 200 St
* `57654` – Maple Meadows Station @ Bay 2
* `57661` – Northbound Harris Rd @ 119 Ave
* `57663` – Pitt Meadows Station @ Bay 3
* `57664` – Northbound Harris Rd @ Davison Rd
* `57665` – Northbound Harris Rd @ McMyn Rd
* `57667` – Westbound Lougheed Hwy @ Ottawa St
* `57670` – Maple Meadows Station @ Bay 4
* `57695` – Port Haney Station @ Bay 2
* `57703` – Port Haney Station @ Bay 1
* `57710` – Northbound Laity St @ Ridge Meadows Hospital
* `57749` – Westbound Dewdney Trunk Rd @ 256 St
* `57751` – Westbound Dewdney Trunk Rd @ 250 St
* `57752` – Westbound Dewdney Trunk Rd @ 248 St
* `57753` – Maple Meadows Station @ Bay 5
* `57770` – Northbound 227 St @ 124 Ave
* `57785` – Southbound 232 St @ Abernethy Way
* `57800` – Northbound 232 St @ 124 Ave
* `57825` – Eastbound Dewdney Trunk Rd @ 256 St
* `57826` – Eastbound Dewdney Trunk Rd @ 25900 Block (Flag)
* `57830` – Northbound McNutt Rd @ 12000 Block (Flag)
* `57833` – Southbound On Sayers Cres @ Arbutus Place (Flag)
* `57837` – Eastbound Dewdney Trunk Rd @ 28300 Block (Flag)
* `57838` – Southbound 284 St @ 11900 Block (Flag)
* `57844` – Southbound 280 St @ 10800 Block (Flag)
* `57851` – Southbound 280 St @ 9600 Block (Flag)
* `57852` – Southbound 280 St @ Lougheed Hwy
* `57853` – Westbound Lougheed Hwy @ 27500 Block (Flag)
* `57854` – Westbound Lougheed Hwy @ 27300 Block (Flag)
* `57855` – Northbound 272 St @ 9400 Block (Flag)
* `57861` – Northbound 272 St @ 10800 Block (Flag)
* `57864` – Northbound 272 St @ 11500 Block (Flag)
* `57866` – Northbound 272 St @ 11900 Block (Flag)
* `57869` – Westbound On 128 Ave @ Willow Place (Flag)
* `57874` – Westbound Dewdney Trunk Rd @ 26000 Block (Flag)
* `57903` – Eastbound On 98 Ave @ 266 St (Flag)
* `57904` – Westbound 100 Ave @ 268 St (Flag)
* `57917` – Northbound Willowbrook Connector @ Mufford Cres
* `58008` – Southbound Lost Lagoon Rd @ Stanley Park Causeway
* `58013` – Bowen Island Trunk Rd @ Snug Cove
* `58018` – Northbound Eaglecliff Rd @ Baker Rd
* `58023` – Westbound W 10 Ave @ Sasamat St
* `58031` – Northbound 208 St @ Willoughby Town Centre Dr
* `58032` – Southbound 208 St @ Willoughby Town Centre Dr
* `58037` – Eastbound W Broadway @ Alma St
* `58043` – Kootenay Loop @ Bay 4
* `58046` – Eastbound Steveston Hwy @ Seaward Gate
* `58047` – Northbound Hwy 99 Offramp @ Steveston Hwy
* `58048` – Southbound Hwy 99 Onramp @ Steveston Hwy
* `58049` – Southbound Capilano Rd @ Clements Ave
* `58051` – Eastbound Keefer Place @ Taylor St
* `58052` – Northbound Russ Baker Way @ Miller Rd
* `58053` – Eastbound Anderson Rd @ No. 3 Rd
* `58054` – Heather Square @ Bay 2
* `58056` – Northbound 200 St @ Willowbrook Dr
* `58058` – Eastbound Miller Rd @ Russ Baker Way
* `58061` – Southbound Cessna Dr @ Miller Rd
* `58065` – Northbound Seymour St @ W Georgia St
* `58067` – Southbound Clarence Taylor Cres @ 4600 Block
* `58068` – Southbound Pinetree Way @ Plateau Blvd
* `58069` – Northbound No. 3 Rd @ Park Rd
* `58073` – Westbound Mountain View Blvd @ Clarence Taylor Cres
* `58075` – Southbound 208 St @ 80 Ave
* `58079` – Braid Station @ Bay 1
* `58082` – Braid Station @ Bay 2
* `58083` – Braid Station @ Bay 3
* `58084` – Braid Station @ Bay 4
* `58085` – Braid Station @ Bay 5
* `58086` – Braid Station @ Bay 6
* `58087` – Northbound Commercial Dr @ E 6th Ave
* `58088` – Southbound Smith Ave @ Moscrop St
* `58089` – Westbound Burke St @ Smith Ave
* `58090` – Southbound Smith Ave @ Belleville Ave
* `58091` – Southbound 128 St @ 103 Ave
* `58092` – Southbound Commercial Dr @ E 12 Ave
* `58093` – Southbound Commercial Dr @ E 14 Ave
* `58099` – Burrard Station @ Bay 5
* `58109` – Eastbound Deer Lake Parkway @ Willingdon Ave
* `58111` – Westbound Edgemont Blvd @ Ridgewood Dr
* `58112` – Westbound Capstan Way @ Sexsmith Rd
* `58113` – Westbound Capstan Way @ No. 3 Rd
* `58114` – Eastbound Capstan Way @ No. 3 Rd
* `58115` – Eastbound Capstan Way @ Sexsmith Rd
* `58116` – Eastbound Capstan Way @ Garden City Rd
* `58117` – Eastbound Kingsway @ Griffiths Dr
* `58124` – Gateway Station @ Bay 2
* `58128` – Northbound North Rd @ Lougheed Hwy
* `58129` – Rupert Station @ Bay 2
* `58130` – Rupert Station @ Bay 1
* `58131` – Southbound No. 6 Rd @ Commerce Parkway
* `58133` – Northbound Granville St @ W 64 Ave
* `58135` – Southbound Granville St @ W Broadway
* `58136` – Southbound Granville St @ W King Edward Ave
* `58144` – Northbound Granville St @ Smithe St
* `58145` – Westbound Kingsway @ E Broadway
* `58150` – Scott Road Station @ Bay 3
* `58152` – Westbound Steveston Hwy @ Hwy 99
* `58153` – Eastbound Steveston Hwy @ Hwy 99
* `58156` – Westbound 100 Ave @ 160 St
* `58157` – Westbound 100 Ave @ 158 St
* `58158` – Westbound 8th Ave @ New Westminster Secondary
* `58160` – Northbound Smith Ave @ Sunset St
* `58161` – Southbound Lillooet Rd @ 1000 Block
* `58164` – King George Station @ Bay 3
* `58167` – Southbound Shaughnessy St @ Lincoln Ave
* `58172` – Eastbound Marine Dr @ 6400 Block
* `58173` – 22nd Street Station @ Bay 1
* `58181` – Southbound Brooke Rd @ River Rd
* `58186` – Eastbound Foster Ave @ Aspen St
* `58189` – Westbound Foster Ave @ Aspen St
* `58191` – Northbound North Rd @ Gatineau Place
* `58194` – Steveston Exchange @ Bay 1
* `58195` – Westbound Williams Rd @ Geal Rd
* `58196` – Lansdowne Station @ Bay 1
* `58200` – Southbound No. 6 Rd @ International Place
* `58202` – Waterfront Station @ Bay 1
* `58211` – Eastbound Vulcan Way @ No. 5 Rd
* `58217` – Westbound Bridgeport Rd @ Viking Way
* `58226` – Eastbound Guildford Way @ Ungless Way
* `58227` – Eastbound St. Johns St @ Albert St
* `58230` – Eastbound Lansdowne Rd @ Cooney Rd
* `58235` – Southbound Abbott St @ W Pender St
* `58237` – Southbound 152 St @ 54A Ave
* `58239` – Westbound W Broadway @ Granville St
* `58249` – Joyce Station @ Bay 6
* `58250` – Eastbound Williams Rd @ Garden City Rd
* `58251` – Northbound No. 5 Rd @ Hartnell Rd
* `58253` – Eastbound Anderson Rd @ Buswell St
* `58254` – Eastbound W Georgia St @ Bidwell St
* `58257` – Eastbound 48 Ave @ Laidlaw St
* `58258` – Eastbound 88 Ave @ 160 St
* `58259` – Eastbound 88 Ave @ 162 St
* `58260` – Westbound 88 Ave @ 162 St
* `58261` – Westbound 88 Ave @ 160 St
* `58262` – Eastbound 12 Ave @ 2nd St
* `58263` – Westbound 12 Ave @ 1st St
* `58273` – Eastbound Old Yale Rd @ 13500 Block
* `58274` – Northbound King George Blvd @ 2900 Block
* `58281` – Westbound E 49 Ave @ Frontenac St
* `58282` – Eastbound Edmonds St @ Canada Way
* `58283` – Eastbound E Hastings St @ McLean Dr
* `58284` – Northbound Kerr St @ E 54 Ave
* `58286` – Southbound Cambie St @ W 29 Ave
* `58290` – Southbound Nanaimo St @ Grandview Hwy
* `58296` – Southbound Coast Meridian Rd @ Laurier Ave
* `58302` – Southbound Victoria Dr @ E 38 Ave
* `58304` – Northbound Production Way @ Broadway
* `58305` – Southbound Production Way @ Broadway
* `58307` – Southbound Main St @ E 18 Ave
* `58311` – Southbound Walnut Grove Dr @ 88 Ave
* `58312` – Northbound Nootka St @ E 29 Ave
* `58313` – Northbound Granville St @ W Pender St
* `58314` – Lynn Valley @ Bay 5
* `58316` – Southbound No. 6 Rd @ Wireless Way
* `58321` – Southbound Howe St @ Dunsmuir St
* `58322` – 22nd Street Station @ Unloading Only
* `58327` – Commercial-Broadway Station @ Bay 3
* `58333` – Northbound 152 St @ 64 Ave
* `58335` – Northbound 212 St @ 92 Ave
* `58342` – Northbound Holdom Ave @ Lougheed Hwy
* `58343` – Southbound Holdom Ave @ Lougheed Hwy
* `58344` – Gilmore Station @ Bay 1
* `58345` – Gilmore Station @ Bay 3
* `58346` – Gilmore Station @ Bay 2
* `58347` – Westbound Lougheed Hwy @ Bell Ave
* `58348` – Eastbound Lougheed Hwy @ Bell Ave
* `58349` – Westbound Lougheed Hwy @ Production Way
* `58352` – Southbound Production Way @ Eastlake Dr
* `58360` – Westbound Enterprise St @ 7900 Block
* `58363` – Northbound Lake City Way @ Enterprise St
* `58370` – Westbound Greystone Dr @ Pinehurst Dr
* `58372` – Eastbound Hastings St @ Inlet Dr
* `58376` – Northbound Sperling Ave @ Curtis St
* `58379` – Southbound Kensington Ave @ Hammarskjold Dr
* `58380` – Southbound Kensington Ave @ Hastings St
* `58381` – Southbound Holdom Ave @ Halifax St
* `58384` – Westbound Goring St @ Douglas Rd
* `58385` – Eastbound Goring St @ Douglas Rd
* `58386` – Westbound Lougheed Hwy @ Gilmore Ave
* `58387` – Southbound Gilmore Ave @ 2nd Ave
* `58388` – Northbound Gilmore Ave @ Halifax St
* `58390` – Westbound Henning Dr @ 3800 Block
* `58391` – Eastbound Henning Dr @ 3800 Block
* `58396` – Northbound Scott Rd @ Preddy Dr
* `58401` – Renfrew Station @ Bay 1
* `58402` – Southbound Commercial Dr @ N Grandview Hwy
* `58407` – Northbound Mariner Way @ Chilko Dr
* `58412` – Northbound 1st St @ Agnes St
* `58420` – Northbound Poirier St @ Winslow Ave
* `58428` – Eastbound Hastings St @ Gilmore Ave
* `58430` – Lougheed Station @ Bay 6
* `58431` – Lougheed Station @ Bay 7
* `58432` – Lougheed Station @ Bay 8
* `58433` – Lougheed Station @ Bay 9
* `58434` – Lougheed Station @ Bay 10
* `58436` – Lougheed Station @ Bay 2
* `58437` – Lougheed Station @ Bay 3
* `58438` – Lougheed Station @ Bay 4
* `58439` – Lougheed Station @ Bay 5
* `58440` – Sperling Station @ Bay 3
* `58441` – Sperling Station @ Bay 4
* `58442` – Sperling Station @ Bay 2
* `58477` – Northbound Fir St @ Buena Vista Ave
* `58478` – Eastbound Buena Vista Ave @ Dolphin St
* `58490` – Northbound 210 St @ 93B Ave
* `58491` – Northbound Commercial Dr @ N Grandview Hwy
* `58499` – Eastbound E Broadway @ Clark Dr
* `58501` – Broadway-City Hall Station @ Bay 4
* `58502` – Westbound W Broadway @ Willow St
* `58504` – Westbound W Georgia St @ Granville St
* `58508` – Westbound Logan Ave @ 56 Ave
* `58510` – Southbound Main St @ E 5th Ave
* `58517` – Northbound 216 St @ Fraser Hwy
* `58519` – Eastbound Fraser Hwy @ 222 St
* `58526` – Eastbound 100 Ave @ 156 St
* `58527` – Eastbound 100 Ave @ 158 St
* `58529` – Eastbound W Pender St @ Abbott St
* `58531` – Eastbound Dollarton Hwy @ Amherst Ave
* `58532` – Westbound Dollarton Hwy @ Amherst Ave
* `58551` – Northbound 200 St @ 40 Ave
* `58552` – Northbound 200 St @ Grade Cres
* `58553` – Northbound 200 St @ 53 Ave
* `58579` – Eastbound 17 Ave @ 128 St
* `58588` – Southbound Granville St @ Nanton Ave
* `58590` – Eastbound Fraser Hwy @ 196 St
* `58591` – Westbound Fraser Hwy @ 196A St
* `58592` – Haney Place @ Bay 9
* `58593` – Northbound Granville St @ Drake St
* `58596` – Eastbound Clarence Taylor Cres @ Harvest Dr
* `58597` – South Delta Exchange @ Bay 2
* `58598` – Southbound Gilmore Ave @ Douglas Rd
* `58604` – Eastbound 84 Ave @ 160 St
* `58605` – Eastbound Sanderson Way @ Gilmore Way
* `58606` – Northbound Wesbrook Mall @ 2100 Block
* `58613` – Westbound E Broadway @ Fraser St
* `58615` – Eastbound Westminster Hwy @ Nelson Rd
* `58616` – Westbound Westminster Hwy @ Nelson Rd
* `58617` – Eastbound River Rd @ 68 St
* `58619` – Westbound River Rd @ 6400 Block
* `58620` – Eastbound N Grandview Hwy @ Commercial Dr
* `58621` – Southbound N Grandview Hwy @ E 8th Ave
* `58622` – Northbound 128 St @ 114 Ave
* `58623` – Northbound King George Blvd @ 100 Ave
* `58624` – Richmond-Brighouse Station @ Bay 5
* `58627` – Westbound Fraser Hwy @ Fleetwood Way
* `58628` – Westbound Fraser Hwy @ 152 St
* `58629` – Eastbound Inglis Dr @ Bell Irving St
* `58635` – Eastbound 104 Ave @ King George Blvd
* `58638` – Eastbound 84 Ave @ 144 St
* `58647` – Westbound SE Marine Dr @ Poplar St
* `58648` – Northbound 52 St @ 7A Ave
* `58661` – Eastbound W 2nd Ave @ Anderson St
* `58666` – Southbound Pinetree Way @ Guildford Way
* `58670` – Southbound Coast Meridian Rd @ Coquitlam Ave
* `58673` – Northbound Coast Meridian Rd @ Riverwood Gate
* `58692` – Northbound Shaughnessy St @ Stirling Ave
* `58693` – Eastbound Lincoln Ave @ Flint St
* `58698` – Southbound Coast Meridian Rd @ Westminster Ave
* `58705` – Westbound W Georgia St @ Burrard St
* `58706` – Westbound McGill St @ N Esmond Ave
* `58707` – Northbound MacDonald Ave @ Albert St
* `58719` – Southbound Kensington Ave @ Joe Sakic Way
* `58720` – Eastbound Sprott St @ Kensington Ave
* `58721` – Southbound Burrard St @ Burnaby St
* `58722` – Eastbound W Georgia St @ Gilford St
* `58723` – Northbound Arbutus St @ W 16 Ave
* `58724` – Lonsdale Quay @ Bay 3
* `58725` – Lonsdale Quay @ Bay 4
* `58727` – Stanley Park Loop @ Bay 1
* `58728` – Stanley Park Loop @ Bay 2
* `58733` – Northbound 203 St @ 56 Ave
* `58735` – Westbound Marine Dr @ Hollis Place
* `58744` – Southbound 1st St @ Agnes St
* `58747` – Southbound 4th St @ Agnes St
* `58748` – Southbound 2nd St @ Queens Ave
* `58760` – Westbound Bedwell Bay Rd @ 3900 Block
* `58762` – Southbound Fraser St @ SE Marine Dr
* `58779` – Southbound Ioco Rd @ Newport Dr
* `58781` – Northbound 160 St @ 26 Ave
* `58782` – Southbound 160 St @ 26 Ave
* `58787` – Northbound Coast Meridian Rd @ Patricia Ave
* `58805` – Westbound Mall Access Rd @ Shaughnessy St
* `58821` – Northbound Pinetree Way @ Cardinal Court
* `58822` – Westbound Robson Dr @ Purcell Dr
* `58824` – Northbound Pinetree Way @ Silverthrone Dr
* `58833` – Northbound Pinetree Way @ Pinewood Ave
* `58860` – Southbound Forest Park Way @ Linden Court
* `58865` – Northbound Plateau Blvd @ 3300 Block
* `58869` – Westbound Panorama Dr @ Lansdowne Dr
* `58895` – UBC Exchange @ Bay 9
* `58900` – Eastbound East Rd @ Sunnyside Rd
* `58908` – Westbound 64 Ave @ 201 St
* `58909` – Eastbound 64 Ave @ 200 St
* `58924` – Northbound Garden City Rd @ Alderbridge Way
* `58929` – Southbound 204 St @ 64 Ave
* `58930` – Westbound 64 Ave @ 204 St
* `58933` – Eastbound 64 Ave @ 203 St
* `58937` – Southbound 203 St @ 66 Ave
* `58938` – Westbound 66 Ave @ 203 St
* `58939` – Eastbound 66 Ave @ 20200 Block
* `58941` – Southbound Mall Access Rd @ 6500 Block
* `58942` – Northbound Mall Access Rd @ 6400 Block
* `58943` – Westbound 64 Ave @ 202 St
* `58945` – Moody Centre Station @ Bay 1 Unloading Only
* `58949` – Westbound 104 Ave @ 158 St
* `58950` – Eastbound 104 Ave @ 158 St
* `58959` – Eastbound Mall Access Rd @ Shaughnessy St
* `58962` – Northbound Joffre Ave @ Carson St
* `58964` – Westbound 84 Ave @ 108 St
* `58966` – Eastbound Hwy 91 Offramp @ Westminster Hwy
* `58967` – Westbound Hwy 91 Onramp @ Westminster Hwy
* `58976` – Westbound Bedwell Bay Rd @ 2000 Block (Flag)
* `58978` – Westbound Bedwell Bay Rd @ Midden Rd
* `58988` – Westbound Kingsway @ Nelson Ave
* `58997` – Northbound Pinetree Way @ Anson Ave
* `59000` – Northbound Orchid Dr @ 2700 Block
* `59003` – Westbound Como Lake Ave @ Elmwood St
* `59008` – Westbound Quayside Dr @ 1200 Block
* `59010` – Northbound 6th St @ Carnarvon St
* `59012` – Southbound 157A St @ 10900 Block
* `59035` – Northbound Scott Rd @ Nordel Way
* `59040` – Westbound Robson St @ Granville St
* `59041` – Southbound Main St @ E 41 Ave
* `59042` – 63 Ave Loop @ Granville St
* `59044` – Southbound Tower Rd @ University High Street
* `59047` – Southbound 168 St @ 64 Ave
* `59059` – Westbound Williams Rd @ Gilbert Rd
* `59060` – Eastbound Williams Rd @ Gilbert Rd
* `59074` – Southbound Railway Ave @ Steveston Hwy
* `59084` – Southbound 144 St @ 76 Ave
* `59085` – Eastbound Fraser Hwy @ 64 Ave
* `59086` – Westbound Fraser Hwy @ 64 Ave
* `59088` – Eastbound Como Lake Ave @ Custer Court
* `59094` – Westbound E 41 Ave @ Fleming St
* `59098` – Southbound Laity St @ Ridge Meadows Hospital
* `59099` – Southbound 152 St @ 36 Ave
* `59118` – Eastbound Ewen Ave @ McGillivray Place
* `59122` – Northbound Arthur Dr @ 3800 Block
* `59127` – Westbound 8th Ave @ 22 St
* `59129` – Northbound Smith Ave @ Linwood St
* `59134` – Westbound 88 Ave @ 12300 Block
* `59135` – Eastbound 88 Ave @ 12400 Block
* `59136` – Westbound 88 Ave @ 12400 Block
* `59137` – Eastbound 88 Ave @ 12500 Block
* `59138` – Westbound 88 Ave @ 126 St
* `59139` – Eastbound 88 Ave @ 126 St
* `59143` – Eastbound 91A Ave @ 20100 Block
* `59144` – Westbound 91A Ave @ 20100 Block
* `59189` – Westbound Lougheed Hwy @ Shaughnessy St
* `59215` – Southbound 128 St @ 82 Ave
* `59228` – Westbound 108 Ave @ 168 St
* `59229` – Westbound Hansen Rd @ Grosvenor Rd
* `59231` – Westbound Kindersley Dr @ Coventry Rd
* `59232` – Eastbound Kindersley Dr @ Coventry Rd
* `59233` – Westbound Kindersley Dr @ 143A St
* `59234` – Eastbound Kindersley Dr @ 143A St
* `59236` – Eastbound Gladstone Dr @ Grosvenor Rd
* `59238` – Northbound 136 St @ Bentley Rd
* `59240` – Northbound 136 St @ 111 Ave
* `59241` – Southbound 136 St @ 111 Ave
* `59242` – Southbound 136 St @ 112 Ave
* `59243` – Westbound 112 Ave @ 136 St
* `59248` – Northbound 156 St @ 108 Ave
* `59249` – Northbound 156 St @ 110 Ave
* `59254` – Eastbound Southridge Dr @ Marine Dr
* `59257` – Northbound Hilton Rd @ Grosvenor Rd
* `59258` – Southbound Arthur Dr @ 3800 Block
* `59263` – Westbound Lincoln Ave @ Oxford St
* `59283` – Westbound David Ave @ Coast Meridian Rd
* `59286` – Eastbound Kindersley Dr @ Grosvenor Rd
* `59300` – Eastbound North Fraser Way @ Glenlyon Parkway West
* `59306` – Eastbound Westminster Hwy @ McMillan Way
* `59312` – Southbound 56 St @ 8A Ave
* `59314` – Production Way Station @ Bay 1
* `59315` – Production Way Station @ Bay 2
* `59316` – Production Station @ Bay 3
* `59317` – Eastbound E 49 Ave @ Kerr St
* `59337` – Eastbound Commerce Parkway @ No. 6 Rd
* `59338` – Northbound Commerce Parkway @ International Place
* `59339` – King George Station @ Bay 4
* `59372` – Eastbound Grace Rd @ 117 St
* `59373` – Westbound River Rd @ Millar Rd
* `59377` – Westbound 112 Ave @ 138 St
* `59390` – Westbound Pacific St @ Jervis St
* `59391` – Westbound Beach Ave @ Bute St
* `59392` – Westbound Beach Ave @ Thurlow St
* `59393` – Westbound Beach Ave @ Hornby St
* `59396` – Westbound Beach Ave @ Howe St
* `59402` – Southbound Smith Ave @ Fir St
* `59414` – Southbound 208 St @ 76 Ave
* `59418` – Northbound Garden City Rd @ Cook Rd
* `59419` – Westbound Cook Rd @ Pimlico Way
* `59420` – Eastbound Davie St @ Broughton St
* `59429` – Westbound Nordel Way @ 116 St
* `59430` – Eastbound Nordel Way @ 116 St
* `59476` – Eastbound Gladstone Dr @ 143A St
* `59478` – Northbound 52 St @ Hwy 17
* `59479` – Northbound Coast Meridian Rd @ Coquitlam Ave
* `59485` – Northbound 52 St @ Canoe Pass Way
* `59491` – Eastbound Murray St @ Klahanie Dr West
* `59493` – Westbound Murray St @ Hugh St
* `59494` – Eastbound Murray St @ Hugh St
* `59498` – Southbound 52 St @ Canoe Pass Way
* `59499` – Westbound Murray St @ Capilano Rd
* `59500` – Westbound Murray St @ 2900 Block
* `59501` – Eastbound Murray St @ 2900 Block
* `59510` – Westbound Canoe Pass Way @ 52 St
* `59511` – Westbound Canoe Pass Way @ Mall Access Rd
* `59525` – Westbound Water St @ Abbott St
* `59526` – Lynn Valley @ Bay 1
* `59531` – Burquitlam Station @ Bay 5
* `59532` – Eastbound Canoe Pass Way @ Mall Access Rd
* `59534` – Eastbound Westminster Hwy @ Hwy 91 Offramp
* `59536` – Southbound Main St @ E 28 Ave
* `59541` – Eastbound Canoe Pass Way @ Salish Sea Dr
* `59542` – Southbound Salish Sea Dr @ Canoe Pass Way
* `59547` – Northbound Salish Sea Dr @ Blue Heron Dr
* `59549` – Eastbound 6th Ave @ 18 St
* `59552` – Westbound Boyd St @ Gifford St
* `59554` – Burquitlam Station @ Bay 1
* `59555` – Eastbound Westminster Hwy @ Boundary Rd
* `59556` – Eastbound Boyd St @ Gifford St
* `59557` – Burquitlam Station @ Bay 2
* `59558` – Westbound Dunsmuir St @ Cambie St
* `59561` – Burquitlam Station @ Bay 3
* `59565` – Lafarge Lake-Douglas Station @ Bay 1
* `59569` – Inlet Centre Station @ Bay 3
* `59575` – Southbound 124 St @ 59A Ave
* `59576` – Northbound N Boundary Rd @ Eton St
* `59577` – Northbound Cariboo Rd @ Cariboo Place
* `59583` – Southbound Main St @ E 49 Ave
* `59586` – Eastbound Halifax St @ Phillips Ave
* `59587` – Northbound Lower Mall @ University Blvd
* `59590` – Eastbound 58 Ave @ 176 St
* `59593` – Eastbound Dewdney Trunk Rd @ 26300 Block
* `59599` – Southbound 200 St @ 86 Ave
* `59605` – Southbound 200 St @ 64 Ave
* `59606` – Southbound Willingdon Ave @ Maywood St
* `59607` – Northbound Willingdon Extension @ Imperial St
* `59621` – Southbound Garden City Rd @ Leslie Rd
* `59623` – Westbound Dewdney Trunk Rd @ 26300 Block
* `59631` – Northbound Harris Rd @ Civic Centre
* `59651` – Northbound Laity St @ 122 Ave
* `59668` – Southbound Nelson Ave @ Patrick Place
* `59669` – Southbound Nelson Ave @ McKee Place
* `59670` – Southbound Nelson Ave @ Clinton St
* `59673` – Westbound Rumble St @ Royal Oak Ave
* `59674` – Northbound Antrim Ave @ Victory St
* `59675` – Southbound Patterson Ave @ Imperial St
* `59676` – Northbound Patterson Ave @ Victory St
* `59677` – Southbound Patterson Ave @ Victory St
* `59678` – Northbound Patterson Ave @ Rumble St
* `59679` – Southbound Patterson Ave @ Rumble St
* `59680` – Westbound Rumble St @ McKay Ave
* `59681` – Westbound Rumble St @ Sussex Ave
* `59682` – Eastbound Rumble St @ Sussex Ave
* `59683` – Westbound Rumble St @ Gray Ave
* `59684` – Eastbound Rumble St @ Gray Ave
* `59685` – Westbound Rumble St @ Nelson Ave
* `59687` – Northbound Greenall Ave @ Marine Way
* `59689` – Northbound Gilley Ave @ Brynlor Dr
* `59690` – Northbound Gilley Ave @ Keith St
* `59702` – Eastbound Lougheed Hwy @ Madison Ave
* `59704` – Eastbound Bridgeport Rd @ Great Canadian Way
* `59709` – Port Haney Station @ Bay 3
* `59715` – Northbound Wesbrook Mall @ Thunderbird Blvd
* `59716` – Northbound Lonsdale Ave @ E 1st St
* `59736` – Westbound Bridgeport Rd @ West Rd
* `59739` – Southbound Byrne Rd @ Meadow Ave
* `59740` – Northbound Byrne Rd @ Marine Way
* `59741` – Westbound Southridge Dr @ Byrnepark Dr
* `59742` – Eastbound Southridge Dr @ Byrnepark Dr
* `59743` – Northbound Griffiths Dr @ 14 Ave
* `59744` – Southbound Griffiths Dr @ 14 Ave
* `59745` – Northbound Griffiths Dr @ 10 Ave
* `59750` – Eastbound Murray St @ Capilano Rd
* `59751` – Westbound W 6th Ave @ Alder Crossing
* `59754` – Eastbound W 6th Ave @ Heather St
* `59758` – Eastbound W 2nd Ave @ Columbia St
* `59760` – Eastbound E 2nd Ave @ Ontario St
* `59761` – Westbound E 2nd Ave @ Main St
* `59763` – Eastbound Great Northern Way @ Brunswick St
* `59765` – Westbound Great Northern Way @ Foley St
* `59766` – Eastbound Great Northern Way @ Foley St
* `59767` – VCC-Clark Station @ Unloading Only
* `59770` – Southbound No. 3 Rd @ Capstan Way
* `59771` – Lansdowne Station @ Bay 4
* `59772` – Westbound 108 Ave @ 158 St
* `59780` – Southbound Patterson Ave @ Clinton St
* `59781` – Westbound Beresford St @ Royal Oak Ave
* `59783` – Northbound Boundary Rd @ E Kent Ave North
* `59784` – Southbound Griffiths Dr @ Southridge Dr
* `59794` – Northbound Cloverdale Bypass @ 58 Ave
* `59796` – Pitt Meadows Station @ Bay 1
* `59798` – Northbound Ferry Rd @ Brigantine Rd
* `59803` – VCC-Clark Station @ Bay 1
* `59809` – Northbound Citadel Dr @ O'Flaherty Gate
* `59810` – South Surrey Park & Ride @ Bay 2
* `59814` – Northbound Roseberry Ave @ Keith St
* `59823` – Westbound Lougheed Hwy @ Harris Rd
* `59825` – Southbound 168 St @ 80 Ave
* `59831` – Southbound Howe St @ W Pender St
* `59832` – Southbound Howe St @ Dunsmuir St
* `59835` – Northbound Main St @ E 18 Ave
* `59836` – Northbound Main St @ E 20 Ave
* `59837` – Northbound Main St @ E 22 Ave
* `59845` – Southbound Ioco Rd @ Suter Brook Way
* `59848` – Eastbound Carnarvon St @ Quayside Dr
* `59849` – Eastbound Fraser Hwy @ 216 St
* `59852` – South Delta Exchange @ Bay 3
* `59856` – Eastbound Fraser Hwy @ 182 St
* `59860` – Eastbound Davie St @ Richards St
* `59863` – Langley Centre @ Bay 7
* `59868` – Edmonds Station @ Bay 7
* `59873` – Eastbound Hwy 10 @ 152 St
* `59874` – Westbound Hwy 10 @ 168 St
* `59875` – Eastbound Hwy 10 @ 168 St
* `59878` – Westbound Hwy 10 @ 175 St
* `59879` – Southbound 175 St @ 58A Ave
* `59884` – Westbound Southridge Dr @ 6600 Block
* `59885` – Eastbound Southridge Dr @ 6600 Block
* `59890` – Lincoln Station @ Bay 1
* `59898` – Northbound 168 St @ 64 Ave
* `59901` – Northbound 144 St @ 68A Ave
* `59903` – Northbound Ioco Rd @ Suter Brook Way
* `59913` – Eastbound Mary Hill Bypass @ Kingsway Ave
* `59914` – Westbound Pacific Blvd @ Cambie St
* `59916` – Eastbound Pacific Blvd @ Boathouse Mews
* `59918` – Eastbound Pacific Blvd @ Marinaside Cres
* `59919` – Northbound Pat Quinn Way @ Pacific Blvd
* `59923` – Westbound Keefer Place @ Taylor St
* `59924` – Southbound Quebec St @ Keefer St
* `59925` – Northbound Quebec St @ Terminal Ave
* `59926` – Eastbound National Ave @ Quebec St
* `59927` – Northbound Quebec St @ National Ave
* `59930` – Southbound Hudson St @ SW Marine Dr
* `59931` – South Surrey Park & Ride @ Bay 4
* `59932` – South Surrey Park & Ride @ Bay 1
* `59933` – South Surrey Park & Ride @ Bay 3
* `59935` – Lincoln Station @ Bay 2
* `59940` – Westbound Stadium Rd @ West Mall
* `59941` – Southbound West Mall @ Hawthorn Lane
* `59942` – Westbound Thunderbird Blvd @ Larkin Dr
* `59946` – Southbound Wesbrook Mall @ Iona Dr
* `59964` – Northbound NW Marine Dr @ Agronomy Rd
* `59974` – Northbound Great Canadian Way @ Beckwith Rd
* `59983` – Westbound Fraser Hwy @ 222 St
* `59985` – Southbound 80 St @ 4700 Block
* `59991` – Westbound Expo Blvd @ Abbott St
* `59992` – Northbound Pat Quinn Way @ Expo Blvd
* `59995` – Northbound Walker Ave @ Imperial Ave
* `60001` – Northbound 160 St @ 96 Ave
* `60006` – Eastbound W Broadway @ Columbia St
* `60014` – Westbound Expo Blvd @ Nelson St
* `60020` – Eastbound E 49 Ave @ Doman St
* `60047` – Eastbound 100 Ave @ 160 St
* `60069` – Westbound River Way @ 82 St
* `60070` – Southbound 80 St @ River Way
* `60071` – Eastbound River Way @ 80 St
* `60072` – Northbound 82 St @ River Way
* `60078` – Eastbound Lincoln Ave @ Westwood St
* `60079` – Eastbound Cumberland St @ Richmond St
* `60097` – Westbound 28 Ave @ 53 St
* `60102` – Southbound Cambie St @ Nelson St
* `60103` – Northbound Cambie St @ Nelson St
* `60104` – Northbound Cambie St @ Robson St
* `60116` – Eastbound Davie St @ Granville St
* `60122` – Eastbound W 2nd St @ Bewicke Ave
* `60136` – Eastbound 72 Ave @ 196 St
* `60137` – Westbound 72 Ave @ 198B St
* `60138` – Eastbound 72 Ave @ 198B St
* `60139` – Westbound 72 Ave @ 200 St
* `60140` – Westbound 72 Ave @ 202B St
* `60142` – Southbound 202B St @ 72 Ave
* `60146` – Northbound 202B St @ 68 Ave
* `60158` – UBC Exchange @ Bay 1
* `60159` – UBC Exchange @ Bay 2
* `60160` – UBC Exchange @ Bay 3
* `60162` – UBC Exchange @ Bay 4
* `60163` – UBC Exchange @ Bay 5
* `60164` – UBC Exchange @ Bay 6
* `60166` – Eastbound Cumberland St @ Miner St
* `60167` – Westbound Cumberland St  @ Sapper St
* `60191` – Westbound Fern Cres @ 23600 Block
* `60195` – Metrotown Station @ Bay 10
* `60197` – Southbound Burrard St @ Harwood St
* `60198` – Southbound 52 St @ Hwy 17
* `60206` – Westbound 64 Ave @ 196 St
* `60207` – Eastbound 64 Ave @ 194 St
* `60208` – 29th Avenue Station @ Bay 4
* `60211` – Metrotown Station @ Bay 11
* `60212` – Metrotown Station @ Bay 12
* `60213` – Metrotown Station @ Bay 13
* `60214` – Metrotown Station @ Bay 14
* `60216` – Southbound Spyglass Rd @ Windjammer Rd
* `60223` – Northbound Fraser St @ E 20 Ave
* `60231` – Northbound King George Blvd @ Crescent Rd
* `60233` – Maple Meadows Station @ Bay 1
* `60234` – Southbound Surrey Rd @ 115A Ave
* `60237` – Artisan Square (Flag)
* `60251` – Eastbound SW Marine Dr @ Hudson St
* `60252` – Southbound 80 St @ 4500 Block
* `60271` – Westbound Bedwell Bay Rd @ White Pine Beach Rd
* `60275` – Northbound 51 St @ 47 Ave
* `60276` – Southbound 15 St @ Ottawa Ave
* `60277` – Northbound Seymour St @ Robson St
* `60290` – Southbound Mt Gardner Rd @ Magee Rd
* `60292` – White Rock Centre @ Bay 4
* `60293` – Southbound 52 St @ Cliff Dr
* `60294` – Northbound 52 St @ Saratoga Dr
* `60295` – Northbound 152 St @ 7900 Block
* `60298` – Eastbound Blundell Rd @ No. 8 Rd
* `60299` – Eastbound Blundell Rd @ York Rd
* `60300` – Westbound Blundell Rd @ York Rd
* `60302` – Northbound Cambie St @ Dunsmuir St
* `60305` – Westbound Blundell Rd @ Zylmans Way
* `60306` – Westbound Blundell Rd @ No. 8 Rd
* `60307` – Broadway-City Hall Station @ Bay 3
* `60314` – Nanaimo Station @ Bay 1
* `60329` – Brentwood Station @ Bay 1
* `60331` – Eastbound David Ave @ Pinetree Way
* `60336` – Eastbound E Cordova St @ Heatley Ave
* `60345` – Southbound Holly Dr @ Campion Way
* `60348` – Eastbound W Georgia St @ Homer St
* `60352` – Westbound Granville Ave @ Moffatt Rd
* `60358` – UBC Exchange @ Unloading Only
* `60359` – Northbound Macdonald St @ W 13 Ave
* `60368` – Northbound 80 St @ 4500 Block
* `60370` – Eastbound Lougheed Hwy @ Laity St
* `60371` – Westbound Lougheed Hwy @ Laity St
* `60372` – Eastbound Lougheed Hwy @ 203 St
* `60373` – Westbound Lougheed Hwy @ 203 St
* `60375` – Westbound Great Northern Way @ Carolina Street
* `60383` – Southbound Wesbrook Mall @ Agronomy Rd
* `60384` – Northbound Wesbrook Mall @ Agronomy Rd
* `60385` – Westbound W 41 Ave @ East Blvd
* `60386` – Eastbound W 41 Ave @ East Blvd
* `60387` – Westbound Blundell Rd @ 16100 Block
* `60390` – Coquitlam Central Station @ Bay 14
* `60391` – Eastbound Great Northern Way @ Carolina St
* `60393` – Dunbar Loop @ Bay 7
* `60394` – Dunbar Loop @ Bay 4
* `60395` – Westbound W 41 Ave @ Carnarvon St
* `60396` – Eastbound W 41 Ave @ Carnarvon St
* `60397` – Westbound W 41 Ave @ Oak St
* `60399` – Eastbound E 41 Ave @ Main St
* `60400` – Westbound E 41 Ave @ Fraser St
* `60402` – Westbound E 41 Ave @ Victoria Dr
* `60403` – Southbound Joyce St @ Kingsway
* `60404` – Northbound Joyce St @ Church St
* `60407` – Eastbound W 3rd St @ Chesterfield Ave
* `60408` – Eastbound W 12th Ave @ Arbutus St
* `60409` – Eastbound W 12th Ave @ Cypress St
* `60410` – Westbound W 12th Ave @ Cypress St
* `60411` – Eastbound W 12 Ave @ Pine St
* `60412` – Westbound W 12th Ave @ Pine St
* `60413` – Eastbound W 12 Ave @ Oak St
* `60414` – Eastbound W 12th Ave @ Laurel St
* `60415` – Westbound W 12th Ave @ Laurel St
* `60416` – Eastbound W 12th Ave @ Heather St
* `60417` – Westbound W 12th Ave @ Heather St
* `60418` – Westbound W 12th Ave @ Cambie St
* `60424` – Eastbound Blundell Rd @ York Rd
* `60436` – YVR Domestic Departures  @ Level Three
* `60437` – Phibbs Exchange @ Bay 12
* `60479` – Westbound E 27 St @ 1200 Block
* `60482` – Richmond-Brighouse Station @ Bay 2
* `60483` – Richmond-Brighouse Station @ Bay 1
* `60504` – Eastbound Robson St @ Broughton St
* `60506` – Eastbound Pacific St @ Seymour Mews
* `60516` – Brentwood Station @ Bay 5
* `60520` – Southbound Griffiths Dr @ Kingsway
* `60522` – Northbound Griffiths Ave @ Kingsway
* `60526` – Westbound Beach Ave @ Bidwell St
* `60527` – Eastbound Beach Ave @ Bidwell St
* `60528` – Eastbound Beach Ave @ Cardero St
* `60529` – Westbound Beach Ave @ Cardero St
* `60530` – Eastbound Pacific St @ Jervis St
* `60533` – Northbound Blanca St @ W 5th Ave
* `60537` – Southbound No. 3rd Rd @ Sea Island Way
* `60559` – Northbound Scott Rd @ 7800 Block
* `60562` – Northbound N Boundary Rd @ Trinity St
* `60569` – Westbound River Rd @ Hwy 17 Offramp
* `60570` – Eastbound W 4th Ave @ Blenheim St
* `60571` – Eastbound W 4th Ave @ Balaclava St
* `60572` – Eastbound W 4th Ave @ Yew St
* `60574` – Westbound W 4th Ave @ Yew St
* `60575` – Southbound Alma St @ W 10 Ave
* `60577` – Westbound W Georgia St @ Richards St
* `60601` – Northbound 128 St @ 80 Ave
* `60663` – Eastbound 56 Ave @ Hwy 10
* `60709` – Southbound Knight St @ E 17 Ave
* `60725` – Eastbound W Broadway @ Collingwood St
* `60726` – Eastbound W Broadway @ Blenheim St
* `60727` – Eastbound W Broadway St @ Balaclava St
* `60728` – Eastbound W Broadway @ Bayswater St
* `60730` – Westbound 115 Ave @ 132 St
* `60731` – Westbound 115 Ave @ Bridgeview Dr
* `60733` – Southbound 124 St @ 112A Ave
* `60734` – Eastbound 111A Ave @ 12300 Block
* `60735` – Eastbound King George Blvd @ Scott Rd Onramp
* `60737` – Westbound 111A Ave @ 12300 Block
* `60738` – Eastbound 112 Ave @ 124 St
* `60740` – Eastbound 115 Ave @ Bridgeview Dr
* `60741` – Southbound 132 St @ 115 Ave
* `60745` – Dunbar Loop @ Bay 3
* `60748` – Westbound On Dewdney Trunk Rd @ Garibaldi St (Flag)
* `60749` – Eastbound On Dewdney Trunk Rd @ Garibaldi St (Flag)
* `60776` – Northbound 200 St @ 119A Ave
* `60780` – Northbound 176A St @ 57 Ave
* `60840` – Southbound Nelson Rd @ Westminster Hwy
* `60841` – Westbound Blundell Rd @ Nelson Rd
* `60842` – Westbound Blundell Rd @ 18300 Block
* `60846` – Eastbound Blundell Rd @ 18300 Block
* `60847` – Northbound Nelson Rd @ Blundell Rd
* `60856` – Northbound Graybar Rd @ Westminster Hwy
* `60857` – Westbound Gordon Way @ 21300 Block
* `60858` – Southbound Fraserwood Place @ Westminster Hwy
* `60859` – Westbound Moscrop St @ Willingdon Ave
* `60864` – Southbound 203 St @ 56 Ave
* `60865` – Westbound 56 Ave @ 203 St
* `60868` – Northbound Ferry Rd @ Commodore Dr
* `60869` – Northbound Ferry Rd @ Windjammer Rd
* `60870` – Northbound Ferry Rd @ 5900 Block
* `60871` – Eastbound Admiral Way @ Ferry Rd
* `60872` – Eastbound Admiral Way @ 5300 Block
* `60873` – Southbound Admiral Blvd @ Cove Inlet Rd
* `60874` – Southbound Admiral Blvd @ Cove Link Rd
* `60879` – Westbound 88 Ave @ 130 St
* `60880` – Eastbound 88 Ave @ 130 St
* `60882` – Westbound Bus Access Rd @ Burns Dr
* `60885` – Westbound Marine Dr @ Nelson Ave
* `60889` – Northbound Thompson Cres @ Thompson Place
* `60909` – Northbound 15 St @ Ottawa Ave
* `60974` – Westbound Steveston Hwy @ Entertainment Blvd
* `60979` – Mission City Station @ Bay 1
* `60980` – Southbound Granville St @ W Georgia St
* `60982` – Northbound 203 St @ 66 Ave
* `60983` – Southbound 203 St @ 68 Ave
* `60984` – Eastbound 68 Ave @ 203 St
* `60985` – Eastbound 68 Ave @ 204A St
* `60986` – Westbound 68 Ave @ 204A St
* `60987` – Westbound 68 Ave @ 206 St
* `60988` – Southbound 206 St @ 68 Ave
* `60989` – Southbound 206 St @ 66 Ave
* `60990` – Northbound 206 St @ 66 Ave
* `60991` – Westbound 66 Ave @ 20400 Block
* `60992` – Eastbound 66 Ave @ 20400 Block
* `60993` – Northbound Granville St @ Dunsmuir St
* `60996` – Northbound Cambie St @ W 45 Ave
* `61009` – Westbound Kingsway @ Marlborough Ave
* `61011` – Phibbs Exchange @ Bay 6
* `61013` – Northbound Shaughnessy St @ McAllister Ave
* `61014` – Eastbound 24 Ave @ 156 St
* `61027` – Broadway-City Hall Station @ Bay 2
* `61028` – Southbound 199A St @ 96 Ave
* `61029` – Westbound Churchill St @ 7900 Block
* `61031` – Westbound W Georgia St @ Granville St
* `61033` – Southbound Scott Rd @ Preddy Dr
* `61035` – Surrey Central Station @ Bay 12
* `61036` – Surrey Central Station @ Bay 13
* `61039` – Olympic Village Station @ Bay 2
* `61040` – Olympic Village Station @ Bay 1
* `61042` – Eastbound W 16 Ave @ Wesbrook Mall
* `61043` – Westbound W 16 Ave @ Wesbrook Mall
* `61044` – Northbound Wesbrook Mall @ W 16 Ave
* `61048` – Westbound 103A Ave @ Scott Rd
* `61051` – Westbound Grace Rd @ 10200 Block
* `61052` – Eastbound Grace Rd @ 10200 Block
* `61057` – Haney Place @ Bay 8
* `61058` – Haney Place @ Bay 7
* `61059` – Haney Place @ Bay 6
* `61060` – Haney Place @ Bay 5
* `61061` – Haney Place @ Bay 4
* `61062` – Haney Place @ Bay 3
* `61063` – Haney Place @ Bay 2
* `61064` – Haney Place @ Bay 1
* `61070` – Capilano University @ Bay 2
* `61071` – Capilano University @ Bay 3
* `61073` – Southbound Cambie St @ Robson St
* `61074` – Southbound Main St @ Powell St
* `61083` – Southbound Endswell Farm @ Smith Rd (Flag)
* `61088` – Eastbound W 16 Ave @ Dunbar St
* `61089` – Eastbound W 16 Ave @ Blenheim St
* `61090` – Eastbound W 16 Ave @ Macdonald St
* `61091` – Eastbound W 16 Ave @ Trafalgar St
* `61092` – Eastbound W 16 Ave @ Vine St
* `61093` – Eastbound W 16 Ave @ East Blvd
* `61094` – Eastbound W 16 Ave @ Burrard St
* `61095` – Eastbound Marpole Ave @ McRae Ave
* `61096` – Eastbound W 16 Ave @ Oak St
* `61097` – Eastbound W 16 Ave @ Willow St
* `61099` – Eastbound E 29 Ave @ Cambie St
* `61100` – Eastbound Midlothian Ave @ Clancy Loranger Way
* `61101` – Eastbound E 33 Ave @ Ontario St
* `61104` – Eastbound E 33 Ave @ Fraser St
* `61105` – Eastbound E 33 Ave @ Somerville St
* `61106` – Eastbound E 33 Ave @ Knight St
* `61107` – Eastbound E 33 Ave @ Argyle St
* `61108` – Eastbound E 33 Ave @ Victoria Dr
* `61109` – Eastbound E 33 Ave @ Nanaimo St
* `61110` – Southbound Slocan St @ Kingsway
* `61111` – Westbound E 33 Ave @ Nanaimo St
* `61112` – Westbound E 33 Ave @ Victoria Dr
* `61113` – Westbound E 33 Ave @ Fleming St
* `61114` – Westbound E 33 Ave @ Knight St
* `61115` – Westbound E 33 Ave @ Windsor St
* `61116` – Westbound E 33 Ave @ Fraser St
* `61117` – Westbound E 33 Ave @ Prince Edward St
* `61118` – Westbound E 33 Ave @ Main St
* `61119` – Westbound E 33 Ave @ Ontario St
* `61120` – Westbound Midlothian Ave @ Clancy Loranger Way
* `61121` – Westbound W 16 Ave @ Cambie St
* `61122` – Westbound W 16 Ave @ Heather St
* `61124` – Westbound W 16 Ave @ Oak St
* `61125` – Westbound W 15 Ave @ Birch St
* `61126` – Westbound W 16 Ave @ Granville St
* `61127` – Westbound W 16 Ave @ Burrard St
* `61128` – Westbound W 16 Ave @ Arbutus St
* `61129` – Westbound W 16 Ave @ Vine St
* `61130` – Westbound W 16 Ave @ Trafalgar St
* `61131` – Westbound W 16 Ave @ Blenheim St
* `61134` – Westbound E 33 Ave @ Inverness St
* `61135` – Eastbound E 33 Ave @ Inverness St
* `61149` – Southbound Cambie St @ W 14 Ave
* `61150` – Eastbound E 33 Ave @ Main St
* `61163` – Eastbound 64 Ave @ Scott Rd
* `61184` – Westbound 64 Ave @ 152 St
* `61185` – Eastbound 64 Ave @ 152 St
* `61186` – Westbound 64 Ave @ Fraser Hwy
* `61189` – Eastbound Nordel Way @ Scott Rd
* `61190` – Westbound Nordel Way @ Scott Rd
* `61191` – Eastbound 88 Ave @ 128 St
* `61192` – Westbound 88 Ave @ 128 St
* `61193` – Eastbound 88 Ave @ 132 St
* `61194` – Westbound 88 Ave @ 132 St
* `61195` – Eastbound 88 Ave @ 133A St
* `61196` – Westbound 88 Ave @ 133A St
* `61197` – Westbound 88 Ave @ King George Blvd
* `61198` – Eastbound 88 Ave @ King George Blvd
* `61199` – Westbound 88 Ave @ Lauder Dr
* `61200` – Eastbound 88 Ave @ Bear Creek Park Access Rd
* `61201` – Westbound 88 Ave @ 140 St
* `61202` – Eastbound 88 Ave @ 156 St
* `61203` – Eastbound 88 Ave @ 158 St
* `61204` – Westbound 88 Ave @ 158 St
* `61205` – Eastbound 88 Ave @ 164 St
* `61206` – Westbound 88 Ave @ 164 St
* `61209` – Westbound 88 Ave @ 168 St
* `61210` – Northbound 168 St @ 88 Ave
* `61211` – Westbound 96 Ave @ 201 St
* `61215` – Yaletown-Roundhouse Station @ Bay 2
* `61218` – Langara-49th Station @ Bay 2
* `61219` – Eastbound 84 Ave @ 108 St
* `61222` – Southbound Tsawwassen Dr N @ Falcon Way
* `61223` – Southbound Tsawwassen Dr N @ 2100 Block
* `61224` – Northbound Tsawwassen Dr N @ 2100 Block
* `61225` – Southbound Tsawwassen Dr N @ 1900 Block
* `61226` – Northbound Tsawwassen Dr N @ 1900 Block
* `61232` – Southbound 52 St @ Springs Blvd
* `61233` – Northbound 52 St @ Spyglass Cres
* `61235` – Westbound Murray St @ Electronic Ave
* `61237` – Eastbound Miller Rd @ Aviation Ave
* `61239` – Northbound Lillooet Rd @ Purcell Way
* `61248` – Northbound Russ Baker Way @ Gilbert Rd
* `61252` – Northbound Cambie St @ W 29 Ave
* `61254` – Olympic Village Station @ Bay 3
* `61258` – Southbound Willingdon Ave @ Still Creek Dr
* `61259` – Northbound Willingdon Ave @ Still Creek Dr
* `61268` – Eastbound 98 Ave @ 26400 Block (Flag)
* `61274` – Northbound Admiral Blvd @ Commodore Dr
* `61275` – Northbound Admiral Blvd @ Cove Reach Rd
* `61276` – Northbound Admiral Blvd @ Cove Inlet Rd
* `61277` – Westbound Admiral Way @ 5300 Block
* `61279` – Southbound Ferry Rd @ 5900 Block
* `61280` – Southbound Ferry Rd @ Windjammer Rd
* `61281` – Oakridge-41st Ave Station  @ Bay 4
* `61285` – Northbound 52 St @ 12 Ave
* `61288` – Southbound 160 St @ 108 Ave
* `61291` – Westbound Lougheed Hwy @ Dominion Ave
* `61292` – Southbound Granville St @ Robson St
* `61293` – Northbound Granville St @ W Georgia St
* `61296` – Westbound 64 Ave @ 203 St
* `61304` – Eastbound Lougheed Hwy @ Harris Rd
* `61306` – Coquitlam Central Station @ Bay 13
* `61309` – Eastbound 60 Ave @ 192 St
* `61321` – Bridgeport Station @ Bay 1 Unloading Only
* `61323` – Bridgeport Station @ Bay 3
* `61324` – Bridgeport Station @ Bay 4
* `61325` – Bridgeport Station @ Bay 5
* `61326` – Bridgeport Station @ Bay 7
* `61327` – Bridgeport Station @ Bay 8
* `61328` – Bridgeport Station @ Bay 9
* `61329` – Bridgeport Station @ Bay 10
* `61330` – Bridgeport Station @ Bay 11
* `61331` – Bridgeport Station @ Bay 12
* `61332` – Richmond-Brighouse Station @ Bay 6
* `61334` – Richmond-Brighouse Station @ Bay 4
* `61337` – Marine Drive Station @ Bay 1
* `61338` – Marine Drive Station @ Bay 2
* `61359` – Northbound 201 St @ 96 Ave
* `61360` – Southbound Oak St @ W 72 Ave
* `61361` – Westbound David Ave @ Genest Way
* `61401` – Southbound Johnston Rd @ Buena Vista Ave
* `61416` – Westbound 68 Ave @ 194 St
* `61422` – Westbound North Fraser Way @ Glenlyon Parkway East
* `61424` – Southbound West Mall @ NW Marine Dr
* `61430` – Bridgeport Station @ Bay 13
* `61437` – Eastbound Westminster Hwy @ Kartner Rd
* `61438` – Westbound Westminster Hwy @ Kartner Rd
* `61443` – Eastbound Cambie Rd @ Hazelbridge Way
* `61444` – Westbound Progress Way @ Venture St
* `61446` – Northbound 200 St @ 7800 Block
* `61453` – Westbound W Broadway @ Arbutus St
* `61477` – Westbound Steveston Hwy @ Coppersmith Place
* `61482` – Southbound 54 St @ 2nd Ave
* `61487` – Southbound 200 St @ 9200 Block
* `61491` – Westbound 110 Ave @ 157 St
* `61492` – Southbound 156 St @ 110 Ave
* `61493` – Southbound 156 St @ 108 Ave
* `61494` – Northbound 156 St @ 104 Ave
* `61496` – Eastbound 108 Ave @ 158 St
* `61497` – Northbound 157A St @ 10900 Block
* `61498` – Westbound Churchill St @ Athabaska St
* `61499` – Eastbound Churchill St @ Athabaska St
* `61501` – Eastbound 64 Ave @ Fraser Hwy
* `61503` – Eastbound E 41 Ave @ Fraser St
* `61504` – Westbound Agnes St @ Elliot St
* `61519` – Northbound Seymour St @ Dunsmuir St
* `61521` – Westbound Westminster Hwy @ Garden City Rd
* `61522` – Southbound Howe St @ Davie St
* `61530` – Northbound Crosscreek Rd @ Oceanview Rd
* `61533` – Northbound E Columbia St @ Cedar St
* `61536` – Eastbound Bedwell Bay Rd @ Midden Rd
* `61538` – Eastbound 24 Ave @ Croydon Dr
* `61540` – Southbound 194 St @ 68 Ave
* `61542` – Northbound 194 St @ 66 Ave
* `61543` – Southbound 194 St @ 66 Ave
* `61553` – Eastbound W 3rd St @ Bewicke Ave
* `61554` – Eastbound Harrison Dr @ Victoria Dr
* `61555` – Eastbound 24 Ave @ 157 St
* `61558` – Westbound Westminster Hwy @ Hwy 91 Offramp
* `61561` – Westbound 96 Ave @ 198 St
* `61566` – Eastbound Marine Dr Offramp @ Lions Gate Bridge
* `61568` – Eastbound Nordel Way @ Brooke Rd
* `61569` – Westbound Nordel Way @ Brooke Rd
* `61570` – Westbound Nordel Way @ Shepherd Way
* `61571` – Eastbound Nordel Way @ Shepherd Way
* `61574` – Southbound Scott Rd @ Tannery Rd
* `61579` – Southbound Wesbrook Mall @ Birney Ave
* `61581` – Southbound Wesbrook Mall @ Triumf Centre
* `61583` – Eastbound W 3rd St @ Mission Rd
* `61597` – Guildford Exchange @ Bay 4
* `61599` – Westbound Sixth Ave @ Eighth St
* `61600` – Eastbound 12 Ave @ 6th St
* `61601` – Westbound 12 Ave @ 6th St
* `61609` – Joyce Station @ Bay 5
* `61610` – Eastbound Como Lake Ave @ Robinson St
* `61612` – Southbound Howes St @ Hwy 91A Offramp
* `61614` – Westbound 96 Ave @ 176 St
* `61615` – Eastbound Golden Ears Way @ 176 St
* `61618` – Westbound River Rd @ 8800 Block
* `61619` – Eastbound 96 Ave @ 168 St
* `61620` – Eastbound 96 Ave @ 172 St
* `61621` – Westbound 96 Ave @ 172 St
* `61622` – Eastbound 96 Ave @ 173A St
* `61623` – Westbound 96 Ave @ 173A St
* `61624` – Southbound 144 St @ 68B Ave
* `61628` – Northbound 140 St @ 100 Ave
* `61629` – White Rock Centre @ Bay 9
* `61630` – Westbound 24 Ave @ 160 St
* `61631` – Eastbound 24 Ave @ 160 St
* `61635` – Eastbound 24 Ave @ 164 St
* `61637` – Eastbound 24 Ave @ 168 St
* `61638` – Westbound 24 Ave @ 192 St
* `61639` – Northbound 192 St @ 24 Ave
* `61641` – Northbound 192 St @ 26 Ave
* `61642` – Southbound 192 St @ 28 Ave
* `61643` – Northbound 192 St @ 28 Ave
* `61645` – Southbound Elliott St @ E 54 Ave
* `61647` – Southbound 123A St @ 96 Ave
* `61649` – New Westminster Station @ Bay 2
* `61650` – New Westminster Station @ Bay 3
* `61651` – New Westminster Station @ Bay 4
* `61652` – New Westminster Station @ Bay 5
* `61654` – New Westminster Station @ Bay 7
* `61655` – New Westminster Station @ Bay 8
* `61656` – New Westminster Station @ Bay 9
* `61657` – New Westminster Station @ Bay 10
* `61658` – Northbound University Dr @ 107A Ave
* `61659` – Southbound University Dr @ 107A Ave
* `61663` – Eastbound 32 Ave @ 192 St
* `61664` – Southbound 192 St @ 32 Ave
* `61666` – Eastbound 3rd Ave @ 6500 Block
* `61668` – Northbound 198 St @ 64 Ave
* `61672` – Eastbound 24 Ave @ 154 St
* `61673` – Westbound 24 Ave @ King George Blvd
* `61674` – Westbound 24 Ave @ Croydon Dr
* `61677` – Surrey Central Station @ Bay 5A
* `61678` – New Westminster Station @ Unloading Only
* `61679` – Eastbound 10 Ave @ 54A St
* `61680` – Northbound 80 St @ Churchill St
* `61681` – Westbound Churchill St @ 80 St
* `61683` – Southbound 200 St @ 56 Ave
* `61684` – Westbound Great Northern Way @ Thornton St
* `61689` – Eastbound Como Lake Ave @ Emerson St
* `61693` – Bridgeport Station @ Bay 6
* `61701` – UBC Exchange @ Bay 10
* `61702` – UBC Exchange @ Bay 11
* `61704` – Newton Exchange @ Bay 9
* `61705` – Newton Exchange @ Bay 10
* `61709` – Carvolth Exchange @ Bay 1 Unloading Only
* `61710` – Carvolth Exchange @ Bay 2
* `61711` – Carvolth Exchange @ Bay 3
* `61712` – Carvolth Exchange @ Bay 4
* `61714` – Carvolth Exchange @ Bay 6
* `61715` – Carvolth Exchange @ Bay 7
* `61716` – Carvolth Exchange @ Bay 8
* `61717` – Carvolth Exchange @ Bay 9
* `61719` – Carvolth Exchange @ Bay 11
* `61720` – Carvolth Exchange @ Bay 12
* `61723` – Northbound 200 St @ 84 Ave
* `61724` – Southbound 200 St @ 84 Ave
* `61727` – Westbound Ewen Ave @ Derwent Way
* `61730` – Southbound 152 St @ 104 Ave
* `61732` – Eastbound 56 Ave @ 128 St
* `61735` – Northbound 116 St @ 72A Ave
* `61736` – Eastbound 8th Ave @ 8th St
* `61737` – Eastbound 8th Ave @ 6th St
* `61748` – Westbound Como Lake Ave @ Robinson St
* `61749` – Westbound Dewdney Trunk Rd @ Westwood St
* `61750` – Eastbound Dewdney Trunk Rd @ Lougheed Hwy
* `61752` – Northbound Willingdon Ave @ Kingsborough St
* `61755` – Westbound 56 Ave @ 128 St
* `61758` – Southbound 168 St @ 96 Ave
* `61759` – Northbound Burrard St @ Comox St
* `61761` – Southbound 202 St @ 8900 Block
* `61762` – Eastbound W 4th Ave @ Maple St
* `61765` – Southbound Nordel Way @ Swenson Way
* `61766` – Southbound Nordel Way @ River Way
* `61767` – Tsawwassen Ferry Terminal @ Bay 2
* `61772` – Park Royal @ Bay 1
* `61777` – Northbound 128 St @ 86 Ave
* `61778` – Southbound 128 St @ 86 Ave
* `61781` – Eastbound 86 Ave @ 200 St
* `61782` – Park Royal @ Bay 4
* `61784` – Eastbound Westminster Hwy @ No. 8 Rd
* `61786` – Northbound King George Blvd @ 58A Ave
* `61787` – Surrey Central Station @ Bay 14
* `61790` – Eastbound 24 Ave @ 180 St
* `61792` – Eastbound 24 Ave @ 184 St
* `61794` – Eastbound 24 Ave @ 190 St
* `61795` – Westbound 24 Ave @ 190 St
* `61796` – Westbound 72 Ave @ 152 St
* `61797` – Westbound 72 Ave @ 148 St
* `61798` – Eastbound 72 Ave @ 148 St
* `61799` – Eastbound 72 Ave @ 144 St
* `61802` – Brentwood Station @ Bay 2
* `61803` – Brentwood Station @ Bay 3
* `61806` – Southbound Willingdon Ave @ Buchanan St
* `61809` – Eastbound Cornwall Ave @ Trafalgar St
* `61810` – Northbound Smithe St @ Expo Blvd
* `61811` – Southbound No. 5 Rd @ Steveston Hwy
* `61823` – Westbound River Rd @ Centre St
* `61825` – Northbound Clarendon St @ E 33 Ave
* `61826` – Southbound Clarendon St @ E 33 Ave
* `61833` – Northbound East Mall @ Eagles Dr
* `61889` – Moody Centre Station @ Bay 8
* `61890` – Moody Centre Station @ Bay 7
* `61891` – Moody Centre Station @ Bay 6
* `61892` – Moody Centre Station @ Bay 5
* `61894` – Eastbound Ross Dr @ Birney Ave
* `61895` – Eastbound Thunderbird Blvd @ Eagles Dr
* `61896` – Southbound Lower Mall @ University Blvd
* `61900` – Eastbound Agronomy Rd @ West Mall
* `61904` – Eastbound 24 Ave @ 172 St
* `61907` – Eastbound 24 Ave @ 176 St
* `61911` – Moody Centre Station @ Bay 3
* `61912` – Moody Centre Station @ Bay 4
* `61917` – Westbound W Pender St @ Howe St
* `61921` – Northbound McKay Ave @ Kingsborough St
* `61922` – Moody Centre Station @ Bay 9
* `61926` – Northbound 124 St @ 60 Ave
* `61927` – Westbound 60 Ave @ 196 St
* `61930` – Westbound Powell St @ Clark Dr
* `61933` – Eastbound Cordova Diversion @ Hawks Ave
* `61935` – UBC Exchange @ Bay 7
* `61937` – Northbound Cessna Dr @ Hudson Ave
* `61943` – Northbound Scott Rd @ 104 Ave
* `61946` – Southbound 196 St @ 56 Ave
* `61950` – Eastbound 32 Ave @ 196 St
* `61954` – Westbound W 2nd Ave @ Manitoba St
* `61959` – Westbound Hwy One Offramp @ 156 St
* `61960` – Eastbound Hwy One Offramp @ 156 St
* `61961` – Northbound 156 St @ Hwy One Onramp
* `61962` – Southbound 156 St @ Hwy One Offramp
* `61966` – Northbound Marine Way @ Market Crossing
* `61967` – Southbound Marine Way @ Market Crossing
* `61968` – Westbound North Fraser Way @ Meadow Ave
* `61969` – Eastbound North Fraser Way @ Glenwood Dr
* `61972` – Westbound North Fraser Way @ Wiggins St
* `61973` – Eastbound North Fraser Way @ Wiggins St
* `61974` – Westbound North Fraser Way @ 8000 Block
* `61978` – Lansdowne Station @ Bay 3
* `61985` – Westbound W 2nd Ave @ Crowe St
* `61986` – Westbound North Fraser Way @ Byrne Rd
* `61987` – Eastbound North Fraser Way @ Byrne Rd
* `61988` – Inlet Centre Station @ Bay 2
* `61990` – Eastbound Kingsway @ Gladstone St

## Stops in OSM that TransLink does not list (199)
* `50002` – [Node 4796015025](https://www.openstreetmap.org/node/4796015025) (Beach Ave (EB) at Burnaby St)
* `50041` – [Node 4368226890](https://www.openstreetmap.org/node/4368226890) (Renfrew St (SB) at Kitchener St)
* `50079` – [Node 4216587772](https://www.openstreetmap.org/node/4216587772) (West Pender St (EB) at Richard St)
* `50082` – [Node 3940288957](https://www.openstreetmap.org/node/3940288957) (Cambie Street (SB) at West Georgia Street)
* `50122` – [Node 4230854293](https://www.openstreetmap.org/node/4230854293) (West King Edward Avenue (WB) at Macdonald Street)
* `50135` – [Node 4230867228](https://www.openstreetmap.org/node/4230867228) (West 41st Ave (EB) at Heather St)
* `50141` – [Node 4232369899](https://www.openstreetmap.org/node/4232369899) (SE Marine Dr (WB) at Main St)
* `50161` – [Node 4339349290](https://www.openstreetmap.org/node/4339349290) (West 41st Avenue (WB) at Heather St)
* `50222` – [Node 4230864424](https://www.openstreetmap.org/node/4230864424) (Granville St (NB) at Nelson St)
* `50227` – [Node 4230853988](https://www.openstreetmap.org/node/4230853988) (East Cordova Street at Carrall Street)
* `50324` – [Node 4272404690](https://www.openstreetmap.org/node/4272404690) (West Broadway (EB) at Maple Street)
* `50326` – [Node 6330072517](https://www.openstreetmap.org/node/6330072517) (West Broadway (EB) at Granville St)
* `50373` – [Node 4230855466](https://www.openstreetmap.org/node/4230855466) (Dunbar St (SB) at West 39th Ave)
* `50420` – [Node 4270712691](https://www.openstreetmap.org/node/4270712691) (Cambie Street (SB) at West 27th Avenue)
* `50434` – [Node 4230864428](https://www.openstreetmap.org/node/4230864428) (Powell St (EB) at Victoria Dr)
* `50463` – [Node 4339326994](https://www.openstreetmap.org/node/4339326994) (West Broadway (WB) at Oak Street)
* `50466` – [Node 4417379816](https://www.openstreetmap.org/node/4417379816) (West Broadway (WB) at Hemlock Street)
* `50482` – [Node 4232366693](https://www.openstreetmap.org/node/4232366693) (Cambie Street (NB) at West 8th Avenue)
* `50504` – [Node 4417343296](https://www.openstreetmap.org/node/4417343296) (East Hastings St (WB) at Hawks Ave)
* `50505` – [Node 3777224212](https://www.openstreetmap.org/node/3777224212) (WB East Hastings Street at Princess Avenue)
* `50530` – [Node 3789883496](https://www.openstreetmap.org/node/3789883496) (Burrard Station Bay 7)
* `50559` – [Node 4417386802](https://www.openstreetmap.org/node/4417386802) (West Broadway (WB) at Maple Street)
* `50560` – [Node 685755414](https://www.openstreetmap.org/node/685755414) (West Broadway (WB) at Arbutus Street Bay 1)
* `50605` – [Node 729440560](https://www.openstreetmap.org/node/729440560) (University Blvd (WB) at Allison Road)
* `50613` – [Node 5099667222](https://www.openstreetmap.org/node/5099667222) (Robson Street (EB) at Jervis Street)
* `50648` – [Node 700713958](https://www.openstreetmap.org/node/700713958) (East 41st Avenue (EB) at Knight St)
* `50696` – [Node 4108981591](https://www.openstreetmap.org/node/4108981591) (Kingsway (WB) at Gladstone Street)
* `50762` – [Node 4368127100](https://www.openstreetmap.org/node/4368127100) (Granville St (NB) at Park Dr)
* `50803` – [Node 4232390800](https://www.openstreetmap.org/node/4232390800) (Southeast Marine Dr (WB) at Fraser St)
* `50823` – [Node 5127255541](https://www.openstreetmap.org/node/5127255541) (Fraser St (NB) at East 57th Ave)
* `50872` – [Node 1723435754](https://www.openstreetmap.org/node/1723435754) (West Broadway (EB) at Laurel Street)
* `50875` – [Node 768406641](https://www.openstreetmap.org/node/768406641) (Broadway-City Hall Station Bay 1)
* `50879` – [Node 725882531](https://www.openstreetmap.org/node/725882531) (Main Street (SB) at Broadway)
* `50881` – [Node 4842607917](https://www.openstreetmap.org/node/4842607917) (East Broadway (WB) at Main Street)
* `50922` – [Node 4417385416](https://www.openstreetmap.org/node/4417385416) (Broadway-City Hall Station Bay 5)
* `50933` – [Node 4368188095](https://www.openstreetmap.org/node/4368188095) (East Hastings Street (EB) at Carrall Street)
* `51192` – [Node 4108981690](https://www.openstreetmap.org/node/4108981690) (Kingsway (WB) at Dumfries Street)
* `51376` – [Node 4439975789](https://www.openstreetmap.org/node/4439975789) (Hastings St (EB) at Boundary Rd)
* `51387` – [Node 6781126261](https://www.openstreetmap.org/node/6781126261) (Willingdon Ave (SB) at Kitchener St)
* `51465` – [Node 5536944423](https://www.openstreetmap.org/node/5536944423) (Hastings St (WB) at Willingdon Ave)
* `51497` – [Node 1682511159](https://www.openstreetmap.org/node/1682511159) (West King Edward Avenue (EB) at Quesnel Drive)
* `51506` – [Node 8914980140](https://www.openstreetmap.org/node/8914980140) (West King Edward Ave (EB) at Alexandra St)
* `51516` – [Node 4230812557](https://www.openstreetmap.org/node/4230812557) (West King Edward Avenue (EB) at Manitoba Street)
* `51517` – [Node 4230812360](https://www.openstreetmap.org/node/4230812360) (East King Edward Avenue (EB) at Quebec Street)
* `51550` – [Node 5404589407](https://www.openstreetmap.org/node/5404589407) (Kincaid St (WB) at MacDonald Ave)
* `51557` – [Node 8060122585](https://www.openstreetmap.org/node/8060122585) (Kingsway (EB) At Smith Avenue)
* `51748` – [Node 4230864421](https://www.openstreetmap.org/node/4230864421) (Oxford St (WB) at Mountain Hwy)
* `51760` – [Node 4230862926](https://www.openstreetmap.org/node/4230862926) (Oxford St (EB) at Mountain Hwy)
* `51817` – [Node 4230870903](https://www.openstreetmap.org/node/4230870903) (East 29th Ave (EB) at Slocan St)
* `51860` – [Node 7882628081](https://www.openstreetmap.org/node/7882628081) (SFU Transportation Centre Bay 3)
* `51891` – [Node 9021676326](https://www.openstreetmap.org/node/9021676326)
* `51906` – [Node 7141943587](https://www.openstreetmap.org/node/7141943587)
* `51910` – [Node 7141943591](https://www.openstreetmap.org/node/7141943591)
* `51912` – [Node 726786177](https://www.openstreetmap.org/node/726786177) (NW Marine Dr (EB) at Locarno Crescent)
* `51934` – [Node 8632707087](https://www.openstreetmap.org/node/8632707087)
* `51935` – [Node 7141943588](https://www.openstreetmap.org/node/7141943588)
* `51937` – [Node 7141943585](https://www.openstreetmap.org/node/7141943585)
* `51938` – [Node 7141943586](https://www.openstreetmap.org/node/7141943586)
* `51965` – [Node 4231066513](https://www.openstreetmap.org/node/4231066513) (West 49th Avenue (EB) at Heather St)
* `52004` – [Node 4231071892](https://www.openstreetmap.org/node/4231071892) (Imperial Street (WB) at Patterson Avenue)
* `52100` – [Node 4279156853](https://www.openstreetmap.org/node/4279156853) (Broadway-City Hall Station Bay 6)
* `52107` – [Node 5141956224](https://www.openstreetmap.org/node/5141956224)
* `52165` – [Node 913811583](https://www.openstreetmap.org/node/913811583) (22nd Street Station Bay 2)
* `52218` – [Node 4355626009](https://www.openstreetmap.org/node/4355626009) (SW Marine Drive (WB) at Logan St)
* `52220` – [Node 7715250699](https://www.openstreetmap.org/node/7715250699) (Marpole Loop Bay 3)
* `52264` – [Node 6497918054](https://www.openstreetmap.org/node/6497918054) (Rumble St (EB) at Gilley Ave)
* `52274` – [Node 6833681567](https://www.openstreetmap.org/node/6833681567) (Government St (WB) at Cardston Court)
* `52316` – [Node 6593526435](https://www.openstreetmap.org/node/6593526435) (Columbia St (EB) at 6 St)
* `52317` – [Node 6593526433](https://www.openstreetmap.org/node/6593526433) (Columbia Station Bay 2)
* `52330` – [Node 6814957335](https://www.openstreetmap.org/node/6814957335) (Columbia St (WB) at 200 Block)
* `52370` – [Node 4603127653](https://www.openstreetmap.org/node/4603127653)
* `52416` – [Node 6840870066](https://www.openstreetmap.org/node/6840870066) (6 St (SB) at 18 Ave)
* `52459` – [Node 1704261683](https://www.openstreetmap.org/node/1704261683) (Kingsway (WB) at Gilley Ave)
* `52645` – [Node 2107540212](https://www.openstreetmap.org/node/2107540212)
* `52751` – [Node 6842488464](https://www.openstreetmap.org/node/6842488464) (Lougheed Hwy (EB) at Beta Ave)
* `52752` – [Node 6842488466](https://www.openstreetmap.org/node/6842488466) (Douglas Rd (SB) at 2200 Block)
* `52753` – [Node 6842488468](https://www.openstreetmap.org/node/6842488468) (Douglas Rd (SB) at Springer Ave)
* `52790` – [Node 6842488467](https://www.openstreetmap.org/node/6842488467) (Douglas Rd (NB) at Springer Ave)
* `53106` – [Node 5697308926](https://www.openstreetmap.org/node/5697308926) (East Columbia St (NB) at Sherbrooke St)
* `53209` – [Node 6450845978](https://www.openstreetmap.org/node/6450845978) (Burquilam Station Bay 6)
* `53224` – [Node 5344031037](https://www.openstreetmap.org/node/5344031037) (Moody Centre Station Bay 2)
* `53249` – [Node 513707337](https://www.openstreetmap.org/node/513707337)
* `53290` – [Node 2294987136](https://www.openstreetmap.org/node/2294987136) (Lansdowne Dr (NB) at David Ave)
* `53395` – [Node 6634144168](https://www.openstreetmap.org/node/6634144168) (Guilby St (SB) at Austin Ave)
* `53443` – [Node 7972532230](https://www.openstreetmap.org/node/7972532230)
* `53624` – [Node 6816591336](https://www.openstreetmap.org/node/6816591336) (6 Ave (WB) at 8 St)
* `53953` – [Node 6579343761](https://www.openstreetmap.org/node/6579343761) (East 3rd St (EB) at Ridgeway Ave)
* `54349` – [Node 6419860499](https://www.openstreetmap.org/node/6419860499) (East 15th St (WB) at Saint George Ave)
* `54375` – [Node 5826074555](https://www.openstreetmap.org/node/5826074555) (West Esplanade St (WB) at Rogers Ave)
* `54481` – [Node 5306104358](https://www.openstreetmap.org/node/5306104358) (Scott Rd (NB) at Highway 10)
* `54916` – [Node 5339980502](https://www.openstreetmap.org/node/5339980502) (Scott Road Station Bay 5)
* `54943` – [Node 5339980503](https://www.openstreetmap.org/node/5339980503) (Scott Road Station Bay 6)
* `54958` – [Node 7051277158](https://www.openstreetmap.org/node/7051277158) (Centre St (EB) at River Rd)
* `55200` – [Node 5339980501](https://www.openstreetmap.org/node/5339980501) (Scott Road Station Bay 4)
* `55455` – [Node 6415466308](https://www.openstreetmap.org/node/6415466308) (King George Blvd (SB) at 88 Ave)
* `55489` – [Node 6419624431](https://www.openstreetmap.org/node/6419624431) (White Rock Centre Bay 4)
* `55666` – [Node 5729400312](https://www.openstreetmap.org/node/5729400312) (132 St (NB) at Old Yale Rd)
* `55667` – [Node 5729400314](https://www.openstreetmap.org/node/5729400314) (132 St (SB) at 103A Ave)
* `55837` – [Node 4426053494](https://www.openstreetmap.org/node/4426053494) (University Dr (NB) at 104 Ave)
* `56135` – [Node 4412326850](https://www.openstreetmap.org/node/4412326850) (Hwy 17 (NB) at Salish Sea Drive)
* `56397` – [Node 5339980499](https://www.openstreetmap.org/node/5339980499) (Scott Road Station Bay 2)
* `56418` – [Node 6486091927](https://www.openstreetmap.org/node/6486091927) (Fraser Hwy (WB) at 148 St)
* `56419` – [Node 5727570414](https://www.openstreetmap.org/node/5727570414) (Fraser Hwy (WB) at 140 St)
* `56420` – [Node 5727581446](https://www.openstreetmap.org/node/5727581446) (Fraser Hwy (WB) at Whalley Blvd)
* `56480` – [Node 8104515703](https://www.openstreetmap.org/node/8104515703) (Cook Road (WB) at No. 3 Road)
* `56558` – [Node 5131257030](https://www.openstreetmap.org/node/5131257030) (Burrard Station Bay 6)
* `56907` – [Node 8140657363](https://www.openstreetmap.org/node/8140657363)
* `56908` – [Node 4603127655](https://www.openstreetmap.org/node/4603127655)
* `56909` – [Node 4603127656](https://www.openstreetmap.org/node/4603127656)
* `56920` – [Node 4218586997](https://www.openstreetmap.org/node/4218586997) (Westminster Hwy (WB) at Fraserside Gate)
* `57071` – [Node 6840912114](https://www.openstreetmap.org/node/6840912114) (200 St (SB) at 7500 Block)
* `57165` – [Node 3980985159](https://www.openstreetmap.org/node/3980985159) (272 St (NB) at Fraser Hwy)
* `57292` – [Node 7490744481](https://www.openstreetmap.org/node/7490744481)
* `57293` – [Node 7490744480](https://www.openstreetmap.org/node/7490744480)
* `57296` – [Node 2140852346](https://www.openstreetmap.org/node/2140852346)
* `57327` – [Node 7490744483](https://www.openstreetmap.org/node/7490744483)
* `57330` – [Node 7490744479](https://www.openstreetmap.org/node/7490744479)
* `57331` – [Node 7490744482](https://www.openstreetmap.org/node/7490744482)
* `57527` – [Node 6711164139](https://www.openstreetmap.org/node/6711164139) (45 Avenue (WB) at 47A Street)
* `58041` – [Node 3492503907](https://www.openstreetmap.org/node/3492503907) (Kootenay Loop Bay 1)
* `58097` – [Node 6439982965](https://www.openstreetmap.org/node/6439982965) (No. 3 Rd (NB) at Capstan Way)
* `58103` – [Node 1720001573](https://www.openstreetmap.org/node/1720001573) (6 St (SB) at Columbia St)
* `58125` – [Node 4230812766](https://www.openstreetmap.org/node/4230812766) (West Cordova St (EB) at Abbott St)
* `58143` – [Node 3492503920](https://www.openstreetmap.org/node/3492503920) (Kootenay Loop Bay 8)
* `58240` – [Node 1723405964](https://www.openstreetmap.org/node/1723405964) (West Broadway (EB) at Fir Street)
* `58256` – [Node 6711164148](https://www.openstreetmap.org/node/6711164148) (48 Avenue (WB) at Laidlaw Street)
* `58269` – [Node 4417385417](https://www.openstreetmap.org/node/4417385417) (West Broadway (WB) NS Ash Street)
* `58287` – [Node 6959309112](https://www.openstreetmap.org/node/6959309112) (Willingdon Ave (NB) at Halifax St)
* `58292` – [Node 6167677491](https://www.openstreetmap.org/node/6167677491) (UBC Exchange Bay 13)
* `58293` – [Node 6418108595](https://www.openstreetmap.org/node/6418108595) (Hastings St (WB) at Duthie Ave)
* `58435` – [Node 3091624941](https://www.openstreetmap.org/node/3091624941) (Lougheed Station Bay 1)
* `58496` – [Node 6450840097](https://www.openstreetmap.org/node/6450840097) (Brentwood Town Centre Station Bay 6)
* `58500` – [Node 6371525840](https://www.openstreetmap.org/node/6371525840) (East Broadway (WB) at Main St)
* `58503` – [Node 768417671](https://www.openstreetmap.org/node/768417671) (West Broadway (EB) at Granville Street)
* `58594` – [Node 7972532229](https://www.openstreetmap.org/node/7972532229)
* `58642` – [Node 8152593144](https://www.openstreetmap.org/node/8152593144)
* `58708` – [Node 5697304580](https://www.openstreetmap.org/node/5697304580)
* `59011` – [Node 5910708286](https://www.openstreetmap.org/node/5910708286) (Angus Dr (NB) at West 64th Ave)
* `59188` – [Node 5924666886](https://www.openstreetmap.org/node/5924666886) (SFU Transit Exchange - Unloading Only)
* `59395` – [Node 5872507691](https://www.openstreetmap.org/node/5872507691)
* `59440` – [Node 4339342999](https://www.openstreetmap.org/node/4339342999) (West Broadway (EB) at Oak Street)
* `59468` – [Node 6188948329](https://www.openstreetmap.org/node/6188948329) (West Hastings Street (EB) at Abbott Street)
* `59729` – [Node 8098691502](https://www.openstreetmap.org/node/8098691502) (Park Road (WB) at 8000 Block)
* `59731` – [Node 6043413440](https://www.openstreetmap.org/node/6043413440) (Park Road (EB) at Buswell Street)
* `59752` – [Node 6575367928](https://www.openstreetmap.org/node/6575367928) (West 6th Ave (EB) at Alder Crossing)
* `59762` – [Node 6389849548](https://www.openstreetmap.org/node/6389849548) (East 2nd Ave (EB) at Main St)
* `59779` – [Node 6450855474](https://www.openstreetmap.org/node/6450855474) (Pitt Meadows Station Bay 4)
* `59811` – [Node 6818818690](https://www.openstreetmap.org/node/6818818690) (Glover Rd (SB) at Duncan Way)
* `59815` – [Node 6587640974](https://www.openstreetmap.org/node/6587640974) (Government St (WB) at Halston Court)
* `59816` – [Node 6587640973](https://www.openstreetmap.org/node/6587640973) (Government St (EB) at Halston Court)
* `59882` – [Node 6450856688](https://www.openstreetmap.org/node/6450856688) (Inlet Centre Bay 4)
* `59928` – [Node 5581881777](https://www.openstreetmap.org/node/5581881777) (National Avenue (EB) at Main Street)
* `59929` – [Node 3874088857](https://www.openstreetmap.org/node/3874088857) (Main Street-Science World Station Bay 3)
* `59937` – [Node 1677835621](https://www.openstreetmap.org/node/1677835621)
* `59993` – [Node 6419658526](https://www.openstreetmap.org/node/6419658526) (100 Ave (WB) at 151 St)
* `59994` – [Node 5727621927](https://www.openstreetmap.org/node/5727621927) (100 Ave (WB) at 152 St)
* `59997` – [Node 685755425](https://www.openstreetmap.org/node/685755425) (West Broadway (EB) at Arbutus Street)
* `60113` – [Node 6450855482](https://www.openstreetmap.org/node/6450855482) (Lincoln Station Bay 3)
* `60228` – [Node 5598537525](https://www.openstreetmap.org/node/5598537525) (Shuttle Bus - International + U.S.A. Terminal)
* `60229` – [Node 5340005235](https://www.openstreetmap.org/node/5340005235) (Air Canada Access Rd (SB) at North Service Rd)
* `60246` – [Node 6625543700](https://www.openstreetmap.org/node/6625543700) (Johnson Rd (SB) at Russell Ave)
* `60248` – [Node 1723548398](https://www.openstreetmap.org/node/1723548398) (East Broadway (EB) at Fraser Street)
* `60249` – [Node 5517799947](https://www.openstreetmap.org/node/5517799947)
* `60285` – [Node 3875500171](https://www.openstreetmap.org/node/3875500171) (Joyce Station Bay 2)
* `60291` – [Node 6816175473](https://www.openstreetmap.org/node/6816175473) (North Bluff Rd (EB) at Foster St)
* `60311` – [Node 7882566470](https://www.openstreetmap.org/node/7882566470) (Lillooet Rd (SB) at Old Lillooet Rd)
* `60443` – [Node 7377334359](https://www.openstreetmap.org/node/7377334359) (Sawmill Crescent (WB) at River District Crossing)
* `60475` – [Node 7764009706](https://www.openstreetmap.org/node/7764009706) (Handydart Bay 4)
* `60484` – [Node 8098623576](https://www.openstreetmap.org/node/8098623576) (Richmond-Brighouse Station Bay 7)
* `60496` – [Node 6200135985](https://www.openstreetmap.org/node/6200135985) (Clark Drive (NB) at East 6th Avenue)
* `60503` – [Node 7996333917](https://www.openstreetmap.org/node/7996333917) (West Georgia St (EB) at Seymour St)
* `60538` – [Node 8689249465](https://www.openstreetmap.org/node/8689249465)
* `60546` – [Node 8837871080](https://www.openstreetmap.org/node/8837871080) (WB Victoria @ Martin St)
* `60568` – [Node 9389598620](https://www.openstreetmap.org/node/9389598620) (Waterfront Station Bay 1)
* `60657` – [Node 9758617381](https://www.openstreetmap.org/node/9758617381) (West Georgia St (EB) at Burrard St)
* `61007` – [Node 6711256331](https://www.openstreetmap.org/node/6711256331) (47 Avenue (EB) at 5100 Block)
* `61068` – [Node 3370960678](https://www.openstreetmap.org/node/3370960678) (Capilano University Exchange Bay 1)
* `61133` – [Node 7141943589](https://www.openstreetmap.org/node/7141943589)
* `61138` – [Node 4345182342](https://www.openstreetmap.org/node/4345182342) (NW Marine Dr (WB) at West 4th Ave)
* `61143` – [Node 4232369896](https://www.openstreetmap.org/node/4232369896) (Spanish Banks Loop)
* `61213` – [Node 7972705374](https://www.openstreetmap.org/node/7972705374) (HandyDart - North Railway Ave (EB) at 33200 Block)
* `61253` – [Node 4270701594](https://www.openstreetmap.org/node/4270701594) (Cambie Street (NB) at West 27th Avenue)
* `61322` – [Node 5339998196](https://www.openstreetmap.org/node/5339998196) (Bridgeport Station Bay 2)
* `61397` – [Node 7796246429](https://www.openstreetmap.org/node/7796246429) (HandyDart)
* `61400` – [Node 5340005233](https://www.openstreetmap.org/node/5340005233)
* `61500` – [Node 6043413444](https://www.openstreetmap.org/node/6043413444) (Anderson Road (WB) at No. 3 Road)
* `61549` – [Node 1723548169](https://www.openstreetmap.org/node/1723548169) (Patterson Station Bay 3)
* `61616` – [Node 3825593159](https://www.openstreetmap.org/node/3825593159) (29th Avenue Station Bay 5)
* `61648` – [Node 4828058921](https://www.openstreetmap.org/node/4828058921) (New Westminster Station Bay 1)
* `61653` – [Node 5861765728](https://www.openstreetmap.org/node/5861765728) (New Westminster Station Bay 6)
* `61713` – [Node 3875452875](https://www.openstreetmap.org/node/3875452875) (Carvolth Exchange Bay 5)
* `61718` – [Node 3875452869](https://www.openstreetmap.org/node/3875452869) (Carvolth Exchange Bay 10)
* `61721` – [Node 3875452879](https://www.openstreetmap.org/node/3875452879) (Carvolth Exchange Bay 13)
* `61722` – [Node 3875452870](https://www.openstreetmap.org/node/3875452870) (Carvolth Exchange Bay 14)
* `61804` – [Node 6788747514](https://www.openstreetmap.org/node/6788747514) (Brentwood Town Centre Station Bay 4)
* `61805` – [Node 3438281943](https://www.openstreetmap.org/node/3438281943) (Brentwood Town Centre Station Bay 5)
* `61909` – [Node 4356764392](https://www.openstreetmap.org/node/4356764392) (Forbes Ave (SB) at West 1st St)
* `61918` – [Node 4957764222](https://www.openstreetmap.org/node/4957764222) (Richard St (SB) at Georgia St)
* `61979` – [Node 4439974089](https://www.openstreetmap.org/node/4439974089) (UBC Exchange Bay 8)
