---
layout: page
title: "Vancouver: Fire hydrant mapping issues"
permalink: hydrants/incorrect.html
categories: fire_hydrants
---
OpenStreetMap® is open data, licensed under the Open Data Commons Open Database License (ODbL) by the OpenStreetMap Foundation (OSMF).

## Duplicate fire hydrant codes
* O08035
  * [Node 3748827406](https://www.openstreetmap.org/node/3748827406)
  * [Node 3748827407](https://www.openstreetmap.org/node/3748827407)
* O08049
  * [Node 3748827408](https://www.openstreetmap.org/node/3748827408)
  * [Node 3789883500](https://www.openstreetmap.org/node/3789883500)
